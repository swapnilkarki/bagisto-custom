<?php

namespace Webkul\Category\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Eloquent\Repository;
use Webkul\Category\Models\Category;
use Webkul\Category\Models\CategoryTranslation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Category Reposotory
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class CategoryRepository extends Repository
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Webkul\Category\Contracts\Category';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        Event::fire('catalog.category.create.before');

        if (isset($data['locale']) && $data['locale'] == 'all') {
            $model = app()->make($this->model());

            foreach (core()->getAllLocales() as $locale) {
                foreach ($model->translatedAttributes as $attribute) {
                    if (isset($data[$attribute])) {
                        $data[$locale->code][$attribute] = $data[$attribute];
                    }
                }
            }
        }

        $category = $this->model->create($data);

        $this->uploadImages($data, $category);

        Event::fire('catalog.category.create.after', $category);

        return $category;
    }

    /**
     * Specify category tree
     *
     * @param integer $id
     * @return mixed
     */
    public function getCategoryTree($id = null)
    {
        return $id
            ? $this->model::orderBy('position', 'ASC')->where('id', '!=', $id)->get()->toTree()
            : $this->model::orderBy('position', 'ASC')->get()->toTree();
    }
    //get brands category id
    public function getBrandId()
    {
        $category = $this->model->whereTranslation('slug', 'brands')->first();

        if ($category) {
            return $category->id;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }
//get mother category id
    public function getMotherId()
    {
        $category = $this->model->whereTranslation('slug', 'mother')->first();

        if ($category) {
            return $category->id;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }
    //get root id
    public function getRootId()
    {
        $category = $this->model->whereTranslation('slug', 'root')->first();

        if ($category) {
            return $category->id;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }
     //get baby id
     public function getBabyId()
     {
         $category = $this->model->whereTranslation('slug', 'baby')->first();
 
         if ($category) {
             return $category->id;
         }
 
         throw (new ModelNotFoundException)->setModel(
             get_class($this->model), $slug
         );
     }
// //get categories without mother node
    public function getEditCategoryTree($id = null)
    {
        $motherId = self::getMotherId();
        return $id
            ? $this->model::orderBy('position', 'ASC')->where('id', '!=', $id)->get()->toTree()
            : $this->model::orderBy('position', 'ASC')->where('id', '!=', $motherId)->get()->toTree(1);
    }

    //custom tree for specific node tree
    public function getSelectedCategoryTree($id = null)
    {
        return $id
            ? $this->model::orderBy('position', 'ASC')->descendantsAndSelf($id)->toTree()
            : $this->model::orderBy('position', 'ASC')->get()->toTree();
    }

    /**
     * Get sub categories tree
     *
     * @return mixed
     */
    public function getSubCategoryTree($id = null)
    {
        return $id
            ? $this->model::orderBy('position', 'ASC')->descendantsOf($id)->toTree($id)
            : $this->model::orderBy('position', 'ASC')->get()->toTree();
    }

    /**
     * Get root categories
     *
     * @return mixed
     */
    public function getRootCategories()
    {
        return $this->model::withDepth()->having('depth', '=', 0)->get();
    }

    /**
     * get visible category tree
     *
     * @param integer $id
     * @return mixed
     */
    public function getVisibleCategoryTree($id = null)
    {
        static $categories = [];

        if(array_key_exists($id, $categories))
            return $categories[$id];

        return $categories[$id] = $id
                ? $this->model::orderBy('position', 'ASC')->where('status', 1)->descendantsOf($id)->toTree()
                : $this->model::orderBy('position', 'ASC')->where('status', 1)->get()->toTree();
    }

     /**
     * check show_in_navbar option used for subcategory
     *
     * @param integer $id
     * @return mixed
     */
    // public function getVisibleSubCategoryTree($id = null)
    // {
    //     static $categories = [];

    //     if(array_key_exists($id, $categories))
    //         return $categories[$id];

    //       return $categories[$id] = $id
    //             ? $this->model::orderBy('position', 'ASC')->where('show_in_navbar', 1)->descendantsOf($id)->toTree()
    //             : $this->model::orderBy('position', 'ASC')->where('show_in_navbar', 1)->get()->toTree();
    // }

    /**
     * Checks slug is unique or not based on locale
     *
     * @param integer $id
     * @param string  $slug
     * @return boolean
     */
    public function isSlugUnique($id, $slug)
    {
        $exists = CategoryTranslation::where('category_id', '<>', $id)
            ->where('slug', $slug)
            ->limit(1)
            ->select(\DB::raw(1))
            ->exists();

        return $exists ? false : true;
    }

    /**
     * Retrive category from slug
     *
     * @param string $slug
     * @return mixed
     */
    public function findBySlugOrFail($slug)
    {
        $category = $this->model->whereTranslation('slug', $slug)->first();

        if ($category) {
            return $category;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }
//for custom tree
    public function findIdBySlugOrFail($slug)
    {
        $category = $this->model->whereTranslation('slug', $slug)->first();

        if ($category) {
            return $category->id;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->model), $slug
        );
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $category = $this->find($id);

        Event::fire('catalog.category.update.before', $id);

        $category->update($data);

        $this->uploadImages($data, $category);

        Event::fire('catalog.category.update.after', $id);

        return $category;
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        Event::fire('catalog.category.delete.before', $id);

        parent::delete($id);

        Event::fire('catalog.category.delete.after', $id);
    }

    /**
     * @param array $data
     * @param mixed $category
     * @return void
     */
    public function uploadImages($data, $category, $type = "image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'category/' . $category->id;

                if ($request->hasFile($file)) {
                    if ($category->{$type}) {
                        Storage::delete($category->{$type});
                    }

                    $category->{$type} = $request->file($file)->store($dir);
                    $category->save();
                }
            }
        } else {
            if ($category->{$type}) {
                Storage::delete($category->{$type});
            }

            $category->{$type} = null;
            $category->save();
        }
    }

    //category tree for filter
    public function getCategoryTreeForFilter($id = null)
    {
        $motherId = self::getMotherId();
        $babyId = self::getBabyId();
        $rootId = self::getRootId();

        if($id == $motherId){
            $idToUse = $motherId;
        }else if($id == $babyId){
            $idToUse = $rootId;
            return $idToUse
            ? $this->model::orderBy('position', 'ASC')->where('id', '!=', $motherId)->where('id', '!=', $babyId)->descendantsOf($idToUse)->toTree($idToUse)
            : $this->model::orderBy('position', 'ASC')->get()->toTree();
        }else{
            $idToUse =$id;
        }

        return $idToUse
            ? $this->model::orderBy('position', 'ASC')->descendantsOf($idToUse)->toTree($idToUse)
            : $this->model::orderBy('position', 'ASC')->get()->toTree();

    }
}