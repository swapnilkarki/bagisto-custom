<?php

namespace Webkul\Customer\Repositories;

use Webkul\Core\Eloquent\Repository;

/**
 * Customer Reposotory
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */

class CustomerAddressRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */

    function model()
    {
        return 'Webkul\Customer\Contracts\CustomerAddress';
    }

    function updateLatLng($id, $data)
    {
        $data = $data['params'];

        $address = $this->model->findOrFail($id);

        $address->update($data);

        return $address;
    }
    
}