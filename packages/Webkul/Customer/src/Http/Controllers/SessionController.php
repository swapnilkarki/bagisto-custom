<?php
namespace Webkul\Customer\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Webkul\Customer\Models\Customer;
use CD\MeMy\Models\SocialLoginTable;
use Webkul\Customer\Http\Listeners\CustomerEventsHandler;
use Laravel\Socialite\Facades\Socialite;
use Webkul\Customer\Repositories\CustomerRepository;
use Cart;
use Cookie;
use Hash;
use Illuminate\Support\Facades\URL;
/**
 * Session controller for the user customer
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;
    /**
     * CustomerRepository object
     *
     */
    protected $customer;
    protected $socialLogin;
    public function __construct(CustomerRepository $customer, SocialLoginTable $socialLogin)
    {
        $this->middleware('customer')->except(['show','create', 'redirectToProvider', 'handleProviderCallback']);
        $this->_config = request('_config');
        $subscriber = new CustomerEventsHandler;
        Event::subscribe($subscriber);
        $this->customer = $customer;
        $this->socialLogin = $socialLogin;
    }
    public function show()
    {
        if (auth()->guard('customer')->check()) {
            return redirect()->route('customer.session.index');
        } else {
            return view($this->_config['view']);
        }
    }
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if (! auth()->guard('customer')->attempt(request(['email', 'password']))) {
            session()->flash('error', trans('shop::app.customer.login-form.invalid-creds'));
            return redirect()->back();
        }
        if (auth()->guard('customer')->user()->status == 0) {
            auth()->guard('customer')->logout();
            session()->flash('warning', trans('shop::app.customer.login-form.not-activated'));
            return redirect()->back();
        }
        if (auth()->guard('customer')->user()->is_verified == 0) {
            session()->flash('info', trans('shop::app.customer.login-form.verify-first'));
            Cookie::queue(Cookie::make('enable-resend', 'true', 1));
            Cookie::queue(Cookie::make('email-for-resend', $request->input('email'), 1));
            auth()->guard('customer')->logout();
            return redirect()->back();
        }
        //Event passed to prepare cart after login
        Event::fire('customer.after.login', $request->input('email'));
        return redirect()->intended(route($this->_config['redirect']));
    }
    public function destroy($id)
    {
        session()->forget(['error_code','url.intended']);
        
        auth()->guard('customer')->logout();
        Event::fire('customer.after.logout', $id);
        // return redirect()->back();
        return redirect()->route('memy.index');
    }
    /**
    * Redirect the user to the facebook authentication page.
    *
    *@return \Illuminate\Http\Response
    */
    public function redirectToProvider($provider)
    {
         if (strpos(url()->previous(), 'memykids') !== false) {
            $intendedUrl = url()->previous();
        } else {
            $intendedUrl = route('memy.index');
        }
        session()->put('url.intended', $intendedUrl);
        return Socialite::driver($provider)->redirect();
    }
    public function handleProviderCallback($provider)
    {
         
        $user = Socialite::driver($provider)->stateless()->user();
       
        $existingUser = $this->socialLogin::where('social_id',$user->id)->first();
        if ($existingUser) {
            $user = $this->customer->findOneWhere(['id' => $existingUser->user_id]);
            auth()->guard('customer')->login($user, true);
            Cart::cartHandleForSocialLogin();
            //Event passed to prepare cart after login
              Event::fire('customer.after.login', $user->email);
               if(session()->has('url.intended')){
                   $url = session()->get('url.intended');
                   session()->forget('url.intended');
               }else{
                    return redirect()->route('memy.profile.index');
               }
             return redirect($url);
        } else {
            $data['first_name'] = $user->name;
            $data['email'] = $user->email.'|'.$provider;
            $data['password'] = Hash::make(str_random(8));
            $data['channel_id'] = core()->getCurrentChannel()->id;
            $customer = $this->customer->create($data);

            $customerId = $this->customer->findOneWhere(['email' => $user->email.'|'.$provider]);
            $socialData['user_id'] = $customerId->id;
            $socialData['name'] = $user->name;
            $socialData['email'] = $user->email;
            $socialData['social_id'] = $user->id;
            $socialData['type'] = $provider;
            $socialData['avatar'] = $user->avatar;
            $socialData['user_data'] = json_encode($user);
            $this->socialLogin->create($socialData);
           
            auth()->guard('customer')->login($customer, true);
            
            Cart::cartHandleForSocialLogin();
              //Event passed to prepare cart after login
            Event::fire('customer.after.login', $user->email);
               if(session()->has('url.intended')){
                   $url = session()->get('url.intended');
                   session()->forget('url.intended');
               }else{
                    return redirect()->route('memy.profile.index');
               }
             return redirect($url);
        }
    }
}