<?php

namespace Webkul\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Webkul\Customer\Repositories\CustomerRepository;
use CD\MeMy\Http\Controllers\SmsController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Forgot Password controlller for the customer.
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    protected $customer;

       /**
     * sms object
     *
     * @var array
     */
    protected $sms;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerRepository $customer,SmsController $sms)
    {
        $this->_config = request('_config');
        $this->customer = $customer;
        $this->sms = $sms;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if(Input::get('submit')) {
            $this->validate(request(), [
                'email' => 'required|email'
            ]);

            $response = $this->broker()->sendResetLink(
                request(['email'])
            );
            if ($response == Password::RESET_LINK_SENT) {

                $customer = $this->customer->findOneByField('email',  request(['email']));
                $confirmation_code = rand(111111, 999999);
                $customer->update(['confirmation_code' => $confirmation_code]);

                try{
                    $this->sms->sendResetCodeSms($confirmation_code,$customer->mobile_number);
                } catch (\Exception $e){
                    \Log::error($e);
                }

                session()->flash('success', trans($response));

                session()->put('resetCodeSet', true);

                return back()->withInput(request(['email']));
            }

            return back()
                ->withInput(request(['email']))
                ->withErrors(
                    ['email' => trans($response)]
                );
        }else if(Input::get('confirm_code')){
            $validator = Validator::make(request()->all(), [
                'email' => 'required|email',
                'confirmation_code' => 'required|min:6|max:6'
            ]);
            if ($validator->fails()) {
                session()->flash('error', 'Validation error. Please check your code');
        
                return back()
                ->withInput(request(['email']));
           }
           $customer = $this->customer->findOneByField('confirmation_code',  request(['confirmation_code']));
           if(!$customer){
                session()->flash('error', 'Code does not match.');
                return back()
                ->withInput(request(['email']));
           }
           if($customer->email != request()->get('email'))
                {
                    session()->flash('error', 'Code does not match or has expired.');
                    return back()
                    ->withInput(request(['email']));
                }

            $token = str_random(60);
            $toResetEmail = request()->get('email');

            DB::transaction(function () use($token,$toResetEmail){
                DB::table('customer_password_resets')->where('email','=',$toResetEmail)->update(['token' =>  Hash::make($token)]);                
            });

            $customer->update(['is_verified' => 1, 'token' => 'NULL', 'confirmation_code' => 'NULL']);

            session()->forget('resetCodeSet');
            return redirect()->route('memy.reset-password.create', ['token' => $token])->withInput(request(['email']));

        }else if(Input::get('resend_code'))
        {

            session()->forget('resetCodeSet');
            return back()
            ->withInput(request(['email']));
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('customers');
    }
}