<?php

namespace Webkul\Inventory\Repositories;

use Webkul\Core\Eloquent\Repository;

/**
 * Inventory Reposotory
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class InventorySourceRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Inventory\Contracts\InventorySource';
    }
}