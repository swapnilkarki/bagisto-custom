<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\OrderRepository as Order;

/**
 * Sales Order controller
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Sales\Repositories\OrderRepository  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->middleware('admin',['except' => ['cancelByCustomer']]);

        $this->_config = request('_config');

        $this->order = $order;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderNumbers = $this->order->getAllOrderNumbers();
        return view($this->_config['view'])->with('orderNumbers', json_decode($orderNumbers));
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $order = $this->order->findOrFail($id);

        return view($this->_config['view'], compact('order'));
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $order_customer_id = $this->order->getCustomerId($id);

        $customer_id = auth()->guard('customer')->user()->id;

        if($customer_id == $order_customer_id){
            $result = $this->order->cancel($id);

            if ($result) {
                session()->flash('success', trans('admin::app.response.cancel-success', ['name' => 'Order']));
            } else {
                session()->flash('error', trans('admin::app.response.cancel-error', ['name' => 'Order']));
            }

            return redirect()->back();

        }else{
            session()->flash('error', trans('admin::app.response.cancel-error', ['name' => 'Order']));

            return redirect()->back();
        }
    }

    /**
     * Cancel action for the specified resource for Customer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancelByCustomer($id)
    {
        $result = $this->order->cancel($id);

        if ($result) {
            session()->flash('success', trans('admin::app.response.cancel-success', ['name' => 'Order']));
        } else {
            session()->flash('error', trans('admin::app.response.cancel-error', ['name' => 'Order']));
        }

        return redirect()->back();
    }

    public function complete($orderId)
    {
        $result = $this->order->completeOrderStatus($orderId);

        if ($result) {
            session()->flash('success', 'Order Delivered');
        } else {
            session()->flash('error', 'Order Cant Be Delivered ');
        }

        return redirect()->back();
    }
}