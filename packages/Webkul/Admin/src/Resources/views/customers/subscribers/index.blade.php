@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.customers.subscribers.title') }}
@stop

@section('content')


    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.customers.subscribers.title') }}</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.customers.subscribers.create') }}" class="btn btn-lg btn-primary">
                   Create Newsletter
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('subscribers','Webkul\Admin\DataGrids\NewsLetterDataGrid')
            {!! $subscribers->render() !!}
        </div>
    </div>
@stop