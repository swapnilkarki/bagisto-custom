@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.customers.reviews.title') }}
@stop

{{-- @push('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush --}}

@push('css')
    <style>
        .slider:after {
            content: "Pending";
        }
        input:checked + .slider:after {
            content: "Approved";
        }
    </style>

@endpush

@inject('review','Webkul\Product\Repositories\ProductFlatRepository')

<?php 
$reviews = $review->review();
?>


@section('content')

{{-- old view for reference --}}
{{-- @inject('review','Webkul\Admin\DataGrids\CustomerReviewDataGrid')
{!! $review->render() !!} --}}
<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>{{ __('admin::app.customers.reviews.title') }}</h1>
        </div>
    </div>

    <div class="page-content">
        <div class="table">
            <table class="table">
                <thead>
                    <tr style="height: 65px;">
                        <th class="grid_head">Id</th>
                        <th class="grid_head">Reviewer</th>
                        <th class="grid_head">Rating</th>
                        <th class="grid_head">Product</th>
                        <th class="grid_head">Comment</th>
                        <th class="grid_head">Status</th>
                        <th class="grid_head">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reviews as $review)
                    <tr>
                        <td data-value="Id">{{$review->product_review_id}}</td>
                        {{-- temporary fix : need toremove last_name field from db --}}
                        <?php
                            $name = strtok($review->name, '#');
                        ?>
                        <td data-value="Name">{{$name}}</td>
                        <td data-value="Rating">
                            @for($i=0 ; $i<$review->product_rating ; $i++)
                            <span class="icon star-blue-icon"></span>
                            @endfor
                        </td>
                        <td data-value="Product">{{$review->product_name}}</td>
                        
                        <td data-value="Comment">{{ str_limit($review->comment, $limit = 100, $end = '...') }}</td>
                        <td data-value="Status">
                            @if($review->product_review_status == 'approved')

                                <form method="POST" action="{{ route('admin.customer.review.update', $review->product_review_id) }}" ref="statusUpdateForm{{$review->product_review_id}}" id="statusUpdateForm{{$review->product_review_id}}">
                                    @csrf()
                                    <input name="_method" type="hidden" value="PUT"/>
                                    <label class="switch">
                                    <input type="checkbox" id="{{$review->product_review_id}}" name="status" value="pending" onclick="document.getElementById('statusUpdateForm{{$review->product_review_id}}').submit();" checked/>
                                    <span class="slider round "></span>
                                    </label>
                                </form>
                                
                            @elseif($review->product_review_status == 'pending')
                                
                                <form method="POST" action="{{ route('admin.customer.review.update', $review->product_review_id) }}" ref="statusUpdateForm{{$review->product_review_id}}" id="statusUpdateForm{{$review->product_review_id}}">
                                    @csrf()
                                    <input name="_method" type="hidden" value="PUT"/>
                                    <label class="switch">
                                    <input type="checkbox" name="status" value="approved" onclick="document.getElementById('statusUpdateForm{{$review->product_review_id}}').submit();"/>
                                    <span class="slider round "></span>
                                    </label>
                                </form>
                                
                            @elseif($review->product_review_status == 'disapproved')
                                <span class="badge badge-danger">Disapproved</span>
                            @endif

                        </td>
                        <td class="actions" data-value="Actions">
                            <a href="{{route('admin.customer.review.edit',$review->product_review_id)}}"><span class="icon pencil-lg-icon"></span></a>&nbsp;
                            <form method="POST" action="{{route('admin.customer.review.delete',$review->product_review_id)}}">
                                @csrf()
                                <button type="submit" style="border:none;" class="icon trash-icon"></button>
                            </form>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{$reviews->links()}}
    </div>
</div>
@endsection