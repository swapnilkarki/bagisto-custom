@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.sales.orders.title') }}
@stop

@section('content')
<div class="order-stats">
    <a class="card" href="{{route('admin.sales.orders.index')}}?status[eq]=pending">
        <div class="title">
            Pending
        </div>
        <div class="data">
           {{$orderNumbers->pendingCount}}
        </div>
    </a>

    <a class="card" href="{{route('admin.sales.orders.index')}}?status[eq]=processing">
        <div class="title">
            Processing
        </div>
        <div class="data">
            {{$orderNumbers->processingCount}}
        </div>
    </a>

    <a class="card" href="{{route('admin.sales.orders.index')}}?status[eq]=shipped">
        <div class="title">
            Shipped
        </div>
        <div class="data">
            {{$orderNumbers->shippedCount}}
        </div>
    </a>

    <a class="card" href="{{route('admin.sales.orders.index')}}?status[eq]=completed">
        <div class="title">
            Completed
        </div>
        <div class="data">
            {{$orderNumbers->completedCount}}
        </div>
    </a>

    <a class="card" href="{{route('admin.sales.orders.index')}}?status[eq]=canceled">
        <div class="title">
            Canceled
        </div>
        <div class="data">
            {{$orderNumbers->canceledCount}}
        </div>
    </a>
</div>

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('admin::app.sales.orders.title') }}</h1>
            </div>

            <div class="page-action">
                <div class="export-import" @click="showModal('downloadDataGrid')">
                    <i class="export-icon"></i>
                    <span>
                        {{ __('admin::app.export.export') }}
                    </span>
                </div>
            </div>
        </div>

        <div class="page-content">
            @inject('orderGrid', 'Webkul\Admin\DataGrids\OrderDataGrid')
            {!! $orderGrid->render() !!}
        </div>
    </div>

    <modal id="downloadDataGrid" :is-open="modalIds.downloadDataGrid">
        <h3 slot="header">{{ __('admin::app.export.download') }}</h3>
        <div slot="body">
            <export-form></export-form>
        </div>
    </modal>

@stop

@push('scripts')
    @include('admin::export.export', ['gridName' => $orderGrid])
@endpush