@inject ('policies','CD\Rma\Repositories\RmaRepository')
<accordian :title="'Policies'" :active="true">
    <div slot="body">
        {{-- start return policies --}}
        <div class="control-group">
            <label>Return Policy</label>
            <select class="control" id="return_policy" name="return_policy">
                @foreach($policies->getReturnsRules() as $policy)
                    @if($productPolicies != null)
                        @if($policy->status == 1 || $policy->id == $productPolicies->return_policy)
                            <option value="{{$policy->id}}" {{($policy->id == $productPolicies->return_policy) ? 'selected' : ''}} >
                                {{$policy->title}}
                            </option>
                        @endif
                    @else
                        @if($policy->status == 1)
                            <option value="{{$policy->id}}">
                                {{$policy->title}}
                            </option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
        {{-- end return policies --}}
        {{-- start warranty policies --}}
        <div class="control-group">
            <label>Warranty Policy</label>
            <select class="control" id="warranty_policy" name="warranty_policy">
                @foreach($policies->getWarrantyRules() as $policy)
                    @if($productPolicies != null)
                        @if($policy->status == 1 || $policy->id == $productPolicies->warranty_policy)
                            <option value="{{$policy->id}}" {{($policy->id == $productPolicies->warranty_policy) ? 'selected' : ''}} >
                                {{$policy->title}}
                            </option>
                        @endif
                    @else
                        @if($policy->status == 1)
                            <option value="{{$policy->id}}">
                                {{$policy->title}}
                            </option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
        {{-- end warranty policies --}}
        {{-- start exchange policy --}}
        <div class="control-group">
            <label>Exchange Policy</label>
            <select class="control" id="exchange_policy" name="exchange_policy">
                @foreach($policies->getExchangeRules() as $policy)
                    @if($productPolicies != null)
                        @if($policy->status == 1 || $policy->id == $productPolicies->exchange_policy)
                            <option value="{{$policy->id}}" {{($policy->id == $productPolicies->exchange_policy) ? 'selected' : ''}} >
                                {{$policy->title}}
                            </option>
                        @endif
                    @else
                        @if($policy->status == 1)
                            <option value="{{$policy->id}}">
                                {{$policy->title}}
                            </option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
        {{-- end exchange policy --}}
    </div>
</accordian>