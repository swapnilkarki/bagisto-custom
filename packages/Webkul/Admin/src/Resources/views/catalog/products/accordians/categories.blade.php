@if ($categories->count())
<accordian :title="'{{ __('admin::app.catalog.products.categories') }}'" :active="true">
    <div slot="body">

        {{--$product->categories->pluck("slug")--}}
        {{--use $category instead of categories in items to get custom tree--}}
        
        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.before', ['product' => $product]) !!}
       {{--Custom tree view for baby and mother--}}
        @if($product->categories->pluck("slug") != '["mother"]')
        <tree-view behavior="normal" value-field="id" name-field="categories" input-type="checkbox" items='@json($categories)' value='@json($product->categories->pluck("id"))'></tree-view>
        @else
        <tree-view behavior="normal" value-field="id" name-field="categories" input-type="checkbox" items='@json($category)' value='@json($product->categories->pluck("id"))'></tree-view>
        @endif       
        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.after', ['product' => $product]) !!}

    </div>
</accordian>
@endif