<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RmaRequestPlacedNotification extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The rma instance.
     *
     * @var rma
     */
    public $rma;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rma, $order)
    {
        $this->rma = $rma;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data =$this->order->customer_email;
        $email = strtok($data, '|');    
        $subject = 'RMA request for order #'.$this->order->id. '.';

        return $this->to($email, $this->order->customer_full_name)
                ->subject($subject)
                ->view('memy::emails.sales.customer.rma-request-placed');
    }
}
