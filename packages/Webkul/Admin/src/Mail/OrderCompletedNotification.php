<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCompletedNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data =$this->order->customer_email;
        $email = strtok($data, '|');    
        $subject = 'Your order #'.$this->order->id. ' has been Completed.';

        return $this->to($email, $this->order->customer_full_name)
                ->subject($subject)
                ->view('memy::emails.sales.customer.order-completed');
    }
}
