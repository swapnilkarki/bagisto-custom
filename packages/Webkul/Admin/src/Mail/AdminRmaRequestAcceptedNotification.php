<?php

namespace Webkul\Admin\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminRmaRequestAcceptedNotification extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The rma instance.
     *
     * @var rma
     */
    public $rma;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rma, $order)
    {
        $this->rma = $rma;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'RMA request for order #'.$this->order->id. ' has been accepted.';

        return $this->to(config('app.admin_mail_to'))
                ->subject($subject)
                ->view('memy::emails.sales.admin.rma-request-accepted');
    }
}
