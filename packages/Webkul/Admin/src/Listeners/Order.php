<?php

namespace Webkul\Admin\Listeners;

use Illuminate\Support\Facades\Mail;
use Webkul\Admin\Mail\NewOrderNotification;
use Webkul\Admin\Mail\NewAdminNotification;
use Webkul\Admin\Mail\NewInvoiceNotification;
use Webkul\Admin\Mail\NewShipmentNotification;
use Webkul\Admin\Mail\NewInventorySourceNotification;
use Webkul\Admin\Mail\OrderCancelledNotification;
use Webkul\Admin\Mail\OrderCompletedNotification;
use Webkul\Admin\Mail\RmaRequestPlacedNotification;
use Webkul\Admin\Mail\RmaRequestAcceptedNotification;
use Webkul\Admin\Mail\RmaRequestDeniedNotification;
use Webkul\Admin\Mail\RmaRequestProcessingNotification;
use Webkul\Admin\Mail\RmaRequestRefundedNotification;
use Webkul\Admin\Mail\NewAdminInvoiceNotification;
use Webkul\Admin\Mail\NewAdminShipmentNotification;
use Webkul\Admin\Mail\AdminOrderCancelledNotification;
use Webkul\Admin\Mail\AdminOrderCompletedNotification;
use Webkul\Admin\Mail\AdminRmaRequestPlacedNotification;
use Webkul\Admin\Mail\AdminRmaRequestAcceptedNotification;
use Webkul\Admin\Mail\AdminRmaRequestDeniedNotification;
use Webkul\Admin\Mail\AdminRmaRequestProcessingNotification;
use Webkul\Admin\Mail\AdminRmaRequestRefundedNotification;
use Webkul\Sales\Repositories\OrderRepository;

/**
 * Order event handler
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class Order {

    protected $orderRepository;


    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param mixed $order
     *
     * Send new order Mail to the customer and admin
     */
    public function sendNewOrderMail($order)
    {
        try {
            Mail::queue(new NewOrderNotification($order));
          
            Mail::queue(new NewAdminNotification($order));
        } catch (\Exception $e) {

        }
    }


    /**
     * @param mixed $invoice
     *
     * Send new invoice mail to the customer and admin
     */
    public function sendNewInvoiceMail($invoice)
    {
        try {
            if ($invoice->email_sent)
                return;

            Mail::queue(new NewInvoiceNotification($invoice));

            Mail::queue(new NewAdminInvoiceNotification($invoice));
        } catch (\Exception $e) {

        }
    }

     /**
     * @param mixed $shipment
     *
     * Send new shipment mail to the customer and admin
     */
    public function sendNewShipmentMail($shipment)
    {
        try {
            if ($shipment->email_sent)
                return;

            Mail::queue(new NewShipmentNotification($shipment));

            Mail::queue(new NewAdminShipmentNotification($shipment));

            // Mail::queue(new NewInventorySourceNotification($shipment));
        } catch (\Exception $e) {

        }
    }

    /**
     * @param mixed $order
     *
     * Send cancelled order Mail to the customer and admin
     */
    public function sendCancelledOrderMail($order)
    {
        try {
            Mail::queue(new OrderCancelledNotification($order));

            Mail::queue(new AdminOrderCancelledNotification($order));
          
        } catch (\Exception $e) {

        }
    }

    /**
     * @param mixed $order
     *
     * Send completed order Mail to the customer and admin
     */
    public function sendCompletedOrderMail($order)
    {
        try {
            Mail::queue(new OrderCompletedNotification($order));

            Mail::queue(new AdminOrderCompletedNotification($order));
          
        } catch (\Exception $e) {

        }
    }

     /**
     * @param mixed $rma
     *
     * Send rma request placed Mail to the customer and admin
     */
    public function sendRmaRequestPlacedMail($rma)
    {
        $order=$this->orderRepository->findOrFail($rma->order_id);

        try{
            Mail::queue(new RmaRequestPlacedNotification($rma, $order));

            Mail::queue(new AdminRmaRequestPlacedNotification($rma, $order));

        }catch(\Exception $e){

        }
    }

     /**
     * @param mixed $rma
     *
     * Send rma request accepted Mail to the customer and admin
     */
    public function sendRmaRequestAcceptedMail($rma)
    {
        $order=$this->orderRepository->findOrFail($rma->order_id);

        try{
            Mail::queue(new RmaRequestAcceptedNotification($rma, $order));

            Mail::queue(new AdminRmaRequestAcceptedNotification($rma, $order));

        }catch(\Exception $e){

        }
    }

    /**
     * @param mixed $rma
     *
     * Send rma request denied Mail to the customer and admin
     */
    public function sendRmaRequestDeniedMail($rma)
    {
        $order=$this->orderRepository->findOrFail($rma->order_id);

        try{
            Mail::queue(new RmaRequestDeniedNotification($rma, $order));

            Mail::queue(new AdminRmaRequestDeniedNotification($rma, $order));

        }catch(\Exception $e){

        }
    }

     /**
     * @param mixed $rma
     *
     * Send rma request processing Mail to the customer and admin
     */
    public function sendRmaRequestProcessingMail($rma)
    {
        $order=$this->orderRepository->findOrFail($rma->order_id);

        try{
            Mail::queue(new RmaRequestProcessingNotification($rma, $order));

            Mail::queue(new AdminRmaRequestProcessingNotification($rma, $order));

        }catch(\Exception $e){

        }
    }

    
     /**
     * @param mixed $rma
     *
     * Send rma refunded processing Mail to the customer
     */
    public function sendRmaRequestRefundedMail($rma)
    {
        $order=$this->orderRepository->findOrFail($rma->order_id);

        try{
            
            Mail::queue(new RmaRequestRefundedNotification($rma, $order));

            Mail::queue(new AdminRmaRequestRefundedNotification($rma, $order));
        }catch(\Exception $e){

        }
    }

    /**
     * @param mixed $shipment
     *
     * Send new shipment mail to the customer
     */
    public function updateProductInventory($order)
    {
    }
}