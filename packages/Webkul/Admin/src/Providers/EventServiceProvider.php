<?php

namespace Webkul\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //Customer
        //new order
        Event::listen('checkout.order.save.after', 'Webkul\Admin\Listeners\Order@sendNewOrderMail');

        Event::listen('checkout.order.save.after', 'CD\MeMy\Http\Controllers\SmsController@sendOrderSms');

        //order confirm
        Event::listen('sales.invoice.save.after', 'Webkul\Admin\Listeners\Order@sendNewInvoiceMail');

        Event::listen('sales.invoice.save.after', 'CD\MeMy\Http\Controllers\SmsController@sendOrderConfirmationSms');

        //order shipped
        Event::listen('sales.shipment.save.after', 'Webkul\Admin\Listeners\Order@sendNewShipmentMail');

        Event::listen('sales.shipment.save.after', 'CD\MeMy\Http\Controllers\SmsController@sendOrderShippedSms');
        
        //order complete
        Event::listen('sales.order.complete.after', 'Webkul\Admin\Listeners\Order@sendCompletedOrderMail');

        Event::listen('sales.order.complete.after', 'CD\MeMy\Http\Controllers\SmsController@sendOrderCompleteSms');

        //order cancel
        Event::listen('sales.order.cancel.after', 'Webkul\Admin\Listeners\Order@sendCancelledOrderMail');

        Event::listen('sales.order.cancel.after', 'CD\MeMy\Http\Controllers\SmsController@sendOrderCancelSms');

        //RMA start
        //order return placed
        Event::listen('rma.order.placed.after', 'Webkul\Admin\Listeners\Order@sendRmaRequestPlacedMail');

        Event::listen('rma.order.placed.after', 'CD\MeMy\Http\Controllers\SmsController@sendRmaPlacedSms');

        //order rma processing
        Event::listen('rma.order.processing.after', 'Webkul\Admin\Listeners\Order@sendRmaRequestProcessingMail');
        
        Event::listen('rma.order.processing.after', 'CD\MeMy\Http\Controllers\SmsController@sendRmaProcessingSms');

        //order rma accepted
        Event::listen('rma.order.accepted.after', 'Webkul\Admin\Listeners\Order@sendRmaRequestAcceptedMail');

        Event::listen('rma.order.accepted.after', 'CD\MeMy\Http\Controllers\SmsController@sendRmaAcceptedSms');

        //order rma denied
        Event::listen('rma.order.denied.after', 'Webkul\Admin\Listeners\Order@sendRmaRequestDeniedMail');

        Event::listen('rma.order.denied.after', 'CD\MeMy\Http\Controllers\SmsController@sendRmaDeniedSms');

        //order rma refunded
        Event::listen('rma.order.refunded.after', 'Webkul\Admin\Listeners\Order@sendRmaRequestRefundedMail');

        Event::listen('rma.order.refunded.after', 'CD\MeMy\Http\Controllers\SmsController@sendRmaRefundedSms');
        
        //RMA end

        //new order
        Event::listen('checkout.order.save.after', 'Webkul\Admin\Listeners\Order@updateProductInventory');

        //datagrid sync
        Event::listen('products.datagrid.sync', 'Webkul\Admin\Listeners\Product@sync');
    }
}