<?php

namespace Webkul\API\Http\Controllers\Shop;

use Illuminate\Http\Request;

/**
 * Resource Controller
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class ResourceController extends Controller
{
    /**
     * Contains current guard
     *
     * @var array
     */
    protected $guard;
    
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var array
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->guard = request()->has('token') ? 'api' : 'customer';

        $this->_config = request('_config');

        if (isset($this->_config['authorization_required']) && $this->_config['authorization_required']) {

            auth()->setDefaultDriver($this->guard);

            $this->middleware('auth:' . $this->guard);
        }

        $this->repository = app($this->_config['repository']);
    }

    /**
     * Returns a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = $this->repository->scopeQuery(function($query) {
            foreach (request()->except(['page', 'limit', 'pagination', 'sort', 'order', 'token']) as $input => $value) {
                $query = $query->whereIn($input, array_map('trim', explode(',', $value)));
            }

            if ($sort = request()->input('sort')) {
                $query = $query->orderBy($sort, request()->input('order') ?? 'desc');
            } else {
                $query = $query->orderBy('id', 'desc');
            }

            return $query;
        });

        if (is_null(request()->input('pagination')) || request()->input('pagination')) {
            $results = $query->paginate(request()->input('limit') ?? 10);
        } else {
            $results = $query->get();
        }

        return $this->_config['resource']::collection($results);
    }

    /**
     * Returns a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return new $this->_config['resource'](
                $this->repository->findOrFail($id)
            );
    }

    public function getAverageRating($id)
    {
        $ratings = \Webkul\Product\Models\ProductReview::where([['product_id',$id],['status','approved']])->get();

        $totalRating = 0;
        $RatingCount = 0;
        foreach($ratings as $rating){
            $totalRating = $totalRating + $rating->rating;
            $RatingCount++;
        }
        return response()->json([
            'message' => 'product rating.',
            'product_id' => $id,
            'total_rating' => $totalRating,
            'rating_count' => $RatingCount,
            'avg_rating' => $totalRating/$RatingCount
        ]);
    }

    /**
     * Delete's a individual resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlistProduct = $this->repository->findOrFail($id);

        $this->repository->delete($id);
        
        return response()->json([
                'message' => 'Item removed successfully.'
            ]);
    }
}
