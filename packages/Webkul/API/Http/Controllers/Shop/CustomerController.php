<?php

namespace Webkul\API\Http\Controllers\Shop;

use Illuminate\Support\Facades\Event;
use Webkul\Customer\Repositories\CustomerRepository;
use CD\MeMy\Http\Controllers\SmsController;
use Illuminate\Support\Facades\Mail;
use Webkul\Customer\Mail\VerificationEmail;
use CD\MeMy\Mail\WelcomeNewUser;
use Cookie;
use Log;

/**
 * Customer controller
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class CustomerController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Repository object
     *
     * @var array
     */
    protected $customerRepository;

     /**
     * sms object
     *
     * @var array
     */
    protected $sms;

    /**
     * @param CustomerRepository object $customer
     */
    public function __construct(CustomerRepository $customerRepository, SmsController $sms)
    {
        $this->_config = request('_config');

        $this->customerRepository = $customerRepository;

        $this->sms = $sms;
    }

    /**
     * Method to store user's sign up form data to DB.
     *
     * @return Mixed
     */
    public function create()
    {
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email|required|unique:customers,email',
            'password' => 'confirmed|min:6|required',
            'mobile_number' => 'required|unique:customers,mobile_number|min:10|max:10'
        ]);

        $data = request()->input();

        $verificationData['email'] = $data['email'];
        $verificationData['token'] = md5(uniqid(rand(), true));
        $data['token'] = $verificationData['token'];

        $confirmation_code = rand(111111, 999999);
        $data['confirmation_code'] = $confirmation_code;

        $data['mobile_number'] = $data['mobile_number'];

        $data = array_merge($data, [
                'password' => bcrypt($data['password']),
                'channel_id' => core()->getCurrentChannel()->id,
                'is_verified' => 0,
                'customer_group_id' => 1
            ]);

        Event::fire('customer.registration.before');

        $customer = $this->customerRepository->create($data);

        Event::fire('customer.registration.after', $customer);
        
        if ($customer) {
            if (core()->getConfigData('customer.settings.email.verification')) {
                try {
                    Mail::queue(new VerificationEmail($verificationData,$confirmation_code));

                    try{
                        $this->sms->sendSms($confirmation_code, $data['mobile_number']);

                        return response()->json([
                            'message' => 'Verfication sent to both email and phone'
                        ]);
                    }catch(\Exception $e){
                        return response()->json([
                            'message' => 'Verfication sent to email and but failed to send to phone',
                            'status' => 'mobile_send_fail'
                        ]);
                    }

                } catch (\Exception $e) {
                    return response()->json([
                        'message' => trans('shop::app.customer.signup-form.success-verify-email-unsent')
                    ]);
                }
            } else {
                return response()->json([
                    'message' => trans('shop::app.customer.signup-form.success')
                ]);
            }
            return redirect()->route($this->_config['redirect']);
        } else {
            return response()->json([
                'message' => trans('shop::app.customer.signup-form.failed')
            ]);
        }

        return response()->json([
                'message' => 'Your account has been created successfully.'
            ]);
    }

    public function checkEmail($email)
    {
        $customer = $this->customerRepository->findOneByField('email', $email);

        if($customer){
            return response()->json([
                'message' => 'email already exists',
                'status' => true
            ]);
        }else{
            return response()->json([
                'message' => 'email available',
                'status' => false
            ]);
        }
      
    }

    public function checkMobileNumber($mobile_number)
    {

        $customer = $this->customerRepository->findOneByField('mobile_number', $mobile_number);

        if($customer){
            return response()->json([
                'message' => 'mobile number already exists',
                'status' => true
            ]);
        }else{
            return response()->json([
                'message' => 'mobile number available',
                'status' => false
            ]);
        }

    }

        /**
     * Method to verify account
     *
     * @param string $token
     */
    public function verifyByCode($confirmation_code)
    {
        $customer = $this->customerRepository->findOneByField('confirmation_code', $confirmation_code);

        if ($customer) {
            $customer->update(['is_verified' => 1, 'token' => 'NULL', 'confirmation_code' => 'NULL']);

            try{
                Mail::queue(new WelcomeNewUser($customer));
            }catch(\Exception $e){
                \Log::alert('welcome user not sent');
            }

            return response()->json([
                'message' => 'Sucessful',
                'status' => true
            ]);
        } else {
            return response()->json([
                'message' => 'Error',
                'status' => false
            ]);
        }

    }

    public function resendVerificationEmail($email)
    {
        $verificationData['email'] = $email;
        $verificationData['token'] = md5(uniqid(rand(), true));

        $customer = $this->customerRepository->findOneByField('email', $email);

        $confirmation_code = rand(111111, 999999);

        $mobile_number = $customer->mobile_number;

        $this->customerRepository->update(['token' => $verificationData['token'],'confirmation_code' =>$confirmation_code], $customer->id);

        try {
            Mail::queue(new VerificationEmail($verificationData,$confirmation_code));

            try{
                $this->sms->sendSms($confirmation_code, $mobile_number);

                return response()->json([
                    'message' => 'Verfication sent to both email and phone'
                ]);
            }catch(\Exception $e){
                return response()->json([
                    'message' => 'Verfication sent to email and but failed to send to phone',
                    'status' => 'mobile_send_fail'
                ]);
            }

            if (Cookie::has('enable-resend')) {
                \Cookie::queue(\Cookie::forget('enable-resend'));
            }

            if (Cookie::has('email-for-resend')) {
                \Cookie::queue(\Cookie::forget('email-for-resend'));
            }
        } catch (\Exception $e) {
            \Log::info($e);
            return response()->json([
                'message' => 'verfication email send failed'
            ]);
        }
        return response()->json([
            'message' => 'verfication email sent successfully',
            'status' => true
        ]);

       
    }

}