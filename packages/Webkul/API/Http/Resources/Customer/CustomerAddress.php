<?php

namespace Webkul\API\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerAddress extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'address1' => explode(PHP_EOL, $this->address1),
            'country' => $this->country,
            'country_name' => country()->name($this->country),
            'state' => $this->state,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'phone' => $this->phone,
            'default_address' => $this->default_address,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'formatted_address' => $this->formatted_address,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}