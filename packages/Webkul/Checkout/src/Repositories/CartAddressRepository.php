<?php

namespace Webkul\Checkout\Repositories;

use Webkul\Core\Eloquent\Repository;

/**
 * Cart Address Reposotory
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */

class CartAddressRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return Mixed
     */
    function model()
    {
        return 'Webkul\Checkout\Contracts\CartAddress';
    }

    function cartAddressById($id)
    {
        $cartAddress = $this->model->where('cart_id',$id)->where('address_type','billing')->first();

        return $cartAddress;
    }
}