@inject ('attributeRepository', 'Webkul\Attribute\Repositories\AttributeRepository')

<div class="layered-filter-wrapper">
    {{-- code for filter according to products, also see below vue data--}}
        <?php $products = $productRepository->getAll($category->id); 
                $max = 500;
                $collection = new \Illuminate\Database\Eloquent\Collection; 
                foreach($products as $productTest){
                    $attributes = $productTest->getAttributes();
                    //filter array keys with null and get only keys
                    $keys = array_keys(array_filter($attributes));
                    //new collection to later add collection into
                   
                    foreach($keys as $key){
                        //get attribute collection by code and filterable
                        $temp = $attributeRepository->getAttributeWithOptionByCode($key);
                        if(isset($temp))
                                $collection  = $collection->merge($temp);
                    } 

                    if($productTest->final_price > $max)
                        $max = (int)$productTest->final_price+1; 
                      
                }
                $subCategories = app('Webkul\Category\Repositories\CategoryRepository')->getSubCategoryTree($category->id);
                // dd($max);
                // dd($subCategories);
               
         ?>
            

    {!! view_render_event('bagisto.shop.products.list.layered-nagigation.before') !!}
    
    <layered-navigation></layered-navigation>

    {{-- sub category tree  --}}
    @if(count($subCategories)>0)
        <accordian :title="'Sub Categories'" :active="false">
            <div slot="body">
                <tree-view-filter behavior="normal" value-field="id" name-field="categories" input-type="checkbox" items='@json($subCategories)' value='@json($subCategories[0]->id)'></tree-view-filter>
            </div>
        </accordian>
    @endif

    {!! view_render_event('bagisto.shop.products.list.layered-nagigation.after') !!}

</div>

@push('scripts')
    <script type="text/x-template" id="layered-navigation-template">
        <div>

            <div class="filter-title">
                {{ __('shop::app.products.layered-nav-title') }}
            </div>
            <div class="filter-content">

                <div class="filter-attributes">
                    @if(count($collection)>0)
                    <filter-attribute-item v-for='(attribute, index) in {{$collection}}' :attribute="attribute" :key="index" :index="index" @onFilterAdded="addFilters(attribute.code, $event)" :appliedFilterValues="appliedFilters[attribute.code]">
                    </filter-attribute-item>
                    @else
                    <filter-attribute-item v-for='(attribute, index) in attributes' :attribute="attribute" :key="index" :index="index" @onFilterAdded="addFilters(attribute.code, $event)" :appliedFilterValues="appliedFilters[attribute.code]">
                        </filter-attribute-item>
                    @endif
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-template" id="filter-attribute-item-template">
        <div class="filter-attributes-item" :class="[active ? 'active' : '']">

            <div class="filter-attributes-title" @click="active = !active">
                @{{ attribute.name ? attribute.name : attribute.admin_name }}

                <div class="pull-right">
                    <span class="remove-filter-link" v-if="appliedFilters.length" @click.stop="clearFilters()">
                        {{ __('shop::app.products.remove-filter-link-title') }}
                    </span>

                    <i class="icon" :class="[active ? 'arrow-up-icon' : 'arrow-down-icon']"></i>
                </div>
            </div>

            <div class="filter-attributes-content">

                <ol class="items" v-if="attribute.type != 'price'">
                    <li class="item" v-for='(option, index) in attribute.options'>

                        <span class="checkbox">
                            <input type="checkbox" :id="option.id" v-bind:value="option.id" v-model="appliedFilters" @change="addFilter($event)"/>
                            <label class="checkbox-view" :for="option.id"></label>
                            @{{ option.label ? option.label : option.admin_name }}
                        </span>

                    </li>
                </ol>

                <div class="price-range-wrapper" v-if="attribute.type == 'price'">
                    <vue-slider
                        ref="slider"
                        v-model="sliderConfig.value"
                        :process-style="sliderConfig.processStyle"
                        :tooltip-style="sliderConfig.tooltipStyle"
                        :max="sliderConfig.max"
                        :lazy="true"
                        @callback="priceRangeUpdated($event)"
                    ></vue-slider>
                </div>

            </div>

        </div>
    </script>

    <script>
        Vue.component('layered-navigation', {

            template: '#layered-navigation-template',

            data: function() {
                return {
                    // attributes : null,
                    attributes: @json($attributeRepository->getFilterAttributes()),
                    appliedFilters: {}
                }
            },

            created: function () {
                var urlParams = new URLSearchParams(window.location.search);

                //var entries = urlParams.entries();

                //for (let pair of entries) {
                    //this.appliedFilters[pair[0]] = pair[1].split(',');
                //}

                var this_this = this;

                urlParams.forEach(function (value, index) {
                    this_this.appliedFilters[index] = value.split(',');
                });
            },

            methods: {
                addFilters: function (attributeCode, filters) {
                    if (filters.length) {
                        this.appliedFilters[attributeCode] = filters;
                    } else {
                        delete this.appliedFilters[attributeCode];
                    }

                    this.applyFilter()
                },

                applyFilter: function () {
                    var params = [];

                    for(key in this.appliedFilters) {
                        params.push(key + '=' + this.appliedFilters[key].join(','))
                    }

                    window.location.href = "?" + params.join('&');
                },

               
            }
        });

        Vue.component('filter-attribute-item', {

            template: '#filter-attribute-item-template',

            props: ['index', 'attribute', 'appliedFilterValues'],

            data: function() {
                return {
                    appliedFilters: [],

                    active: false,

                    sliderConfig: {
                        value: [
                            0,
                            0
                        ],
                        max: <?php echo($max)?>,
                        processStyle: {
                            "backgroundColor": "#FF6472"
                        },
                        tooltipStyle: {
                            "backgroundColor": "#FF6472",
                            "borderColor": "#FF6472"
                        }
                    }
                }
            },

            created: function () {
                if (!this.index)
                    this.active = true;

                if (this.appliedFilterValues && this.appliedFilterValues.length) {
                    this.appliedFilters = this.appliedFilterValues;

                    if (this.attribute.type == 'price') {
                        this.sliderConfig.value = this.appliedFilterValues;
                    }

                    this.active = true;
                }
            },

            methods: {
                addFilter: function (e) {
                    this.$emit('onFilterAdded', this.appliedFilters)
                },

                priceRangeUpdated: function (value) {
                    this.appliedFilters = value;

                    this.$emit('onFilterAdded', this.appliedFilters)
                },

                clearFilters: function () {
                    if (this.attribute.type == 'price') {
                        this.sliderConfig.value = [0, 0];
                    }

                    this.appliedFilters = [];

                    this.$emit('onFilterAdded', this.appliedFilters)
                }
            }

        });

    </script>
@endpush