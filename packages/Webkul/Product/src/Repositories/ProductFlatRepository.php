<?php

namespace Webkul\Product\Repositories;

use Illuminate\Container\Container as App;
use Webkul\Core\Eloquent\Repository;
use Webkul\Product\Repositories\ProductRepository as Product;
use DB;

/**
 * Product Repository
 *
 * @author Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class ProductFlatRepository extends Repository
{
    protected $product;

    /**
     * Price Object
     *
     * @var array
     */
    protected $price;

    public function __construct(
        Product $product,
        App $app
    ) {
        $this->product = $product;
        parent::__construct($app);
    }

    public function model()
    {
        return 'Webkul\Product\Contracts\ProductFlat';
    }

    protected $index = 'product_review_id'; //column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function review()
    {
        $reviews = DB::table('product_reviews as pr')
            ->leftjoin('product_flat as pf', 'pr.product_id', '=', 'pf.product_id')
            ->select('pr.id as product_review_id', 'pr.title', 'pr.comment','pr.name', 'pf.name as product_name', 'pr.status as product_review_status', 'pr.rating as product_rating')
            ->where('channel', core()->getCurrentChannelCode())
            ->where('locale', app()->getLocale())->orderBy('product_review_id', 'DESC')->paginate(12);
        return $reviews;
    }

}