<?php

namespace Webkul\Product\Repositories;

use Illuminate\Container\Container as App;
use DB;
use Illuminate\Support\Facades\Event;
use Webkul\Core\Eloquent\Repository;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Attribute\Repositories\AttributeOptionRepository;
use Webkul\Product\Models\ProductAttributeValue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Sales\Repositories\OrderItemRepository as OrderItem;
use CD\Rma\Repositories\ProductPoliciesRepository as ProductPolicies;
use Storage;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;

use Illuminate\Support\Collection;

use Carbon\Carbon;

/**
 * Product Repository
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class ProductRepository extends Repository
{
    /**
     * AttributeRepository object
     *
     * @var array
     */
    protected $attribute;

     /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $category;

      /**
     * OrderItemRepository object
     *
     * @var array
     */
    protected $orderItem;

    /**
     * AttributeOptionRepository object
     *
     * @var array
     */
    protected $attributeOption;

    /**
     * ProductAttributeValueRepository object
     *
     * @var array
     */
    protected $attributeValue;

    /**
     * ProductFlatRepository object
     *
     * @var array
     */
    protected $productInventory;

    /**
     * ProductImageRepository object
     *
     * @var array
     */
    protected $productImage;

      /**
     * ProductPoliciesRepository object
     *
     * @var array
     */
    protected $productPolicies;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Attribute\Repositories\AttributeRepository             $attribute
     * @param  Webkul\Attribute\Repositories\AttributeOptionRepository       $attributeOption
     * @param  Webkul\Attribute\Repositories\ProductAttributeValueRepository $attributeValue
     * @param  Webkul\Product\Repositories\ProductInventoryRepository        $productInventory
     * @param  Webkul\Product\Repositories\ProductImageRepository            $productImage
     * @param  \Webkul\Sales\Repositories\OrderItemRepository                $orderItem
     * @param  \CD\Rma\Repositories\ProductPoliciesRepository                $ProductPolicies
     * @return void
     */
    public function __construct(
        AttributeRepository $attribute,
        AttributeOptionRepository $attributeOption,
        ProductAttributeValueRepository $attributeValue,
        ProductInventoryRepository $productInventory,
        ProductImageRepository $productImage,
        Category $category,
        OrderItem $orderItem,
        ProductPolicies $productPolicies,
        App $app)
    {
        $this->attribute = $attribute;

        $this->attributeOption = $attributeOption;

        $this->attributeValue = $attributeValue;

        $this->category = $category;

        $this->orderItem = $orderItem;

        $this->productInventory = $productInventory;

        $this->productImage = $productImage;

        $this->productPolicies = $productPolicies;

        parent::__construct($app);
    }

    /**->where('product_flat.visible_individually', 1)
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Product\Contracts\Product';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $vendor_id = Auth::guard('admin')->user()->id;
        $data['vendor_id'] = $vendor_id;
        //before store of the product
        Event::fire('catalog.product.create.before');

        $product = $this->model->create($data);

        $nameAttribute = $this->attribute->findOneByField('code', 'status');
        $this->attributeValue->create([
                'product_id' => $product->id,
                'attribute_id' => $nameAttribute->id,
                'value' => 1
            ]);

        if (isset($data['super_attributes'])) {
            // \Log::info($data['super_attributes']);
            $super_attributes = [];
            $category = $data['categories'];

            foreach ($data['super_attributes'] as $attributeCode => $attributeOptions) {
                $attribute = $this->attribute->findOneByField('code', $attributeCode);

                $super_attributes[$attribute->id] = $attributeOptions;

                $product->super_attributes()->attach($attribute->id);
            }

            foreach (array_permutation($super_attributes) as $permutation) {
                $this->createVariant($product, $permutation,$category);
            }
        }

        //after store of the product
        Event::fire('catalog.product.create.after', $product);

        return $product;
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id")
    {
        Event::fire('catalog.product.update.before', $id);

        $product = $this->find($id);

        if ($product->parent_id && $this->checkVariantOptionAvailabiliy($data, $product)) {
            $data['parent_id'] = NULL;
        }

        $product->update($data);
        
        // product policies
        $policies['product_id']=$id;
        $policies['return_policy']=$data['return_policy'];
        $policies['warranty_policy']=$data['warranty_policy'];
        $policies['exchange_policy']=$data['exchange_policy'];

        $productPolicies = $this->productPolicies->add($policies);

        // product policies end


        $attributes = $product->attribute_family->custom_attributes;

        foreach ($attributes as $attribute) {
            if (! isset($data[$attribute->code]) || (in_array($attribute->type, ['date', 'datetime']) && ! $data[$attribute->code]))
                continue;

            if ($attribute->type == 'multiselect') {
                $data[$attribute->code] = implode(",", $data[$attribute->code]);
            }

            if ($attribute->type == 'image' || $attribute->type == 'file') {
                $dir = 'product';
                if (gettype($data[$attribute->code]) == 'object') {
                    $data[$attribute->code] = request()->file($attribute->code)->store($dir);
                } else {
                    $data[$attribute->code] = NULL;
                }
            }

            $attributeValue = $this->attributeValue->findOneWhere([
                    'product_id' => $product->id,
                    'attribute_id' => $attribute->id,
                    'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                    'locale' => $attribute->value_per_locale ? $data['locale'] : null
                ]);

            if (! $attributeValue) {
                $this->attributeValue->create([
                    'product_id' => $product->id,
                    'attribute_id' => $attribute->id,
                    'value' => $data[$attribute->code],
                    'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                    'locale' => $attribute->value_per_locale ? $data['locale'] : null
                ]);
            } else {
                $this->attributeValue->update([
                    ProductAttributeValue::$attributeTypeFields[$attribute->type] => $data[$attribute->code]
                    ], $attributeValue->id
                );

                if ($attribute->type == 'image' || $attribute->type == 'file') {
                    Storage::delete($attributeValue->text_value);
                }
            }
        }

        if (request()->route()->getName() != 'admin.catalog.products.massupdate') {
           //Using for product_categories update
          // dd($data['categories']);
            if  (isset($data['categories'])) {
                $product->categories()->sync($data['categories']);
            }

            if (isset($data['up_sell'])) {
                $product->up_sells()->sync($data['up_sell']);
            } else {
                $data['up_sell'] = [];
                $product->up_sells()->sync($data['up_sell']);
            }

            if (isset($data['cross_sell'])) {
                $product->cross_sells()->sync($data['cross_sell']);
            } else {
                $data['cross_sell'] = [];
                $product->cross_sells()->sync($data['cross_sell']);
            }

            if (isset($data['related_products'])) {
                $product->related_products()->sync($data['related_products']);
            } else {
                $data['related_products'] = [];
                $product->related_products()->sync($data['related_products']);
            }

            $previousVariantIds = $product->variants->pluck('id');

            if (isset($data['variants'])) {
                foreach ($data['variants'] as $variantId => $variantData) {
                    if (str_contains($variantId, 'variant_')) {
                        $permutation = [];
                        foreach ($product->super_attributes as $superAttribute) {
                            $permutation[$superAttribute->id] = $variantData[$superAttribute->code];
                        }

                        dd($product);
                        $this->createVariant($product, $permutation, $variantData);
                    } else {
                        if (is_numeric($index = $previousVariantIds->search($variantId))) {
                            $previousVariantIds->forget($index);
                        }

                        $variantData['channel'] = $data['channel'];
                        $variantData['locale'] = $data['locale'];

                        $this->updateVariant($variantData, $variantId);

                        // product policies
                        $policies['product_id']=$variantId;
                        $productPolicies = $this->productPolicies->add($policies);
                        // product policies end
                    }
                }
            }

            foreach ($previousVariantIds as $variantId) {
                $this->delete($variantId);
            }

            $this->productInventory->saveInventories($data, $product);

            $this->productImage->uploadImages($data, $product);
        }

        Event::fire('catalog.product.update.after', $product);

        return $product;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        Event::fire('catalog.product.delete.before', $id);

        parent::delete($id);

        Event::fire('catalog.product.delete.after', $id);
    }

    /**
     * @param mixed $product
     * @param array $permutation
     * @param array $data
     * @return mixed
     */
    public function createVariant($product, $permutation,$categoryName, $data = [])
    {
        if (! count($data)) {
            $data = [
                    "sku" => $product->sku . '-variant-' . implode('-', $permutation),
                    "name" => "",
                    "inventories" => [],
                    "price" => 0,
                    "weight" => 0,
                    "status" => 1
                ];
        }

        $variant = $this->model->create([
                'parent_id' => $product->id,
                'type' => 'simple',
                'attribute_family_id' => $product->attribute_family_id,
                'sku' => $data['sku'],
            ]);

        foreach (['sku', 'name', 'price', 'weight', 'status'] as $attributeCode) {
            $attribute = $this->attribute->findOneByField('code', $attributeCode);

            if ($attribute->value_per_channel) {
                if ($attribute->value_per_locale) {
                    foreach (core()->getAllChannels() as $channel) {
                        foreach (core()->getAllLocales() as $locale) {
                            $this->attributeValue->create([
                                    'product_id' => $variant->id,
                                    'attribute_id' => $attribute->id,
                                    'channel' => $channel->code,
                                    'locale' => $locale->code,
                                    'value' => $data[$attributeCode]
                                ]);
                        }
                    }
                } else {
                    foreach (core()->getAllChannels() as $channel) {
                        $this->attributeValue->create([
                                'product_id' => $variant->id,
                                'attribute_id' => $attribute->id,
                                'channel' => $channel->code,
                                'value' => $data[$attributeCode]
                            ]);
                    }
                }
            } else {
                if ($attribute->value_per_locale) {
                    foreach (core()->getAllLocales() as $locale) {
                        $this->attributeValue->create([
                                'product_id' => $variant->id,
                                'attribute_id' => $attribute->id,
                                'locale' => $locale->code,
                                'value' => $data[$attributeCode]
                            ]);
                    }
                } else {
                    $this->attributeValue->create([
                            'product_id' => $variant->id,
                            'attribute_id' => $attribute->id,
                            'value' => $data[$attributeCode]
                        ]);
                }
            }
        }

        foreach ($permutation as $attributeId => $optionId) {
            $this->attributeValue->create([
                    'product_id' => $variant->id,
                    'attribute_id' => $attributeId,
                    'value' => $optionId
                ]);
        }

        //get category id of for category in create view
        $categoryNameId = $this->category->findIdBySlugOrFail($categoryName);
        $categoryId=array();
        //make id into array to sync
        array_push($categoryId,"$categoryNameId");
        //sync with products_categories table
        if  (isset($categoryId)) {
            $variant->categories()->sync($categoryId);
        }

        $this->productInventory->saveInventories($data, $variant);

        return $variant;
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function updateVariant(array $data, $id)
    {
        $variant = $this->find($id);

        $variant->update(['sku' => $data['sku']]);

        foreach (['sku', 'name', 'price', 'weight', 'status'] as $attributeCode) {
            $attribute = $this->attribute->findOneByField('code', $attributeCode);

            $attributeValue = $this->attributeValue->findOneWhere([
                    'product_id' => $id,
                    'attribute_id' => $attribute->id,
                    'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                    'locale' => $attribute->value_per_locale ? $data['locale'] : null
                ]);

            if (! $attributeValue) {
                $this->attributeValue->create([
                        'product_id' => $id,
                        'attribute_id' => $attribute->id,
                        'value' => $data[$attribute->code],
                        'channel' => $attribute->value_per_channel ? $data['channel'] : null,
                        'locale' => $attribute->value_per_locale ? $data['locale'] : null
                    ]);
            } else {
                $this->attributeValue->update([
                    ProductAttributeValue::$attributeTypeFields[$attribute->type] => $data[$attribute->code]
                ], $attributeValue->id);
            }
        }

        $this->productInventory->saveInventories($data, $variant);

        return $variant;
    }

    /**
     * @param array $data
     * @param mixed $product
     * @return mixed
     */
    public function checkVariantOptionAvailabiliy($data, $product)
    {
        $parent = $product->parent;

        $superAttributeCodes = $parent->super_attributes->pluck('code');

        $isAlreadyExist = false;

        foreach ($parent->variants as $variant) {
            if ($variant->id == $product->id)
                continue;

            $matchCount = 0;

            foreach ($superAttributeCodes as $attributeCode) {
                if (! isset($data[$attributeCode]))
                    return false;

                if ($data[$attributeCode] == $variant->{$attributeCode})
                    $matchCount++;
            }

            if ($matchCount == $superAttributeCodes->count()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param integer $categoryId
     * @return Collection
     */
    public function getProductsOnSale()
    {
        $params = request()->input();

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($params) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                $qb = $query->distinct()
                        ->addSelect('product_flat.*')
                        ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS final_price'))

                        ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                        ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                        ->leftJoin('product_ordered_inventories','product_flat.product_id', '=','product_ordered_inventories.product_id')
                        ->addSelect('product_ordered_inventories.qty as orderedQty')
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->whereDate('product_flat.special_price_to', '>', Carbon::now())
                        ->whereNotNull('product_flat.url_key');


                if (is_null(request()->input('status'))) {
                    $qb->where('product_flat.status', 1);
                }

                if (is_null(request()->input('visible_individually'))) {
                    $qb->where('product_flat.visible_individually', 1);
                }

                $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function($qb) use($channel, $locale) {
                    $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                        ->where('flat_variants.channel', $channel)
                        ->where('flat_variants.locale', $locale);
                });

                if(isset($params['topSelling'] )){
                    $qb->orderBy('orderedQty','desc');
                }

                if (isset($params['search'])) {
                    $qb->where('product_flat.name', 'like', '%' . urldecode($params['search']) . '%');
                }

                if (isset($params['sort'])) {
                    $attribute = $this->attribute->findOneByField('code', $params['sort']);

                    if ($params['sort'] == 'price') {
                        if ($attribute->code == 'price') {
                            $qb->orderBy('final_price', $params['order']);
                        } else {
                            $qb->orderBy($attribute->code, $params['order']);
                        }
                    } else {
                        $qb->orderBy($params['sort'] == 'created_at' ? 'product_flat.created_at' : $attribute->code, $params['order']);
                    }
                }

                $qb = $qb->where(function($query1) {
                    foreach (['product_flat', 'flat_variants'] as $alias) {
                        $query1 = $query1->orWhere(function($query2) use($alias) {
                            $attributes = $this->attribute->getProductDefaultAttributes(array_keys(request()->input()));

                            foreach ($attributes as $attribute) {
                                $column = $alias . '.' . $attribute->code;

                                $queryParams = explode(',', request()->get($attribute->code));

                                if ($attribute->type != 'price') {
                                    $query2 = $query2->where(function($query3) use($column, $queryParams) {
                                        foreach ($queryParams as $filterValue) {
                                            $query3 = $query3->orwhereRaw("find_in_set($filterValue, $column)");
                                        }
                                    });
                                } else {
                                    if ($attribute->code != 'price') {
                                        $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                    } else {
                                        $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                    }
                                }
                            }
                        });
                    }
                });

                return $qb->groupBy('product_flat.id');
            })->paginate(isset($params['limit']) ? $params['limit'] : 18);

        return $results;
    }

    /**
     * @param integer $categoryId
     * @return Collection
     */
    public function getAll($categoryId = null)
    {
        $params = request()->input();

        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($params, $categoryId) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                $qb = $query->distinct()
                        ->addSelect('product_flat.*')
                        ->addSelect(DB::raw('IF( product_flat.special_price_from IS NOT NULL
                            AND product_flat.special_price_to IS NOT NULL , IF( NOW( ) >= product_flat.special_price_from
                            AND NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , IF( product_flat.special_price_from IS NULL , IF( product_flat.special_price_to IS NULL , IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , IF( NOW( ) <= product_flat.special_price_to, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) ) , IF( product_flat.special_price_to IS NULL , IF( NOW( ) >= product_flat.special_price_from, IF( product_flat.special_price IS NULL OR product_flat.special_price = 0 , product_flat.price, LEAST( product_flat.special_price, product_flat.price ) ) , product_flat.price ) , product_flat.price ) ) ) AS final_price'))

                        ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                        ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                        ->leftJoin('product_ordered_inventories','product_flat.product_id', '=','product_ordered_inventories.product_id')
                        ->addSelect('product_ordered_inventories.qty as orderedQty')
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->whereNotNull('product_flat.url_key');

                if ($categoryId) {
                    $qb->where('product_categories.category_id', $categoryId);
                }

                if (isset($params['subCategories_id'])){
                    $subCategories = explode(",",$params['subCategories_id']);
                    foreach($subCategories  as $index=>$subCategory){
                        if($subCategory != null){
                            if($index == 0)
                                $qb->where('product_categories.category_id', $subCategory);
                            else
                                $qb->orWhere('product_categories.category_id', $subCategory);
                        }
                    }

                }


                if (is_null(request()->input('status'))) {
                    $qb->where('product_flat.status', 1);
                }

                if (is_null(request()->input('visible_individually'))) {
                    $qb->where('product_flat.visible_individually', 1);
                }

                $queryBuilder = $qb->leftJoin('product_flat as flat_variants', function($qb) use($channel, $locale) {
                    $qb->on('product_flat.id', '=', 'flat_variants.parent_id')
                        ->where('flat_variants.channel', $channel)
                        ->where('flat_variants.locale', $locale);
                });

                if(isset($params['topSelling'] )){
                    $qb->orderBy('orderedQty','desc');
                }

                if (isset($params['search'])) {
                    $qb->where('product_flat.name', 'like', '%' . urldecode($params['search']) . '%');
                }

                if (isset($params['sort'])) {
                    $attribute = $this->attribute->findOneByField('code', $params['sort']);

                    if ($params['sort'] == 'price') {
                        if ($attribute->code == 'price') {
                            $qb->orderBy('final_price', $params['order']);
                        } else {
                            $qb->orderBy($attribute->code, $params['order']);
                        }
                    } else {
                        $qb->orderBy($params['sort'] == 'created_at' ? 'product_flat.created_at' : $attribute->code, $params['order']);
                    }
                }

                $qb = $qb->where(function($query1) {
                    foreach (['product_flat', 'flat_variants'] as $alias) {
                        $query1 = $query1->orWhere(function($query2) use($alias) {
                            $attributes = $this->attribute->getProductDefaultAttributes(array_keys(request()->input()));

                            foreach ($attributes as $attribute) {
                                $column = $alias . '.' . $attribute->code;

                                $queryParams = explode(',', request()->get($attribute->code));

                                if ($attribute->type != 'price') {
                                    $query2 = $query2->where(function($query3) use($column, $queryParams) {
                                        foreach ($queryParams as $filterValue) {
                                            $query3 = $query3->orwhereRaw("find_in_set($filterValue, $column)");
                                        }
                                    });
                                } else {
                                    if ($attribute->code != 'price') {
                                        $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                    } else {
                                        $query2 = $query2->where($column, '>=', current($queryParams))->where($column, '<=', end($queryParams));
                                    }
                                }
                            }
                        });
                    }
                });

                return $qb->groupBy('product_flat.id');
            })->paginate(isset($params['limit']) ? $params['limit'] : 18);

        return $results;
    }
    
    /**
     * Retrive product from slug
     *
     * @param string $slug
     * @return mixed
     */
    public function findBySlugOrFail($slug, $columns = null)
    {
        $product = app('Webkul\Product\Repositories\ProductFlatRepository')->findOneWhere([
                'url_key' => $slug,
                'locale' => app()->getLocale(),
                'channel' => core()->getCurrentChannelCode(),
            ]);

        if (! $product) {
            throw (new ModelNotFoundException)->setModel(
                get_class($this->model), $slug
            );
        }

        return $product;
    }

     /**
     * Retrive product from product id
     *
     * @param string $slug
     * @return mixed
     */
    public function findByProductId($id)
    {
        //defined in core repo
        // $product = app('Webkul\Product\Repositories\ProductFlatRepository')->findOneWhere([
        //         'product_id' => $id,
        //     ]);

        //custom scope
        $product = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($id){
            $qb = $query->addSelect('product_flat.*')
                        ->where('product_flat.product_id',$id)
                        ->whereNotNull('product_flat.name');
            return $qb->groupBy('product_flat.id');
        })->first();

        if (! $product) {
            throw (new ModelNotFoundException)->setModel(
                get_class($this->model), $id
            );
        }

        return $product;
    }

    /**
     * Returns newly added product
     *
     * @return Collection
     */
    public function getNewProducts()
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                return $query->distinct()
                        ->addSelect('product_flat.*')
                        ->where('product_flat.status', 1)
                        ->where('product_flat.visible_individually', 1)
                        ->where('product_flat.new', 1)
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->orderBy('product_id', 'desc');
            })->paginate(8);

        return $results;
    }

    /**
     * Returns featured product
     *
     * @return Collection
     */
    public function getFeaturedProducts()
    {
        $results = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                return $query->distinct()
                        ->addSelect('product_flat.*')
                        ->where('product_flat.status', 1)
                        ->where('product_flat.visible_individually', 1)
                        ->where('product_flat.featured', 1)
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->orderBy('product_id', 'desc');
            })->paginate(8);

        return $results;
    }

    /**
     * Search Product,category,tags by Attribute
     *
     * @return Collection
     */
    public function searchProductByAttribute($searchQuery)
    {
        // \Log::info($searchQuery);

        foreach($searchQuery as $temp){
            if(strpos($temp, 'term') !== false){
                $term = last(explode("=",$temp));
            }
        }
        $products = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($term) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                return $query->distinct()
                        ->addSelect('product_flat.*')
                        ->leftJoin('product_inventories','product_flat.product_id', '=','product_inventories.product_id')
                        ->addSelect('product_inventories.qty as qty')
                        ->where('product_flat.status', 1)
                        ->where('product_flat.visible_individually', 1)
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->whereNotNull('product_flat.url_key')
                        ->where('product_flat.name', 'like', '%' . urldecode($term) . '%')
                        ->orWhere('product_flat.for_label', 'like', '%' . urldecode($term) . '%')                        
                        ->orderBy('product_id', 'desc');
            })->paginate(18);

        $categories = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($term) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                    ->leftJoin('product_categories', 'product_categories.product_id', '=', 'product_flat.product_id')
                    ->leftJoin('category_translations', 'category_translations.id', '=', 'product_categories.category_id')
                    ->addSelect('product_flat.*','category_translations.name as categoryName')
                    ->leftJoin('product_inventories','product_flat.product_id', '=','product_inventories.product_id')
                    ->addSelect('product_inventories.qty as qty')
                    ->where('product_flat.status', 1)
                    ->where('product_flat.visible_individually', 1)
                    ->where('product_flat.channel', $channel)
                    ->where('product_flat.locale', $locale)
                    ->whereNotNull('product_flat.url_key')                       
                    ->where('category_translations.name', 'like', '%' . urldecode($term) . '%' )                        
                    ->orderBy('product_id', 'desc');
        })->paginate(18);

        $tags = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($term){
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            return $query->distinct()
                        ->leftJoin('product_tags', 'product_tags.product_id', '=', 'product_flat.product_id')
                        ->addSelect('product_flat.*','product_tags.tags as tag')
                        ->leftJoin('product_inventories','product_flat.product_id', '=','product_inventories.product_id')
                        ->addSelect('product_inventories.qty as qty')
                        ->where('product_flat.status', 1)
                        ->where('product_flat.visible_individually', 1)
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->whereNotNull('product_flat.url_key') 
                        ->where('product_tags.tags', 'like', '%' . urldecode($term) . '%' )                        
                        ->orderBy('product_id', 'desc');  
        })->paginate(18);


        // $tags = \Webkul\Product\Models\Product::whereHas('getProductTags', function ($query) use($term) {
        //     $query->where('tags', 'like', '%' . urldecode($term) . '%');            ;
        // })->paginate(18);  

        // Add type of data to each item of each set of results
        $products = $this->appendValue($products, 'product', 'class');
        $categories = $this->appendValue($categories, 'category', 'class');
        $tags = $this->appendValue($tags, 'tag', 'class');
        
        // return $categories;

        $temp = $products->merge($categories);

        $results = $temp->merge($tags);

        $filterResult = $results;

        foreach($searchQuery as $temp){
            $query = explode('=', $temp);
            $queryKey = $query[0];
            $queryValue = $query[1];
            if($queryKey !== 'term'){
                if($queryKey == 'price'){
                    $priceRange = explode(',', $queryValue);
                    $priceMin = $priceRange[0];
                    $priceMax = $priceRange[1];
                    $filterResult = $filterResult->where('price','>=', $priceMin)->where('price','<=', $priceMax);
                    // $filterResult = $filterResult->merge($temp);
                }
                else if($queryKey == 'sort'){
                    $sortKey = $queryValue;
                }
                else if($queryKey == 'order'){
                    $sortOrder = $queryValue;
                }else if($queryKey == 'for'){
                    $ageValue = explode(',', $queryValue);

                    if(count($ageValue) > 1){
                        $filterResult = $filterResult->where($queryKey, $queryValue);
                    }else{
                        $filterResult = $filterResult->reject(function($element) use ($queryValue) {
                            return mb_strpos($element['for'], $queryValue) === false;
                        });
                    }
                }else if($queryKey !== 'order'){
                    $attrValues = explode(',',$queryValue);
                    foreach($attrValues as $attrValue){
                        $filterResult = $filterResult->where($queryKey, $attrValue);
                    }
                    // $filterResult = $filterResult->where($queryKey, $queryValue);
                    // $filterResult = $filterResult->merge($test);
                }
            }
        } 
        
        if(count($searchQuery) > 1){
            $results = $filterResult;
        }

        //sorting
        if(isset($sortKey)){
            if($sortOrder == 'asc'){
                $results = $results->unique('product_id');
                $results = $results->sortBy($sortKey, SORT_NATURAL|SORT_FLAG_CASE);
                // $aCollection ->sortBy('Key', SORT_NATURAL|SORT_FLAG_CASE);
                // $results = $results->sortBy(function($element) use ($sortKey)
                // {
                //     return $element[$sortKey];
                // });
                $results = $results->values();
            
                // $results->dump();
            }else if($sortOrder == 'desc'){
                $results = $results->unique('product_id');
                $results = $results->sortByDesc($sortKey, SORT_NATURAL|SORT_FLAG_CASE);

                // $results = $results->sortByDesc(function($element) use ($sortKey)
                // {
                //     return $element[$sortKey];
                // });
                $results = $results->values();
            }
        }else{
            $results = $results->unique('product_id');
            $results = $results->sortByDesc(function($element)
            {
                return $element['created_at'];
            });
            $results = $results->values();

            // $results->dd();
        }

        $perPage = 18;

        $page = null;
        
        $options = ['path' => LengthAwarePaginator::resolveCurrentPath(),'pageName' => 'pageName',];

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $results = $results instanceof Collection ? $results : Collection::make($results);

        // return $results;
        return new LengthAwarePaginator($results->forPage($page, $perPage), $results->count(), $perPage, $page, $options);

           
    }

       /**
     * Search Product by Attribute
     *
     * @return Collection
     */
    public function searchOnlyProductByAttribute($term)
    {
        $products = app('Webkul\Product\Repositories\ProductFlatRepository')->scopeQuery(function($query) use($term) {
                $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

                $locale = request()->get('locale') ?: app()->getLocale();

                return $query->distinct()
                        ->addSelect('product_flat.*')
                        ->where('product_flat.status', 1)
                        ->where('product_flat.visible_individually', 1)
                        ->where('product_flat.channel', $channel)
                        ->where('product_flat.locale', $locale)
                        ->whereNotNull('product_flat.url_key')
                        ->where('product_flat.name', 'like', '%' . urldecode($term) . '%')
                        ->orWhere('product_flat.for_label', 'like', '%' . urldecode($term) . '%')                        
                        ->orderBy('product_id', 'desc');
            })->paginate(18);
        return $products;
    }

    public function appendValue($data, $type, $element)
	{
		// operate on the item passed by reference, adding the element and type
		foreach ($data as $key => & $item) {
			$item[$element] = $type;
			$item['images'] = $item->images;
            $item['totalReviews']=$item->reviews()->where('status', 'approved')->count();
            $item['averageRating'] = number_format(round($item->reviews()->where('status', 'approved')->average('rating'), 2), 1);
		}
		return $data;		
    }
    /**
     * Returns product's super attribute with options
     *
     * @param Product $product
     * @return Collection
     */
    public function getSuperAttributes($product)
    {
        $superAttrbutes = [];

        foreach ($product->super_attributes as $key => $attribute) {
            $superAttrbutes[$key] = $attribute->toArray();

            foreach ($attribute->options as $option) {
                $superAttrbutes[$key]['options'][] = [
                    'id' => $option->id,
                    'admin_name' => $option->admin_name,
                    'sort_order' => $option->sort_order,
                    'swatch_value' => $option->swatch_value,
                ];
            }
        }

        return $superAttrbutes;
    }

      /**
     * Generate unique product sku.
     */
    public function productSkuGenerate(){
        $vendor= Auth::guard('admin')->user()->code;
        $time= Carbon::now()->format('gis');
        $date = Carbon::now()->format('dmY');
        // $rand=mt_rand(100000, 999999);

        $uniqueSku = $vendor . '-'. $time . '-' . $date;
        
        // $allSku = app('Webkul\Product\Repositories\ProductFlatRepository')->pluck('sku');

        // foreach($allSku as $sku){
        //     if($sku == $uniqueSku){
        //         self.productSkuGenerate();
        //         // return 'change proc';
        //     }
        // }

        return $uniqueSku;
      
    }

      /**
     * Returns top selling products
     * @return mixed
     */
    public function getTopSellingProducts()
    {
        $productDetails =  $this->orderItem->getModel()
        ->select(DB::raw('SUM(qty_ordered) as total_qty_ordered'))
        ->addSelect('product_id')
        ->whereNull('parent_id')
        ->groupBy('product_id')
        ->orderBy('total_qty_ordered', 'DESC')
        ->limit(8)
        ->get();

        $result = array();
        
        $id = $productDetails->pluck('product_id');

        foreach($id as $id){

            $product = app('Webkul\Product\Repositories\ProductFlatRepository')->findOneWhere([
                'product_id' => $id,
                'locale' => app()->getLocale(),
                'channel' => core()->getCurrentChannelCode(),
            ]);

            array_push($result, $product);

        }
        return $result;
      
    }

    
}