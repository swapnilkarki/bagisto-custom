require('./bootstrap');

window.route = require('./route');

import Vue from 'vue'
import VueProgressBar from 'vue-progressbar';
import { VLazyImagePlugin } from "v-lazy-image";
import VModal from 'vue-js-modal';
import TextHighlight from 'vue-text-highlight';
import BeatLoader from 'vue-spinner/src/BeatLoader.vue';
import * as VueGoogleMaps from "vue2-google-maps";
import VueRecaptcha from 'vue-recaptcha';

Vue.component('vue-recaptcha', VueRecaptcha);

Vue.component('beat-loader', BeatLoader);

Vue.component('text-highlight', TextHighlight);

Vue.use(VModal, { componentName: "vue-modal" });

Vue.use(VLazyImagePlugin);

window.Vue = require('vue');
window.VeeValidate = require("vee-validate");
window.axios = require("axios");

Vue.use(VeeValidate)

Vue.prototype.$http = axios

Vue.component("vue-slider", require("vue-slider-component"));

Vue.component('category-nav', require('./components/CategoryNav.vue'));
Vue.component('category-item', require('./components/CategoryItem.vue'));

Vue.component('rma-section', require('./components/RmaSection.vue'));
Vue.component('rma-modal-component', require('./components/RmaModal.vue'));
Vue.component('rma-modal-form', require('./components/RmaForm.vue'));

Vue.component('register-customer', require('./components/RegisterCustomer.vue'));

Vue.component('product-section', require('./components/ProductSection.vue'));

Vue.component('product-box', require('./components/ProductBox.vue'));

Vue.component('google-map', require('./components/GoogleMap.vue'));

Vue.component('cart-add-button', require('./components/CartAddButton.vue'));
Vue.component('wishlist-add-button', require('./components/WishlistAddButton.vue'));

Vue.component('wishlist-login-register', require('./components/WishlistLoginRegister.vue'));

Vue.component("memy-tree-view-filter", require("./components/tree-view-filter/memy-tree-view-filter"));
Vue.component("memy-tree-item-filter", require("./components/tree-view-filter/memy-tree-item-filter"));
Vue.component("memy-tree-checkbox-filter", require("./components/tree-view-filter/memy-tree-checkbox-filter"));
Vue.component("memy-tree-radio-filter", require("./components/tree-view-filter/memy-tree-radio-filter"));

const options = {
    color: '#42f593',
    failedColor: '#874b4b',
    thickness: '6px',
    transition: {
        speed: '0.2s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}

Vue.use(VueProgressBar, options);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyCuQBHuZ8U8q9g_auPjVJhK_mJ5SCh42_M",
        libraries: "places" // necessary for places input
    }
});

$(document).ready(function() {

    const app = new Vue({
        el: "#app",

        data: {
            modalIds: {}
        },

        mounted: function() {
            this.addServerErrors();
            this.addFlashMessages();
        },

        methods: {
            onSubmit: function(e) {
                this.toggleButtonDisable(true);

                if (typeof tinyMCE !== 'undefined')
                    tinyMCE.triggerSave();

                this.$validator.validateAll().then(result => {
                    if (result) {
                        e.target.submit();
                    } else {
                        this.toggleButtonDisable(false);
                    }
                });
            },

            toggleButtonDisable(value) {
                var buttons = document.getElementsByTagName("button");

                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].disabled = value;
                }
            },

            addServerErrors: function(scope = null) {
                for (var key in serverErrors) {
                    var inputNames = [];
                    key.split('.').forEach(function(chunk, index) {
                        if (index) {
                            inputNames.push('[' + chunk + ']')
                        } else {
                            inputNames.push(chunk)
                        }
                    })

                    var inputName = inputNames.join('');

                    const field = this.$validator.fields.find({
                        name: inputName,
                        scope: scope
                    });
                    if (field) {
                        this.$validator.errors.add({
                            id: field.id,
                            field: inputName,
                            msg: serverErrors[key][0],
                            scope: scope
                        });
                    }
                }
            },

            addFlashMessages: function() {
                const flashes = this.$refs.flashes;

                flashMessages.forEach(function(flash) {
                    flashes.addFlash(flash);
                }, this);
            },

            responsiveHeader: function() {},

            showModal(id) {
                this.$set(this.modalIds, id, true);
            },

            loginToUseWishlist: function() {
                var this_this = this;
                Swal.fire({
                    title: 'Please Login to use Wishlist?',
                    text: "If you dont have an account please register!",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                    if (result.value) {
                        this_this.$modal.show('login-modal');
                    }
                })
            }
        }
    });
});