@extends('memy::layouts.master')

@section('page_title')
    {{ __('memy::app.me-my.title') }}
@stop

@section('content')

    @guest('customer')
    <?php $authUser = 'false'?>
    @endguest
    @auth('customer')
    <?php $authUser = 'true'?>
    @endauth
    
    @include('memy::sections.banner')
    @include('memy::sections.shop-for')
    @include('memy::sections.high-selling')
    @include('memy::sections.view-all')
    @include('memy::sections.new-products')
    @include('memy::sections.featured-products')
    {{-- @include('memy::sections.offer') --}}
    @include('memy::sections.brands')
    @include('memy::sections.testimonial')
    @include('memy::sections.service')
@stop