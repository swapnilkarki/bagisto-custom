@extends('memy::layouts.master')

@section('page_title')
 {{ __('shop::app.customer.forgot-password.page_title') }}
@stop

@section('content')
<section class="register reset-password">
        <div class="container">
            <div class="register__box">
                <div class="register__block">
                    <h5 class="register--heading">Reset Password</h5>
                    <form method="post" action="{{ route('memy.forgot-password.store') }}">
                            {{ csrf_field() }}
                        <div class="form-group">
                            <label class="form-label" for="email">Email</label>
                            <input type="email" class="form-control form-box" name="email" value="{{old('email')}}" v-validate="'required|email'">
                        </div>

                        @if(session()->has('resetCodeSet'))
                            <div class="form-group">
                                <label class="form-label" for="email">Code</label>
                                <input type="number" class="form-control form-box" name="confirmation_code" v-validate="'required|min:6|max:6|numeric'">
                            </div>
                            <button type="submit" class="button-as-anchor" name="resend_code" value="resend_code">Resend Code</button>
                            <button type="submit" class="btn btn-primary" name="confirm_code" value="confirm_code">Reset Password</button>
                            
                        @else
                            <button type="submit" class="btn btn-primary" name="submit" value="submit">Reset Password</button>
                        @endif

                    </form>
                </div>
            </div>
        </div>
</section>
@endsection