@extends('memy::layouts.master')

@section('page_title')
{{ __('shop::app.customer.reset-password.title') }}
@endsection

@section('content')
<section class="register signup">
        <div class="container">
            <div class="register__box signup__box">
                <div class="register__block">
                    <h5 class="register--heading">Reset Password</h5>
                    <form method="post" action="{{ route('memy.reset-password.store') }}" >
                        {{ csrf_field() }}
                
                        <input type="hidden" name="token" value="{{ $token }}">
                        
                        <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                            <label class="form-label required" for="email">Registered Email</label>
                            <input type="email" class="form-control form-box" id="email" name="email" v-validate="'required|email'" value="{{ old('email') }}">
                            <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                        </div>
                    
                        <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                            <label class="form-label" for="password">Password</label>
                            <input type="password" class="form-control form-box" name="password" v-validate="'required|min:6'" ref="password">
                            <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                        </div>

                        <div class="form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                            <label class="form-label" for="cpassword">Confirm Password</label>
                            <input type="password" class="form-control form-box" name="password_confirmation"  v-validate="'required|min:6|confirmed:password'">
                            <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>
                        </div>

                        <button type="submit" class="btn btn-primary">Reset Password</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('memy::sections.service')
@endsection