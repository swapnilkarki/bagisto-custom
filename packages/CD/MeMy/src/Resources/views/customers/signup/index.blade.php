@extends('memy::layouts.master')

@section('page_title')
{{ __('shop::app.customer.signup-form.page-title') }}
@endsection

@section('content')
<section class="register signup">
        <div class="container">
            <div class="register__box signup__box">
                <register-customer></register-customer>
                  <!-- set progressbar -->
                <vue-progress-bar></vue-progress-bar>
            </div>
        </div>
    </section>
    @include('memy::sections.service')
@endsection
{{-- 

<div class="register__block">
        <h5 class="register--heading">Welcome to Memykids</h5>
        <form method="post" action="{{ route('memy.register.index') }}" @submit.prevent="onSubmit">
            
                {{ csrf_field() }}

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="form-group" :class="[errors.has('first_name') ? 'has-error' : '']">
                        <label class="form-label required" for="fname">First Name</label>
                        <input type="text" class="form-control form-box" name="first_name" v-validate="'required'" value="{{ old('first_name') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.firstname') }}&quot;">
                        <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>
                    </div>
                </div>

                <input type="hidden" name="last_name" value="####">

                <div class="col-md-6 col-sm-6">
                    <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                        <label class="form-label required" for="email">Email</label>
                        <input type="email" class="form-control form-box" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.email') }}&quot;">
                        <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                        <label class="form-label" for="password">Password</label>
                        <input type="password" class="form-control form-box" name="password" v-validate="'required|min:6'" ref="password" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.password') }}&quot;">
                        <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                        <label class="form-label" for="cpassword">Confirm Password</label>
                        <input type="password" class="form-control form-box" name="password_confirmation"  v-validate="'required|min:6|confirmed:password'" data-vv-as="&quot;{{ __('shop::app.customer.signup-form.confirm_pass') }}&quot;">
                        <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>
                    </div>
                </div>

            </div>
            <button type="submit" class="btn btn-primary">Sign Up</button>
        </form>
    </div> --}}