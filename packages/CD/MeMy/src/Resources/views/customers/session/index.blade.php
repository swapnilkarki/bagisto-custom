@extends('memy::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.login-form.page-title') }}
@endsection

@section('content')
<section class="register">
        <div class="container">
            <div class="register__box">
                <div class="register__block">
                    <h5 class="register--heading">Welcome to Memykid</h5>
                    <form method="POST" action="{{ route('memy.session.create') }}" @submit.prevent="onSubmit">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="form-label" for="email">Email</label>
                            {{-- <input type="email" v-validate="'required|email'" class="form-control form-box"> --}}
                            <input type="text" class="form-control form-box" name="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.email') }}&quot;">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="password">Password</label>
                            <input type="password" class="form-control form-box" name="password" v-validate="'required'" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.password') }}&quot;">
                        </div>
                        
                        <div class="register--forgotpwd">
                            <a href="{{ route('memy.forgot-password.create') }}">Forgot password?</a><br>
                            @if (Cookie::has('enable-resend'))
                            @if (Cookie::get('enable-resend') == true)
                                <a href="{{ route('customer.resend.verification-email', Cookie::get('email-for-resend')) }}">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                            @endif
                        @endif
                        </div>
                        {{-- <button type="submit" class="btn btn-primary">Log In</button> --}}
                        <input class="btn btn-primary" type="submit" value="{{ __('shop::app.customer.login-form.button_title') }}">
                    </form>
                    <p class="register--divider">Or</p>
                    <div class="alternate-login">
                        <a href="{{ route('redirect', 'facebook') }}" class="btn btn-primary btn-facebook">
                            <figure><img src="{{ asset('vendor/webkul/memy/assets/images/facebook-logo.png')}}" class="img-fluid"></figure>
                            Log In with Facebook</a>
                        <a href="{{ route('redirect', 'google') }}" class="btn btn-primary btn-google">
                            <figure><img src="{{ asset('vendor/webkul/memy/assets/images/google-logo.png')}}" class="img-fluid"></figure>
                            Log In with Google</a>
                    </div>
                   <register-customer></register-customer>
                     <!-- set progressbar -->
                    <vue-progress-bar></vue-progress-bar>
                </div>
            </div>
        </div>
    </section>
    @include('memy::sections.service')
@endsection
