@extends('memy::layouts.master')

@section('page_title')
My Orders
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h4>My Order Details</h4>
                        <div class="table-responsive">
                                @inject('order','CD\MeMy\DataGrids\OrderDataGrid')
                                {!! $order->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection