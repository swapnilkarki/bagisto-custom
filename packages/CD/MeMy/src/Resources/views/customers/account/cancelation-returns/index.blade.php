@extends('memy::layouts.master')

@section('page_title')
Cancelation & Returns | MeMykids
@endsection

@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

{{-- findByProductId --}}
@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h5>My Cancellation & Returns</h5>
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>id</th>
                                        <th>Item</th>
                                        <th>Item Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Reason</th>
                                        <th>Remarks</th>
                                    </tr>
                                    @foreach($rmaRequests as $rmaRequest)
                                    <?php 
                                        $product = $productRepository->findByProductId($rmaRequest->product_id);
                                        $image = $productImageHelper->getProductBaseImage($product); 
                                    ?>
                                        <tr>
                                            <td>
                                                {{$rmaRequest->id}}
                                            </td>
                                            <td>
                                                <figure class="cart__image">
                                                    <a href="{{ url()->to('/').'/memykids/products/'.$product->url_key }}" title="{{ $product->name }}">
                                                        <img src="{{ $image['small_image_url'] }}" class="img-fluid" alt="memykid">
                                                    </a>
                                                </figure>
                                            </td>
                                            <td>
                                               {{$product->name}}
                                            </td>
                                            <td>{{$rmaRequest->quantity}}</td>
                                            <td>{{core()->currency($product->price)}}</td>

                                            <td>
                                                @if($rmaRequest->status == 'pending')
                                                    <span class="badge badge-md badge-warning">Pending</span>
                                                @elseif($rmaRequest->status == 'processing')
                                                    <span class="badge badge-md badge-info">Processing</span>
                                                @elseif($rmaRequest->status == 'refunded')
                                                    <span class="badge badge-md badge-success">Refunded</span>
                                                @elseif($rmaRequest->status == 'denied')
                                                    <span class="badge badge-md badge-danger">Denied</span>
                                                @endif
                                            </td>

                                            <td>{{core()->formatDate($rmaRequest->updated_at, 'd M Y')}}</td>
                                            <td>{{$rmaRequest->reason}}</td>
                                            <td>{{$rmaRequest->remarks}}</td>
                                        </tr>
                                      
                                    @endforeach
                                </table>
                                @include('memy::datagrid.pagination',['results' => $rmaRequests])
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection