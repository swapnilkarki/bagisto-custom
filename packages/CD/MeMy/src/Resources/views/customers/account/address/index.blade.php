<?php
$addresses=auth()->guard('customer')->user()->addresses;
?>

@extends('memy::layouts.master')

@section('page_title')
 My Address
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content address-block">
                        <h5>Shipping Address</h5>

                        @if (! $addresses->isEmpty())
                            <a href="{{ route('memy.address.create') }}" class="btn btn-outline">Add New Address</a>
                        @else
                        <span></span>
                        @endif

                        @if ($addresses->isEmpty())
                            <div>{{ __('shop::app.customer.account.address.index.empty') }}</div>
                            <br/>
                            <a href="{{ route('memy.address.create') }}">Add New Address</a>
                        @else
                        <div class="row">
                            @foreach ($addresses as $address)
                            <div class="col-lg-6">
                                <div class="bill-address">
                                    @if( $address->title)
                                        <h5> {{$address->title}}</h5>
                                    @else
                                        <h5> Address</h5>
                                    @endif

                                    @if($address->default_address === 1)
                                        <p class="pull-right" style="margin-top: -40px;">Default Address</p>
                                    @endif

                                    <address>
                                        Name : {{ auth()->guard('customer')->user()->name }} <br>
                                        Address : {{ $address->address1 }}/
                                        {{ $address->city }}/
                                        {{ $address->state }}/
                                        {{ country()->name($address->country) }}<br>
                                        Phone No. : {{ $address->phone }}<br>
                                        {{-- Postal Code :  {{ $address->postcode }}<br> --}}
                                        Email : {{ auth()->guard('customer')->user()->email }}
                                    </address>
                                    <a href="{{ route('memy.address.edit', $address->id) }}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;
                                    {{-- <a href="#" class="btn btn-outline"><i class="fa fa-pencil" aria-hidden="true"></i> Edit<a> &nbsp; --}}
                                    @if($address->default_address != 1)
                                        <a href="javascript:void(0);" onclick="deleteAddress({{$address->id}})" data-toggle="tooltip" data-placement="top" title="Delete" ><i class="fa fa-trash"></i><a>&nbsp;
                                        <a href="{{ route('memy.address.makeDefault', $address->id) }}" data-toggle="tooltip" data-placement="top" title="Make Default" ><i class="fa fa-check-square-o" aria-hidden="true"></i></i></a>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection



@push('scripts')
    <script>
        function deleteAddress(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.value) {
                    window.location = route('address.delete',id);
                }
                })
        }
    </script>
@endpush
