@extends('memy::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->id]) }}
@endsection

@section('content')
<section class="dashboard">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-lg-3 col-md-3 nav-bg">
                        @include('memy::customers.account.partials.sidemenu')
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="dashboard-content">

                            <div class="row" style="border-bottom: 1px solid #ddd;">
                                <div class="col">
                                    <h5 style="border-bottom:none;">{{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->id]) }}
                                        @if ($order->status == 'processing')
                                            <span class="badge badge-md badge-info">Processing</span>
                                        @elseif ($order->status == 'completed')
                                            <span class="badge badge-md badge-success">Completed</span>
                                        @elseif ($order->status == "canceled")
                                            <span class="badge badge-md badge-danger">Canceled</span>
                                        @elseif ($order->status == "pending")
                                            <span class="badge badge-md badge-warning">Pending</span>
                                        @elseif ($order->status == "shipped")
                                        <span class="badge badge-md badge-info">Shipped</span>
                                        @endif
                                    </h5>
                                </div>

                                @if($order->canCancel())
                                    <div class="col">
                                        <div class="float-right"><a href="{{ route('memy.orders.cancel', $order->id) }}" onClick="cancelConfirmation(this)" style="padding: 6px 12px;background:#c82333;font-weight: 600;color:#fff;">Cancel</a></div>
                                    </div>
                                @endif
                            </div>

                            <div class="table-responsive">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li><a class="active" href="#information" role="tab" data-toggle="tab">Information</a></li>&nbsp;&nbsp;
                                    
                                    @if ($order->invoices->count())
                                        <li><a href="#invoices" role="tab" data-toggle="tab">Invoices</a></li>&nbsp;&nbsp;
                                    @endif

                                    @if ($order->shipments->count())
                                    <li><a href="#shipments" role="tab" data-toggle="tab">Shipments</a></li>&nbsp;&nbsp;
                                    @endif
                                    
                                    @if($order->status == 'completed')
                                        <li><a href="#returns" role="tab" data-toggle="tab">Returns</a></li>
                                    @endif
                                </ul>
                                
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!--Info-->
                                    <div class="tab-pane active" id="information">
                                        {{--<div class="container--">--}}
                                            {{--<div class="row--">--}}
                                               <strong><span>
                                                        {{ __('shop::app.customer.account.order.view.placed-on') }}
                                                </span></strong>&nbsp;&nbsp;
                                                <span class="value">
                                                    {{ core()->formatDate($order->created_at, 'd M Y') }}
                                                </span>
                                            {{--</div>--}}

                                            
                                            <h5><span>{{ __('shop::app.customer.account.order.view.products-ordered') }}</span></h5>
                                            
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.item-status') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                                    
                                                        {{-- Tax columns --}}
                                                        {{-- <th>{{ __('shop::app.customer.account.order.view.tax-percent') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th> --}}
                                                        
                                                        <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order->items as $item)
                                                        <tr>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">
                                                                {{ $item->type == 'configurable' ? $item->child->sku : $item->sku }}
                                                            </td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($item->price, $order->order_currency_code) }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.item-status') }}">
                                                                <span class="qty-row">
                                                                    {{ __('shop::app.customer.account.order.view.item-ordered', ['qty_ordered' => $item->qty_ordered]) }}
                                                                </span><div></div>
                    
                                                                <span class="qty-row">
                                                                    {{ $item->qty_invoiced ? __('shop::app.customer.account.order.view.item-invoice', ['qty_invoiced' => $item->qty_invoiced]) : '' }}
                                                                </span><div></div>
                    
                                                                <span class="qty-row">
                                                                    {{ $item->qty_shipped ? __('shop::app.customer.account.order.view.item-shipped', ['qty_shipped' => $item->qty_shipped]) : '' }}
                                                                </span><div></div>
                    
                                                                <span class="qty-row">
                                                                    {{ $item->qty_canceled ? __('shop::app.customer.account.order.view.item-canceled', ['qty_canceled' => $item->qty_canceled]) : '' }}
                                                                </span>
                                                            </td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">{{ core()->formatPrice($item->total, $order->order_currency_code) }}</td>

                                                            @if(intval($item->tax_percent) != 0)
                                                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-percent') }}">{{ number_format($item->tax_percent, 2) }}%</td>
                                                            @endif
                                                            @if(intval($item->tax_amount) != 0)
                                                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                            @endif

                                                            <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">{{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div>
                                                <table class="pull-right order-totalling">
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}</td>
                                                        </tr>
            
                                                        @if(intval($order->shipping_amount) != 0)
                                                        <tr>
                                                            <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
            
                                                        @if(intval($order->tax_amount) != 0)
                                                        <tr>
                                                            <td>{{ __('shop::app.customer.account.order.view.tax') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
            
                                                        @if(intval($order->grand_total) != 0)
                                                        <tr class="bold">
                                                            <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
            
                                                        @if(intval($order->grand_total_invoiced) != 0)
                                                        <tr class="bold">
                                                            <td>{{ __('shop::app.customer.account.order.view.total-paid') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->grand_total_invoiced, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
            
                                                        @if(intval($order->grand_total_refunded) != 0)
                                                        <tr class="bold">
                                                            <td>{{ __('shop::app.customer.account.order.view.total-refunded') }}</td>
                                                            <td> : </td>
                                                            <td>{{ core()->formatPrice($order->grand_total_refunded, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
            
                                                        @if(intval($order->total_due) != 0)
                                                        <tr class="bold">
                                                            <td class="order-grand-total">{{ __('shop::app.customer.account.order.view.total-due') }}</td>
                                                            <td> : </td>
                                                            <td class="order-grand-total">{{ core()->formatPrice($order->total_due, $order->order_currency_code) }}</td>
                                                        </tr>
                                                        @endif
                                                    <tbody>
                                                </table>
                                            </div>
                                        {{--</div>--}}
                                    </div>

                                    <!--Invoices-->
                                    @if ($order->invoices->count())
                                    <div class="tab-pane" id="invoices">
                                        @foreach ($order->invoices as $invoice)
                                        
                                            <strong><span>
                                                    {{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $invoice->id]) }}
                                            </span></strong>&nbsp;&nbsp;
                                            <span class="value">
                                                <a href="{{ route('customer.orders.print', $invoice->id) }}" class="pull-right">
                                                    {{ __('shop::app.customer.account.order.view.print') }}
                                                </a>
                                            </span>
                                            
                                            <hr>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($invoice->items as $item)
                                                        <tr>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($item->price, $order->order_currency_code) }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $item->qty }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">{{ core()->formatPrice($item->total, $order->order_currency_code) }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                            <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">{{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div>
                                                <table class="pull-right">
                                                    <tr>
                                                        <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                                        <td> : </td>
                                                        <td>{{ core()->formatPrice($invoice->sub_total, $order->order_currency_code) }}</td>
                                                    </tr>
                
                                                    <tr>
                                                        <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                                                        <td> : </td>
                                                        <td>{{ core()->formatPrice($invoice->shipping_amount, $order->order_currency_code) }}</td>
                                                    </tr>
                
                                                    <tr>
                                                        <td>{{ __('shop::app.customer.account.order.view.tax') }}</td>
                                                        <td> : </td>
                                                        <td>{{ core()->formatPrice($invoice->tax_amount, $order->order_currency_code) }}</td>
                                                    </tr>
                
                                                    <tr class="bold">
                                                        <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                                                        <td> : </td>
                                                        <td>{{ core()->formatPrice($invoice->grand_total, $order->order_currency_code) }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                        
                                        @endforeach
                                    </div>
                                    @endif

                                    <!--Shipments-->
                                    @if ($order->shipments->count())
                                    <div class="tab-pane" id="shipments">
                                        @foreach ($order->shipments as $shipment)
                                            <strong><span>
                                                {{ __('shop::app.customer.account.order.view.individual-shipment', ['shipment_id' => $shipment->id]) }}
                                            </span></strong>&nbsp;&nbsp;
                                            <hr>
                                            <p>Carrier Title : {{$shipment->carrier_title}}</p>
                                            <p>Tracking Number : {{$shipment->track_number}}</p>
                                            <p>Shipped at : {{$shipment->created_at}}</p>
                                            <p>Ordered at : {{$shipment->order->created_at}}</p>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                                        <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    

                            
                                                    @foreach ($shipment->items as $item)
            
                                                        <tr>
                                                            <td data-value="{{  __('shop::app.customer.account.order.view.SKU') }}">{{ $item->sku }}</td>
                                                            <td data-value="{{  __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                                            <td data-value="{{  __('shop::app.customer.account.order.view.qty') }}">{{ $item->qty }}</td>
                                                        </tr>
            
                                                    @endforeach
                
                                                </tbody>
                                            </table>
                                        @endforeach
                                    </div>
                                    @endif

                                    {{-- Returns --}}
                                    @if($order->status == 'completed')
                                        {{-- component here --}}
                                    <rma-section :order="{{$order}}"></rma-section>
                                    <rma-modal-form></rma-modal-form>

                                    @endif
                                    {{-- Returns end --}}
                                </div>
                            </div>

                            {{-- include rma modal component --}}
                            <rma-modal-component></rma-modal-component>

                            <div class="container">
                                <div class="row">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Shipping Address</h5>
                                            <p class="card-text">
                                                @include ('admin::sales.address', ['address' => $order->shipping_address])
                                            </p>
                                        </div>
                                    </div>
                                    &nbsp;
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Shipping Method</h5>
                                            <p class="card-text">
                                                {{ $order->shipping_title }}
                                            </p>
                                        </div>
                                    </div>
                                    &nbsp;
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Payment Method</h5>
                                            <p class="card-text">
                                                {{ core()->getConfigData('sales.paymentmethods.' . $order->payment->method . '.title') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection

@push('scripts')
<script>
    function cancelConfirmation(obj) {
        event.preventDefault(); // Prevent the href from redirecting directly
        var linkURL = obj.getAttribute("href");
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = linkURL;
            }
        })

    }
</script>
@endpush