<div class="dashboard-sidebar-profile" {{Route::currentRouteName()=='memy.profile.index' ? 'style=display:none;' : ''}}>
    <div class="profile-image">
        @if(auth()->guard('customer')->user()->profile_picture_path != null)
            <figure>
                <img src="{{route('memy.index')}}/storage/{{ auth()->guard('customer')->user()->profile_picture_path }}" alt="memykid">
                {{-- <figcaption class="halfCircleBottom">
                    <a href="">Change Image</a>
                </figcaption> --}}
            </figure>
        @else
            <figure>
                <img src="{{ asset('vendor/webkul/ui/assets/images/product/small-product-placeholder.png')}}" class="img-fluid" alt="memykid">
            </figure>
        @endif
        <article>
            <h6>{{auth()->guard('customer')->user()->first_name}}</h6>
            <p>{{auth()->guard('customer')->user()->email}}</p>
        </article>

        {{-- <input type="file" accept="image/*" style="display: none;" > --}}

        {{--<button class="btn btn-primary">Upload</button>--}}
        {{--<button class="btn btn-primary">Cancel</button>--}}
    </div>
</div>
<aside class="dashboard-sidebar">
    {{--<h5>Dashboard</h5>--}}
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.account.index' ? 'active' : ''}}" id="profile-tab" href="{{route('memy.account.index')}}" data-toggle="tooltip" data-placement="top" title="Dashboard"><i class="fa fa-dashboard" aria-hidden="true"></i> &nbsp; <span>Dashboard</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.profile.index' ? 'active' : ''}}" id="profile-tab" href="{{route('memy.profile.index')}}" data-toggle="tooltip" data-placement="top" title="Profile"><i class="fa fa-user" aria-hidden="true"></i> &nbsp; <span>Profile</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.orders.index' || Route::currentRouteName()=='memy.orders.view' ? 'active' : ''}}" id="myorder-tab" href="{{route('memy.orders.index')}}" data-toggle="tooltip" data-placement="top" title="My Order"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> &nbsp; <span>My Orders</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.wishlist.index' ? 'active' : ''}}" id="mywishlist-tab" href="{{route('memy.wishlist.index')}}" data-toggle="tooltip" data-placement="top" title="Wishlist"><i class="fa fa-heart" aria-hidden="true"></i> &nbsp; <span>Wishlist</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.address.index' ||  Route::currentRouteName()=='memy.address.edit' ? 'active' : ''}}" id="billship-tab" href="{{route('memy.address.index')}}" data-toggle="tooltip" data-placement="top" title="Shipping"><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp; <span>My Addresses</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.reviews.index' ? 'active' : ''}}" id="myreview-tab" href="{{route('memy.reviews.index')}}" data-toggle="tooltip" data-placement="top" title="My Reviews"><i class="fa fa-commenting" aria-hidden="true"></i> &nbsp; <span>My Reviews</span></a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="top" title="Completed Orders"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp; <span>Completed Orders</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="top" title="Cancelled Orders"><i class="fa fa-ban" aria-hidden="true"></i>&nbsp; <span>Cancelled Orders</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="top" title="Pending Orders"><i class="fa fa-repeat" aria-hidden="true"></i>&nbsp; <span>Pending Orders</span></a>
        </li> --}}

        <li class="nav-item">
            <a class="nav-link {{Route::currentRouteName()=='memy.cancelation-returns.index' ? 'active' : ''}}" id="cancellation-tab" href="{{route('memy.cancelation-returns.index')}}" data-toggle="tooltip" data-placement="top" title="Cancellation & Returns"><i class="fa fa-exchange" aria-hidden="true"></i> &nbsp; <span>Cancellation & Returns</span></a>
        </li>
        {{-- <li class="nav-item">
        <a class="nav-link {{Route::currentRouteName()=='memy.tracking.index' ? 'active' : ''}}" id="trackorder-tab" href="{{route('memy.tracking.index')}}">Track My Order</a>
        </li> --}}
    </ul>
</aside>