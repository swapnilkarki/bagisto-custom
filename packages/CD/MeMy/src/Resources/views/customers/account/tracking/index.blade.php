@extends('memy::layouts.master')

@section('page_title')
Tracking
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h5>Track My Order</h5>
                        <div class="trackorder">
                            <p>Libero fames integer risus alias libero cumque pariatur dictumst ante fermentum unde aute labore! Natus at dicta iure sociosqu rhoncus, eleifend senectus dapibus felis atque, irure ipsa eu felis etiam.</p>
                            <div class="track-line">
                                <div class="circular-shape active">
                                    <figure class="circular-icon">
                                        <img src="{{ asset('vendor/webkul/memy/assets/images/confirm.png')}}" class="img-fluid" alt="memykid">
                                    </figure>
                                    <p>Confirm Order</p>
                                </div>
                                <div class="circular-shape center-circle">
                                    <figure class="circular-icon">
                                        <img src="{{ asset('vendor/webkul/memy/assets/images/shipped.png')}}" class="img-fluid" alt="memykid">
                                    </figure>
                                    <p>Order Dispatch</p>
                                </div>
                                <div class="circular-shape">
                                    <figure class="circular-icon">
                                        <img src="{{ asset('vendor/webkul/memy/assets/images/deliver.png')}}" class="img-fluid" alt="memykid">
                                    </figure>
                                    <p>Product Delivered</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection