@extends('memy::layouts.master')

@section('page_title')
Edit Profile
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h5>Edit Profile</h5>
                            <form method="post" action="{{ route('customer.profile.edit') }}" @submit.prevent="onSubmit">

                            <div class="profile">
                                @csrf

                                <div class="form-group" :class="[errors.has('first_name') ? 'has-error' : '']">
                                    <label for="first_name" class="'required'" >Full Name</label>
                                    <p><input class="form-control form-box" type="text" name="first_name" value="{{ old('first_name') ?? $customer->first_name }}" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.fname') }}&quot;"></p>
                                    <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>                      
                                </div>

                                <input class="form-control form-box" type="hidden" name="last_name" value="####"></<input>
                                   
                                <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                    <label for="email" class="'required'">Email</label>
                                    <p><input class="form-control form-box" type="email" name="email" value="{{ old('email') ?? $customer->email }}" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.email') }}&quot;"></p>
                                    <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>                       
                                </div>

                                <div class="form-group" :class="[errors.has('gender') ? 'has-error' : '']">
                                    <label for="password" class="'required'">Gender</label>
                                    <p><select class="form-control form-box" name="gender" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.gender') }}&quot;">
                                        <option value=""  @if ($customer->gender == "") selected @endif></option>
                                        <option value="Other"  @if ($customer->gender == "Other") selected @endif>Other</option>
                                        <option value="Male"  @if ($customer->gender == "Male") selected @endif>Male</option>
                                        <option value="Female" @if ($customer->gender == "Female") selected @endif>Female</option>
                                    </select></p>
                                    <span class="control-error" v-if="errors.has('gender')">@{{ errors.first('gender') }}</span>
                                                            
                                </div>
                                <div class="form-group" :class="[errors.has('date_of_birth') ? 'has-error' : '']">
                                    <label for="password">DOB</label>
                                    <p><input class="form-control form-box" type="date" name="date_of_birth" value="{{ old('date_of_birth') ?? $customer->date_of_birth }}" v-validate="" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.dob') }}&quot;"></p>
                                    <span class="control-error" v-if="errors.has('date_of_birth')">@{{ errors.first('date_of_birth') }}</span>                       
                                </div>

                                <div class="form-group" :class="[errors.has('oldpassword') ? 'has-error' : '']">
                                    <label for="oldpassword">Old Password</label>
                                    <p><input class="form-control form-box" type="password" name="oldpassword" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.opassword') }}&quot;" v-validate="'min:6'"></p>
                                    <span class="control-error" v-if="errors.has('oldpassword')">@{{ errors.first('oldpassword') }}</span>                           
                                </div>

                                <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                    <label for="password">New Password</label>
                                    <p><input class="form-control form-box" type="password" id="password" name="password" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.password') }}&quot;" v-validate="'min:6'" ref="password"></P>
                                    <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>                               
                                </div>

                                <div class="form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <p><input class="form-control form-box" type="password" id="password_confirmation" name="password_confirmation" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.cpassword') }}&quot;" v-validate="'min:6|confirmed:password'"></p>
                                    <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>                                   
                                </div>

                                <input class="btn btn-outline" type="submit" value="{{ __('shop::app.customer.account.profile.submit') }}">
                            </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
