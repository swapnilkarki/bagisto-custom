@extends('memy::layouts.master')

@section('page_title')
Create Address
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content address-block">
                        <h5>New Address</h5>
                            <form method="post" action="{{ route('memy.address.create') }}" @submit.prevent="onSubmit">
                            <div class="profile">
                                @csrf
                                <div class="form-group" :class="[errors.has('title') ? 'has-error' : '']">
                                    <label for="title" class="required">Title</label>
                                    <input class="form-control form-box" type="text" name="title" id="title"  v-validate="'required'" data-vv-as="&quot;Title&quot;">
                                    <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>                      
                                </div>

                                <div class="form-group" :class="[errors.has('address1[]') ? 'has-error' : '']">
                                    <label for="address_0" class="required">Street Address</label>
                                    <input class="form-control form-box" type="text" name="address1[]" id="address_0"  v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.street-address') }}&quot;">
                                    <span class="control-error" v-if="errors.has('address1[]')">@{{ errors.first('address1[]') }}</span>                      
                                </div>
                                
                                <div class="form-group" :class="[errors.has('city') ? 'has-error' : '']">
                                    <label for="city" class="required">City</label>
                                    <input class="form-control form-box" type="city" name="city"  v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.city') }}&quot;">
                                    <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>                       
                                </div>

                                <div class="form-group" :class="[errors.has('state') ? 'has-error' : '']">
                                        <label for="state" class="required">State</label>
                                        <input class="form-control form-box" type="text" name="state"  v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.state') }}&quot;">
                                        <span class="control-error" v-if="errors.has('state')">@{{ errors.first('state') }}</span>
                                </div>
                                
                                <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                    <label for="phone" class="required">Phone</label>
                                    <input class="form-control form-box" type="phone" name="phone"  v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.phone') }}&quot;">
                                    <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>                          
                                </div>

                                <input type="hidden" name="country" value="NP">
                                <input type="hidden" name="postcode" value="44600">
                        
                                <input class="btn btn-primary" type="submit" value="{{ __('shop::app.customer.account.address.create.submit') }}">
                            </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection