@extends('memy::layouts.master')

@section('page_title')
{{ __('shop::app.customer.account.profile.index.title') }}
@endsection

@inject('order','Webkul\Sales\Repositories\OrderRepository')

<?php
    $orderInfo = json_decode($order->getOrderNumbers(), true);
?>

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h4>Dashboard</h4>
                        <h5>Total Stats</h5>
                        <div class="dashboard-block">
                            <a href = "{{route('memy.orders.index')}}?status[eq]=completed">
                                <div class="dashboard-block-box green-box">
                                    <article>
                                        <p>Completed Orders</p>
                                        <h3>{{$orderInfo['completedCount']}}</h3>
                                    </article>
                                </div>
                            </a>
                            <a href = "{{route('memy.orders.index')}}?status[eq]=canceled">
                                <div class="dashboard-block-box red-box">
                                    <article>
                                        <p>Cancelled Orders</p>
                                        <h3>{{$orderInfo['canceledCount']}}</h3>
                                    </article>
                                </div>
                            </a>
                            <a href = "{{route('memy.orders.index')}}?status[eq]=pending">
                                <div class="dashboard-block-box yellow-box">
                                    <article>
                                        <p>Pending Orders</p>
                                        <h3>{{$orderInfo['pendingCount']}}</h3>
                                    </article>
                                </div>
                            </a>
                            <a href = "{{route('memy.orders.index')}}?status[eq]=shipped">
                                <div class="dashboard-block-box blue-box">
                                    <article>
                                        <p>Shipped Orders</p>
                                        <h3>{{$orderInfo['shippedCount']}}</h3>
                                    </article>
                                </div>
                            </a>
                        </div>
                        <div class="dashboard-total-spent">
                            <h5>Total Transactions Amount</h5>
                            <article>
                                <p>Total amount spent shopping with us</p>
                                <h3>Rs. {{$orderInfo['totalTransaction']}}.00</h3>
                            </article>
                        </div>
                        <h5>Total Pending Transaction</h5>
                        <div class="dashboard-lower-block">
                            <div class="dashboard-lower-block-box">
                                <article>
                                    <p>Number of Items</p>
                                    <h3>{{$orderInfo['numberOfItemsProcessing']}}</h3>
                                </article>
                            </div>
                            <div class="dashboard-lower-block-box" onclick="location.href='{{route('memy.orders.index')}}?status[eq]=processing'" style="cursor:pointer;">
                                <article>
                                    <p>To be shipped</p>
                                    <h3>{{$orderInfo['processingCount']}}</h3>
                                </article>
                            </div>
                        </div>
                        <h5>Help and support</h5>
                        <div class="dashboard-support">
                            <article>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <a href="{{route('faq.page.render')}}">FAQ</a>
                                <a href="">Help & Support</a>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection