@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@inject('wishlist', 'Webkul\Customer\Repositories\WishlistRepository')
@inject ('priceHelper', 'Webkul\Product\Helpers\Price')


<?php
$items = $wishlist->findWhere([
    'channel_id' => core()->getCurrentChannel()->id,
    'customer_id' => auth()->guard('customer')->user()->id]
);

?>

@extends('memy::layouts.master')

@section('page_title')
Wishlist
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                    @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">                        
                        <h5>My Wishlist</h5>
                        <div class="table-responsive">
                            <table class="table">
                                    @if ($items->count())
                                <tr>
                                    <th></th>
                                    <th>Item</th>
                                    <th>Item Detail</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>

                                @foreach ($items as $item)

                                <tr>
                                    <td><a class="text-red" href="{{ route('customer.wishlist.remove', $item->id) }}" onclick="removeLink('Do you really want to do this?')" data-toggle="tooltip" data-placement="top"  title="Remove From Wishlist"><ion-icon name="close"></ion-icon></a></td>
                                    <td>
                                        @php
                                            $image = $productImageHelper->getProductBaseImage($item->product);
                                        @endphp
                                        <figure class="cart__image">
                                            <a href={{route('memy.products.gallery',$item->product->url_key)}}><v-lazy-image src="{{ $image['small_image_url'] }}" class="img-fluid" alt="{{$item->product->name}}"/></a>
                                        </figure>
                                    </td>
                                    <td>
                                            <a href={{route('memy.products.gallery',$item->product->url_key)}}>{{$item->product->name}}</a>
                                            {{-- <span>Dreamy Dots Nursing Pillow Blue</span> --}}
                                    </td>

                                    {{-- price start --}}
                                    @if ($item->product->type == 'configurable')
                                        <td>{{ core()->currency($priceHelper->getMinimalPrice($item->product)) }}</td>
                                    @else
                                        @if ($priceHelper->haveSpecialPrice($item->product))
                                            <td>
                                                {{ core()->currency($priceHelper->getSpecialPrice($item->product)) }}
                                                <br>
                                                <strike>{{ core()->currency($item->product->price) }}</strike>
                                            </td>
                                        @else
                                            <td> {{ core()->currency($item->product->price) }}</td>
                                        @endif
                                    @endif
                                    {{-- price end --}}

                                    @if ($item->product->type == 'simple')
                                        @if($item->product->haveSufficientQuantity(1))
                                            <td><a href="{{ route('customer.wishlist.move', $item->id) }}" data-toggle="tooltip" data-placement="top"  title="{{ __('shop::app.wishlist.move-to-cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></td>
                                        @else
                                            <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"  title="Out Of Stock"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></td>
                                        @endif
                                    @else
                                        <td><a href="{{ route('customer.wishlist.move', $item->id) }}" data-toggle="tooltip" data-placement="top"  title="{{ __('shop::app.wishlist.move-to-cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></td>
                                    @endif

                                </tr>
                                @endforeach
                                    {{-- <tr class="total">
                                        <th colspan="4">Total</th>
                                        <th colspan="2">Rs. 620/-</th>
                                    </tr> --}}
                                @else
                                    <span>
                                        {{ __('customer::app.wishlist.empty') }}
                                    </span>
                                @endif
                            </table>
                    </div>
                        @if (count($items))
                            <div class="">
                                <a class="btn btn-secondary" href="{{ route('customer.wishlist.removeall') }}">{{ __('shop::app.wishlist.deleteall') }}</a>
                            </div>
                        @endif
                        <div></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection