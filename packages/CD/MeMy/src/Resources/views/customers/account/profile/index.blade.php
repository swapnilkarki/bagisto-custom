@extends('memy::layouts.master')

@section('page_title')
{{ __('shop::app.customer.account.profile.index.title') }}
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <profile></profile>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/x-template" id="profile-template">
<div class="row">
    <div class="col-lg-3 col-md-3 nav-bg">
            <div class="dashboard-sidebar-profile">
                <div class="profile-image">
                    <figure>
                        <img :src="previewImage" class="img-fluid" alt="memykid">
                        {{-- <figcaption class="halfCircleBottom">
                            <a href="">Change Image</a>
                        </figcaption> --}}
                    </figure>
                    <article>
                        <h6>@{{newName}}</h6>
                        <p>@{{newEmail}}</p>
                    </article>
            
                    {{-- <input type="file" accept="image/*" style="display: none;" > --}}
            
                    {{--<button class="btn btn-primary">Upload</button>--}}
                    {{--<button class="btn btn-primary">Cancel</button>--}}
                </div>
            </div>
             @include('memy::customers.account.partials.sidemenu')
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="dashboard-content">
            {{-- change password modal start --}}
            <div class="modal fade review-modal review" id="changepassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                                <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                    <label for="password">New Password</label>
                                    <input class="form-control form-box" type="password" v-model="newPassword" id="password" name="password" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.password') }}&quot;" v-validate="'min:6'" ref="password">
                                    <span class="control-error" v-if="errors.has('password')">@{{ errors.first('password') }}</span>                               
                                </div>

                                <div class="form-group" :class="[errors.has('password_confirmation') ? 'has-error' : '']">
                                    <label for="password_confirmation">Confirm Password</label>
                                    <input class="form-control form-box" type="password" v-model="confirmPassword" id="password_confirmation" name="password_confirmation" data-vv-as="&quot;{{ __('shop::app.customer.account.profile.cpassword') }}&quot;" v-validate="'min:6|confirmed:password'">
                                    <span class="control-error" v-if="errors.has('password_confirmation')">@{{ errors.first('password_confirmation') }}</span>                                   
                                </div>
                                <button class="btn btn-primary" @click="updatePassword()">Update Password</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- change password modal end --}}
            <div class="profile">
                <h5>Personal Information</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-image">
                            {{-- <figure>
                                <img src="{{ asset('vendor/webkul/memy/assets/images/avatar.png')}}" class="img-fluid" alt="memykid">
                                <figcaption>
                                    <a href="">Change Image</a>
                                </figcaption>
                            </figure> --}}
                            <figure>
                                <img :src="previewImage" alt="memykid" onclick="document.getElementById('profileimage').click()"/>
                                <figcaption class="halfCircleBottom">
                                    <a href="javascript:void(0);" onclick="document.getElementById('profileimage').click()" >Change Image</a>
                                </figcaption>
                            </figure>
                            <input id="profileimage" ref="profileimage" type="file" accept="image/*" style="display: none;" @change=uploadImage>
                            <button v-if="imageChanged" @click=confirmImageChange class="btn btn-primary">Upload</button>
                            <button v-if="imageChanged" @click=cancelImageChange class="btn btn-secondary">Cancel</button>
                        </div>
                        <br v-if="imageChanged">
                        <div title="Click To Edit" class="form-group" >
                            {{--<label for="name">Name</label>--}}
                            <input title="Click To Edit" id="editname" placeholder="Name" type="text" name="first_name" v-model="newName" v-validate="'required'" data-vv-as="&quot;Name&quot;" @blur="updateInfo()" v-on:keyup.enter="$event.target.blur()">
                            <span class="control-error" v-if="errors.has('first_name')">@{{ errors.first('first_name') }}</span>   
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" placeholder="E-mail" v-model="newEmail" readonly>
                        </div>
                        <div class="form-group">
                            <select title="Click To Edit" class="form-control no-padder" v-model="newGender" @change="updateInfo()">
                                <option disabled>Select Gender</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div title="Click To Edit" class="form-group">
                            {{-- <label for="password">DOB</label> --}}

                            <input type="date" class="form-control no-padder" placeholder="dob" v-model="newDob" @change="updateInfo()">
                        </div>

                        <div title="Click To Edit" class="form-group">
                            <input title="Click To Edit" type="text" name="mobile_number" placeholder="98XXXXXXXX" v-model="newPhone"  v-on:keyup="checkNumber"  v-validate="'required|numeric|min:10|max:10'" data-vv-as="&quot;'Mobile Number'&quot;">
                            <span class="control-error" v-if="errors.has('mobile_number')">@{{errors.first('mobile_number')}}</span>
                            <span class="control-error" v-if="mobileNumberExists">Mobile number already registred</span>
                            <button v-if="confirmNewNumber" @click=updateInfo>Update</button>
                        </div>

                        {{-- <a class="edit-btn"  href="{{ route('memy.profile.edit') }}">{{ __('shop::app.customer.account.profile.index.edit') }}</a> --}}
                        <a href="javascript:void(0);" class="edit-btn change-password" data-toggle="modal" data-target="#changepassword">Change Password</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script> Vue.component('profile', {
        template: '#profile-template',

        data: function() {
            return {
                customerInfo : [],
                gender : '',
                newName : '',
                newEmail : '',
                newDob : '',
                newGender : 'Select Gender',
                newPhone : '',

                newPassword : '',
                confirmPassword : '',

                imageFile : null,

                previewImage : null,

                imageChanged : false,

                mobileNumberExists : false,

                confirmNewNumber : false
            }
        },

       created: function(){
            this.getCustomerData();
            // this.updateCustomerData();
        },

        methods: {
            getCustomerData : function(){
                this_this = this;
                // console.log('get customer info');
                axios.get('/api/customer/get')
                    .then(response => {
                        this_this.customerInfo = response.data.data;
                        if(response.data.data.profile_picture == null){
                            this_this.previewImage= route('memy.index')+'/vendor/webkul/ui/assets/images/product/small-product-placeholder.png';
                        }else{
                            this_this.previewImage= route('memy.index')+'/storage/'+response.data.data.profile_picture;
                        }
                        this_this.gender = response.data.data.gender;
                        this_this.newName= response.data.data.first_name;
                        // this_this.newEmail= response.data.data.email;
                        
                          var str = response.data.data.email;
                        var res = str.split("|");
                        if(res.length == 2){
                            this_this.newEmail= res[0];
                        }else{
                            this_this.newEmail= response.data.data.email;
                        }

                        this_this.newDob= response.data.data.date_of_birth;
                        this_this.newGender= response.data.data.gender;
                        this_this.newPhone= response.data.data.mobile_number;
                    })
                    .catch(error => console.log(error))
                  
            },

            updateInfo : function(){
                // console.log('update info');
                var this_this = this;
                var data = {
                    first_name: this.newName,
                    last_name: null,
                    gender: this.newGender,
                    email : this.newEmail,
                    date_of_birth: this.newDob,
                    mobile_number: this.newPhone
                }
                axios.put('/api/customer/profile', data)
                    .then(response => {
                        if(response){
                            Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'info updated',
                            showConfirmButton: false,
                            timer: 1000
                            })
                            this_this.confirmNewNumber = false;
                            }
                    })
                    .catch(error => console.log(error))
            },

            updatePassword : function(){
                this_this = this;
                if(this.newPassword.length >=6 && this.confirmPassword.length >=6){
                    if(this.newPassword.length == this.confirmPassword.length){
                        var data = {
                            first_name: this.newName,
                            last_name: null,
                            gender: this.newGender,
                            email : this.newEmail,
                            date_of_birth: this.newDob,
                            password: this.newPassword,
                            mobile_number: this.newPhone,
                            password_confirmation: this.confirmPassword
                        }
                        axios.put('/api/customer/profile', data)
                            .then(response => {
                                if(response){
                                    Swal.fire({
                                    position: 'center',
                                    type: 'success',
                                    title: 'password updated',
                                    showConfirmButton: false,
                                    timer: 1000
                                    })
                                    $('#changepassword').modal('hide');
                                    }
                            })
                            .catch(error => console.log(error))
                    }else{
                        // console.log("password mismatch");
                        Swal.fire({
                                    position: 'center',
                                    type: 'error',
                                    title: 'password mismatch',
                                    showConfirmButton: false,
                                    timer: 1000
                                    })
                    }
                   
                }else{
                    console.log("error");
                }
            },
            cancelImageChange : function(){
                this.previewImage = route('memy.index')+'/storage/'+this_this.customerInfo.profile_picture
                this.imageChanged = false;
            },

            uploadImage : function(e){
                const image = e.target.files[0];
                this.imageFile = image;
                const reader = new FileReader();
                reader.readAsDataURL(image);
                reader.onload = e =>{
                    this.previewImage = e.target.result;
                    this.imageChanged = true;
                };
            },

            confirmImageChange : function(){
                // console.log('image change confirmed');

                var data = this.imageFile;

                var this_this = this;

                let formData = new FormData();
                formData.append('image', data);

                axios.post('/api/customer/profile/picture',  formData,{headers: {'Content-Type': 'multipart/form-data'}
                    })
                            .then(response => {
                                // console.log(response.data);
                                if(response){
                                    Swal.fire({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Profile Image Uploaded',
                                    showConfirmButton: false,
                                    timer: 1500
                                    })
                                }
                                this_this.imageChanged = false;
                            })
                            .catch(error => console.log(error))
            },
            checkNumber : function(){
                this_this = this;
                this.confirmNewNumber = false;
                if(this.newPhone.length == 10){
                    this.checkMobileNumber(this_this.newPhone);
                }
            },

            checkMobileNumber : function(mobile_number){
            //    console.log('check mobile number'+mobile_number);
                this_this=this;
                axios.get('/api/customer/check-mobile-number/'+mobile_number)
                .then(function(response){
                    // console.log(response.data);
                        this_this.mobileNumberExists = response.data.status;
                        if(response.data.status == false){
                            this_this.confirmNewNumber = true;
                        }
                })
                .catch(function(error){
                    console.log(error);
                })
           }
        }
    })
</script>
@endpush