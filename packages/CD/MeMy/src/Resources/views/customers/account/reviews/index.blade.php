@extends('memy::layouts.master')

@section('page_title')
My Reviews
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="dashboard-content">
                        <h5>My Reviews </h5>
                        <review></review>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/x-template" id="reviews-template"> 
    <div class="table-responsive">

        <span style="position:relative;left: 45%;"><beat-loader :loading="Loading" :color="loaderColor" :size="loaderSize" :margin="loaderMargin"></beat-loader></span>

        <table class="table" v-if="meta.total >= 1">
                <tr>
                    <th>Item</th>
                    <th>Product Name</th>
                    <th>Rating</th>
                    <th>Review</th>
                    <th>Date</th>
                </tr>
                <tr v-for="(review,index) in reviews" :key="index">
                    <td>
                        <figure class="cart__image">
                            <a :href="route+'/'+review.product.url_key" :title="review.product.name">
                                <img :src="review.product.base_image.small_image_url" class="img-fluid" :alt="review.product.name">
                            </a>
                        </figure>
                    </td>

                    <td>
                        @{{review.product.name}}
                    </td>

                    <td class="review-rating-table">
                        <ion-icon name="star" class="checked" v-for="index in parseInt(review.rating)" :key="'checked'+index"></ion-icon>
                        <ion-icon name="star" style="color:#6c757d;" v-for="index in 5-parseInt(review.rating)" :key="'unchecked'+index"></ion-icon>
                    </td>

                    <td>
                        @{{ review.comment }}
                    </td>

                    <td>@{{review.created_at.date | dateOnly}}</td>

                </tr>
        </table>

        <span v-else-if="meta.total == 0 && !Loading">
            {{ __('customer::app.reviews.empty') }}
        </span>
           
        <div class="pagination-block text-center" v-if="(meta.last_page != 1)">
            <nav aria-label="...">
                <ul class="pagination">
                    <li :class="links.prev == null ? 'page-item disabled' : 'page-item'">
                        <a class="page-link" href="javascript:void(0);" tabindex="-1" @click="paginate(links.prev)" rel="prev">Prev</a>
                    </li>
    
                            <!-- Pagination Elements -->
                    <span v-for="(n,index) in pages" :key="index">
                        <li v-if="meta.current_page == n" class="page-item active"><a class="page-link" href="javascript:void(0);">@{{ n }}</a></li>
                        <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(n)">@{{ n }}</a></li>
                    </span>
    
                    <li :class="links.next == null ? 'page-item disabled' : 'page-item'">
                        <a class="page-link" href="javascript:void(0);" @click="paginate(links.next)">Next</a>
                    </li>
                </ul>
            </nav>
        </div>

</div>

</script>
<script>
Vue.component('review', {
    template: '#reviews-template',

    data: function() {
        return {
            userId : <?php echo(auth()->guard('customer')->user()->id)?>,
            route :  route('memy.products.vue.gallery'),
            reviews : [],
            meta : [],
            links : [],
            pages : null,
            Loading : false,
            loaderSize : '25px',
            loaderMargin : '4px',
            loaderColor : '#E3007B'
        }
    },

    created : function(){
        this.getProductReview();
    },

    methods : {
        getProductReview : function(){
            this_this = this;
            this.Loading = true;
            axios.get('/api/reviews?customer_id='+this.userId+'&limit=5&page=1')
            .then((response) => {
                this_this.reviews = response.data.data;
                this_this.meta = response.data.meta;
                this_this.links = response.data.links;
                this_this.pages = parseInt(response.data.meta.last_page);
                this_this.Loading = false;
            })
            .catch((error) => {
                console.log(error);
            })

        },
        
        paginate : function(link){
                window.scrollTo(0, 0);
                var self = this;
                this.reviews = [];
                this.meta = [];
                this.Loading = true;
                axios
                .get(link)
                .then(function (response) {
                    // handle success
                    this_this.reviews = response.data.data;
                    this_this.meta = response.data.meta;
                    this_this.links = response.data.links;
                    this_this.pages = parseInt(response.data.meta.last_page);
                    this_this.Loading = false;
                                      
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })   
            },

        paginateNumber : function(n){
            // console.log(n);
            var link = this.links.first;
            var newLink = link.replace("page=1", "page="+n);
            this.paginate(newLink);
        }
    },

    filters: {
        dateOnly: function (value) {
            return value.substr(0,value.indexOf(' '));
        }
    }
})
</script>

@endpush