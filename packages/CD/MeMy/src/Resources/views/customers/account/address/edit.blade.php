@extends('memy::layouts.master')

@section('page_title')
Edit Address
@endsection

@section('content')
<section class="dashboard">
    <div class="container">
        <div class="wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 nav-bg">
                   @include('memy::customers.account.partials.sidemenu')
                </div>
                <div class="col-lg-9 col-md-9">

                    @if($address->default_address == 1)
                        <p>Default Address</p>
                    @endif

                    <edit-address :address-id="{{$address->id}}"></edit-address>
                   {{-- vue component here --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/x-template" id="address-template">
    <div class="dashboard-content address-block">
        <h5>Edit Address</h5>
            <form method="post" action="{{ route('memy.address.edit', $address->id) }}" id="edit-address-form" data-vv-scope="edit-address-form" @submit.prevent="updateForm('edit-address-form')">
            <div class="profile">
                @method('PUT')
                @csrf

                <div class="form-group" :class="[errors.has('title') ? 'has-error' : '']">
                    <label for="title" class="required">Title</label>
                    <input class="form-control form-box" type="text" name="title" v-model="addressInfo.title" id="title" v-validate="'required'" data-vv-as="&quot;Title&quot;">
                    <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>                       
                </div>

                <div class="form-group" :class="[errors.has('address1[]') ? 'has-error' : '']">
                    <label for="address_0" class="required">Street Address</label>
                    <input class="form-control form-box" type="text" name="address1[]" id="address_0" v-model="address1" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.street-address') }}&quot;">
                    <span class="control-error" v-if="errors.has('address1[]')">@{{ errors.first('address1[]') }}</span>                       
                </div>

                <div class="form-group" :class="[errors.has('city') ? 'has-error' : '']">
                    <label for="city" class="required">City</label>
                    <input class="form-control form-box" type="city" name="city" v-model="addressInfo.city" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.city') }}&quot;">
                    <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>                      
                </div>

                <div class="form-group" :class="[errors.has('state') ? 'has-error' : '']">
                    <label for="state" class="required">State</label>
                    <select v-validate="'required'" class="form-control form-box" id="state" name="state" v-model="addressInfo.state" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.state') }}&quot;">
                        <option value="Province No. 1">Province No. 1</option>
                        <option value="Province No. 2">Province No. 2</option>
                        <option value="Bagmati">Bagmati</option>
                        <option value="Gandaki">Gandaki</option>
                        <option value="Province No. 5">Province No. 5</option>
                        <option value="Karnali">Karnali</option>
                        <option value="Sudurpashchim">Sudurpashchim</option>
                    </select>
                    <span class="control-error" v-if="errors.has('state')">@{{ errors.first('state') }}</span>
                </div>

                <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                    <label for="phone" class="required">Phone</label>
                    <input class="form-control form-box" type="phone" name="phone" v-model="addressInfo.phone" v-validate="'required'" data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.phone') }}&quot;">
                    <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>                           
                </div>

                <div class="form-group" v-if="addressInfo.default_address !=1 ">
                    <span class="checkbox">
                        <input type="checkbox" id="default_address" name="default_address"/>
                        <label class="checkbox-view" for="default_address"></label>
                        Make Default Address
                    </span>
                </div>
                
                <input v-else type="hidden" id="default_address" name="default_address" value='on'/>

                <input type="hidden" name="country" value="NP">
                <input type="hidden" name="postcode" :value="addressInfo.postcode">
        
                <input class="btn btn-primary" type="submit" value="{{ __('shop::app.customer.account.address.create.submit') }}">
            </div>
            </form>
            <div style="float:right;margin-top:-485px;">
                <google-map :address-id="addressId" custom-style="{width:350px;  height: 300px;}" show-auto-complete="true"></google-map>
            </div>
    </div>
</script>
<script>
    Vue.component('edit-address', {
        template: '#address-template',

        props: ['addressId'],

        data: function() {
            return {
                addressInfo : [],
                location : [],
                address1 : null
            }
        },

        mounted(){
            self = this;
            this.$root.$on('addressChanged', (newAddress,location) => {
                self.address1 = '';
                self.location = location;
                newAddress.address_components.forEach(element =>{
                    if(element.types.includes("route")){
                        self.address1 = self.address1 +element.short_name;
                    }
                    else if(element.types.includes("sublocality")){
                        self.address1 =  self.address1 + ', '+ element.short_name;
                    }
                    else if(element.types.includes("locality")){
                        self.addressInfo.city = element.short_name;
                    }
                    else if(element.types.includes("administrative_area_level_2")){
                    var administrative_area_level_2 = element.short_name;
                    }
                    else if(element.types.includes("postal_code")){
                        self.addressInfo.postcode = element.short_name;
                    }
                });

            });
        },

        created: function(){
            this.getData();
        },

        methods : {
            getData : function(){
                var self = this;
                axios.get('/api/addresses/'+this.addressId)
                    .then(response =>{
                        self.addressInfo = response.data.data;
                        self.address1 = response.data.data.address1[0];
                        const marker = {
                            lat: response.data.data.lat,
                            lng: response.data.data.lng
                        }
                        self.location = new Object({position : marker, infoText : self.locationAddress});
                    })
                    .catch(error => {
                        console.log(error);
                    })
            },
            updateForm: function (formScope) {
                    var self = this;
                    this.$validator.validateAll(formScope).then(result => {
                            if (result) {
                                self.saveLatLng(self.location);
                            } else {
                                 Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: "Form validation failed.",
                                showConfirmButton: false,
                                timer: 1500
                                })
                                // console.log('validator fail : form not valid');
                            }
                        }
                    );
            },
            saveLatLng : function(location){
                var url = "/api/update/lat-lng/"+this.addressId;
                var params = {
                    lat : this.location.position.lat,
                    lng : this.location.position.lng,
                    formatted_address : this.location.infoText,
                }
                axios.put(url,{params})
                    .then(response =>{
                    // console.log(response);
                    document.getElementById('edit-address-form').submit();
                    // console.log("stored new info in db");
                    })
                    .catch(error=>{
                    console.log(error);
                    })
            },
        }
    
    })
</script>
@endpush