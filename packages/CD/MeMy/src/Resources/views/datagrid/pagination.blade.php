@if($results->lastPage() != 1)
    <div class="pagination">
        <!-- Previous Page Link -->
        @if ($results->onFirstPage())
            <li class="page-item disabled"><span>&laquo;</span></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $results->previousPageUrl() }}" rel="prev">&laquo;Previous</a></li>
        @endif

        <!-- Pagination Elements -->
        @for ($i = 1; $i <= $results->lastPage(); $i++)
            @if($results->currentPage() == $i)
                <li class="page-item active"><span>{{ $i }}</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $results->url($i) }}">{{ $i }}</a></li>
            @endif
        @endfor

        <!-- Next Page Link -->
        @if ($results->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $results->nextPageUrl() }}" rel="next">Next&raquo;</a></li>
        @else
            <li class="page-item disabled"><span>&raquo;</span></li>
        @endif
    </div>
@endif