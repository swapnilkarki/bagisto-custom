@extends('memy::layouts.master')

@section('page_title')
    MeMyKids - Search
@endsection

@guest('customer')
<?php $authUser = 'false'?>
@endguest
@auth('customer')
<?php $authUser = 'true'?>
@endauth
@inject ('searchRepository', 'Webkul\Product\Repositories\SearchRepository')

{{-- {{$temp = $searchRepository->search('just some data')}}
{{dd($temp)}} --}}


@section('content')
<section class="category searched-results">
    <div class="container">
            <div class="row">
                @include('memy::search.partials.filter')
                <product-results></product-results>
                <!-- set progressbar -->
                <vue-progress-bar></vue-progress-bar>

            </div>
    </div>
    
    <vue-modal name="loader-modal" classes="loader-modal" :clickToClose="false" :height="30" :width="30">
        <div class="text-center">
            <beat-loader :loading="true" color="#E3007B" size="25px"></beat-loader>
        </div>
    </vue-modal>

</section>
@endsection

@push('scripts')
<script type="text/x-template" id="product-results-template">
    <div class="col-lg-9 col-md-12">
        <div class="result-display">
            <div class="row">
                <div class="col-md-3">
                    {{-- results count start--}}

                    <p  v-if="resultCount == 1"><b> 1 &nbsp;&nbsp;</b>SEARCH RESULT</p>

                    <p v-else-if="resultCount > 1 "><b>@{{ resultCount }}&nbsp;&nbsp;</b>SEARCH RESULTS</p>

                    <p v-else-if="resultCount == 0"><b>0&nbsp;&nbsp;</b>SEARCH RESULTS</p>

                    {{-- results count end --}}

                    {{-- mobile filter start--}}
                    <div class="mobile-filter">
                        <div id="mySidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#8592;</a>
                            <aside class="filter"><p>Filter By</p>
                                <div class="category-blocks">
                                    <div class="accordion">
                                        <div class="option active"><input type="checkbox" id="toggle0" class="toggle"> <label for="toggle0" class="title">Price
                                            </label> <br>
                                            <div class="vue-slider-component vue-slider-horizontal" style="width: auto; padding: 8px;">
                                                <div aria-hidden="true" class="vue-slider" style="height: 6px;">
                                                    <div class="vue-slider-always vue-slider-dot" style="width: 16px; height: 16px; top: -5px; transition-duration: 0s; transform: translateX(-8px);">
                                                        <div class="vue-slider-dot-handle"></div>
                                                        <div class="vue-slider-tooltip-top vue-slider-tooltip-wrap" style="visibility: hidden;"><span class="vue-slider-tooltip"
                                                                                                                                                        style="background-color: rgb(255, 100, 114); border-color: rgb(255, 100, 114);">0</span>
                                                        </div>
                                                    </div>
                                                    <div class="vue-slider-always vue-slider-dot" style="width: 16px; height: 16px; top: -5px; transition-duration: 0s; transform: translateX(-8px);">
                                                        <div class="vue-slider-dot-handle"></div>
                                                        <div class="vue-slider-tooltip-top vue-slider-tooltip-wrap" style="visibility: hidden;">
                                                            <span class="vue-slider-tooltip" style="background-color: rgb(255, 100, 114); border-color: rgb(255, 100, 114);">0</span>
                                                        </div>
                                                    </div>
                                                    <ul class="vue-slider-piecewise"></ul>
                                                    <div class="vue-slider-process" style="background-color: rgb(255, 100, 114); transition-duration: 0s; width: 0px; left: 0px;">
                                                        <div class="vue-merged-tooltip vue-slider-tooltip-top vue-slider-tooltip-wrap" style="top: -14px; left: 50%; visibility: inherit;">
                                                            <span class="vue-slider-tooltip" style="background-color: rgb(255, 100, 114); border-color: rgb(255, 100, 114);">
                                                                0
                                                            </span>
                                                        </div>
                                                    </div> <!---->
                                                </div>
                                            </div> <!---->
                                        </div>
                                        <div class="option"><input type="checkbox" id="toggle1" class="toggle"> <label for="toggle1" class="title">For
                                            </label>
                                            <div class="content">
                                                <div class="sub-content">
                                                    <div class="form-check"><input type="checkbox" id="26" class="form-check-input" value="26"> <label for="26" class="form-check-label">Boy</label></div>
                                                    <div class="form-check"><input type="checkbox" id="27" class="form-check-input" value="27"> <label for="27" class="form-check-label">Girl</label></div>
                                                </div>
                                            </div> <!---->
                                        </div>
                                        <div class="option"><input type="checkbox" id="toggle2" class="toggle"> <label for="toggle2" class="title">Age
                                            </label>
                                            <div class="content">
                                                <div class="sub-content">
                                                    <div class="form-check"><input type="checkbox" id="10" class="form-check-input" value="10"> <label for="10" class="form-check-label">0-3 months</label></div>
                                                    <div class="form-check"><input type="checkbox" id="11" class="form-check-input" value="11"> <label for="11" class="form-check-label">3-6 months</label></div>
                                                    <div class="form-check"><input type="checkbox" id="12" class="form-check-input" value="12"> <label for="12" class="form-check-label">6-12 months</label></div>
                                                    <div class="form-check"><input type="checkbox" id="13" class="form-check-input" value="13"> <label for="13" class="form-check-label">1-2 yrs</label></div>
                                                    <div class="form-check"><input type="checkbox" id="14" class="form-check-input" value="14"> <label for="14" class="form-check-label">2-4 yrs</label></div>
                                                    <div class="form-check"><input type="checkbox" id="15" class="form-check-input" value="15"> <label for="15" class="form-check-label">4-6 yrs</label></div>
                                                    <div class="form-check"><input type="checkbox" id="16" class="form-check-input" value="16"> <label for="16" class="form-check-label">6-8 yrs</label></div>
                                                    <div class="form-check"><input type="checkbox" id="17" class="form-check-input" value="17"> <label for="17" class="form-check-label">8-10 yrs</label></div>
                                                    <div class="form-check"><input type="checkbox" id="28" class="form-check-input" value="28"> <label for="28" class="form-check-label">10-12 yrs</label></div>
                                                </div>
                                            </div> <!---->
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <span style="font-size:14px;text-transform: uppercase; color:#495057; border: 1px solid #ddd; padding: 12px 32px; line-height: 40px; cursor:pointer" onclick="openNav()"><i class="fa fa-filter" aria-hidden="true"></i> Filter</span>
                    </div>
                    {{-- mobile filter end --}}
                </div>

                @include('memy::search.partials.toolbar')
            </div>
        </div>

        <h2 v-if="results == null">{{ __('shop::app.products.whoops') }}</h2>
        <span v-if="results == null">{{ __('shop::app.search.no-results') }}</span>

        <div v-if="results != null " class="row">
            <div class="search-product" v-for="(test,index) in results">
                <product-box-in-search :productFlat="test" :authUser="{{$authUser}}" :key="index"></product-box-in-search>
            </div>
        </div>

    {{-- @include('memy::products.pagination') --}}
    </div>
</script>

<script type="text/x-template" id="product-box-template">
    <div class="product">
        <div v-if="productFlat.new == 1" href="#" class="circle-shape">
            <p> New </p>
        </div>

        <a v-if="specialPrice" class="circle-shape">
                <p>@{{discount}} % <br> <span>Off</span></p>
        </a>

        <!--wishlist start-->

            <div class="product-wishlist" v-if="authUser">
            <wishlist-add-button :id="productFlat.id"></wishlist-add-button>
        </div>

        <div class="product-wishlist" v-else>
            <wishlist-login-register></wishlist-login-register>
        </div>

        <!--wishlist end-->

        <figure class="product-image">
            <a :href="productLink" data-toggle="tooltip" data-placement="top" :title="productFlat.name">
                <v-lazy-image v-if="mediumImage != null" :src="mediumImage" :src-placeholder="smallImage" class="img-fluid" :alt="productFlat.name"/>
                <v-lazy-image v-else src="{{asset('vendor/webkul/ui/assets/images/product/meduim-product-placeholder.png')}}" src-placeholder="{{asset('vendor/webkul/ui/assets/images/product/small-product-placeholder.png')}}" class="img-fluid" alt="memykid"/>
            </a>
        </figure>

        <a :href="productLink" data-toggle="tooltip" data-placement="top" :title="productFlat.name" class="product-ellipseshape">
            <div class="product-info">

                <h5>@{{ productFlat.name }}</h5>

                <!--price end-->
                <span v-if="specialPrice">

                    <strong>@{{ productFlat.special_price | currency}}</strong>
                    <br>
                    <strike style="font-size:12px;">@{{ productFlat.price | currency}}</strike>

                </span>

                <span v-else>
                    <strong>@{{ productFlat.price | currency }}</strong>
                </span>
                <!--price end-->

                <p v-if="productFlat.qty == 0">out of stock</p>


                <div class="rating-star display-none">
                    <i class="fa fa-star checked" aria-hidden="true" v-for="index in averageRating" :key="'checked'+index"></i>

                    <i class="fa fa-star" aria-hidden="true" v-for="index in 5-averageRating" :key="'unchecked'+index" ></i>

                    <span class="bestseller__rater">(@{{totalRating}} reviews)</span>
                </div>

            </div>
        </a>

    </div>
</script>

<script>
    Vue.component('product-results', {
        template: '#product-results-template',

        data: function() {
            return {
                searchTerm : window.location.search,
                results : [],
                toolbarFilter : '',
                resultCount : 0
            }
        },

        mounted(){
            this.$root.$on('filterEvent' , (filter) => {
               this.applyFilter(filter);
            });

            this.$root.$on('toolbarEvent' , (value) => {
                this.applyToolbarFilter(value);
            });
        },

        created(){
            this.getResults();
        },

        methods : {
            applyFilter : function(filter){
                // console.log('filter event ' + filter);
                this.searchTerm = filter;
                this.results = [];
                // window.location = route('memy.search.index')+this.searchTerm+this.toolbarFilter;
                this.getResults();
            },
            applyToolbarFilter : function(value){
                // console.log(value);
                this.toolbarFilter = value;
                this.results = [];
                // window.location = route('memy.search.index')+this.searchTerm+this.toolbarFilter;
                this.getResults();
            },

            getResults : function(){

                this_this = this;
                this.$Progress.start();
                this.$modal.show('loader-modal');
                axios.get('/api/search'+this.searchTerm+this.toolbarFilter)
                    .then(response =>{
                        this_this.results = response.data.data;
                        // console.log(this_this.results);
                        this_this.resultCount = Object.keys(this_this.results).length;
                        if(this_this.resultCount == 0){
                            this_this.results = null;
                        }

                        this_this.$Progress.finish();
                        this_this.$modal.hide('loader-modal');
                        this_this.calculateHighestPrice();
                    })
                    .catch(error =>{
                        console.log(error);
                        this_this.$Progress.fail();
                        this_this.$modal.hide('loader-modal');

                    })
            },

            calculateHighestPrice : function(){
                var max = 500;
                for(index in this.results){
                    if(this.results[index].price > max)
                        max = parseInt(this.results[index].price);
                }
                this.$root.$emit('maxPriceEvent', max);
            }

        }

    });

    Vue.component('product-box-in-search', {
        template: '#product-box-template',

        props : ['productFlat','authUser'],

        data: function() {
            return {
                specialPriceDate : null,
                route :  route('memy.products.vue.gallery'),
                productLink: 'product link',
                averageRating : Math.round(this.productFlat.averageRating),
                totalRating : this.productFlat.totalReviews,
                specialPrice : false,
                discount : null,
                mediumImage :'',
                smallImage : ''

            }
        },

        created : function(){
            // console.log(this.productFlat);
            if(this.productFlat.special_price_to != null){
                this.specialPriceDate = this.productFlat.special_price_to
                this.specialAvailability();
            }

            this.productLink = this.route +'/'+ this.productFlat.url_key;
            if(this.productFlat.images[0]){
                this.mediumImage = window.location.origin + '/cache/medium/'+ this.productFlat.images[0].path,
                this.smallImage = window.location.origin + '/cache/small/'+ this.productFlat.images[0].path
            }else{
                this.mediumImage =null
            }


        },

        methods : {

            specialAvailability : function(){

                var today =  Date.parse(new Date());
                var d = Date.parse(this.specialPriceDate);
                var minutes = 1000 * 60;
                var hours = minutes * 60;
                var days = hours * 24;
                var years = days * 365;

                if(Math.round(d / days) - Math.round(today / days)  >= 0)
                {
                    this.specialPrice = true;
                    this.discount = (((parseInt(this.productFlat.price)-parseInt(this.productFlat.special_price))/parseInt(this.productFlat.price))*100).toFixed(1);
                }
            }

        },

        filters: {
            currency: function (value) {
                if (!value) return ''
                return 'Rs. '+parseInt(value)+'.00';
            }
        }

    });
</script>

<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
@endpush