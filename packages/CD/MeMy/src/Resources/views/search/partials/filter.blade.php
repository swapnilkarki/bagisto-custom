@inject ('attributeRepository', 'Webkul\Attribute\Repositories\AttributeRepository')

<div class="col-lg-3 col-md-12">
        <layered-navigation></layered-navigation>
</div>

@push('scripts')
    <script type="text/x-template" id="layered-navigation-template">
        <aside class="filter mobile-hidden" >
            <p>Filter By</p>
            <div class="category-blocks">
                <div class="accordion">

                <filter-attribute-item v-for='(attribute, index) in attributes' :attribute="attribute" :key="index" :index="index" @onFilterAdded="addFilters(attribute.code, $event)" :appliedFilterValues="appliedFilters[attribute.code]">
                </filter-attribute-item>

                </div>
            </div>
        </aside>
    </script>

    <script type="text/x-template" id="filter-attribute-item-template">
        <div class="option" :class="[active ? 'active' : '']"  v-if="attribute.type != 'price'">
            
            <input type="checkbox" :id="toggle" class="toggle" @click="active = !active"/>
            <label class="title" :for="toggle">@{{ attribute.name ? attribute.name : attribute.admin_name }}
            </label>
            
            <div class="content">
                <div class="sub-content">
                    <div class="form-check" v-for='(option, index) in attribute.options'>
                        <input type="checkbox" class="form-check-input" :id="option.id" v-bind:value="option.id" v-model="appliedFilters" @change="addFilter($event)"/>
                        <label class="form-check-label" :for="option.id">@{{ option.label ? option.label : option.admin_name }}</label>
                    </div>
                </div>
            </div>

            <span class="pull-right" style="font-size : 12px" v-if="appliedFilters.length" @click.stop="clearFilters()">
                {{ __('shop::app.products.remove-filter-link-title') }}
            </span>
        </div>

        <div class="option" :class="[active ? 'active' : '']" v-else-if="attribute.type == 'price'">
            <input type="checkbox" :id="toggle" class="toggle" @click="active = !active"/>
            <label class="title" :for="toggle">@{{ attribute.name ? attribute.name : attribute.admin_name }}
            </label>
            <br>

            <vue-slider
                ref="slider"
                v-model="sliderConfig.value"
                :process-style="sliderConfig.processStyle"
                :tooltip-style="sliderConfig.tooltipStyle"
                :max="sliderConfig.max"
                :lazy="true"
                @callback="priceRangeUpdated($event)"
            ></vue-slider>

            <span class="pull-right "style="font-size : 12px" v-if="appliedFilters.length" @click.stop="clearFilters()">
                {{ __('shop::app.products.remove-filter-link-title') }}
            </span>
        </div>
    </script>

    <script>
        Vue.component('layered-navigation', {

            template: '#layered-navigation-template',

            data: function() {
                return {
                    // attributes : null,
                    attributes: @json($attributeRepository->getFilterAttributes()),
                    appliedFilters: {}
                }
            },

            created: function () {
                var urlParams = new URLSearchParams(window.location.search);

                //var entries = urlParams.entries();

                //for (let pair of entries) {
                    //this.appliedFilters[pair[0]] = pair[1].split(',');
                //}

                var this_this = this;

                urlParams.forEach(function (value, index) {
                    this_this.appliedFilters[index] = value.split(',');
                });
            },

            methods: {
                addFilters: function (attributeCode, filters) {
                    if (filters.length) {
                        this.appliedFilters[attributeCode] = filters;
                    } else {
                        delete this.appliedFilters[attributeCode];
                    }

                    this.applyFilter()
                },

                applyFilter: function () {
                    var params = [];

                    for(key in this.appliedFilters) {
                        params.push(key + '=' + this.appliedFilters[key].join(','))
                    }

                    // window.location.href = "?" + params.join('&');

                    var filter = "?" + params.join('&');
                    this.$root.$emit('filterEvent', filter);
                },

               
            }
        });

        Vue.component('filter-attribute-item', {

            template: '#filter-attribute-item-template',

            props: ['index', 'attribute', 'appliedFilterValues'],

            data: function() {
                return {
                    appliedFilters: [],

                    active: false,

                    toggle: 'toggle',

                    sliderConfig: {
                        value: [
                            0,
                            0
                        ],
                        max: 500,
                        processStyle: {
                            "backgroundColor": "#FF6472"
                        },
                        tooltipStyle: {
                            "backgroundColor": "#FF6472",
                            "borderColor": "#FF6472"
                        }
                    }
                }
            },

            mounted(){
                this.$root.$on('maxPriceEvent' , (maxPrice) => {
                    this.sliderConfig.max = maxPrice ;
                });
            },

            created: function () {
                if (!this.index)
                    this.active = true;

                if (this.appliedFilterValues && this.appliedFilterValues.length) {
                    this.appliedFilters = this.appliedFilterValues;

                    if (this.attribute.type == 'price') {
                        this.sliderConfig.value = this.appliedFilterValues;
                    }

                    this.active = true;
                }
                this.toggle=this.toggle+this.index;
            },

            methods: {
                addFilter: function (e) {
                    this.$emit('onFilterAdded', this.appliedFilters)
                },

                priceRangeUpdated: function (value) {
                    this.appliedFilters = value;

                    this.$emit('onFilterAdded', this.appliedFilters)
                },

                clearFilters: function () {
                    if (this.attribute.type == 'price') {
                        this.sliderConfig.value = [0, 0];
                    }

                    this.appliedFilters = [];

                    this.$emit('onFilterAdded', this.appliedFilters)
                }
            }

        });

    </script>
@endpush