@inject ('toolbarHelper', 'Webkul\Product\Helpers\Toolbar')

<div class="col-md-9">
    <div class="result-sort">
        <toolbar></toolbar>
    </div>
</div>

@push('scripts')
<script type="text/x-template" id="toolbar-template">
    <div class="row">
        <div class="col-md-6">
           
        </div>
        <div class="col-md-6">
            <div class="result-list">
                <label>Sort By &nbsp; </label>
                <select class="custom-select"  v-model="selected" @change='sort(selected)'>
                    <option v-for="order in availableOrders" v-bind:value="order.value">
                        @{{ order.text }}
                    </option>
                </select>
            </div>
        </div>
    </div>
</script>

<script>
      Vue.component('toolbar', {

        template: '#toolbar-template',

        data: function() {
            return {

                selected: '&sort=created_at&order=desc',

                availableOrders: [
                    { text: 'A-Z', value: '&sort=name&order=asc' },
                    { text: 'Z-A', value: '&sort=name&order=desc' },
                    { text: 'Newest First', value: '&sort=created_at&order=desc' },
                    { text: 'Oldest First', value: '&sort=created_at&order=asc' },
                    { text: 'Price low to high', value: '&sort=price&order=asc' },
                    { text: 'Price high to low', value: '&sort=price&order=desc' }
                ],
            }
            
        },

        methods: {
            sort : function(value){
                this.$root.$emit('toolbarEvent', value);
            },
            
        }

    });
   
</script>
@endpush