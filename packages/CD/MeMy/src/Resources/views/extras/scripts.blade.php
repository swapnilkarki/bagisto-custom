<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/easescroll.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/slick.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('vendor/webkul/memy/assets/js/zoom-image.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('vendor/webkul/memy/assets/js/product-zoom.js') }}"></script> --}}
<script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
<script src="{{asset('js/wow.js')}}"></script>
<script src="{{ asset('vendor/webkul/memy/assets/js/memy.js') }}"></script>
<script src="{{ asset('vendor/webkul/memy/assets/js/memy-main.js') }}"></script>
<script src="{{ asset('js/megamenu.js') }}"></script>

<script defer type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/igorlino/elevatezoom-plus@1.1.20/src/jquery.ez-plus.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>