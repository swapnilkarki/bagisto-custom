<accordian :title="'Product Tags'" :active="true">
    <div slot="body">
        <product-tags></product-tags>
    </div>
</accordian>
@push('scripts')

<script type="text/x-template" id="product-tags-template">
    <div>
        <div class="control-group">

            <label for="up-selling">
                Tags
            </label>

            <input type="text" class="control" autocomplete="off"  v-model="tag_term" placeholder="Add product tags" v-on:keyup="search()" v-on:keydown.enter.prevent="addTag()">

            <div class="linked-product-search-result">
                <ul>
                    <li v-for='(tag, index) in tags' v-if='tags.length' @click="addExistingTag(tag)">
                        @{{ tag }}
                    </li>

                    <li v-if='!tags.length && tag_term.length && !is_searching'>
                        No Results Found
                    </li>

                    <li v-if="is_searching && tag_term">
                        Searching...
                    </li>
                </ul>
            </div>

            <span class="filter-tag" style="text-transform: capitalize; margin-top: 10px; margin-right: 0px; height: auto;justify-content: flex-start; flex-wrap: wrap;">
                <span class="wrapper" style="margin-left: 0px; margin-right: 10px; margin-bottom : 10px;" v-for='(tag, index) in addedTags'>
                        @{{tag}}
                    <span class="icon cross-icon" @click="removeTag(tag)"></span>
                </span>
            </span>
        </div>
    </div>
</script>
<script>
    Vue.component('product-tags', {

        template: '#product-tags-template',

        data: function() {
            return {
                tags : [],

                addedTags : [],
                productTags : @json($product->getProductTags()->get()->pluck('tags')),

                productId: {{ $product->id }},

                tag : '',

                tag_term: '',

                is_searching : false,

                exists : false,

                
            
            }
        },

        created: function () {
            if (this.productTags.length >= 1) {
                for (var index in this.productTags) {
                    this.addedTags.push(this.productTags[index]);
                }
            }
        },

        methods: {
            addTag: function() {
                this.tag = this.tag_term;
                if(this.addedTags.length){
                    for(var index in this.addedTags)
                    {
                        if(this.addedTags[index] == this.tag_term){
                             this.exists = true;
                        }
                    }
                    if(this.exists == false){
                        this.addedTags.push(this.tag_term);
                        this.storeTag();
                    }
                }else{
                    this.addedTags.push(this.tag_term);
                    this.storeTag();
                }
                this.tag_term = '';
                this_this.tags = [];
                this.exists=false;
            },
            addExistingTag: function(tag) {
                this.tag = tag;
                this.addedTags.push(tag);
                this.storeTag();
                this.tag_term = '';
                this_this.tags =[];
            },
            removeTag: function(tag) {
                for (var index in this.addedTags) {
                    if (this.addedTags[index] == tag ) {
                        this.addedTags.splice(index, 1);
                        this.$http.get("{{route('memy.catalog.products.deleteTag')}}", {params: {'product_id' : this.productId , 'tag' : tag}})
                        .then(function(response){
                            // console.log(response);
                            // console.log('tag deleted');
                        })
                        .catch(function(error){
                            console.log(error);
                        })
                    }
                }
            },
            storeTag : function(){
                this_this=this;
                this.$http.post("{{route('memy.catalog.products.addTag')}}", {'product_id' : this.productId , 'tags' : this.tag})
                    .then(function(response){
                        // console.log('tag stored');
                    })
                    .catch(function(error){
                        console.log(error);
                    })
            },
            search: function () {
                this_this = this;

                this.is_searching = true;

                if (this.tag_term.length >= 1) {
                    this.$http.get ("{{ route('admin.catalog.products.searchForTag') }}", {params: {query: this.tag_term}})
                        .then (function(response) {
                            this_this.tags = [];
                            // console.log(response);

                            if(this_this.addedTags.length){
                                for(var tag in this_this.addedTags){
                                    for(var index in response.data){
                                        if(response.data[index].tag == this_this.addedTags[tag]){
                                            response.data.splice(index , 1);
                                        }
                                    }
                                   
                                }
                            }

                            for(var temp in response.data){
                                this_this.tags.push(response.data[temp].tag)
                            }

                            this_this.is_searching = false;
                        })

                        .catch (function (error) {
                            this_this.is_searching = false;
                        })
                } else {
                    this_this.tags = [];
                    this_this.is_searching = false;
                }
            }
        }
    });
</script>
@endpush