@extends('memy::layouts.master')

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
<?php $productBaseImage = $productImageHelper->getProductBaseImage($product); ?>

@push('meta')
    <!-- For Facebook -->
    <meta property="og:url" content="{{ urlencode(Request::fullUrl()) }}" />
    <meta property="og:title" content="{{ $product->name }}" />
    <meta property="og:image" content="{{ $productBaseImage['medium_image_url'] }}" />
    <meta property="og:description" content="{{ $product->short_description}}" />
    <meta property="og:type" content="article" />

    <!-- For Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="{{ $product->name }}" />
    <meta name="twitter:description" content="{{ $product->short_description}}" />
    <meta name="twitter:image" content="{{ $productBaseImage['medium_image_url'] }}" />

@endpush

@section('content')
    {{-- @include('memy::products.breadcrumbs') --}}

<section class="productdetail">
    <div class="container">
        <product-view>
            @csrf()
            <input type="hidden" name="product" value="{{ $product->product_id }}">
            <div class="row">
                <div class="col-md-7">
                    @include('memy::products.views.gallery')

                </div>
                <div class="col-md-5">
                    <div class="productdetail__container">
                        <article class="productdetail__type">
                            <h4>{{ $product->name }}</h4>
                        </article>
                        @include('memy::products.review')
                        @include('memy::products.views.price')

                        <div class="productdetail__number">
                            <label>Quantity</label>
                            <div class="numberbox">
                                <div class="button decrement" onclick="updateQunatity('remove')" readonly>-</div>
                                <input name="quantity" class="display" id="quantity" value="1" v-validate="'required|numeric|min_value:1|max_value:5'" data-vv-as="&quot;{{ __('shop::app.products.quantity') }}&quot;" readonly></input>
                                <div class="button increment" onclick=updateQunatity('add') readonly>+</div>
                            </div>
                        </div>


                        @if ($product->type == 'configurable')
                            <input type="hidden" value="true" name="is_configurable">
                        @else
                            <input type="hidden" value="false" name="is_configurable">
                        @endif

                        @include('memy::products.views.configurable-options')

                        @include('memy::products.views.product-add')

                        @include('memy::products.views.social-share')

                    </div>
                </div>

            </div>
            @include('memy::products.views.product-details')
            @include('memy::products.views.related-products')
            @include('memy::products.views.up-sells')
            {{-- @include('memy::products.views.cross-sells') --}}

        </product-view>
        @include('memy::products.views.reviews')
    </div>
</section>
@stop

@push('scripts')

<script type="text/x-template" id="product-view-template">
    <form method="POST" id="product-form" action="{{ route('cart.add', $product->product_id) }}" @click="onSubmit($event)">

        <slot></slot>

    </form>
</script>

<script>

    Vue.component('product-view', {

        template: '#product-view-template',

        inject: ['$validator'],

        methods: {
            onSubmit: function(e) {
                if (e.target.getAttribute('type') != 'submit')
                    return;

                e.preventDefault();

                this.$validator.validateAll().then(function (result) {
                    if (result) {
                        if (e.target.getAttribute('data-href')) {
                            var action = e.target.getAttribute('data-href');
                            // console.log(action);
                            document.getElementById("product-form").setAttribute('action',action);
                            document.getElementById('product-form').submit();
                        } else {
                        document.getElementById('product-form').submit();
                        }
                    }
                });
            }
        }
    });

    document.onreadystatechange = function () {
        var state = document.readyState
        var galleryTemplate = document.getElementById('product-gallery-template');
        var addTOButton = document.getElementsByClassName('add-to-buttons')[0];

        if (galleryTemplate) {
            if (state != 'interactive') {
                document.getElementById('loader').style.display="none";
                addTOButton.style.display="flex";
            }
        }
    }

    window.onload = function() {
        var thumbList = document.getElementsByClassName('thumb-list')[0];
        var thumbFrame = document.getElementsByClassName('thumb-frame');
        var productHeroImage = document.getElementsByClassName('product-hero-image')[0];

        if (thumbList && productHeroImage) {

            for(let i=0; i < thumbFrame.length ; i++) {
                thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
            }

            if (screen.width > 720) {
                thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                thumbList.style.height = productHeroImage.offsetHeight + "px";
            }
        }

        window.onresize = function() {
            if (thumbList && productHeroImage) {

                for(let i=0; i < thumbFrame.length; i++) {
                    thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                    thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                }

                if (screen.width > 720) {
                    thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.height = productHeroImage.offsetHeight + "px";
                }
            }
        }
    };

    function updateQunatity(operation) {
        var quantity = document.getElementById('quantity').value;

        if (operation == 'add') {
            if(quantity < 5)
                quantity = parseInt(quantity) + 1;
        } else if (operation == 'remove') {
            if (quantity > 1)
                quantity = parseInt(quantity) - 1;
        }
        document.getElementById("quantity").value = quantity;
        document.getElementById("buyNowQuantity").value = quantity;
        event.preventDefault();
    }
</script>
@endpush

