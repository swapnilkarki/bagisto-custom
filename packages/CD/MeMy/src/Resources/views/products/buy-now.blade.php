<button type="submit" data-href="{{ route('shop.product.buynow', $product->product_id)}}" class="btn btn-primary" {{ $product->type != 'configurable' && ! $product->haveSufficientQuantity(1) ? 'disabled' : '' }}>
    {{ __('shop::app.products.buy-now') }}
</button>