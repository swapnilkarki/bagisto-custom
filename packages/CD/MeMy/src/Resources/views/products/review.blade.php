@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')

@if ($total = $reviewHelper->getTotalReviews($product))
    <div class="rating">
        @for ($i = 1; $i <= round($reviewHelper->getAverageRating($product)); $i++)
            <ion-icon name="star" class="checked"></ion-icon>
        @endfor
        @for ($i = 1; $i <= 5-round($reviewHelper->getAverageRating($product)); $i++)
        <ion-icon name="star"></ion-icon>
        @endfor
        <span class="bestseller__rater">
            {{
                __('shop::app.products.total-rating', [
                        'total_rating' => $reviewHelper->getTotalRating($product),
                        'total_reviews' => $total,
                    ])
            }}
        </span>
    </div>
@endif
