<?php
$returnPolicy = $rma->findById($policies->return_policy);
$warrantyPolicy = $rma->findById($policies->warranty_policy);
$exchangePolicy = $rma->findById($policies->exchange_policy);
?>
<article>    
   @if($returnPolicy->validity !=0)
    <h6 style="border-bottom-width: 1px;border-bottom-style: solid;">Return Policy</h6>
    <span>Validity : {{$returnPolicy->validity}}&nbsp;days</span>
    <p>{{$returnPolicy->remarks}}</p>
    <hr>
   @endif
   @if($warrantyPolicy->validity !=0)
    <h6 style="border-bottom-width: 1px;border-bottom-style: solid;">Warranty Policy</h6>
    <span>Validity : {{$warrantyPolicy->validity}}&nbsp;days</span>
    <p>{{$warrantyPolicy->remarks}}</p>
    <hr>
   @endif
   @if($exchangePolicy->validity !=0)
    <h6 style="border-bottom-width: 1px;border-bottom-style: solid;">Exchange Policy</h6>
    <span>Validity : {{$exchangePolicy->validity}}&nbsp;days</span>
    <p>{{$exchangePolicy->remarks}}</p>
    <hr>
   @endif
</article>