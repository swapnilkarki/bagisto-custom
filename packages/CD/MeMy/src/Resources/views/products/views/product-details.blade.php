@inject('productPolicies','CD\Rma\Repositories\ProductPoliciesRepository')
@inject('rma','CD\Rma\Repositories\RmaRepository')

<?php
 $policies = $productPolicies->getPoliciesByProductId($product->product_id);
 $hasValidPolicies = $rma->hasValidPolicity($policies);
?>

<div class="productdetail__detail">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-controls="specification" aria-selected="false">Specification</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="delivery-tab" data-toggle="tab" href="#delivery" role="tab" aria-controls="delivery" aria-selected="false">Delivery</a>
        </li>
        @if($hasValidPolicies)
            <li class="nav-item">
                <a class="nav-link" id="policies-tab" data-toggle="tab" href="#policies" role="tab" aria-controls="policies" aria-selected="false">Policies</a>
            </li>
        @endif
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
            <article>
                   {!! $product->description !!}
            </article>
        </div>
        <div class="tab-pane fade" id="specification" role="tabpanel" aria-labelledby="specification-tab">
            @include('memy::products.views.attribute')
        </div>
        <div class="tab-pane fade" id="delivery" role="tabpanel" aria-labelledby="delivery-tab">
            <article>Somethig about delivery</article>
        </div>
        {{-- start polices tab --}}
        @if($hasValidPolicies)
            <div class="tab-pane fade" id="policies" role="tabpanel" aria-labelledby="policies-tab">
                @include('memy::products.views.policies')
            </div>
        @endif
        {{-- end policies tab --}}
    </div>
</div>