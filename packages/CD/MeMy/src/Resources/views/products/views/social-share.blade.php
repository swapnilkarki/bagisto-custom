<div class="productdetail__socialshare">
    <p>Share it on</p>
    <ul class="footer__social">
        <li>
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}" class="footer__social-icon" target="_blank">
                {{--<i class="fa fa-facebook" aria-hidden="true"></i>--}}
                <img src="{{ asset('vendor/webkul/memy/assets/images/facebook.svg')}}" class="img-fluid" alt="memykid">
            </a>
        </li>
        <li>
            <a href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}" class="footer__social-icon" target="_blank">
                {{--<i class="fa fa-twitter" aria-hidden="true"></i>--}}
                <img src="{{ asset('vendor/webkul/memy/assets/images/twitter.svg')}}" class="img-fluid" alt="memykid">
            </a>
        </li>
        {{-- <li>
            <a href="#" class="footer__social-icon" target="_blank">
                <img src="{{ asset('vendor/webkul/memy/assets/images/instagram.svg')}}" class="img-fluid" alt="memykid">
            </a>
        </li> --}}
    </ul>
</div>