<?php
    $productUpSells = $product->up_sells()->get();
?>

@if ($productUpSells->count())
    <div class="similar-products">
        <h3 class="section-heading">{{ __('shop::app.products.up-sell-title') }}</h3>

        <div class="row">

            @foreach ($productUpSells as $up_sell_product)
            <div class="product-box">
                @include ('memy::products.product-box', ['product' => $up_sell_product])
            </div>
            @endforeach

        </div>

    </div>
@endif