
@if ($product->type == 'simple')
<div {{! $product->haveSufficientQuantity(1) ? '' : 'active' }}>
        <span>{{ $product->haveSufficientQuantity(1) ? __('shop::app.products.in-stock') : __('shop::app.products.out-of-stock') }}</span>
</div>
@else
<div in-stock active id="in-stock" style="display: none;">
        <span>{{ __('shop::app.products.in-stock') }}</span>
</div>
@endif