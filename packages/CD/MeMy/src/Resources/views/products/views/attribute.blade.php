@inject ('productViewHelper', 'Webkul\Product\Helpers\View')

@if ($customAttributeValues = $productViewHelper->getAdditionalData($product))
<article>    
    <table class="full-specifications">
        @foreach ($customAttributeValues as $attribute)
            <tr>
                @if ($attribute['label'])
                    <td><strong>{{ $attribute['label'] }}</strong></td>
                @else
                    <td>{{ $attribute['admin_name'] }}</td>
                @endif
                    @if ($attribute['type'] == 'file' && $attribute['value'])
                        <td>
                            <a href="{{ Storage::url($attribute['value']) }}" target="_blank">
                                <i class="icon sort-down-icon download"></i>
                            </a>
                        </td>
                    @elseif ($attribute['type'] == 'image' && $attribute['value'])
                        <td>
                            <a href="{{ Storage::url($attribute['value']) }}" target="_blank">
                                <img src="{{ Storage::url($attribute['value']) }}" style="height: 20px; width: 20px;"/>
                            </a>
                        </td>
                    @else
                        <td>: {{ $attribute['value'] }}</td>
                    @endif
            </tr>

        @endforeach

    </table>
</article>
@endif