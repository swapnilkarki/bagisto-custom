{{-- 

@foreach ($cart->items as $item)
    <?php
        $product = $item->product;

        if ($product->cross_sells()->count()) {
            $products[] = $product;
            $products = array_unique($products);
        }
    ?>
@endforeach

@if (isset($products))

    <div class="similar-products">

        <h3 class="section-heading">{{ __('shop::app.products.cross-sell-title') }}</h3>

        <div class="row">
            @foreach($products as $product)

                @foreach ($product->cross_sells()->paginate(2) as $cross_sell_product)
                <div class="col">
                    @include ('memy::products.product-box', ['product' => $cross_sell_product])
                </div>
                @endforeach

            @endforeach

        </div>

    </div>

@endif --}}