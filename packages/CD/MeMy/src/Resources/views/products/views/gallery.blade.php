{{-- get image gallery --}}
<?php $images = $productImageHelper->getGalleryImages($product); ?>

<div class="product-main-img">
        <img class="zoom-img" id="zoom_01"
        src="{{ $productBaseImage['medium_image_url'] }}"
        data-zoom-image="{{ $productBaseImage['large_image_url'] }}"/>

        <div class="small-img">
          <img src="{{ asset('vendor/webkul/memy/assets/images/bluearrow.png')}}" class="icon-left" alt="" id="prev-img" onclick="changeImage('up')">
        <div class="small-container">
            <div id="small-img-roll">
                <?php $counter=1;?>
               @foreach($images as $image)
                <a id="imageId{{$counter}}" data-image="{{ $image['medium_image_url'] }}" data-zoom-image="{{ $image['large_image_url'] }}" onclick="changeCurrentValue({{$counter}})">
                    <img id="zoom_01" src="{{ $image['small_image_url'] }}" class="show-small-img" alt="">
                </a>
                <?php if($counter++ == 5) break;?>
                @endforeach
            </div>
        </div>
          <img src="{{ asset('vendor/webkul/memy/assets/images/bluearrow.png')}}" class="icon-right" alt="" id="next-img" onclick="changeImage('down')">
    </div>
</div>

{{-- <img id="zoom_01" src='images/large/image1.png' /> --}}

@push('css')
    <style>
        /*Change the colour*/
        .active img {
            border: 2px solid #333 !important;
        }
    </style>
@endpush
 
@push('scripts')
<script>
      $(document).ready(function() {
        // $('#zoom_01').ezPlus();

        //initiate the plugin and pass the id of the div containing gallery images
        $('#zoom_01').ezPlus({
            constrainType: 'height', constrainSize: 'auto', zoomType: 'lens',
            containLensZoom: true, gallery: 'small-img-roll', cursor: 'pointer', galleryActiveClass: 'active'
        });

        //pass the images to Fancybox
        $('#zoom_01').bind('click', function (e) {
            var ez = $('#zoom_01').data('ezPlus');
            $.fancyboxPlus(ez.getGalleryList());
            return false;
        });

      })

    var currentValue = 1;
    // var images = <?php echo json_encode($images) ?>;

    function changeCurrentValue(newValue){
        currentValue = newValue
    }

    function changeImage(value){
        if(value === 'up' && currentValue != 1){
            currentValue--;
            document.getElementById('imageId'+currentValue).click();
        }else if(value === 'down' && currentValue < 5){
            currentValue++;
            document.getElementById('imageId'+currentValue).click();
        }
    }

      
</script> 
@endpush
