<?php
    $relatedProducts = $product->related_products()->get();
?>

@if ($relatedProducts->count())
    <div class="similar-products">
        <h3 class="section-heading"> {{ __('shop::app.products.related-product-title') }}</h3>

        <div class="row">

            @foreach ($relatedProducts as $related_product)
            <div class="product-box">
                @include ('memy::products.product-box', ['product' => $related_product])
            </div>
            @endforeach

        </div>

    </div>
@endif