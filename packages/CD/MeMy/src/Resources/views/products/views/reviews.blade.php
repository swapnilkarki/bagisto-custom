@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')

<review productid='{{$product->product_id}}'></review>

@push('scripts')
<script type="text/x-template" id="review-template"> 
<div>  
    <div id="review-section" class="review" v-if="meta.total >= 1">
        <h4 class="section-heading">Reviews & Ratings</h4>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="review__log">
                            <p>   @{{meta.total}} Reviews</p>
                        </div>
    
                        <div class="review__reviewer" v-for="review in reviews">
                            <div class="bestseller__rating">
                                <ion-icon name="star" class="checked" v-for="index in parseInt(review.rating)" :key="'checked'+index"></ion-icon>
                                <ion-icon name="star" class=""v-for="index in 5-parseInt(review.rating)" :key="'unchecked'+index"></ion-icon>
                                <div class="review__date">
                                    <span>  @{{ review.created_at.date | dateOnly}}</span>
                                </div>
                            </div>
                            <article class="review__detail" style="padding-left: inherit;">
                                <span>@{{review.name}}</span>
                                <p> @{{review.comment}}</p>
                            </article>
                        </div>
    
                    </div>
                    <div class="col-md-12" >
                        <div class="review-footer">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#writereview">
                                Write your review
                            </button>
                            <div class="pagination-block text-center" v-if="(meta.last_page != 1)">
                                <nav aria-label="...">
                                    <ul class="pagination">
                                        <li :class="links.prev == null ? 'page-item disabled' : 'page-item'">
                                            <a class="page-link" href="javascript:void(0);" tabindex="-1" @click="paginate(links.prev)" rel="prev">Prev</a>
                                        </li>
                        
                                                <!-- Pagination Elements -->
                                        <span v-for="(n,index) in pages" :key="index">
                                            <li v-if="meta.current_page == n" class="page-item active"><a class="page-link" href="javascript:void(0);">@{{ n }}</a></li>
                                            <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(n)">@{{ n }}</a></li>
                                        </span>
                        
                                        <li :class="links.next == null ? 'page-item disabled' : 'page-item'">
                                            <a class="page-link" href="javascript:void(0);" @click="paginate(links.next)">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
    
                    {{-- @if (core()->getConfigData('catalog.products.review.guest_review') || auth()->guard('customer')->check()) --}}
                        @include('memy::products.reviews.create')
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </div>
    <div id="review-section" class="review" v-if="meta.total == 0">
        <h3 class="section-heading">Reviews & Ratings</h3>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="review__log">
                            <p>   {{ __('shop::app.products.total-reviews', ['total' => 0]) }}</p>
                        </div>
                        <article class="review__detail">
                                <p>No Reviews Yet</p>
                            </article>
                    </div>
                    <div class="col-md-12">
                            <div class="review-footer">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#writereview">
                                    Write your review
                                </button>
                            </div>
                        </div>        
                    {{-- @if (core()->getConfigData('catalog.products.review.guest_review') || auth()->guard('customer')->check()) --}}
                        @include('memy::products.reviews.create')
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </div>
</div>
</script>

<script>
     Vue.component('review', {
        template: '#review-template',

        props : ['productid'],

        data: function() {
            return {
                productId : this.productid,
                reviews : [],
                meta : [],
                links : [],
                pages : null
            }
        },

        created : function(){
            this.getProductReview();
        },

        methods : {
            getProductReview : function(){
                this_this = this;
                axios.get('/api/reviews?product_id='+this.productId+'&limit=5&status=approved&page=1')
                .then(function (response) {
                    // console.log(response.data);
                    this_this.reviews = response.data.data;
                    this_this.meta = response.data.meta;
                    this_this.links = response.data.links;
                    this_this.pages = parseInt(response.data.meta.last_page);
                })
                .catch(function (error) {
                    console.log(error);
                })

            },

            paginate : function(link){
                var self = this;
                
                this.reviews = [];

                // console.log(link);
                
                axios
                .get(link)
                .then(function (response) {
                    // handle success
                    this_this.reviews = response.data.data;
                    this_this.meta = response.data.meta;
                    this_this.links = response.data.links;
                    this_this.pages = parseInt(response.data.meta.last_page);
                                      
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })   
            },

            paginateNumber : function(n){
                // console.log(n);
                var link = this.links.first;
                var newLink = link.replace("page=1", "page="+n);
                this.paginate(newLink);
            }

        },

        filters: {
            dateOnly: function (value) {
               return value.substr(0,value.indexOf(' '));
            }
        }

    })
</script>

@endpush

