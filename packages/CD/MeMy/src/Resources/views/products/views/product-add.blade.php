     {{--CartController at shop package --}}
    <button type="submit" data-href="{{ route('shop.product.buynow', $product->product_id)}}" class="btn btn-lg btn-primary buynow" {{ $product->type != 'configurable' && ! $product->haveSufficientQuantity(1) ? 'disabled' : '' }}>
        {{ __('shop::app.products.buy-now') }}
    </button>

    <button type="submit" class="btn btn-secondary" {{ $product->type != 'configurable' && ! $product->haveSufficientQuantity(1) ? 'disabled' : '' }}>
        {{ __('shop::app.products.add-to-cart') }}
    </button>
   

<div class="productdetail__wishlist">
    @auth('customer')
        <a href="{{ route('customer.wishlist.add', $product->product_id) }}"><ion-icon name="heart-empty"></ion-icon> Add to wishlist</a>
    @endauth
    <a href="#review-section"><i class="fa fa-commenting-o" aria-hidden="true"></i>&nbsp; Reviews</a>
</div>