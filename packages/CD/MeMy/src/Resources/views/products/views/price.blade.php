@inject ('priceHelper', 'Webkul\Product\Helpers\Price')
<div class="productdetail__total">
    <article class="productdetail__price">
        @if ($product->type == 'configurable')

            <h4 style="color:#ba0065">{{ core()->currency($priceHelper->getMinimalPrice($product)) }}</h4>
            
            @include('memy::products.views.stock')

        @else
            @if ($priceHelper->haveSpecialPrice($product))

                <h4 style="color:#ba0065">{{ core()->currency($priceHelper->getSpecialPrice($product)) }}</h4>

                <strike>{{ core()->currency($product->price) }}</strike>

                @include('memy::products.views.stock')

            @else

                <h4 style="color:#ba0065">{{ core()->currency($product->price) }}</h4>

                @include('memy::products.views.stock')
            @endif

        @endif
    </article>
</div>