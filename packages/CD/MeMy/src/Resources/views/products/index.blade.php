@extends('memy::layouts.master')

@section('page_title')
    {{ $category->name }} | MeMyKids
@stop

@section('seo')
    <meta name="description" content="{{ $category->meta_description }}"/>
    <meta name="keywords" content="{{ $category->meta_keywords }}"/>
@stop

@guest('customer')
<?php $authUser = 'false'?>
@endguest
@auth('customer')
<?php $authUser = 'true'?>
@endauth

@section('content')
    {{-- @include('memy::products.breadcrumbs') --}}
<section class="category">
    <div class="container">
            @inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
        <div class="row">

            @if (in_array($category->display_mode, [null, 'products_only', 'products_and_description']))
                @include('memy::products.list.filter-navigation')
            @endif

            <div class="col-lg-9 col-md-12">
                @include('memy::products.list.toolbar')

                <product-list categoryid="{{$category->id}}"></product-list>

                 <!-- set progressbar -->
                <vue-progress-bar></vue-progress-bar>

            </div>
        </div>
    </div>

    <vue-modal name="loader-modal" classes="loader-modal" :clickToClose="false" :height="30" :width="30">
        <div class="text-center">
            <beat-loader :loading="true" color="#E3007B" size="25px"></beat-loader>
        </div>
    </vue-modal>

</section>
@stop


@push('scripts')

<script type="text/x-template" id="product-list-template">
    <div class="row">
        <div v-if="products.length  != 0" class="filter-products" v-for="(productFlat,index) in filteredProducts">
           <product-box :productFlat="productFlat" :authUser="{{$authUser}}" :key="index"></product-box>
        </div>

        {{--<h2>@{{msg1 }}</h2>--}}

        <h5 style="margin: 40px auto;">
            @{{ msg2}}
            {{--There are no product currently. Please visit again.--}}
        </h5>

        {{-- <div class="col-md-12">
            <nav aria-label="Page navigation" class="pagination-nav">
                <ul class="pagination">

                    <!-- Previous Page Link -->

                    <li v-if="links.prev == null " class="page-item disabled"><span>&laquo;</span></li>

                    <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginate(links.prev)" rel="prev">&laquo;Previous</a></li>


                    <!-- Pagination Elements -->
                    <span v-for="(n,index) in pages" :key="index">
                            <li v-if="meta.current_page == n" class="page-item active"><span>@{{ n }}</span></li>
                            <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(n)">@{{ n }}</a></li>
                    </span>

                    <!-- Next Page Link -->
                    <li v-if="links.next != null" class="page-item"><a class="page-link"  href="javascript:void(0);" @click="paginate(links.next)" rel="next">Next&raquo;</a></li>

                    <li v-else class="page-item disabled"><span>&raquo;</span></li>
                </ul>
            </nav>
        </div> --}}
        <div class="col-md-12" v-if="meta.last_page != 1">
            <div class="pagination-block text-center">
                <div class="container">
                    <nav aria-label="...">
                        <ul class="pagination">

                            <!-- prev button -->
                            <li :class="links.prev == null ? 'page-item disabled' : 'page-item'">
                                <a class="page-link" href="javascript:void(0);" tabindex="-1" @click="paginate(links.prev)" rel="prev">Prev</a>
                            </li>

                            <!-- first page button -->
                            <li v-if="meta.current_page == 1" class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                            <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(1)">1</a></li>

                            <!-- first ... -->
                            <li v-if="meta.current_page != 1 && meta.current_page - 2 > 1">...</li>

                            <!-- Pagination Elements -->
                            <li v-if="meta.current_page - 2 > 1" class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(meta.current_page - 2)">@{{ meta.current_page - 2 }}</a></li>
                            <li v-if="meta.current_page - 1 > 1" class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(meta.current_page - 1)">@{{ meta.current_page - 1 }}</a></li>

                            <li v-if="meta.current_page != 1 && meta.current_page != meta.last_page " class="page-item active"><a class="page-link" href="javascript:void(0);">@{{ meta.current_page }}</a></li>

                            <li v-if="meta.current_page + 1 < meta.last_page" class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(meta.current_page + 1)">@{{ meta.current_page + 1 }}</a></li>
                            <li v-if="meta.current_page + 2 < meta.last_page" class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(meta.current_page + 2)">@{{ meta.current_page + 2 }}</a></li>

                            <!-- end ... -->
                            <li v-if="meta.current_page != meta.last_page && meta.current_page + 2 < meta.last_page ">...</li>

                            <!-- last page button -->
                            <li v-if="meta.current_page == meta.last_page" class="page-item active"><a class="page-link" href="javascript:void(0);">@{{meta.last_page}}</a></li>
                            <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(meta.last_page)">@{{meta.last_page}}</a></li>

                            <!-- next button -->
                            <li :class="links.next == null ? 'page-item disabled' : 'page-item'">
                                <a class="page-link" href="javascript:void(0);" @click="paginate(links.next)">Next</a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</script>

<script>
    Vue.component('product-list', {
        template: '#product-list-template',

        props : ['categoryid'],

        data: function() {
            return {
                products : [],
                categoryId : this.categoryid,
                links : [],
                meta : [],
                filter: '',
                toolbarFilter: '',
                subCategoryId : null,
                pages : null,
                url : null,
                msg1 : null,
                msg2 : null,
                multipleSubCategoryId : []
            }
        },

        mounted(){
            this.$root.$on('filterEvent' , (filter) => {
               this.applyFilter(filter);
            });
            this.$root.$on('onSubCategoryAdded' , (id) => {
                // console.log('event catched: subcategory => ' + id);
                var self = this;

                if(this.multipleSubCategoryId.includes(id)){
                    this.multipleSubCategoryId.splice(this.multipleSubCategoryId.indexOf(id), 1 );
                    // console.log(this.multipleSubCategoryId);
                }else{
                    this.multipleSubCategoryId.push(id);
                    // console.log(this.multipleSubCategoryId);
                }

                var subCategories = '';

                if(this.multipleSubCategoryId.length == 0)
                    subCategories = this.categoryId;    

                this.multipleSubCategoryId.forEach(element => {
                    subCategories = element+','+ subCategories;
                });

                // console.log(subCategories);

                this.subCategoryId = id;

                this.products = [];

                // var newUrl = self.url + '?' +'category_id='+ id + this.filter + self.toolbarFilter;
                // history.pushState({
                //         id: 'Filter'
                //     }, 'Filter | MeMyKids', newUrl);

                // console.log('/api/products?subCategories_id='+subCategories + this.filter + self.toolbarFilter);

                this.$Progress.start()
                this.$modal.show('loader-modal');

                axios
                .get('/api/products?subCategories_id='+subCategories + this.filter + self.toolbarFilter)
                .then(function (response) {
                    // handle success
                    // console.log(response.data.data);
                    self.products = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    self.pages = parseInt(response.data.meta.last_page);

                    if(self.products.length == 0 ){
                        self.msg1 = "Whoops";
                        self.msg2 = "No Products!!";
                    }else{
                        self.msg1 = "";
                        self.msg2 = "";
                    }

                    self.$Progress.finish();
                    self.$modal.hide('loader-modal');
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail()
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })
            });
            this.$root.$on('toolbarEvent' , (toolbarFilter) => {
                // console.log('toolbar event catched  ' + toolbarFilter);

                this.toolbarFilter = toolbarFilter;

                var self = this;


                this.products = [];

                var newUrl = self.url + self.filter + toolbarFilter;
                // history.pushState({
                //         id: 'Filter'
                //     }, 'Filter | MeMyKids', newUrl);

                var subCategories = '';

                if(this.multipleSubCategoryId.length == 0)
                    subCategories = this.categoryId;    

                this.multipleSubCategoryId.forEach(element => {
                    subCategories = element+','+ subCategories;
                });

                // console.log('/api/products?subCategories_id='+subCategories + self.filter + toolbarFilter);

                this.$Progress.start()
                this.$modal.show('loader-modal');

                axios
                .get('/api/products?subCategories_id='+subCategories +  self.filter + toolbarFilter)
                .then(function (response) {
                    // handle success
                    self.products = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    self.pages = parseInt(response.data.meta.last_page);

                    if(self.products.length == 0 ){
                        self.msg1 = "Whoops";
                        self.msg2 = "No Products!!";
                    }else{
                        self.msg1 = "";
                        self.msg2 = "";
                    }

                    self.$Progress.finish();                        
                    self.$modal.hide('loader-modal');
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail();                        
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })
            });
        },

        created : function(){
            var currentUrl = window.location.href;
            var temp = currentUrl.split("?",2);

            if(temp[1] != null){
                var filter = '&'+temp[1];

                this.applyFilter(filter);

            }else{
                this.getData(this.categoryId);
            }
        },

        methods : {
            applyFilter : function(filter){

                // console.log('filter event catched  ' + filter + this.toolbarFilter);
                this.filter = filter;
                var self = this;

                this.products = [];

                // var newUrl = self.url + '?' + filter + this.toolbarFilter;
                // history.pushState({
                //         id: 'Filter'
                //     }, 'Filter | MeMyKids', newUrl);

                // console.log('/api/products?category_id='+this.categoryId + filter + this.toolbarFilter);

                this.$Progress.start();
                this.$modal.show('loader-modal');

                axios
                .get('/api/products?category_id='+this.categoryId + filter + this.toolbarFilter)
                .then(function (response) {
                    // handle success
                    self.products = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    self.pages = parseInt(response.data.meta.last_page);

                    if(self.products.length == 0 ){
                        self.msg1 = "Whoops";
                        self.msg2 = "No Products!!";
                    }else{
                        self.msg1 = "";
                        self.msg2 = "";
                    }

                    self.$Progress.finish()                        
                    self.$modal.hide('loader-modal');
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail()
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })

            },
            getData : function(categoryId){
                // console.log(categoryId);
                var self = this;
                self.url = window.location.href;
                this.products = [];
                this.$Progress.start()
                this.$modal.show('loader-modal');

                axios
                .get('/api/products?category_id='+categoryId)
                .then(function (response) {
                    // console.log(response)
                    // handle success
                    self.products = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    if(self.products.length == 0 ){
                            self.msg1 = "Whoops";
                            self.msg2 = "No Products!!";
                        }else{
                            self.msg1 = "";
                            self.msg2 = "";
                        }
                    self.pages = parseInt(response.data.meta.last_page);
                    self.$Progress.finish();
                    self.$modal.hide('loader-modal');
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail();
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })

            },

            paginate : function(link){
                var self = this;

                this.products = [];

                //to replace url without action
                // var temp = link;
                // var newUrl = temp.replace("/api", "/memykids");

                // history.pushState({
                //         id: 'Filter'
                //     }, 'Filter | MeMyKids', newUrl);

                this.$Progress.start();            
                this.$modal.show('loader-modal');
                // console.log(link);

                axios
                .get(link)
                .then(function (response) {
                    // handle success
                    // console.log(response);
                    self.products = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    self.pages = parseInt(response.data.meta.last_page);
                    if(self.products.length == 0 ){
                        self.msg1 = "Whoops";
                        self.msg2 = "No Products!!";
                    }else{
                        self.msg1 = "";
                        self.msg2 = "";
                    }

                    self.$Progress.finish();
                    self.$modal.hide('loader-modal');
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail();
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })
            },

            paginateNumber : function(n){
                // console.log(n);
                var link = this.links.first;
                var newLink = link.replace("page=1", "page="+n);
                this.paginate(newLink);
            }

        },

        computed : {
            filteredProducts(){
                return this.products.filter( (product) =>{
                    if(product.name != null)
                        return product;
                });
            }
        }

    });
</script>

@endpush