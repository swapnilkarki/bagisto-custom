{!! view_render_event('memy.products.box.before', ['product' => $product]) !!}
        @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
        @inject ('priceHelper', 'Webkul\Product\Helpers\Price')
        @inject ('reviewHelper', 'Webkul\Product\Helpers\Review')

        <?php $productBaseImage = $productImageHelper->getProductBaseImage($product); ?>

        <div class="product">

            @if($product->new)
                <div href="#" class="circle-shape">
                    <p> {{ __('shop::app.products.new') }}</p>
                </div>
            @endif
            @if ($priceHelper->haveSpecialPrice($product))
            <a class="circle-shape">
                    <p> {{$priceHelper->getDiscountPercent($product).'%' }} <br> <span>Off</span></p>
            </a>
            @endif
            @include('memy::products.wishlist')

            <figure class="product-image">
                <a href="{{ route('memy.products.gallery', $product->url_key) }}" title="{{ $product->name }}">
                    {{-- v-lazyload --}}
                    <v-lazy-image src="{{ $productBaseImage['medium_image_url'] }}" src-placeholder="{{ $productBaseImage['small_image_url'] }}" class="img-fluid" alt="{{ $product->name }}"/>
            </figure>
            <a href="{{ route('memy.products.gallery', $product->url_key) }}" title="{{ $product->name }}" class="product-ellipseshape">
                <div class="product-info">
                    {{-- <h5>Johnson's</h5> --}}
                    <h5>{{ $product->name }}</h5>
                    {{-- <p>{{ $product->name }}</p> --}}
                    @include('memy::products.price', ['product' => $product])

                    @if(!($product->haveSufficientQuantity(1)) & $product->type != "configurable")
                        <p>out of stock</p>
                    @endif

                    {{-- @include('memy::products.add-buttons', ['product' => $product]) --}}

                    {{--rating section start--}}
                    <br>

                 {{--rating section end--}}
                    <div class="rating-star display-none">
                        @for($i=1 ; $i<=round($reviewHelper->getAverageRating($product)) ; $i++ )
                        <i class="fa fa-star checked" aria-hidden="true"></i>
                        @endfor
                        @for($i=1 ; $i<=5-round($reviewHelper->getAverageRating($product)) ; $i++ )
                        <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                        <span class="bestseller__rater">({{$reviewHelper->getTotalReviews($product)}} reviews)</span>
                    </div>
                </div>
            </a>
        </div>
{!! view_render_event('memy.products.box.after', ['product' => $product]) !!}