<div class="row">
    <div class="col-md-12">
        <nav aria-label="Page navigation" class="pagination-nav">
            <ul class="pagination">

                <!-- Previous Page Link -->
                @if ($products->onFirstPage())
                    <li class="page-item disabled"><span>&laquo;</span></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $products->previousPageUrl() }}" rel="prev">&laquo;Previous</a></li>
                @endif

                <!-- Pagination Elements -->
                @for ($i = 1; $i <= $products->lastPage(); $i++)
                    @if($products->currentPage() == $i)
                        <li class="page-item active"><span>{{ $i }}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $products->url($i) }}">{{ $i }}</a></li>
                    @endif
                @endfor

                <!-- Next Page Link -->
                @if ($products->hasMorePages())
                    <li class="page-item"><a class="page-link" href="{{ $products->nextPageUrl() }}" rel="next">Next&raquo;</a></li>
                @else
                    <li class="page-item disabled"><span>&raquo;</span></li>
                @endif
            </ul>
        </nav>
    </div>
</div>