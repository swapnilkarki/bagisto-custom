@if ($product->type == "configurable")
    <form action="{{ route('memy.cart.add.configurable', $product->url_key) }}" method="GET">
    <button  class="btn btn-white">
        {{ __('shop::app.products.add-to-cart') }}
    </button>
    </form>
@else
    <cart-add-button id="{{$product->product_id}}" stock="{{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}"></cart-add-button>
    {{-- <button class="btn btn-white {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}" onclick="addToCart({{$product->product_id}})"> {{ __('shop::app.products.add-to-cart') }} </button> --}}
    {{-- <form action="{{ route('cart.add', $product->product_id) }}" method="POST">
        @csrf
        <input type="hidden" name="product" value="{{ $product->product_id }}">
        <input type="hidden" name="quantity" value="1">
        <input type="hidden" value="false" name="is_configurable">
        <button class="btn btn-white" {{ $product->haveSufficientQuantity(1) ? '' : 'disabled' }}>{{ __('shop::app.products.add-to-cart') }}</button>
    </form> --}}
            
@endif