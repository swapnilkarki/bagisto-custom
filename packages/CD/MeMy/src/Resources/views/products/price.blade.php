{!! view_render_event('memy.products.price.before', ['product' => $product]) !!}

    @inject ('priceHelper', 'Webkul\Product\Helpers\Price')

    @if ($product->type == 'configurable')
        <span>{{ __('shop::app.products.price-label') }}</span><br>

        <strong>{{ core()->currency($priceHelper->getMinimalPrice($product)) }}</strong>

    @else
        @if ($priceHelper->haveSpecialPrice($product))

            <strike>{{ core()->currency($product->price) }}</strike>

            <strong>{{ core()->currency($priceHelper->getSpecialPrice($product)) }}</strong>

        @else
            <strong>{{ core()->currency($product->price) }}</strong>
        @endif

    @endif


{!! view_render_event('memy.products.price.after', ['product' => $product]) !!}
