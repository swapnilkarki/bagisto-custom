<div class="modal fade review-modal review" id="writereview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Write your review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <!--component here-->
                   <review-form></review-form>
                </div>
            </div>
        </div>
    </div>


@push('scripts')

<script type="text/x-template" id="review-form-template">          
    <div class="review__form">
        <form method="POST"  @submit.prevent="submitReview" action="{{ route('memy.reviews.store', $product->product_id ) }}" id="reviewForm">
            @csrf
            <div class="rate">
                <p>Rate the product</p>
                <label for="star5" title="5 Stars" onclick="calculateRating(id)" id="5">5 stars</label>

                <label for="star4" title="4 Stars" onclick="calculateRating(id)" id="4">4 stars</label>

                <label for="star3" title="3 Stars" onclick="calculateRating(id)" id="3">3 stars</label>

                <label for="star2" title="2 Stars" onclick="calculateRating(id)" id="2">2 stars</label>

                <label for="star1" title="1 Star" onclick="calculateRating(id)" id="1">1 star</label>
            </div>

            {{-- <input type="hidden" id="rating" name="rating" v-validate="'required'"> --}}
            <input type="hidden" id="rating" name="rating">
            <div class="control-error" v-if="errors.has('rating')">@{{ errors.first('rating') }}</div>

            <div class="clearfix"></div>

            
            @if (! auth()->guard('customer')->user())
                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                    <label for="form-label" class="required">
                        {{ __('shop::app.reviews.name') }}
                    </label>
                    {{-- <input  type="text" class="form-control form-box" name="name" v-validate="'required'" value="{{ old('name') }}"> --}}
                    <input  type="text" class="form-control form-box" name="name" value="{{ old('name') }}">
                    <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                </div>
            @endif

            <div class="form-group" :class="[errors.has('comment') ? 'has-error' : '']">
                <label for="contact-textarea" class="required">
                    {{ __('admin::app.customers.reviews.comment') }}
                </label>
                {{-- <textarea type="text" class="form-control form-box" rows="2" name="comment" v-validate="'required'" value="{{ old('comment') }}"> --}}
                <textarea type="text" class="form-control form-box" rows="2" name="comment" value="{{ old('comment') }}">
                </textarea>
                <span class="control-error" v-if="errors.has('comment')">@{{ errors.first('comment') }}</span>
            </div>

            <button type="submit" class="btn btn-primary">Post</button>

            <vue-recaptcha  ref="invisibleRecaptcha" sitekey="{{config('app.captcha_site_key')}}" @verify="onVerify" @expired="onExpired" size="invisible">
            </vue-recaptcha>
        </form>
    </div>
</script>

<script>
Vue.component('review-form', {
        template: '#review-form-template',

        data: function() {
            return {
              
            }
        },

        created : function(){

        },

        methods : {
            submitReview : function(){
                this.$refs.invisibleRecaptcha.execute();
                // console.log("handle form submit");
            },
            onVerify : function(response){
                if(response){
                    document.getElementById('reviewForm').submit();
                }
                // console.log('Verify: ' + response)
            },
            onExpired : function(){
                // console.log("Expired");
            }

        }
    })
</script>

<script>
    function calculateRating(id) {
        var a = document.getElementById(id);
        document.getElementById("rating").value = id;

        for (let i=1 ; i <= 5 ; i++) {
            if (id >= i) {
                document.getElementById(i).style.color="#deb217";
            } else {
                document.getElementById(i).style.color="#d3cfbe";
            }
        }
    }
</script>

@endpush
