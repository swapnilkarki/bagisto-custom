@guest('customer')
    <div class="product-wishlist">
        <!--compnent here-->
        <wishlist-login-register></wishlist-login-register>
    </div>
@endguest
@auth('customer')
    <div class="product-wishlist">
        {{-- <a href="{{ route('customer.wishlist.add', $product->product_id) }}">
                <span class="add-to-wishlist"></span>
        </a> --}}
        <wishlist-add-button id="{{$product->product_id}}"></wishlist-add-button>
        {{-- <span class="add-to-wishlist" onclick="addToWishlist({{$product->product_id}})"></span> --}}
    </div>
@endauth
