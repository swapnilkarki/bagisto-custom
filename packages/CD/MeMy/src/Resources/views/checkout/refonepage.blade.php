@extends('memy::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.onepage.title') }}
@stop

@section('content')
<checkout></checkout>

@endsection

@push('scripts')

    <script type="text/x-template" id="checkout-template">
        <section class="shipping">
            <div class="container">
                <h5 class="cart--head">Shipping / Billing</h5>
                <div class="row">
                    @include('memy::checkout.onepage.partials.customer-info')
                </div>
            </div>
        </section>

        {{-- @include('memy::checkout.onepage.confirm') --}}
    </script>

    <script>
        var shippingHtml = '';
        var paymentHtml = '';
        var reviewHtml = '';
        var summaryHtml = Vue.compile(
            "<div class='order-summary'><h3>"
                + '{{ __('shop::app.checkout.total.order-summary') }}'  +
            "</h3><div class='item-detail'><label>"
                +  '{{ intval($cart->items_qty) }}  {{ __('shop::app.checkout.total.sub-total') }}  {{ __('shop::app.checkout.total.price') }}' +
            "</label><label class='right'>"
                + '{{ core()->currency($cart->base_sub_total) }}' +
            "</label></div><?php if($cart->base_tax_total): ?><div class='item-detail'><label>"
                + '{{ __('shop::app.checkout.total.tax') }}' +
            "</label><label class='right'>"
                + '{{ core()->currency($cart->base_tax_total) }}' +
            "</label></div><?php endif; ?><?php if($cart->selected_shipping_rate): ?><div class='item-detail'><label>"
                + '{{ __('shop::app.checkout.total.delivery-charges') }}' +
            "</label><label class='right'>"
                + '{{ core()->currency($cart->selected_shipping_rate->base_price) }}' +
            "</label></div><?php endif; ?><div class='payble-amount'><label>"
                    + '{{ __('shop::app.checkout.total.grand-total') }}' +
            "</label><label class='right'>"
                + '{{ core()->currency($cart->base_grand_total) }}' +
            "</label></div></div>"
        );
        var customerAddress = null;

        @auth('customer')
            @if(auth('customer')->user()->addresses)
                customerAddress = @json(auth('customer')->user()->addresses);
                customerAddress.email = "{{ auth('customer')->user()->email }}";
                customerAddress.first_name = "{{ auth('customer')->user()->first_name }}";
                customerAddress.last_name = "{{ auth('customer')->user()->last_name }}";
            @endif
        @endauth

        Vue.component('checkout', {
            template: '#checkout-template',
            inject: ['$validator'],

            data: function() {
                return {
                    currentStep: 1,
                    completedStep: 0,

                    currentAddress : {
                        id : '',
                        fullname:'',
                        email :'',
                        street : '',
                        city : '',
                        state : '',
                        phone : '',
                    },

                    address: {
                        billing: {
                            address1: [''],
                            last_name: '####',
                            postcode: '44600',
                            country: 'NP',
                            // address_id: '',
                            use_for_shipping: true,
                        },
                        shipping: {
                            address1: ['']
                        },
                    },
                    selected_shipping_method: '',
                    selected_payment_method: '',
                    disable_button: false,
                    new_shipping_address: false,
                    new_billing_address: false,

                    allAddress: {},


                    // countryStates: @json(core()->groupedStatesByCountries()),

                    // country: @json(core()->countries()),

                    selected: '',
                    
                }
            },

            created: function() {
                if(! customerAddress) {
                    this.new_shipping_address = true;
                    this.new_billing_address = true;
                } else {
                    if (customerAddress.length < 1) {
                        this.new_shipping_address = true;
                        this.new_billing_address = true;
                    } else {
                        this.allAddress = customerAddress;

                        for (var country in this.country) {
                            for (var code in this.allAddress) {
                                if (this.allAddress[code].country) {
                                    if (this.allAddress[code].country == this.country[country].code) {
                                        this.allAddress[code]['country'] = this.country[country].name;
                                    }
                                }
                            }
                        }
                    }
                }       
            },

            methods: {
                
                setCurrentAddress : function(){
                  if(this.selected =='newBillingAddress'){
                      this.newBillingAddress();
                  }
                  else{
                    this.currentAddress.street=this.selected.address1;     
                    this.currentAddress.city=this.selected.city;     
                    this.currentAddress.state=this.selected.state;     
                    this.currentAddress.phone=this.selected.phone;    
                    
                    Vue.set(this.address.billing,'address_id',this.selected.id);
                  

                    this.new_billing_address =false;
                  }
                },

                navigateToStep: function(step) {
                    if (step <= this.completedStep) {
                        this.currentStep = step
                        this.completedStep = step - 1;
                    }
                },

                haveStates: function(addressType) {
                    if (this.countryStates[this.address[addressType].country] && this.countryStates[this.address[addressType].country].length)
                        return true;

                    return false;
                },

                validateForm: function(scope) {
                    var this_this = this;

                    this.$validator.validateAll(scope).then(function (result) {
                        if (result) {
                            if (scope == 'address-form') {
                                this_this.saveAddress();
                            } else if (scope == 'shipping-form') {
                                this_this.saveShipping();
                            } else if (scope == 'payment-form') {
                                this_this.savePayment();
                            }
                        }
                    });
                },

                saveAddress: function() {
                    var this_this = this;
                    // console.log("save Address function start");

                    this.disable_button = true;

                    this.$http.post("{{ route('shop.checkout.save-address') }}", this.address)
                        .then(function(response) {
                            this_this.disable_button = false;

                            if (response.data.jump_to_section == 'shipping') {
                                // console.log(response.data.html);
                                shippingHtml = Vue.compile(response.data.html)
                                this_this.completedStep = 1;
                                this_this.currentStep = 2;
                            }
                        })
                        .catch(function (error) {
                            this_this.disable_button = false;

                            this_this.handleErrorResponse(error.response, 'address-form')
                        })
                    // console.log("save address function end")
                },

                saveShipping: function() {
                    var this_this = this;

                    this.disable_button = true;

                    this.$http.post("{{ route('shop.checkout.save-shipping') }}", {'shipping_method': this.selected_shipping_method})
                        .then(function(response) {
                            this_this.disable_button = false;

                            if (response.data.jump_to_section == 'payment') {
                                // console.log(response.data.html);
                                paymentHtml = Vue.compile(response.data.html)
                                this_this.completedStep = 2;
                                this_this.currentStep = 3;
                            }
                        })
                        .catch(function (error) {
                            this_this.disable_button = false;

                            this_this.handleErrorResponse(error.response, 'shipping-form')
                        })
                },

                savePayment: function() {
                    var this_this = this;
                    // console.log("save payment method start");
                    this.disable_button = true;

                    this.$http.post("{{ route('shop.checkout.save-payment') }}", {'payment': this.selected_payment_method})
                        .then(function(response) {
                            this_this.disable_button = false;

                            if (response.data.jump_to_section == 'review') {
                                // console.log(response.data.html);
                                reviewHtml = Vue.compile(response.data.html)
                                this_this.completedStep = 3;
                                this_this.currentStep = 4;
                            }
                        })
                        .catch(function (error) {
                            this_this.disable_button = false;

                            this_this.handleErrorResponse(error.response, 'payment-form')
                        })
                        // console.log("save payment method end");
                },

                placeOrder: function() {
                    var this_this = this;

                    this.disable_button = true;

                    this.$http.post("{{ route('shop.checkout.save-order') }}", {'_token': "{{ csrf_token() }}"})
                        .then(function(response) {
                            if (response.data.success) {
                                if (response.data.redirect_url) {
                                    window.location.href = response.data.redirect_url;
                                } else {
                                    window.location.href = "{{ route('memy.checkout.success') }}";
                                }
                            }
                        })
                        .catch(function (error) {
                            this_this.disable_button = true;

                            window.flashMessages = [{'type': 'alert-error', 'message': "{{ __('shop::app.common.error') }}" }];

                            this_this.$root.addFlashMessages()
                        })
                },

                handleErrorResponse: function(response, scope) {
                    if (response.status == 422) {
                        serverErrors = response.data.errors;
                        this.$root.addServerErrors(scope)
                    } else if (response.status == 403) {
                        if (response.data.redirect_url) {
                            window.location.href = response.data.redirect_url;
                        }
                    }
                },

                shippingMethodSelected: function(shippingMethod) {
                    this.selected_shipping_method = shippingMethod;
                },

                paymentMethodSelected: function(paymentMethod) {
                    this.selected_payment_method = paymentMethod;
                },

                newBillingAddress: function() {
                    this.new_billing_address = true;
                    this.address.billing.address_id=null;
                  
                },

                newShippingAddress: function() {
                    this.new_shipping_address = true;
                }
            }
        })

        var summaryTemplateRenderFns = [];
        Vue.component('summary-section', {
            inject: ['$validator'],

            data: function() {
                return {
                    templateRender: null
                }
            },

            staticRenderFns: summaryTemplateRenderFns,

            mounted: function() {
                this.templateRender = summaryHtml.render;

                for (var i in summaryHtml.staticRenderFns) {
                    summaryTemplateRenderFns.push(summaryHtml.staticRenderFns[i]);
                }
            },

            render: function(h) {
                return h('div', [
                    (this.templateRender ?
                        this.templateRender() :
                        '')
                    ]);
            }
        })

        var shippingTemplateRenderFns = [];
        Vue.component('shipping-section', {
            inject: ['$validator'],

            data: function() {
                return {
                    templateRender: null,
                    selected_shipping_method: '',
                }
            },

            staticRenderFns: shippingTemplateRenderFns,

            mounted: function() {
                this.templateRender = shippingHtml.render;
                for (var i in shippingHtml.staticRenderFns) {
                    shippingTemplateRenderFns.push(shippingHtml.staticRenderFns[i]);
                }

                // eventBus.$emit('after-checkout-shipping-section-added');
            },

            render: function(h) {
                return h('div', [
                    (this.templateRender ?
                        this.templateRender() :
                        '')
                    ]);
            },

            methods: {
                methodSelected: function() {
                    this.$emit('onShippingMethodSelected', this.selected_shipping_method)

                    // eventBus.$emit('after-shipping-method-selected');
                }
            }
        })

        var paymentTemplateRenderFns = [];
        Vue.component('payment-section', {
            inject: ['$validator'],

            data: function() {
                return {
                    templateRender: null,

                    payment: {
                        method: ""
                    },
                }
            },

            staticRenderFns: paymentTemplateRenderFns,

            mounted: function() {
                this.templateRender = paymentHtml.render;

                for (var i in paymentHtml.staticRenderFns) {
                    paymentTemplateRenderFns.push(paymentHtml.staticRenderFns[i]);
                }

                // eventBus.$emit('after-checkout-payment-section-added');
            },

            render: function(h) {
                return h('div', [
                    (this.templateRender ?
                        this.templateRender() :
                        '')
                    ]);
            },

            methods: {
                methodSelected: function() {
                    this.$emit('onPaymentMethodSelected', this.payment)

                    // eventBus.$emit('after-payment-method-selected');
                }
            }
        })

        var reviewTemplateRenderFns = [];
        Vue.component('review-section', {
            data: function() {
                return {
                    templateRender: null
                }
            },

            staticRenderFns: reviewTemplateRenderFns,

            mounted: function() {
                this.templateRender = reviewHtml.render;

                for (var i in reviewHtml.staticRenderFns) {
                    reviewTemplateRenderFns.push(reviewHtml.staticRenderFns[i]);
                }
            },

            render: function(h) {
                return h('div', [
                    (this.templateRender ?
                        this.templateRender() :
                        '')
                    ]);
            }
        })
    </script>

@endpush