@extends('memy::layouts.master')

@section('page_title')
{{ __('shop::app.checkout.cart.title') }}
@endsection

@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

@section('content')
    <cart></cart>
    <!-- set progressbar -->
    <vue-progress-bar></vue-progress-bar>

    <vue-modal name="loader-modal" classes="loader-modal" :clickToClose="false" :height="30" :width="30">
        <div class="text-center">
            <beat-loader :loading="true" color="#E3007B" size="25px"></beat-loader>
        </div>
    </vue-modal>

    @guest('customer')
    <?php $authUser = 'false'?>
    @endguest
    @auth('customer')
    <?php $authUser = 'true'?>
    @endauth

    
    <section class="product-wrapper">
            <product-section :auth-user="{{$authUser}}" section-type="featured-products" key="featured-products"></product-section>
            {{-- @include('memy::products.product-box',['product' => $productFlat]) --}}
    </section>

@endsection

@push('scripts')

<script type="text/x-template" id="cart-template">
    <div class="cart-flow">
        @if($cart)
        <div class="container">
            <form action="{{ route('memy.checkout.cart.update') }}" method="POST" ref="form" @submit="submit">
                <div class="row">
                    <div class="col-md-8">
                        <div class="item-selected">
                            <h5><i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp; @{{cartItemQuantity }} new item(s) have been added to your cart</h5>
                            @csrf
                            <h4>@{{message}}</h4>
                            <div class="item-selected-info" v-for="(item, index) in cartItems" :key="index">
                                <div class="serial-number"><p>@{{index+1}}</p></div>
                                <figure>
                                    <a v-bind:href="'{{ url()->to('/').'/memykids/products/'}}'+ item.product.url_key "><v-lazy-image :src="item.product.base_image.small_image_url" class="img-fluid" alt="memykid"/></a>
                                </figure>
                                <div class="item-selected-desc">
                                    <article>
                                        
                                        <a v-if="item.type == 'configurable'" v-bind:href="'{{ url()->to('/').'/memykids/products/'}}'+ item.product.url_key ">
                                            <div v-for="variant in item.product.variants">
                                                <h5 v-if="variant.id == item.additional.selected_configurable_option">@{{ variant.name }}</h5>
                                            </div>
                                        </a>
                                        <a v-else v-bind:href="'{{ url()->to('/').'/memykids/products/'}}'+ item.product.url_key "><h5>@{{ item.product.name }}</h5></a>

                                        <p v-html="item.product.short_description"></p>
                                        <strong>@{{item.formated_base_price}}</strong>

                                        <div class="productdetail__number">   
                                            <div class="numberbox">
                                                <input class="button decrement" value="-" style="width: 35px; padding: 0 12px; border-radius: 0px 3px 3px 0px;" @click="updateQunatity('remove', index, item.id)" readonly>

                                                <input type="text" class="display" id="'cart-quantity'+index" v-validate="'required|numeric|min_value:1|max_value:5'" :name="'qty['+item.id+']'" :value="item.quantity" data-vv-as="&quot;{{ __('shop::app.checkout.cart.quantity.quantity') }}&quot;" style="border-right: none; border-left: none; border-radius: 0px;" readonly>

                                                <input class="button increment" value="+" style="width: 35px; padding: 0 12px; border-radius: 0px 3px 3px 0px;" @click="updateQunatity('add', index,item.id)" readonly>
                                            </div>
                                        </div>

                                    </article>
                                </div>

                                <div class="col" style="font-size: 24px;color:#fd4364;">
                                    @auth('customer')
                                    <div v-if="item.parent_id != 'null' ||item.parent_id != null"><a @click="moveToWishlist(item.id,index)" data-toggle="tooltip" data-placement="top" title="{{ __('shop::app.checkout.cart.move-to-wishlist') }}"><ion-icon name="heart"></ion-icon></a></div>
                                    <div v-else><a class="text-red wishlist-btn" @click="moveToWishlit(item.id,index)" data-toggle="tooltip" data-placement="top"  title="{{ __('shop::app.checkout.cart.move-to-wishlist') }}"><ion-icon name="heart"></ion-icon></a></div>
                                    @endauth
                                </div>


                                 {{--wishlist add part--}}
                                 {{--@auth('customer')--}}
                                {{--<div v-if="item.parent_id != 'null' ||item.parent_id != null"><a @click="moveToWishlist(item.id,index)" title="{{ __('shop::app.checkout.cart.move-to-wishlist') }}"><ion-icon name="heart"></ion-icon></a></div>--}}
                                {{--<div v-else><a class="text-red wishlist-btn" @click="moveToWishlit(item.id,index)" title="{{ __('shop::app.checkout.cart.move-to-wishlist') }}"><ion-icon name="heart"></ion-icon></a></div>--}}
                                {{--@endauth--}}


                                <div class="col"><div class="close-btn"><a class="btn-red" v-on:click="removeItem(item.id,index)" data-toggle="tooltip" data-placement="top" title="{{ __('shop::app.checkout.cart.remove-link') }}"><ion-icon name="close"></ion-icon></a></div></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shopping-cart">
                            <h5>My Shopping Cart <span>(@{{cartItemQuantity }} item(s))</span></h5>
                            <div class="shopping-cart-billing">
                                <div class="shopping-bills subtotal">
                                    <p>Subtotal</p>
                                    <p>Rs. @{{cartTotal}}.00</p>
                                </div>
                                <div class="shopping-bills shipping-charge" v-if="cart.selected_shipping_rate">
                                    <p>Shipping fee</p>
                                    <p>@{{cart.selected_shipping_rate.formated_price}}</p>
                                </div>
                                <div class="shopping-bills total">
                                    <h5>Total</h5>
                                    <h5>Rs. @{{cartTotal}}.00</h5>
                                </div>
                                <div class="shopping-cart-actions">
                                    <a href="{{ route('memy.index') }}" class="btn btn-secondary">Continue Shopping</a>
                                    @if (! cart()->hasError())
                                    <a href="javascript:void(0)"  @click="submit()" class="btn btn-primary" >Checkout</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
        @else
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    No Items in Cart
                    <div class="col-md-4">
                        <div class="shopping-cart">
                            <div class="shopping-cart-actions">
                                <a href="{{ route('memy.index') }}" class="btn btn-secondary">Continue Shopping</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</script>

<script>
    Vue.component('cart', {
        template: '#cart-template',

        data: function() {
            return {
                cartItemQuantity : null,
                info : null,
                cartTotal : null,
                cartItems : [],
                cart : [],
                message : null,
                
            }
        },

        mounted(){
            this.getData();  
        },

        methods: {
            emitEvent : function (status){
                // console.log('emit event')
                this.$root.$emit('cartEvent', status);
            },

            getData : function(){
                // console.log('get cart items');
                var self =this;
                this.$Progress.start();
            axios.get('/api/checkout/cart')
                .then(function (response) {
                    // handle success
                    var data = response.data.data;
                    self.cartItems = data.items;
                    self.cart = data;
                    self.cartItemQuantity = parseInt(data.items_qty);
                    self.cartTotal = parseInt(data.grand_total);
                    self.$Progress.finish();
                    // console.log(response);
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail();
                    self.cartItemQuantity = 0;
                    self.cartTotal = 0;
                    self.message = "Your Cart Is Empty";
                    // console.log(error);
                })
               
            },
            updateQunatity : function(operation,index,itemId){
                // console.log(operation + ' on ' + itemId);
                // this.$Progress.start();
                var self = this;
                var temp = this.cartItems[index].quantity;
                var total = this.cartItems.base_grand_total;
                // console.log(this.cartItems[index].base_price);
                if(operation == 'add'){
                    if(temp < 5){
                        this.cartItems[index].quantity=temp+1;
                        this.cartItemQuantity ++;
                        this.cartItems.base_grand_total = parseInt(total) + parseInt(this.cartItems[index].base_price);
                        this.cartTotal = parseInt(this.cartTotal) + parseInt(this.cartItems[index].base_price);
                    }
                }
                else if(operation == 'remove'){
                    if(temp >1){
                        this.cartItems[index].quantity=temp-1;
                        this.cartItemQuantity --;
                        this.cartItems.base_grand_total = parseInt(total) - parseInt(this.cartItems[index].base_price);
                        this.cartTotal = parseInt(this.cartTotal) - parseInt(this.cartItems[index].base_price);
                    }                     
                }

                var data = {"qty":{ [itemId] : this.cartItems[index].quantity}};
                axios.put('/api/checkout/cart/update',data)
                .then(function (response) {
                    // self.$Progress.finish();
                })
                 .catch(function (error) {
                    // handle error
                    // self.$Progress.fail();
                    console.log(error);

                })

            },

            moveToWishlist : function(itemId,index){
                // console.log('move to wishlist'+ itemId)
                // console.log('/api/checkout/cart/move-to-wishlist/'+itemId)
                var self = this;
              
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, move it!'
                    }).then((result) => {
                      
                    if (result.value) {
                        this.$Progress.start();
                        this.$modal.show('loader-modal');
                        axios
                        .get('/api/checkout/cart/move-to-wishlist/'+itemId)
                        .then(function (response) {
                            // handle success
                            self.cartItems.splice(index, 1);
                            //update qty and total
                            self.getData();
                            // console.log('item'+itemId + 'moved to wishlist');
                            var status = "item moved to wishlist";
                            self.emitEvent(status);
                            Swal.fire(
                            'Moved!',
                            'Item Moved To Wishlist.',
                            'success'
                            )
                            self.$Progress.finish();
                            self.$modal.hide('loader-modal');
                        })
                        .catch(function (error) {
                            // handle error
                            self.$Progress.fail();
                            self.$modal.hide('loader-modal');
                            console.log(error);
                        })
                      
                    }
                    })
            },

            removeItem : function($id,index){
                // console.log('/api/checkout/cart/remove-item/'+$id);
                var self =this;
               
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, remove it!'
                    }).then((result) => {
                   
                    if (result.value) {
                        this.$Progress.start();
                        this.$modal.show('loader-modal');
                        axios
                        .get('/api/checkout/cart/remove-item/'+$id)
                        .then(function (response) {
                            // handle success
                            //update qty and total
                            self.cartItems.splice(index, 1);
                            self.getData();
                            var status = "item removed";
                            self.emitEvent(status);

                            Swal.fire(
                            'Removed!',
                            'Your Item has been removed.',
                            'success'
                            )
                            self.$Progress.finish();
                            self.$modal.hide('loader-modal');
                        })
                        .catch(function (error) {
                            // handle error
                            self.$Progress.fail();
                            self.$modal.hide('loader-modal');
                            console.log(error);
                        })
                    }
                    })
            },

            emptyCart : function(){
                var self = this;
               
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, remove all!'
                    }).then((result) => {
                      
                    if (result.value) {
                        this.$Progress.start();
                        axios
                            .get('/api/checkout/cart/empty')
                            .then(function (response) {
                            // handle success
                            //update qty and total
                            self.cartItems = [];
                            self.cart = [];
                            self.cartItemQuantity = 0;
                            self.cartTotal = 0;
                            self.message = "Your Cart Is Empty";
                            self.getData();
                            status = "cart emptied";
                            self.emitEvent(status);
                            Swal.fire(
                            'Emptied!',
                            'Your Cart  has been Emptied.',
                            'success'
                            )
                            self.$Progress.finish();
                            })
                            .catch(function (error) {
                                // handle error
                                self.$Progress.fail();
                                console.log(error);
                            })
                    }
                    })
            },

            submit : function(){
                // console.log("submit");
                if(this.cartItemQuantity == 0){
                    
                    Swal.fire(
                            'Empty Cart!',
                            'Your Cart is Empty.',
                            'Info'
                            )
                }
                else{
                    this.$refs.form.submit();
                }
            },
            
        },

        filters: {
            currency: function (value) {
                if (!value) return ''
                return 'Rs. '+parseInt(value)+'.00';
            }
        },

    })

</script>
@endpush