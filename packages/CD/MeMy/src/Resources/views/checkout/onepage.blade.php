@extends('memy::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.onepage.title') }}
@stop

@section('content')
<checkout></checkout>
<!-- set progressbar -->
<vue-progress-bar></vue-progress-bar>

@endsection

@push('scripts')

    <script type="text/x-template" id="checkout-template">
        <section class="shipping">
            <div class="container">
                <h5 class="cart--head">Shipping / Billing</h5>
                <div class="row">
                    @include('memy::checkout.onepage.partials.customer-info')
                </div>
            </div>
            <vue-modal name="loader-modal" classes="loader-modal" :clickToClose="false" :height="30" :width="30">
                <div class="text-center">
                    <beat-loader :loading="true" color="#E3007B" size="25px"></beat-loader>
                </div>
            </vue-modal>
        </section>
    </script>

    <script>
        var customerAddress = null;

        @guest('customer')
            isGuest = true;
            customerAddress = "null"
            customerAddress.first_name = "null";
            customerAddress.email = "null";
        @endguest

        @auth('customer')
            isGuest = false;
            @if(auth('customer')->user()->addresses)
                customerAddress = @json(auth('customer')->user()->addresses);
                
                var tempEmailHolder = "{{ auth('customer')->user()->email }}";
                if(tempEmailHolder.indexOf('|') > 0 )
                    customerAddress.email =  tempEmailHolder.slice(0, tempEmailHolder.indexOf('|'));
                else
                    customerAddress.email = tempEmailHolder;
                
                customerAddress.first_name = "{{ auth('customer')->user()->first_name }}";
                customerAddress.last_name = "{{ auth('customer')->user()->last_name }}";
            @endif
        @endauth

        Vue.component('checkout', {
            template: '#checkout-template',
            inject: ['$validator'],

            data: function() {
                return {
                    currentStep: 1,
                    completedStep: 0,

                    shippingRates : [],

                    paymentMethods : [],

                    currentAddress : {
                        id : '',
                        fullname:'',
                        email :'',
                        street : '',
                        city : '',
                        state : '',
                        phone : '',
                    },

                    address: {
                        billing: {
                            first_name : customerAddress.first_name,
                            email : customerAddress.email,
                            address1: [''],
                            city : '',
                            last_name: '####',
                            postcode: '44600',
                            country: 'NP',
                            lat : '',
                            lng: '',
                            formatted_address: '',
                            // address_id: '',
                            use_for_shipping: true,
                        },
                        shipping: {
                            address1: ['']
                        },
                    },
                    selected_shipping_method: 'free_free',
                    selected_payment_method: 'cashondelivery',
                    disable_button: false,
                    new_shipping_address: false,
                    new_billing_address: false,
                    
                    allAddress: {},

                    selected: '',

                    payment: {
                        method: "cashondelivery"
                    },
                    
                    rateGroup: {
                        method: 'free_free'
                    }
                }
            },

            created: function() {
                if(! customerAddress) {
                    this.new_shipping_address = true;
                    this.new_billing_address = true;
                } else {
                    if (customerAddress.length < 1) {
                        this.new_shipping_address = true;
                        this.new_billing_address = true;
                    } else {
                        this.allAddress = customerAddress;
                    }
                }  

                this.getShippingRates();     
                this.getPaymentMethods();     
            },

            mounted(){
                if(isGuest){
                    this.newBillingAddress();
                    this.$modal.show('login-modal', { requiredLogin: 'true'});
                }
                var self = this;
                this.$root.$on('addressChanged', (addressInfo,location) =>{
                    self.address.billing.address1[0] = '';
                    self.address.billing.lat = location.position.lat;
                    self.address.billing.lng = location.position.lng;
                    self.address.billing.formatted_address = location.infoText;
                    addressInfo.address_components.forEach(element =>{
                    if(element.types.includes("route")){
                        self.address.billing.address1[0] = self.address.billing.address1[0] +element.short_name;
                    }
                    else if(element.types.includes("sublocality")){
                        self.address.billing.address1[0] = self.address.billing.address1[0] + ', '+ element.short_name;
                    }
                    else if(element.types.includes("locality")){
                        self.address.billing.city= element.short_name;
                    }
                    else if(element.types.includes("postal_code")){
                        self.address.billing.postcode = element.short_name;
                    }
                });
                })
            },

            methods: {
                getPaymentMethods : function(){
                    this_this = this;
                    axios.get('/api/checkout/get-payment-methods')
                        .then(function(response) {
                            this_this.paymentMethods = response.data;
                        })
                        .catch(function (error) {
                            this_this.handleErrorResponse(error.response, 'payment-form')
                        })
                },

                getShippingRates : function(){
                    this_this = this;
                    axios.get('/api/checkout/get-shipping-rates')
                        .then(function(response) {
                            this_this.shippingRates = response.data;
                        })
                        .catch(function (error) {
                            this_this.handleErrorResponse(error.response, 'shipping-form')
                        })
                },
                
                setCurrentAddress : function(){
                  if(this.selected =='newBillingAddress'){
                      this.newBillingAddress();
                  }
                  else{
                    this.currentAddress.street=this.selected.address1;     
                    this.currentAddress.city=this.selected.city;     
                    this.currentAddress.state=this.selected.state;     
                    this.currentAddress.phone=this.selected.phone;    
                    
                    Vue.set(this.address.billing,'address_id',this.selected.id);
                  
                    this.$root.$emit("addressSelected", this.selected.id);

                    this.new_billing_address =false;
                  }
                },

                saveAddress: function() {
                    var this_this = this;
                    this.$Progress.start();
                    this.$modal.show('loader-modal');
                    this.$http.post("{{ route('shop.checkout.save-address') }}", this.address)
                    .then(function(response) {
                        // console.log('saved address');
                        this_this.saveShipping();
                    })
                    .catch(function (error) {
                        // console.log('save address' + error);
                        this_this.$Progress.fail();
                        this_this.$modal.hide('loader-modal');
                        this_this.handleErrorResponse(error.response, 'address-form')
                    })
                 
                },

                saveShipping: function() {
                    var this_this = this;
                    this.$http.post("{{ route('shop.checkout.save-shipping') }}", {'shipping_method': this.selected_shipping_method})
                    .then(function(response) {
                        // console.log('saved shipping');
                        // console.log(this_this.selected_payment_method);
                        this_this.savePayment();
                    })
                    .catch(function (error) {
                        // console.log('save shipping' + error);
                        this_this.$Progress.fail();
                        this_this.$modal.hide('loader-modal');
                        this_this.handleErrorResponse(error.response, 'shipping-form')
                    })
                },

                savePayment: function() {
                    var this_this = this
                    this.$http.post("{{ route('shop.checkout.save-payment') }}", {'payment': this.payment})
                    .then(function(response) {
                    // console.log('saved payment method');
                    this_this.$http.post("{{ route('shop.checkout.save-order') }}", {'_token': "{{ csrf_token() }}"})
                        .then(function(response) {
                            if (response.data.success) {
                                this_this.$Progress.finish();
                                this_this.$modal.hide('loader-modal');
                                if (response.data.redirect_url) {
                                    window.location.href = response.data.redirect_url;
                                } else {
                                    window.location.href = "{{ route('memy.checkout.success') }}";
                                }
                            }
                        })
                        .catch(function (error) {
                            // console.log('save order' + error);
                            window.flashMessages = [{'type': 'alert-error', 'message': "{{ __('shop::app.common.error') }}" }];
                            this_this.$Progress.fail();                              
                            this_this.$modal.hide('loader-modal');
                            this_this.$root.addFlashMessages()
                        })
                    })
                    .catch(function (error) {
                        // console.log('save payment' + error);
                        this_this.$Progress.fail();
                        this_this.$modal.hide('loader-modal');
                        this_this.handleErrorResponse(error.response, 'payment-form')
                    })            
                },

                placeOrder: function() {
                    this.saveAddress();
                    // calls saveaddress then save shipping and save payment then palces order
                },

                handleErrorResponse: function(response, scope) {
                    if (response.status == 422) {
                        serverErrors = response.data.errors;
                        this.$root.addServerErrors(scope)
                    } else if (response.status == 403) {
                        if (response.data.redirect_url) {
                            window.location.href = response.data.redirect_url;
                        }
                    }
                },

                shippingMethodSelected: function(shippingMethod,shippingPrice) {
                    this.selected_shipping_method = shippingMethod;
                    // console.log(shippingPrice);
                    this.$root.$emit('shippingMethodSelected', shippingPrice);
                },

                paymentMethodSelected: function(paymentMethod) {
                    this.selected_payment_method = paymentMethod;
                },

                newBillingAddress: function() {
                    this.new_billing_address = true;
                    this.address.billing.address_id=null;
                },

                newShippingAddress: function() {
                    this.new_shipping_address = true;
                }
            }
        })

      
    </script>

@endpush