<tr class="total">
        <th colspan="3">Quantity</th>
        <th colspan="2">@{{cartItemQuantity }}</th>
</tr>
@if ($cart->selected_shipping_rate)
<tr class="total" v-if="cart.selected_shipping_rate">
        <th colspan="3">{{ __('shop::app.checkout.total.delivery-charges') }}</th>
        <th colspan="2">@{{cart.selected_shipping_rate.formated_price}}</th>
</tr>
@endif
@if ($cart->base_tax_total > 0)
<tr class="total">
        <th colspan="3">{{ __('shop::app.checkout.total.tax') }}</th>
        <th colspan="2">@{{cart.formated_base_tax_total }}</th>
</tr>
@endif
<tr class="total">
        <th colspan="3">Grand Total</th>
        <th colspan="2">NPR @{{cartTotal}}.00</th>
</tr>
