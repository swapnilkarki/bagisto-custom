<h5 class="productlist--heading">Final Order Summary</h5>
<div class="table-responsive">
    <table class="table">
        <final-cart></final-cart>
    </table>
</div>



@push('scripts')

<script type="text/x-template" id="final-cart-template">
    <tbody>
        <tr v-for="(item, index) in cartItems" :key="index">
            <td>
                <figure class="productlist__image">
                    <v-lazy-image v-if="item.type == 'configurable' " :src="item.product.base_image.medium_image_url" src-placeholder="https://www.spinny.com/www/web/partials/base/images/loading-ring.gif" class="img-fluid" alt="memykids"/>

                    <v-lazy-image v-else :src="item.product.base_image.medium_image_url" src-placeholder="https://www.spinny.com/www/web/partials/base/images/loading-ring.gif" class="img-fluid" alt="memykids"/>

                    <figcaption class="productlist__number">@{{item.quantity}}</figcaption>
                </figure>
            </td>
            <td class="productlist__detail">
                    @{{ item.product.name }}
            </td>

            <td class="productdetail__amount">@{{item.base_price | currency}}</td>
  
        </tr>

        <tr>
            <td colspan="2">Qty</td>
            <td>@{{cart.items_qty}}</td>
        </tr>

        <tr>
            <td colspan="2">Sub Total</td>
            <td>@{{cart.sub_total | currency}}</td>
        </tr>

        <tr v-if="cart.base_tax_total > 0">
            <td colspan="2">Tax</td>
            <td>@{{cart.base_tax_total | currency}}</td>
        </tr>

        <tr v-if="cart.selected_shipping_rate">
            <td colspan="2">Shipping</td>
            <td>@{{ cart.selected_shipping_rate.base_price | currency}}</td>
        </tr>

        <tr class="prductlist__total">
            <td colspan="2">Total</td>
            <td>@{{ cart.base_grand_total | currency }}</td>
        </tr>
    </tbody>

</script>
<script>
    Vue.component('final-cart', {
        template: '#final-cart-template',

        data: function() {
            return {
                info : null,
                cartItems : [],
                cart : [],
                message : null,
                
            }
        },

        created : function(){
            // console.log('get cart items');
                var self =this;
            axios.get('/api/checkout/cart')
                .then(function (response) {
                    // handle success
                    var data = response.data.data;
                    self.cartItems = data.items;
                    self.cart = data;
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })  
        },

        methods: {
        },

        filters: {
            currency: function (value) {
                if (!value) return ''
                return 'NPR.'+parseInt(value)+'.00';
            }
        },

    })

</script>
@endpush
    