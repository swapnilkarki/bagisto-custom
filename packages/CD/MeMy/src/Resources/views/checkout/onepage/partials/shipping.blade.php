<div class="delivery-location">
    <form data-vv-scope="shipping-form">
        <h5 class="shipping--heading">Select Shipping Method</h5>
        <div class="form-check form-check-inline" v-for="rateGroup in shippingRates">
            <input v-validate="'required'" class="form-check-input" type="radio" :id="rateGroup.method" name="shipping_method" :value="rateGroup.method" v-model="selected_shipping_method" @change="shippingMethodSelected(rateGroup.method,rateGroup.base_price)">
            <label class="form-check-label" :for="rateGroup.method"> @{{ rateGroup.carrier_title }} (Rs. @{{ rateGroup.base_price }})</label>
        </div>
    </form>
</div>
