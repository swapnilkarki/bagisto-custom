@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

<div class="col-md-7">
<div class="shipping__bill" v-if="currentStep == 1">
    <h5 class="shipping--heading">Shipping Address</h5>
    <form data-vv-scope="address-form">
        @auth('customer')
            <div class="form-group" v-if="Object.keys(allAddress).length != 0">
                <select v-model="selected" v-on:change = "setCurrentAddress()" class="form-control">
                    <option disabled value="">Please select Address</option>
                    <option v-for='(addresses, index) in this.allAddress' :value='addresses' >
                        <span v-if="addresses.title != null "> @{{addresses.title}}</span>
                        <span v-else> @{{addresses.address1}}/ @{{addresses.city}}</span>
                    </option>
                    <option value='newBillingAddress' >New Address</option>
                </select>
            </div>
        @endauth

        <p v-if="this.new_billing_address">Please fill in the details<p>
      
        
        <div class="form-container" v-if="!this.new_billing_address">
            
             {{-- <span>Selected: @{{ selected }}</span> --}}
            <div class="shipping__block">
                {{-- <input type="radio" id="billing[address_id]" name="billing[address_id]" v-bind:value="currentAddress.id" v-model="address.billing.address_id" > --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" >
                            <label class="form-label" for="fname">Full Name</label>
                            <input type="text" class="form-control form-box" name="full_name" :value="allAddress.first_name" readonly>
                        </div>
                    </div>
                    {{--<div class="col-lg-6 col-md-12">--}}
                        {{----}}
                    {{--</div>--}}
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label" for="email">Email</label>
                            <input type="email" class="form-control form-box" name="email" :value="allAddress.email" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="form-label" for="mobile_number">Mobile Number</label>
                            <input type="text" class="form-control form-box" name="phone" :value="currentAddress.phone" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="state">State</label>
                            <input type="text" class="form-control form-box" name="state" :value="currentAddress.state" readonly>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="city">City</label>
                            <input type="text" class="form-control form-box" name="city" :value="currentAddress.city" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="street">Street Address</label>
                            <input type="text" class="form-control form-box"  name="street" :value="currentAddress.street" readonly>
                        </div>
                    </div>
                </div>
                <google-map address-id="notSet" custom-style="{width:570px;  height: 200px;}" show-auto-complete="false"></google-map>
            </div>
        </div>
        {{--new address start--}}
        <div class="form-container" v-if="this.new_billing_address">

            {{-- @guest('customer')
            <div class="col-6 col-md-4 offset-md-8">
                <a class="btn btn-primary" href="javascript:void(0)" @click="$modal.show('login-modal')">
                    {{ __('shop::app.checkout.onepage.sign-in') }}
                </a>
            </div>
            @endguest --}}

            {{-- <h4>New Shipping Address</h4> --}}

            <div class="shipping__block" id="new-address">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[first_name]') ? 'has-error' : '']">
                            <label class="form-label" for="billing[first_name]">Full Name</label>
                            <input type="text" v-validate="'required'" class="form-control form-box" id="billing[first_name]" name="billing[first_name]" v-model="address.billing.first_name" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.first-name') }}&quot;"/>
                            <span class="control-error" v-if="errors.has('address-form.billing[first_name]')">
                                @{{ errors.first('address-form.billing[first_name]') }}
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[email]') ? 'has-error' : '']">
                            <label class="form-label" for="billing[email]">Email</label>
                            <input type="email" v-validate="'required|email'" class="form-control form-box"  id="billing[email]" name="billing[email]" v-model="address.billing.email" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.email') }}&quot;"/>
                            <span class="control-error" v-if="errors.has('address-form.billing[email]')">
                                    @{{ errors.first('address-form.billing[email]') }}
                            </span>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[address1][]') ? 'has-error' : '']">
                            <label class="form-label" for="billing_address_0">Street Address</label>
                            <input type="text" v-validate="'required'" class="form-control form-box"  id="billing_address_0" name="billing[address1][]" v-model="address.billing.address1[0]" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.address1') }}&quot;"/>
                            <span class="control-error" v-if="errors.has('address-form.billing[address1][]')">
                                @{{ errors.first('address-form.billing[address1][]') }}
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[city]') ? 'has-error' : '']">
                            <label class="form-label" for="billing[city]">City</label>
                            <input type="text" v-validate="'required'" class="form-control form-box" id="billing[city]" name="billing[city]" v-model="address.billing.city" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.city') }}&quot;"/>
                            <span class="control-error" v-if="errors.has('address-form.billing[city]')">
                                @{{ errors.first('address-form.billing[city]') }}
                            </span>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[state]') ? 'has-error' : '']">
                            <label class="form-label" for="billing[state]">State</label>
                            <select v-validate="'required'" class="form-control form-box" id="billing[state]" name="billing[state]" v-model="address.billing.state" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.state') }}&quot;">
                                <option value="Province No. 1">Province No. 1</option>
                                <option value="Province No. 2">Province No. 2</option>
                                <option value="Bagmati">Bagmati</option>
                                <option value="Gandaki">Gandaki</option>
                                <option value="Province No. 5">Province No. 5</option>
                                <option value="Karnali">Karnali</option>
                                <option value="Sudurpashchim">Sudurpashchim</option>
                            </select>
                            {{-- <input type="text" v-validate="'required'" class="form-control form-box" id="billing[state]" name="billing[state]" v-model="address.billing.state" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.state') }}&quot;"/> --}}
                            <span class="control-error" v-if="errors.has('address-form.billing[state]')">
                                @{{ errors.first('address-form.billing[state]') }}
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="form-group" :class="[errors.has('address-form.billing[phone]') ? 'has-error' : '']">
                            <label class="form-label" for="billing[phone]">Mobile Number</label>
                            <input type="text" v-validate="'required|numeric|min:10|max:10'" class="form-control form-box" id="billing[phone]" name="billing[phone]" v-model="address.billing.phone" data-vv-as="&quot;{{ __('shop::app.checkout.onepage.phone') }}&quot;"/>
                            <span class="control-error" v-if="errors.has('address-form.billing[phone]')">
                                @{{ errors.first('address-form.billing[phone]') }}
                            </span>
                        </div>
                    </div>
                </div>

                <google-map address-id="notSet" custom-style="{width:570px;  height: 200px;}" show-auto-complete="true"></google-map>


                @auth('customer')
                <div class="control-group">
                    <span class="checkbox">
                        <input type="checkbox" id="billing[save_as_address]" name="billing[save_as_address]" v-model="address.billing.save_as_address"/>
                        <label class="checkbox-view" for="billing[save_as_address]"></label>
                        {{ __('shop::app.checkout.onepage.save_as_address') }}
                    </span>
                </div>
                @endauth

            </div>
        </div>
          {{--new address end--}}
    </form>
    {{-- shipping methods --}}
    @include('memy::checkout.onepage.partials.shipping')
    {{-- payment methods --}}
    @include('memy::checkout.onepage.partials.payment')

    <button type="button" class="btn btn-primary" @click="placeOrder()" :disabled="disable_button" id="checkout-place-order-button">
        Place Order
    </button>
</div>


{{--review section start--}}
{{-- <div> --}}
    {{-- <review-section></review-section> --}}
    {{-- @include('memy::checkout.onepage.partials.final-summary') --}}
    {{-- <button type="button" class="btn btn-primary" @click="placeOrder()" :disabled="disable_button" id="checkout-place-order-button">
        Place Order
    </button> --}}
{{-- </div> --}}
{{--review section end--}}
</div>
<div class="col-md-4 offset-md-1">
@include('memy::checkout.onepage.partials.summary')
</div>
