 <div class="payment-option">
    <form data-vv-scope="payment-form">
        <h5 class="shipping--heading">Select Payment Method</h5>
        <div class="form-check form-check-inline" v-for="paymentType in paymentMethods">
            <input v-validate="'required'" class="form-check-input" type="radio" :id="paymentType.method" :name="paymentType.method" :value="paymentType.method" v-model="payment.method" @change="paymentMethodSelected(payment.method)">
            <label class="form-check-label" :for="paymentType.method">@{{paymentType.method_title}}</label>
        </div>
    </form>
</div>