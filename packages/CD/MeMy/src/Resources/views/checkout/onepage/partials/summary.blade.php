@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

<onepage-cart></onepage-cart>


@push('scripts')

<script type="text/x-template" id="onepage-cart-template">
    <div class="productlist">
        <h5 class="productlist--heading" v-if="message === null">Order Summary</h5>
        @{{message}}
        <a href="{{ route('memy.index') }}" v-if="message != null" class="btn btn-secondary">Continue Shopping</a>
        <div class="productlist-desc" v-for="(item, index) in cartItems" :key="index">
            <div class="productlist__image">
                <img :src="item.product.base_image.medium_image_url" class="img-fluid v-lazy-image v-lazy-image-loaded" alt="memykids">
                {{-- <v-lazy-image :src="item.product.base_image.medium_image_url" :src-placeholder="item.product.base_image.medium_image_url" class="img-fluid" alt="memykids"/> --}}
                <figcaption class="productlist__number">@{{item.quantity}}</figcaption>
            </div>
            <div class="productlist-info">
                <article>
                    <div v-if="item.type == 'configurable'">
                        <div v-for="variant in item.product.variants">
                            <p v-if="variant.id == item.additional.selected_configurable_option">@{{ variant.name }}</p>
                        </div>
                    </div>
                    <div v-else>
                        <p> @{{ item.product.name }}</p>
                    </div>
                    
                    <p>@{{item.base_price | currency }}</p>
    
                    <div class="productdetail__number">
                        <div class="numberbox">
                            <div class="button decrement" @click="updateQunatity('remove', index, item.id)">-</div>
                            <input type="num" id="'cart-quantity'+index" class="display" v-validate="'required|numeric|min_value:1|max_value:5'" :name="'qty['+item.id+']'" :value="item.quantity" data-vv-as="&quot;{{ __('shop::app.checkout.cart.quantity.quantity') }}&quot;" readonly>
                            <div class="button increment" @click="updateQunatity('add', index,item.id)">+</div>
                        </div>
                    </div>
    
                </article>
            </div>
            <div class="productlist-remove">
                <a v-on:click="removeItem(item.id,index)" title="Remove"><i class="fa fa-times"></i></a>
            </div>
        </div>
      
        <div class="productlist-totaling" v-if="message === null">
            <div class="productlist-totaling-list">
                <p>Subtotal</p>
                <p>@{{cartTotal | currency }}</p>
            </div>
            <div class="productlist-totaling-list" v-if="shippingPrice === 0">
                <p>Shipping</p>
                <p>Rs. 0.00</p>
            </div>
            <div class="productlist-totaling-list" v-if="shippingPrice != 0">
                <p>Shipping</p>
                <p>@{{shippingPrice | currency}}</p>
            </div>
            <div class="productlist-totaling-list">
                <h5>Total</h5>
                <h5>@{{grandTotal | currency}}</h5>
            </div>
        </div>
    </div>
</script>
<script>
    Vue.component('onepage-cart', {
        template: '#onepage-cart-template',

        data: function() {
            return {
                cartItemQuantity : null,
                info : null,
                cartTotal : null,
                cartItems : [],
                cart : [],
                message : null,
                shippingPrice : 0,
                grandTotal : ''
                
            }
        },

        created : function(){
            this.getData();  
        },

        mounted(){
            var this_this = this;
            this.$root.$on('shippingMethodSelected' , (shippingPrice) => {
                // console.log('event catched  ' + shippingPrice);
                this_this.shippingPrice = shippingPrice;
                this_this.grandTotal = parseInt(this_this.cartTotal) + parseInt(shippingPrice);
                // this.getData();
            });
        },

        methods: {
            emitEvent : function (status){
                // console.log('emit event')
                this.$root.$emit('cartEvent', status);
            },

            getData : function(){
                // console.log('get cart items');
                var this_this =this;
                this.$Progress.start();
                axios.get('/api/checkout/cart')
                .then(function (response) {
                    // handle success
                    var data = response.data.data;
                    if(data === null){
                        this_this.cartItemQuantity = 0;
                        this_this.cartTotal = 0;
                        this_this.message = "Your Cart Is Empty";
                    }else{
                        this_this.cartItems = data.items;
                        this_this.cart = data;
                        this_this.cartItemQuantity = parseInt(data.items_qty);
                        // this_this.cartTotal = parseInt(data.grand_total);
                        this_this.cartTotal = parseInt(data.sub_total);
                        this_this.grandTotal = parseInt(data.sub_total) + parseInt(this_this.shippingPrice);
                    }
                    this_this.$Progress.finish();
                })
                .catch(function (error) {
                    // handle error
                    this_this.$Progress.fail();
                })
               
            },
            updateQunatity : function(operation,index,itemId){
                // console.log(operation + ' on ' + itemId);
                this.$Progress.start();
                this.$modal.show('loader-modal');
                var this_this = this;

                var temp = this.cartItems[index].quantity;
                var total = this.cartItems.base_grand_total;
                // console.log(this.cartItems[index].base_price);
                if(operation == 'add'){
                    if(temp < 5){
                        this.cartItems[index].quantity=temp+1;
                        this.cartItemQuantity ++;
                        this.cartItems.base_grand_total = parseInt(total) + parseInt(this.cartItems[index].base_price);
                        this.cartTotal = parseInt(this.cartTotal) + parseInt(this.cartItems[index].base_price);
                        this.grandTotal = parseInt(this.cartTotal) + parseInt(this.shippingPrice);
                    }
                }
                else if(operation == 'remove'){
                    if(temp >1){
                        this.cartItems[index].quantity=temp-1;
                        this.cartItemQuantity --;
                        this.cartItems.base_grand_total = parseInt(total) - parseInt(this.cartItems[index].base_price);
                        this.cartTotal = parseInt(this.cartTotal) - parseInt(this.cartItems[index].base_price);
                        this.grandTotal = parseInt(this.cartTotal) + parseInt(this.shippingPrice);
                    }                     
                }

                var data = {"qty":{ [itemId] : this.cartItems[index].quantity}};
                axios.put('/api/checkout/cart/update',data)
                .then(function (response) {
                    this_this.$modal.hide('loader-modal');
                    this_this.$Progress.finish();
                })
                 .catch(function (error) {
                    // handle error
                    this_this.$Progress.fail();
                    this_this.$modal.hide('loader-modal');
                    console.log(error);

                })

            },

            moveToWishlist : function(itemId,index){
                // console.log('move to wishlist'+ itemId)
                // console.log('/api/checkout/cart/move-to-wishlist/'+itemId)
                this.$Progress.start();
                var this_this = this;
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, move it!'
                    }).then((result) => {
                    if (result.value) {
                        axios
                        .get('/api/checkout/cart/move-to-wishlist/'+itemId)
                        .then(function (response) {
                            // handle success
                            this_this.cartItems.splice(index, 1);
                            //update qty and total
                            this_this.getData();
                            // console.log('item'+itemId + 'moved to wishlist');
                            var status = "item moved to wishlist";
                            this_this.emitEvent(status);
                            Swal.fire(
                            'Moved!',
                            'Item Moved To Wishlist.',
                            'success'
                            )
                            this_this.$Progress.finish();

                        })
                        .catch(function (error) {
                            // handle error
                        this_this.$Progress.fail();

                            console.log(error);
                        })
                    }
                    else{
                        this_this.$Progress.fail();
                    }
                    })
            },

            removeItem : function($id,index){
                // console.log('/api/checkout/cart/remove-item/'+$id);
                var this_this =this;
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, remove it!'
                    }).then((result) => {
                    if (result.value) {
                        this_this.$Progress.start();
                        this_this.$modal.show('loader-modal');
                        axios
                        .get('/api/checkout/cart/remove-item/'+$id)
                        .then(function (response) {
                            // handle success
                            //update qty and total
                            this_this.cartItems.splice(index, 1);
                            this_this.getData();
                            var status = "item removed";
                            this_this.emitEvent(status);
                            Swal.fire(
                            'Removed!',
                            'Your Item has been removed.',
                            'success'
                            )
                            this_this.$Progress.finish();
                            this_this.$modal.hide('loader-modal');
                        })
                        .catch(function (error) {
                            // handle error
                            this_this.$Progress.fail();
                            this_this.$modal.hide('loader-modal');
                            console.log(error);
                        })
                    }else{
                        this_this.$Progress.fail();
                    }
                    })
            },

            emptyCart : function(){
                var this_this = this;
                Swal.fire({
                    title: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, remove all!'
                    }).then((result) => {
                    if (result.value) {
                        this_this.$Progress.start();
                        axios
                            .get('/api/checkout/cart/empty')
                            .then(function (response) {
                            // handle success
                            //update qty and total
                            this_this.cartItems = [];
                            this_this.cart = [];
                            this_this.cartItemQuantity = 0;
                            this_this.cartTotal = 0;
                            this_this.message = "Your Cart Is Empty";
                            this_this.getData();
                            status = "cart emptied";
                            this_this.emitEvent(status);
                            Swal.fire(
                            'Emptied!',
                            'Your Cart  has been Emptied.',
                            'success'
                            )
                            this_this.$Progress.finish();
    
                            })
                            .catch(function (error) {
                                // handle error
                                this_this.$Progress.fail();

                                console.log(error);
                            })
                    }
                    else{
                        this_this.$Progress.fail();
                    }
                    })
            },

            submit : function(){
                // console.log("submit");
                this.$refs.form.submit();
            },
            
        },

        filters: {
            currency: function (value) {
                if (!value) return ''
                return 'Rs. '+parseInt(value)+'.00';
            }
        },

    })

</script>
@endpush