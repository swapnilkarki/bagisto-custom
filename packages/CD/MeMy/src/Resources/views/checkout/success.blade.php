@extends('memy::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.success.title') }}
@stop

@section('content')
<section class="thankyou">
    <div class="container">
        <div class="thankyou__block">
            <article class="thankyou__textbox">
                <ion-icon name="checkmark"></ion-icon>
                <h4>{{ __('shop::app.checkout.success.thanks') }}</h4>
                <p>{{ __('shop::app.checkout.success.order-id-info', ['order_id' => $order->id]) }}</p>
                <p>{{ __('shop::app.checkout.success.info') }}</p>

                <p>For any queries, Call us: 9841654321 or <br>Email us: info@memykids.com</p>
                <a href="{{ route('memy.index') }}" class="btn btn-primary"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                    {{ __('shop::app.checkout.cart.continue-shopping') }}</a>
            </article>
        </div>
    </div>
</section>
@endsection
