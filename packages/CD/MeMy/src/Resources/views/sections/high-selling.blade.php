<section class="product-wrapper high-selling">
    <product-section :auth-user="{{$authUser}}" section-type="high-selling-products" key="high-selling-products"></product-section>
    {{-- <!-- set progressbar -->
    <vue-progress-bar></vue-progress-bar> --}}
</section>
