@if (app('Webkul\Product\Repositories\ProductRepository')->getNewProducts()->count())
<section class="product-wrapper">
    <product-section :auth-user="{{$authUser}}" section-type="new-products" key="new-products"></product-section>
    {{-- <!-- set progressbar -->
    <vue-progress-bar></vue-progress-bar> --}}
</section>
@endif