<section class="brands">
        <div class="container">
            <h3 class="section-heading">Choose Your Brand</h3>
            <div class="brand-slider">
                @inject('category','Webkul\Category\Repositories\CategoryRepository')
                <?php
                    $brands = [];

                    foreach ($category->getSubCategoryTree($category->getBrandId()) as $brand){
                        if ($brand->slug)
                            array_push($brands, $brand);
                    }
                    // dd($brands);
                ?>
                @if(count($brands))
                    @foreach ($brands as $key => $brand)
                        <div class="brand-item">
                            <figure class="brand-logo">
                            <a href="memykids/categories/{{$brand->slug}}">
                                <v-lazy-image src="{{ asset('storage/'.$brand->image)}}" class="img-fluid" alt="{{$brand->slug}}"/>
                                    {{-- <img src="" class="img-fluid" alt="memykid"> --}}
                                </a>
                            </figure>
                        </div>
                    @endforeach
                  
                @endif
              
            </div>
        </div>
    </section>