@inject('attribtue','Webkul\Attribute\Repositories\AttributeOptionRepository')
<section class="age-group">
        <div class="container">
            <div class="icon-slider">
                <div class="icon-item">
                    <a href="memykids/categories/mother">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/mom.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/mom.svg') }}"  class="img-fluid" alt="Mom"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/mother" class="icon-title">
                        Mom
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('0-3 months');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/newborn.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/newborn.svg') }}"  class="img-fluid" alt="new born"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        New Born
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('3-6 months');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby2.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby2.svg') }}"  class="img-fluid" alt="3mths-6mths"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        3<sub>Mths</sub> - 6<sub>Mths</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('6-12 months');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby3.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby3.svg') }}"  class="img-fluid" alt="6mnths-1yr"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        6<sub>Mths</sub> - 1<sub>Yr</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('1-2 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby4.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby4.svg') }}"  class="img-fluid" alt="1yr-2yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        1<sub>Yr</sub> - 2<sub>Yrs</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('2-4 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby5.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby5.svg') }}"  class="img-fluid" alt="2yrs-4yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        2<sub>Yrs</sub> - 4<sub>Yrs</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('4-6 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby6.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby6.svg') }}"  class="img-fluid" alt="4yrs-6yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        4<sub>Yrs</sub> - 6<sub>Yrs</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('6-8 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby7.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby7.svg') }}"  class="img-fluid" alt="6yrs-8yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        6<sub>Yrs</sub> - 8<sub>Yrs</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('8-10 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby8.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby8.svg') }}"  class="img-fluid" alt="8yrs-10yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        8<sub>Yrs</sub> - 10<sub>Yrs</sub>
                    </a>
                </div>
                <div class="icon-item">
                        <?php
                        $ageCode = $attribtue->getAttributeCode('10-12 yrs');
                        ?>
                    <a href="memykids/categories/baby/?age={{$ageCode}}">
                        <div class="circle">
                            <figure class="circle-icon">
                                {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/svg/baby9.svg') }}" class="img-responsive" alt="memykid"> --}}
                                <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/svg/baby9.svg') }}"  class="img-fluid" alt="10yrs-12yrs"/>
                            </figure>
                        </div>
                    </a>
                    <a href="memykids/categories/baby/?age={{$ageCode}}" class="icon-title">
                        10<sub>Yrs</sub> - 12<sub>Yrs</sub>
                    </a>
                </div>
    
            </div>
        </div>
    </section>