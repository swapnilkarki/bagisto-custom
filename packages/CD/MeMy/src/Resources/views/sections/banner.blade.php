<section class="banner">
        <div class="banner-slider">
            @inject ('sliderRepository', 'Webkul\Core\Repositories\SliderRepository')
           <?php $currentChannel = core()->getCurrentChannel();
           $sliderData = $sliderRepository->findByField('channel_id', $currentChannel->id)->toArray();
           ?>
           @foreach($sliderData as $slider)
           {{-- {{dd($slider['path'])}} --}}
            <div class="banner-item">
                <figure class="banner-image">
                    <a href="#">
                    <v-lazy-image src="{{ asset('storage/'.$slider['path']) }}"  class="img-fluid" alt="{{$slider['title']}}"/>
                        {{-- <img src="{{ asset('storage/'.$slider['path']) }}" class="img-fluid" alt="memykid"> --}}
                    </a>
                </figure>
                <div class="banner-caption">
                    <article>
                        <div class="circle-red">Sale</div>
                        <h1>{{$slider['title']}}</h1>
                        {!!$slider['content']!!}
                    </article>
                </div>
            </div>
            @endforeach
        </div>
</section>

