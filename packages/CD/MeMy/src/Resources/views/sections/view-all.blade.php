@inject('attribtue','Webkul\Attribute\Repositories\AttributeOptionRepository')
<section class="offers">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="bg-image">
                        <figure>
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/offerbaby.jpg')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/offerbaby.jpg')}}"  class="img-fluid" alt="For Boy"/>
                        </figure>
                        <div class="text-content" style="left:29%">
                            <h3>For Boy</span></h3>
                              {{-- 26 and 27 category id hard codded                 --}}
                              <?php
                              $forCode = $attribtue->getAttributeCode('for-boy');
                              ?>
                            <a href="memykids/categories/baby?for={{$forCode}}" class="btn btn-secondary">Shop Now &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="bg-image">
                        <figure>
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/baby_girl.jpg')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/baby_girl.jpg')}}"  class="img-fluid" alt="For Girl"/>
                        </figure>
                        <div class="text-content text-contentblue" style="left:71%">
                            <h3>For Girl</h3>
                            <?php
                            $forCode = $attribtue->getAttributeCode('for-girl');
                            ?>
                            <a href="memykids/categories/baby?for={{$forCode}}" class="btn btn-secondary">Shop Now &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>