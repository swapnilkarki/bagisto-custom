<section class="offers">
        <div class="container">
            <h3 class="section-heading">Discounts & Offers</h3>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="bg-image">
                        <figure>
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/offerbaby.jpg')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/offerbaby.jpg')}}"  class="img-fluid" alt="memykid"/>
                        </figure>
                        <div class="text-content">
                            <h3>20% <span>Off <br> Baby Nutrients</span></h3>
                            <a href="{{route('memy.sale')}}" class="btn btn-secondary">Shop Now &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="bg-image">
                        <figure>
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/offeritems.png')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/offeritems.png')}}"  class="img-fluid" alt="memykid"/>
                        </figure>
                        <div class="text-content text-contentblue">
                            <h3>30% <span>Off <br> Baby Accessories</span></h3>
                            <a href="{{route('memy.sale')}}" class="btn btn-secondary">Shop Now &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="more-offers">
                    <a href="#" class="btn btn-outline">View all Discount & Offers &nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>