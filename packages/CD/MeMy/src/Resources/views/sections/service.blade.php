<section class="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="icons-block">
                        <figure class="icon-img">
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/quality.png')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/quality.png')}}"  class="img-fluid" alt="Highest Quality Gurantee"/>
                        </figure>
                        <figcaption>Highest Quality Gurantee</figcaption>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="icons-block">
                        <figure class="icon-img">
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/shipped.png')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/shipped.png')}}"  class="img-fluid" alt="Fast Delivery System"/>
                        </figure>
                        <figcaption>Fast Delivery System</figcaption>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="icons-block">
                        <figure class="icon-img">
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/service.png')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/service.png')}}"  class="img-fluid" alt="Best Customer Care"/>
                        </figure>
                        <figcaption>Best Customer Care</figcaption>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="icons-block">
                        <figure class="icon-img">
                            {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/track.png')}}" class="img-fluid" alt="memykid"> --}}
                            <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/track.png')}}"  class="img-fluid" alt="Track Your Order"/>
                        </figure>
                        <figcaption>Track Your Order</figcaption>
                    </div>
                </div>
            </div>
        </div>
    </section>