@inject('testimonialRepository','CD\Manager\Repositories\TestimonialRepository')
<?php
    $testimonials = $testimonialRepository->getApprovedTestimonails()
?>
<section class="testimonial">
    <div class="container">
        <div class="testimonial-slider">
            @foreach($testimonials as $testimonial)
            <div class="testimonial-item">
                <div class="quote">
                    {{-- <ion-icon name="quote"></ion-icon> --}}
                    <i class="fa fa-quote-right hydrated"></i>
                </div>
                <p>
                    {{$testimonial->statement}}
                </p>
                
                <div class="rating">
                    @for ($i = 1; $i <= $testimonial->rating ; $i++)
                        {{-- <ion-icon name="star" class="checked"></ion-icon> --}}
                        <i class="fa fa-star checked"></i>
                    @endfor
                    @for ($i = 1; $i <= 5-$testimonial->rating; $i++)
                        {{-- <ion-icon name="star"></ion-icon> --}}
                        <i class="fa fa-star"></i>
                    @endfor
                </div>
                <article>
                    <h5>{{$testimonial->full_name}}</h5>
                    <p>{{$testimonial->created_at}}</p>
                </article>
            </div>
            @endforeach
        </div>
    </div>
</section>