@extends('memy::layouts.master')

@section('page_title')
   Sale | MeMyKids
@endsection

@guest('customer')
<?php $authUser = 'false'?>
@endguest
@auth('customer')
<?php $authUser = 'true'?>
@endauth

@section('content')
<section class="category searched-results">
    <div class="container">
            <div class="row">
                @include('memy::search.partials.filter')
                <product-results></product-results>
                <!-- set progressbar -->
                <vue-progress-bar></vue-progress-bar>

            </div>
    </div>
</section>
@endsection


@push('scripts')
<script type="text/x-template" id="product-results-template">
    <div class="col-lg-9 col-md-12">
        <div class="result-display">
            <div class="row">
                <div class="col-md-3">
                    {{-- results count start--}}

                    <p  v-if="totalCount == 1"><b> 1 of&nbsp;&nbsp;@{{totalCount}}&nbsp;&nbsp;</b>PRODUCTS</p>

                    <p v-else-if="totalCount > 1 "><b>@{{ resultCount }} of&nbsp;&nbsp;@{{totalCount}}&nbsp;&nbsp;</b>PRODUCTS</p>

                    <p v-else-if="totalCount == 0"><b>0&nbsp;&nbsp;</b>PRODUCTS ON SALES</p>

                    {{-- results count end --}}

                </div>

                @include('memy::search.partials.toolbar')
            </div>
        </div>

        <h2 v-if="results == null">{{ __('shop::app.products.whoops') }}</h2>
        <span v-if="results == null">{{ __('shop::app.search.no-results') }}</span>

        <div v-if="results != null " class="row">
            <div class="search-product" v-for="(test,index) in results">
                <product-box-for-sale :productFlat="test" :authUser="{{$authUser}}" :key="index"></product-box-for-sale>
            </div>
        </div>
        {{-- pagination start --}}
        <div class="col-md-12" v-if="meta.last_page > 1">
            <div class="pagination-block text-center">
                <div class="container">
                    <nav aria-label="...">
                        <ul class="pagination">

                            <li :class="links.prev == null ? 'page-item disabled' : 'page-item'">
                                <a class="page-link" href="javascript:void(0);" tabindex="-1" @click="paginate(links.prev)" rel="prev">Prev</a>
                            </li>

                                <!-- Pagination Elements -->
                                <span v-for="(n,index) in pages" :key="index">
                                    <li v-if="meta.current_page == n" class="page-item active"><a class="page-link" href="javascript:void(0);">@{{ n }}</a></li>
                                    <li v-else class="page-item"><a class="page-link" href="javascript:void(0);" @click="paginateNumber(n)">@{{ n }}</a></li>
                            </span>

                            <li :class="links.next == null ? 'page-item disabled' : 'page-item'">
                                <a class="page-link" href="javascript:void(0);" @click="paginate(links.next)">Next</a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        {{-- pagination end --}}

    </div>
</script>

<script type="text/x-template" id="product-box-template">
    <div class="product">

        <a class="circle-shape">
            <p>@{{discount}} % <br> <span>Off</span></p>
        </a>

        <!--wishlist start-->

        <div class="product-wishlist" v-if="authUser">
            <wishlist-add-button :id="productFlat.id"></wishlist-add-button>
        </div>

        <div class="product-wishlist" v-else>
            <wishlist-login-register></wishlist-login-register>
        </div>

        <!--wishlist end-->

        <figure class="product-image">
            <a :href="productLink" data-toggle="tooltip" data-placement="top" :title="productFlat.name">
                <v-lazy-image v-if="mediumImage != null" :src="mediumImage" :src-placeholder="smallImage" class="img-fluid" :alt="productFlat.name"/>
                <v-lazy-image v-else src="{{asset('vendor/webkul/ui/assets/images/product/meduim-product-placeholder.png')}}" src-placeholder="{{asset('vendor/webkul/ui/assets/images/product/small-product-placeholder.png')}}" class="img-fluid" alt="memykid"/>
            </a>
        </figure>

        <a :href="productLink" data-toggle="tooltip" data-placement="top" :title="productFlat.name" class="product-ellipseshape">
            <div class="product-info">

                <h5>@{{ productFlat.name }}</h5>

                <!--price end-->
                <span >
                    <strong>@{{ productFlat.special_price | currency}}</strong>
                    <br>
                    <strike style="font-size:12px;">@{{ productFlat.price | currency}}</strike>
                </span>

                <!--price end-->

                <p v-if="!productFlat.in_stock">out of stock</p>


                <div class="rating-star display-none">
                    <i class="fa fa-star checked" aria-hidden="true" v-for="index in averageRating" :key="'checked'+index"></i>

                    <i class="fa fa-star" aria-hidden="true" v-for="index in 5-averageRating" :key="'unchecked'+index" ></i>

                    <span class="bestseller__rater">(@{{totalRating}} reviews)</span>
                </div>

            </div>
        </a>

    </div>
</script>


<script>
    Vue.component('product-results', {
        template: '#product-results-template',

        data: function() {
            return {
                searchTerm : '',
                query : '',
                results : [],
                toolbarFilter : '',
                resultCount : 0,
                totalCount : 0,
                links : [],
                meta : [],
                pages : null
            }
        },

        mounted(){
            this.$root.$on('filterEvent' , (filter) => {
               this.applyFilter(filter);
            });

            this.$root.$on('toolbarEvent' , (value) => {
                this.applyToolbarFilter(value);
            });
        },

        created(){
            this.getResults();
        },

        methods : {
            applyFilter : function(filter){
                this.searchTerm = filter;
                this.results = [];
                this.query = this.searchTerm + this.toolbarFilter;
                this.getResults();
            },
            applyToolbarFilter : function(value){
                // console.log(value);
                if(this.searchTerm.length == 0){
                    this.query = value.replace('&','?');
                }else{
                    this.toolbarFilter = value;
                    this.query = this.searchTerm + this.toolbarFilter;
                }
                
                this.results = [];
                this.getResults();
            },

            getResults : function(){
                this.results = [];
                // console.log(this.searchTerm+this.toolbarFilter);
                self = this;
                this.$Progress.start()
                // axios.get('/api/sale'+this.searchTerm+this.toolbarFilter)
                axios.get('/api/sale'+this.query)
                    .then(response =>{
                        self.results = response.data.data;
                        self.links = response.data.links;
                        self.meta = response.data.meta;
                        self.totalCount = response.data.meta.total;
                        self.pages = parseInt(response.data.meta.last_page);

                        self.resultCount = Object.keys(self.results).length;
                        if(self.resultCount == 0){
                            self.results = null;
                        }
                        self.calculateHighestPrice();
                        self.$Progress.finish();
                    })
                    .catch(error =>{
                        console.log(error);
                        self.$Progress.fail();
                    })
            },
            calculateHighestPrice : function(){
                var max = 500;
                for(index in this.results){
                    if(this.results[index].price > max)
                        max = parseInt(this.results[index].price);
                }
                this.$root.$emit('maxPriceEvent', max);
            },

            paginate : function(link){
                var self = this;

                this.results = [];

                this.$Progress.start()

                axios
                .get(link)
                .then(function (response) {
                    // handle success
                    self.results = response.data.data;
                    self.links = response.data.links;
                    self.meta = response.data.meta;
                    self.totalCount = response.data.meta.total;

                    self.pages = parseInt(response.data.meta.last_page);

                    self.resultCount = Object.keys(self.results).length;
                    if(self.resultCount == 0){
                            self.results = null;
                        }

                    self.$Progress.finish()
                })
                .catch(function (error) {
                    // handle error
                    self.$Progress.fail()

                    console.log(error);
                })
            },

            paginateNumber : function(n){
                var link = this.links.first;
                var newLink = link.replace("page=1", "page="+n);
                this.paginate(newLink);
            }

        }

    });

    Vue.component('product-box-for-sale', {
        template: '#product-box-template',

        props : ['productFlat','authUser'],

        data: function() {
            return {
                route :  route('memy.products.vue.gallery'),
                productLink: 'product link',
                averageRating : Math.round(this.productFlat.reviews.average_rating),
                totalRating : this.productFlat.reviews.total,
                discount : null,
                mediumImage : this.productFlat.base_image.medium_image_url,
                smallImage : this.productFlat.base_image.small_image_url

            }
        },

        created : function(){
            this.productLink = this.route +'/'+ this.productFlat.url_key;
            this.discount = (((parseInt(this.productFlat.price)-parseInt(this.productFlat.special_price))/parseInt(this.productFlat.price))*100).toFixed(1);
        },

        methods : {
          
        },

        filters: {
            currency: function (value) {
                if (!value) return ''
                return 'Rs. '+parseInt(value)+'.00';
            }
        }

    });

</script>


@endpush