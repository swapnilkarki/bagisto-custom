@extends('memy::layouts.master')

@section('page_title')
   FAQ's
@stop

{{-- not good practice, either find a way to use with repository patterm or with controller --}}
@php
    $queries = \CD\Manager\Models\CustomerQueries::where('approved','1')->get()
@endphp 

@section('content')
<section class="faq">
    <div class="container">
        {{--<div class="row">--}}
        <div class="faq-container">
            <h3 class="section-heading">How can we help you?</h3>
            <div class="faq-askquestion">
                <faq></faq>
            </div>

            <div class="support-questions" style="margin-top: 32px;">
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                    @foreach($queries as $faq)
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne{{$faq->id}}">
                                <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne{{$faq->id}}" aria-expanded="true"
                                aria-controls="collapseOne1">
                                    <h5 class="mb-0">
                                        {{$faq->question}}
                                    </h5>
                                </a>
                            </div>
                            <div id="collapseOne{{$faq->id}}" class="collapse show" role="tabpanel" aria-labelledby="headingOne{{$faq->id}}"
                                data-parent="#accordionEx">
                                <div class="card-body">
                                    {{$faq->answer}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{-- //approved from customer queries --}}
                    @foreach($faqs as $faq)
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo{{$faq->id}}">
                            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo{{$faq->id}}" aria-expanded="true"
                            aria-controls="collapseTwo1">
                                <h5 class="mb-0">
                                    {{$faq->question}}
                                </h5>
                            </a>
                        </div>
                        <div id="collapseTwo{{$faq->id}}" class="collapse show" role="tabpanel" aria-labelledby="headingTwo{{$faq->id}}"
                            data-parent="#accordionEx">
                            <div class="card-body">
                                {{$faq->answer}}
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>

        </div>
        {{--</div>--}}
    </div>
</section>
@endsection

@push('scripts')
<script type="text/x-template" id="faq-template">
    <div class="subscribe-form">
        <form method="POST" id="faqForm" class="form-inline" action="{{route('customer-queries.create')}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            @csrf()
            <input name="_method" type="hidden" value="PUT">
            <input class="form-control subscribe-formcontrol" name="question" type="search" placeholder="Ask Us Question" aria-label="Search">
            <button class="btn btn-primary" type="submit">Send</button>
            <vue-recaptcha  ref="recaptchaFaqForm" sitekey="{{config('app.captcha_site_key')}}" @verify="onVerify" @expired="onExpired" size="invisible" :loadRecaptchaScript="true">
            </vue-recaptcha>
        </form>
    </div>
</script>

<script>
    Vue.component('faq', {
        template: '#faq-template',

        methods : {
            onSubmit : function(){
                this.$refs.recaptchaFaqForm.execute();
            },
            onVerify : function(response){
                if(response){
                    document.getElementById('faqForm').submit();
                }
            },
            onExpired : function(){
                // console.log("Expired");
            }
        }
    });
</script>
@endpush
