<contact-us></contact-us>
@push('scripts')
<script type="text/x-template" id="contact-us-template">
    <div class="contact__form">
        <form method="POST" id="contactUsForm" action="{{ route('contact-us.postQuestion') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
        @csrf()
        <input name="_method" type="hidden" value="PUT">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">Name</label>
                        <input type="text" v-validate="'required'" class="form-control form-box" id="name" name="name" data-vv-as="&quot;'Name'&quot;" value="{{old('name')}}"/>
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                        <label for="email" class="required">Email</label>
                        <input type="email" v-validate="'required'" class="form-control form-box" id="email" name="email" data-vv-as="&quot;'Email'&quot;" value="{{old('email')}}"/>
                        <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="control-group" :class="[errors.has('phone') ? 'has-error' : '']">
                        <label for="phone" class="required">Phone</label>
                        <input type="text" v-validate="'required|numeric|max:10|min:10'" class="form-control form-box" id="phone" name="phone" data-vv-as="&quot;'phone'&quot;" value="{{old('phone')}}"/>
                        <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="control-group" :class="[errors.has('subject') ? 'has-error' : '']">
                        <label for="subject" class="required">Subject</label>
                        <input type="subject" v-validate="'required'" class="form-control form-box" id="subject" name="subject" data-vv-as="&quot;'subject'&quot;" value="{{old('subject')}}"/>
                        <span class="control-error" v-if="errors.has('subject')">@{{ errors.first('subject') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="control-group" :class="[errors.has('comments') ? 'has-error' : '']">
                    <label for="comments" class="required">comments</label>
                    <textarea v-validate="'required'" class="form-control form-box" id="comments" name="comments" data-vv-as="&quot;'comments'&quot;" value="{{old('comments')}}"></textarea>
                    <span class="control-error" v-if="errors.has('comments')">@{{ errors.first('comments') }}</span>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Send</button>
            <vue-recaptcha  ref="recaptchaContactUs" sitekey="{{config('app.captcha_site_key')}}" @verify="onVerify" @expired="onExpired" size="invisible" :loadRecaptchaScript="true">
            </vue-recaptcha>
        </form>
    </div>
</script>

<script>
    Vue.component('contact-us', {
        template: '#contact-us-template',

        methods : {
            onSubmit : function(){
                this.$refs.recaptchaContactUs.execute();
            },
            onVerify : function(response){
                if(response){
                    document.getElementById('contactUsForm').submit();
                }
            },
            onExpired : function(){
                // console.log("Expired");
            }
        }
    });
</script>
@endpush