<div class="col-md-4">
    <div class="footer-block">
    
        <?php
            $categories = [];

            foreach (app('Webkul\Category\Repositories\CategoryRepository')->getVisibleCategoryTree(core()->getCurrentChannel()->root_category_id) as $category){
                if ($category->slug)
                    array_push($categories, $category);
            }
        ?>
        @if (count($categories))
            <strong>Categories</strong>
            <ul class="footer-list">
                @foreach ($categories as $key => $category)
                    @if($category->slug != "baby")
                        <li>
                            <a href="{{ route('memy.categories.index', $category->slug) }}">{{ $category->name }}</a>
                        </li>
                    @else
                        @foreach ($category->children as  $child)
                            <li>
                                <a href="{{ route('memy.categories.index', $child->slug) }}">{{ $child->name }}</a>
                            </li>
                        @endforeach
                    @endif
                @endforeach
            </ul>
        @endif
    </div>
</div>