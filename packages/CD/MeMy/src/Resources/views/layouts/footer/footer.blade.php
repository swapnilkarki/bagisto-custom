<footer class="footer">

    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="row">
                        @include('manager::front.footer.sections.company-info.company-info')    

                        @include('memy::layouts.footer.categories')

                        @include('manager::front.footer.sections.shipping-and-policy.shipping-policy')    
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 subscribe-column">
                    <div class="footer-block">
                        <strong>Subscribe</strong>
                        <p>Fill in your email address and we will update you with latest news and offers.</p>
                        @include('memy::layouts.footer.subscribe')
                        @include('memy::layouts.footer.social')
                    </div>
                </div>
            </div>
        </div>
    </footer>
<div class="copyright">
    <p>
        @if (core()->getConfigData('general.content.footer.footer_content'))
            {{ core()->getConfigData('general.content.footer.footer_content') }}
        @else
            {{ trans('admin::app.footer.copy-right') }}
        @endif
    </p>
</div>

{{-- @include('memy::layouts.footer.sticky-footer') --}}

@push('scripts')
<script defer>
    $(document).ready(function(){
        // ===== Scroll to Top ==== 
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) { 
                $('#return-to-top').fadeIn(200);
            } else {
                $('#return-to-top').fadeOut(200);
            }
        });
        $('#return-to-top').click(function() {
            $('body,html').animate({
                scrollTop : 0
            }, 500);
        });
    }); 
</script>
@endpush