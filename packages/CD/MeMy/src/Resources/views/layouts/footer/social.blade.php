<div class="social-network">
        <strong>Connect with us</strong>
        <ul>
            <li>
                <a href="#" target="_blank"><ion-icon name="logo-facebook"></ion-icon></a>
            </li>
            <li>
                <a href="#" target="_blank"><ion-icon name="logo-instagram"></ion-icon></a>
            </li>
            <li>
                <a href="#" target="_blank"><ion-icon name="logo-twitter"></ion-icon></a>
            </li>
            <li>
                <a href="#" target="_blank"><ion-icon name="logo-linkedin"></ion-icon></a>
            </li>
            <li>
                <a href="#" target="_blank"><ion-icon name="logo-youtube"></ion-icon></a>
            </li>
        </ul>
    </div>