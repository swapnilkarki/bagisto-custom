@if(core()->getConfigData('customer.settings.newsletter.subscription'))
    <subscribe></subscribe>
@endif

@push('scripts')

<script type="text/x-template" id="subscribe-template">
    <div class="subscribe-form">
        <form class="form-inline" id="newSubscription" @submit.prevent="submitReview" action="{{ route('shop.subscribe') }}">
            <input class="form-control subscribe-formcontrol" type="email" placeholder="Email" name="subscriber_email" required>
            <button class="btn btn-md btn-primary" type="submit">{{ __('shop::app.subscription.subscribe') }}</button>
            <vue-recaptcha  ref="recaptchaInSubscribe" sitekey="{{config('app.captcha_site_key')}}" @verify="onVerify" @expired="onExpired" size="invisible" :loadRecaptchaScript="true">
            </vue-recaptcha>
        </form>
    </div>
</script>


<script>
    Vue.component('subscribe', {
        template: '#subscribe-template',

        methods : {
            submitReview : function(){
                this.$refs.recaptchaInSubscribe.execute();
            },
            onVerify : function(response){
                if(response){
                    document.getElementById('newSubscription').submit();
                }
            },
            onExpired : function(){
                // console.log("Expired");
            }
        }
    });
</script>

@endpush

