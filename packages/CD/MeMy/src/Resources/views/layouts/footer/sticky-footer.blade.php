<div class="footer-fix-mobnav fixed">
    <ul class="list-inline">
        <li class="list-inline-item">
            <a href="" class="active">
                <i class="fa fa-home"></i>
                {{--<p>Home</p>--}}
            </a>
        </li>
        <li class="list-inline-item">
            <a href="">
                <i class="fa fa-heart"></i>
                {{--<p>Wishlist</p>--}}
            </a>
        </li>
        <li class="list-inline-item">
            <a href="">
                <i class="fa fa-shopping-cart"></i>
                {{--<p>Cart</p>--}}
            </a>
        </li>
        <li class="list-inline-item">
            <ul class="list-inline">
                <li class="list-inline-item">
                    <a href="">
                        <i class="fa fa-user"></i>
                        {{--<p>My Account</p>--}}
                    </a>
                </li>
                <li class="text-center">
                </li>
            </ul>

        </li>
    </ul>
</div>