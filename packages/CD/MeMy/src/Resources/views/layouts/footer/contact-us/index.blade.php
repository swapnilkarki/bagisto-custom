@extends('memy::layouts.master')

@section('seo')
    <meta name="description" content="{{ trim($pageInfo->meta_description) != "" ? $pageInfo->meta_description : str_limit(strip_tags($pageInfo->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $pageInfo->meta_keywords }}"/>
@stop

@section('page_title')
   {{$pageInfo->title}}
@stop

@section('content')
<section class="contact">
    <div class="contact__parallaxbg">
        <h4 class="contact--tag">Contact Us</h4>
    </div>
    <div class="contact__container">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6">
                    <article class="contact__text">
                        <h5 class="contact--heading">Have a question ?</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                    </article>

                    @include('memy::layouts.footer.contact-us.contact-form')

                </div>
                <div class="col-lg-4 col-md-6">
                    <article class="contact__text">
                        <h5 class="contact--heading">Contact Information</h5>
                        <address>
                            Tikhidebal, Mahalaxmisthan, Lalitpur <br>
                            Tel: +977-01-5512345 <br>
                            Email: info@memykids.com
                        </address>
                    </article>
                    <div class="contact__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.8944311877704!2d85.3146793145372!3d27.658737734234588!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19d5889c18a1%3A0xf876386435669c81!2sCore+Dreams+Innovations+Pvt.+Ltd.!5e0!3m2!1sen!2snp!4v1553166698141" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- render from database --}}
{{-- {!!DbView::make($pageInfo)->field('page_content')->render();!!} --}}
@stop