<div class="top-navbar">
    @inject('wishlist','Webkul\Customer\Repositories\WishlistRepository')
    <?php $cart = cart()->getCart();?>
    @if ($cart)
        @php
            Cart::collectTotals();
        @endphp
    @endif
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{ route('memy.index') }}">
                    {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/logo.png')}}" class="img-fluid" alt="memykid"> --}}
                    <v-lazy-image src="{{ asset('vendor/webkul/memy/assets/images/logo.png')}}"  class="img-fluid" alt="MeMyKids"/>
                </a>
                @include('memy::layouts.header.search')
                <div>
                    <ul class="navbar-nav ml-auto">
                    @guest('customer')
                        <li class="nav-item active">
                            <a class="nav-link" href="javascript:void(0)" @click="loginToUseWishlist()">
                                <ion-icon name="heart-empty"></ion-icon>
                                {{--<span>Wishlist</span>--}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('memy.checkout.cart.index') }}">
                                <ion-icon name="cart"></ion-icon>
                                {{--<span>Cart</span>--}}
                            </a>
                            {{-- @if($cart) --}}
                            <quantity method="cart_quantity"></quantity>
                            {{-- @endif --}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="loginModalOpen" href="#" @click="$modal.show('login-modal')">
                                <ion-icon name="contact"></ion-icon>
                                <span>{{ __('shop::app.header.sign-in') }}</span>
                            </a>
                        </li>
                    @endguest

                    <?php $customerId='null'?>
                    @auth('customer')
                        <?php $customerId = auth()->guard('customer')->user()->id;?>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('memy.wishlist.index') }}">
                                <ion-icon name="heart-empty"></ion-icon>
                                {{--<span>Wishlist</span>--}}
                            </a>
                            {{-- @if($wishlist->getItemsWithId(auth()->guard('customer')->user()->id)) --}}
                            <quantity method="wish_quantity"></quantity>
                            {{-- @endif --}}
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('memy.checkout.cart.index') }}">
                                <ion-icon name="cart"></ion-icon>
                                {{--<span>Cart</span>--}}
                            </a>
                            {{-- @if($cart) --}}
                            <quantity method="cart_quantity"></quantity>
                            {{-- @endif --}}
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(auth()->guard('customer')->user()->profile_picture_path != null)
                                    <figure class="user-image">
                                        <img src="{{route('memy.index')}}/storage/{{ auth()->guard('customer')->user()->profile_picture_path }}" alt="memykid">
                                    </figure>
                                @else
                                <figure class="user-image">
                                    <img src="{{ asset('vendor/webkul/ui/assets/images/product/small-product-placeholder.png')}}" class="img-fluid" alt="memykid">
                                </figure>
                                @endif
                                {{ auth()->guard('customer')->user()->first_name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('memy.account.index') }}">Dashboard</a>
                                <a class="dropdown-item" href="{{ route('memy.profile.index') }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('memy.orders.index') }}">My Orders</a>
                                <a class="dropdown-item" href="{{ route('memy.address.index') }}">My Addresses</a>
                                {{-- <a class="dropdown-item" href="dashboard.php">Dashboard</a> --}}
                                <a class="dropdown-item" href="{{ route('customer.session.destroy') }}">{{ __('shop::app.header.logout') }}</a>
                            </div>
                        </li>
                    @endauth
                    </ul>

                </div>
            </div>
        </nav>
        <div class="container">
            <!--login component here-->
            <login-customer></login-customer>
        </div>
    </div>
    @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
        <?php /* */$error_code=5;/* */ ?>
    @else
        <?php /* */$error_code=0;/* */ ?>
    @endif

@push('scripts')
<script type="text/x-template" id="login-template">
    <div>
        <vue-modal name="login-modal" height="auto" :scrollable="true" :clickToClose="false" @before-open="beforeOpen">
            <div class="register__box">
                <div class="register__block">
                    <button @click="$modal.hide('login-modal')" v-if="!requiredLogin" style="border: none; background-color: transparent;  position: absolute; top:5px; right: 5px; filter: brightness(0);">
                            ❌
                    </button>
                    <h5 class="register--heading">Welcome to Memykid</h5>
                
                        <div class="form-group">
                            <label class="form-label" for="email">Email</label>
                            {{-- <input type="email" v-validate="'required|email'" class="form-control form-box"> --}}
                            <input type="text" class="form-control form-box" name="email" v-model="email" v-validate="'required|email'" value="{{ old('email') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.email') }}&quot;">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="password">Password</label>
                            <input type="password" class="form-control form-box" name="password" v-model="password" v-validate="'required'" value="{{ old('password') }}" data-vv-as="&quot;{{ __('shop::app.customer.login-form.password') }}&quot;">
                        </div>
                        
                        <div class="register--forgotpwd">
                            <a href="{{ route('memy.forgot-password.create') }}">Forgot password?</a><br>
                        
                            <a @click="resendVerification()" v-if="enableResend" href="javascript:void(0)">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                    
                        </div>
                        <div class="col-md-6 col-sm-6" v-if="confirmationStep">
                                <span>Confirmation code has been sent to your phone and email</span>
                            <div class="form-group">
                                <label class="form-label" for="ccode">Confirmation Code</label>
                                <input type="text" name="ccode" class="form-control form-box" v-model="confirmation_code" v-validate="'required|numeric|min:6|max:6'">
                            </div>
                        </div>
                        <button v-if="confirmationStep" @click.prevent='confirmCode()' class="btn btn-primary">Confirm</button>
                    
                        <button v-if="!confirmationStep" @click.prevent='loginCustomer()' class="btn btn-primary">{{ __('shop::app.customer.login-form.button_title') }}</button>
                
                    <p class="register--divider">Or</p>
                    <div class="alternate-login">
                        <a href="{{ route('redirect', 'facebook') }}" class="btn btn-primary btn-facebook" style="width:100%;background: #3e5c9a;">
                            {{--<figure><img src="{{ asset('vendor/webkul/memy/assets/images/facebook-logo.png')}}" class="img-fluid" style="width: 16px;display: inline-block; margin-right: 5px;"></figure>--}}
                            <i class="fa fa-facebook"></i> Log In with Facebook
                        </a>
                        <a href="{{ route('redirect', 'google') }}" class="btn btn-primary btn-google" style="width:100%;background: transparent; border-color: #ddd;color: #484848;">
                            <figure><img src="{{ asset('vendor/webkul/memy/assets/images/google-logo.png')}}" class="img-fluid" style="width: 16px;display: inline-block; margin-right: 5px;"></figure>
                            Log In with Google</a>
                    </div>
                    <register-customer></register-customer>
                        <!-- set progressbar -->
                    <vue-progress-bar></vue-progress-bar>
                </div>
            </div>
        </vue-modal>
        <vue-modal name="loader-modal" classes="loader-modal" :clickToClose="false" :height="30" :width="30">
            <div class="text-center">
                <beat-loader :loading="true" color="#E3007B" size="25px"></beat-loader>
            </div>
        </vue-modal>
    </div>
</script>

<script>
    Vue.component('login-customer', {
        template: '#login-template',

        data: function() {
            return {
                confirmationStep : false,
                confirmation_code : '',
                email : '',
                password : '',
                enableResend : false,
                requiredLogin : false,
                errorCode : <?php echo($error_code)?>
            }
        },

        methods: {
            submit : function(){
                // console.log("submit");
                this.$refs.login-form.submit();
            },

            show : function(){
            },

            checkForRedirectionError : function(){
                if(this.errorCode == 5)
                    this.$modal.show('login-modal');
            },

            confirmCode : function(){
            //    console.log('confirmation called');
            //    console.log(route('customer.verify-by-code',this.confirmation_code));
                self=this;
                this.$modal.show('loader-modal');
                axios.get('/api/customer/verify/'+this.confirmation_code)
                .then(function(response){
                    // console.log(response.data);
                      Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Account Confirmed. Please try to login.',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    self.confirmationStep = false
                    self.enableResend = false;
                    self.$modal.hide('loader-modal');
                })
                .catch(function(error){
                    console.log(error);
                    self.$modal.hide('loader-modal');
                })
            },

            resendVerification :function(){
            // console.log('resend verification email ');
               self=this;
               this.$modal.show('loader-modal');
               verificationEmail =this.email;
            //    console.log(verificationEmail);
                axios.get('/api/customer/resend-verification/'+ verificationEmail)
                .then(function(response){
                    // console.log(response.data);
                    self.confirmationStep = response.data.status;
                    self.$modal.hide('loader-modal');
                })
                .catch(function(error){
                    console.log(error);
                    self.$modal.hide('loader-modal');
                })
           },

           loginCustomer : function(){
            //    console.log('login method called');
               this.$modal.show('loader-modal');
               self=this;
               axios.post('/api/customer/login',{
                    email: self.email,
                    password: self.password,
                })
                .then(function(response){
                    if(response.data.status == 'invalid_credentials'){
                        Swal.fire({
                        position: 'center',
                        type: 'info',
                        title: 'Invalid Email or Password',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                    else if(response.data.status =='unverified_account'){
                        Swal.fire({
                        position: 'center',
                        type: 'info',
                        title: 'Please verify your account',
                        showConfirmButton: false,
                        timer: 1500
                        });
                        self.enableResend = true;
                        // console.log(self);
                        // self.$modal.show('login-modal');
                    }
                    else if(response.data.url == 'reload'){
                            window.location.reload(); 
                    }else{
                        window.location = response.data.url;
                    }
                    self.$modal.hide('loader-modal');
                })
                .catch(function(error){
                    self.$modal.hide('loader-modal');
                    console.log(error);
                })

           },

           beforeOpen : function(event){
                this.requiredLogin = event.params.requiredLogin;
                // console.log(this.requiredLogin);
                Swal.fire({
                        position: 'top',
                        type: 'info',
                        toast: 'true',
                        title: 'Please Login To Continue',
                        showConfirmButton: false,
                        timer: 5000
                        });
           }
        },

        mounted(){
          this.checkForRedirectionError();
        }
    })
</script>
<script type="text/x-template" id="quantity-template">
    <span v-if="type == 'cart_quantity'" class="notification">@{{ cartQuantity }}</span>

    <span v-else-if="type == 'wish_quantity'" class="notification wishlist-notification">@{{ wishlistQuantity }}</span>

</script>

<script>
     Vue.component('quantity', {

        template: '#quantity-template',

        props : ['method'],

        data() {
            return {
                cartQuantity : 0,
                wishlistQuantity : 0,
                customerId : null,
                type : this.method,
            }
        },

        mounted(){
                this.$root.$on('cartEvent' , (status) => {
                    console.log('event catched  ' + status);
                    this.getData();
                });
        },

        created(){
            this.getData();   
        },

        methods : {
            getData : function(){
                if(this.type == 'cart_quantity'){
                    this.getCartQuantity();
                }
                else if(this.type == 'wish_quantity'){
                    this.customerId = <?php echo($customerId)?>;
                    if(this.customerId != null) {
                        this.getwishlistQuantity( this.customerId);
                    }
                }
            },

            getCartQuantity : function(){
                // console.log('get cart quantity')
                var self = this;
                var url = "/api/checkout/cart";
                axios.get(url)
                .then(function (response) {
                    //item_count gives total unique items
                    //use items_qty for total number of items
                    var qty = parseInt(response.data.data.items_count);
                    self.cartQuantity = qty;
                })
                .catch(function (error) {
                    // console.log('No Items in Cart');
                    self.cartQuantity = 0;
                });
            },

            getwishlistQuantity : function(customerId){
                // console.log('get wishlist items')
                var self = this;
                axios.get('/api/wishlist?customer_id='+customerId)
                .then(function (response) {
                    var wishQty = parseInt(response.data.meta.total);
                    self.wishlistQuantity = wishQty;
                })
                .catch(function (error) {
                    console.log('No Items in Wishlist');
                    self.wishlistQuantity = 0;
                });
            },
        },
    });
</script>

@endpush