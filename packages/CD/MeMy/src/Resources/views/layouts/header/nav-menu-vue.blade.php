<?php

$categories = [];

foreach (app('Webkul\Category\Repositories\CategoryRepository')->getVisibleCategoryTree(core()->getCurrentChannel()->root_category_id) as $category) {
    if ($category->slug)
        array_push($categories, $category);
        // dd($categories);
       // show sub categories in nav bar
    if ($category->children){
        foreach(app('Webkul\Category\Repositories\CategoryRepository')->getVisibleCategoryTree($category->id) as $childrenCategory){
            if($childrenCategory->show_in_navbar ===1)
                array_push($categories,$childrenCategory);
        }
    }
}
// dd($childrenCategory);
?>
<div class="menu-container">
    <div class="menu">
        <div class="menu-search">
                <form role="search" action="{{ route('memy.search.index') }}" method="GET">
                <div class="search-wrapper">
                        <input type="search" name="term" class="search-input" placeholder="{{ __('shop::app.header.search-text') }}" required>

                                <i class="custom-icon search-icon" aria-hidden="true"></i>

                </div>
            </form>

        </div>
        <category-nav categories='@json($categories)' url="{{url()->to('/memykids')}}"></category-nav>
    </div>
</div>