<search></search>
@push('scripts')
<script type="text/x-template" id="search-template">
        <form role="search" class="form-inline my-2 my-lg-0" action="{{ route('memy.search.index') }}" method="GET" ref="searchForm">
        <input type="text" name="term" autocomplete="off" class="form-control formbox" v-model="searchTerm" v-on:keyup="searchTimeOut()" placeholder="Search for Category, Brand or Product" required>
                 <button class="btn btn-secondary">
                        <i class="custom-icon search-icon" aria-hidden="true"></i>
                </button>
        
                <div class="search-suggestion">
                        <ul>
                                {{-- products suggestions --}}
                                <span class="search-suggestion-title" v-if='productsResults.length'>Products</span>
                                <li v-for='(productsResult, index) in productsResults' v-if='productsResults.length' @click=(redirect(productsResult))>
                                        {{-- <img src="{{ asset('vendor/webkul/ui/assets/images/product/small-product-placeholder.png') }}" style="width:16px;height:16px;"> --}}
                                        <text-highlight :queries="searchTerm"> @{{productsResult.name}}</text-highlight>
                                </li>
                                

                                {{-- category suggestions --}}
                                <span class="search-suggestion-title" v-if='categoryResults.length'>Categories</span>
                                <li v-for='(categoryResult, index) in categoryResults' v-if='categoryResults.length' @click=(redirect(categoryResult))>
                                        {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/folder.png') }}" style="width:16px;height:16px;"> --}}
                                        <text-highlight :queries="searchTerm">  @{{categoryResult.name}}</text-highlight>    
                                </li>
                                

                                {{-- tag suggestions --}}
                                <span class="search-suggestion-title" v-if='tagResults.length'>Tags</span>
                                <li v-for='(tagResult, index) in tagResults' v-if='tagResults.length' @click="searchTag(tagResult)">
                                        {{-- <img src="{{ asset('vendor/webkul/memy/assets/images/tag.png') }}" style="width:16px;height:16px;"> --}}
                                        <text-highlight :queries="searchTerm">  @{{tagResult}}</text-highlight>
                                </li>
                                
                                <li v-if='!searchResults && searchTerm.length && !is_searching'>
                                        No Results Found
                                </li>
                        
                                <li v-if="is_searching && searchTerm">
                                        Searching...
                                </li>

                        </ul>
                </div>
        </form>
</script>

<script>
 Vue.component('search', {

        template: '#search-template',

        data: function() {
                return {
                        searchResults : false,

                        searchTerm : '',

                        productsResults : [],

                        categoryResults : [],

                        tagResults : [],

                        is_searching : false,

                        timer : ''
                }
        },

        methods: {
                searchTimeOut : function(){
                        this.is_searching = true;
                        if (this.timer) {
                                clearTimeout(this.timer);
                                this.timer = null;
                        }
                        this.timer = setTimeout(() => {
                                this.search();
                        }, 800);

                },
                search: function(){
                        this_this = this;
                        // console.log('searching... '+this.searchTerm);

                        this_this.productsResults = [];
                        this_this.categoryResults = [];
                        this_this.tagResults = [];

                        this.$http.get ("{{ route('memy.search.products.suggestion') }}", {params: {query: this.searchTerm}})
                        .then (function(response) {
                                
                                var suggestions = response.data.data;

                                // console.log(suggestions.length);

                                if(suggestions.length >= 1){
                                        this_this.searchResults = true;
                                }else{
                                        this_this.searchResults = false;
                                }

                                // console.log(suggestions);

                                for(var index in suggestions){
                                        if(suggestions[index].class == 'product'){

                                                this_this.productsResults.push(suggestions[index])

                                        }else if(suggestions[index].class == 'category'){

                                                this_this.categoryResults.push(suggestions[index])

                                        }else if(suggestions[index].class == 'tag'){

                                                this_this.tagResults.push(suggestions[index].tags)

                                        }
                                }

                                this_this.is_searching = false;
                        })

                        .catch (function (error) {
                                this_this.is_searching = false;
                        })
                },

                redirect : function(data){
                        window.location = data.url
                        // console.log(data.url);
                },

                searchTag : function(tagResult){
                        // this.$refs.searchForm.submit();
                        // console.log(tagResult);
                        // console.log(route('memy.search.index')+'?term='+tagResult);
                        window.location = route('memy.search.index')+'?term='+tagResult;
                }
        
        }
});
</script>
@endpush