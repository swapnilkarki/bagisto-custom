<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158950246-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-158950246-1');
    </script>


    <title>@yield('page_title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="{{ app()->getLocale() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="title" content="MeMyKids">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="15 days">
    <link rel="canonical" href="https://www.memykids.com">
    {{--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700|Roboto&display=swap" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
    {{--<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap" rel="stylesheet">--}}
   
    @include('memy::extras.css')
   
    {{-- @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="icon" href="{{ asset('vendor/webkul/memy/assets/images/favicon-new.png') }}" type="image/gif" sizes="16x16">
    @endif --}}

    <link rel="icon" href="{{ asset('vendor/webkul/memy/assets/images/favicon-new.png') }}" type="image/gif" sizes="16x16">



    @section('seo')
        <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
        <meta name="keywords" content="baby stores, baby stores online, baby apparel, baby clothes, kids store, kids clothes, toys"/>
    @show

    @stack('meta')

    @stack('css')


    {{-- <script type="text/javascript" src="{{ asset('vendor/webkul/memy/assets/js/memy-jquery.js') }}"></script> --}}
    {!! view_render_event('bagisto.memy.layout.head') !!}

    {{-- @laravelPWA --}}

    
</head>

<body id="top">
    <!-- Start of Async Callbell Code -->
    <script>
        window.callbellSettings = {
        token: "yQ7JfVnSjXr5Fq6RxcqzxHiU"
        };
    </script>
    <script defer>
        (function(){var w=window;var ic=w.callbell;if(typeof ic==="function"){ic('reattach_activator');ic('update',callbellSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Callbell=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://dash.callbell.eu/include/'+window.callbellSettings.token+'.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
    </script>
    <!-- End of Async Callbell Code -->
  
   {{-- <!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>
   <script>window.fbAsyncInit=function(){FB.init({xfbml:!0,version:"v4.0"})},function(e,t,n){var c,o=e.getElementsByTagName(t)[0];e.getElementById(n)||((c=e.createElement(t)).id=n,c.src="https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js",o.parentNode.insertBefore(c,o))}(document,"script","facebook-jssdk");</script>

   <!-- Your customer chat code -->
   <div class="fb-customerchat"
     attribution=setup_tool
     page_id="109780143726691"
     greeting_dialog_display='fade'
     greeting_dialog_delay='300'>
   </div> --}}

    <div id='app'>
        <flash-wrapper ref='flashes'></flash-wrapper>
        
        @include('memy::layouts.header.index')

        @yield('content')

        @include('memy::layouts.footer.footer')
       
    </div>

        <script type="text/javascript">
            window.flashMessages = [];

            @if ($success = session('success'))
                window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
            @elseif ($warning = session('warning'))
                window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
            @elseif ($error = session('error'))
                window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
                ];
            @elseif ($info = session('info'))
                window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
                ];
            @endif

            window.serverErrors = [];
            @if(isset($errors))
                @if (count($errors))
                    window.serverErrors = @json($errors->getMessages());
                @endif
            @endif
        </script>

        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/'),
                'routes' => collect(\Route::getRoutes())->mapWithKeys(function ($route) { return [$route->getName() => $route->uri()]; })
            ]) !!};
        </script>

        @include('memy::extras.scripts')
        @stack('scripts')  
         {{-- bootstrap tooltip --}}
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script> 

        
        {{-- <!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5d5a6b20eb1a6b0be6082ef7/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script--> --}}
</body>
</html>