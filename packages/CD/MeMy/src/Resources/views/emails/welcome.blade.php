@component('shop::emails.layouts.master')
    <div>
        <div style="text-align: center;">
            <a href="{{ config('app.url') }}">
                {{-- <img src="{{ bagisto_asset('images/logo.svg') }}"> --}}
                <img src="{{ asset('vendor/webkul/memy/assets/images/logo.png')}}">
            </a>
        </div>

        <div  style="font-size:16px; color:#242424; font-weight:600; margin-top: 60px; margin-bottom: 15px">
            Welcome to MeMyKids
        </div>

        <div>
           You are Registered
        </div>

        <form class="form-inline" action="{{ route('shop.subscribe') }}">
            <input type="hidden" value="{{$customer['email']}}" name="subscriber_email" required>
            <button class="btn btn-md btn-primary" type="submit">{{ __('shop::app.subscription.subscribe') }}</button>
        </form>

        {{-- <div  style="margin-top: 40px; text-align: center">
            <a href="{{ route('shop.unsubscribe', $data['token']) }}" style="font-size: 16px;
            color: #FFFFFF; text-align: center; background: #0031F0; padding: 10px 100px;text-decoration: none;">Unsubscribe</a>
        </div> --}}
    </div>

@endcomponent