@component('memy::emails.layouts.master')
    <div style="text-align: center;">
        <a href="{{ config('app.url') }}">
            {{-- <img src="{{ bagisto_asset('images/logo.svg') }}"> --}}
            <img src="{{ asset('themes/default/assets/images/logo.svg') }}">
        </a>
    </div>

    <div style="padding: 30px;">
        <div style="font-size: 20px;color: #242424;line-height: 30px;margin-bottom: 34px;">
            <span style="font-weight: bold;">
                Rma Request Refunded!
            </span> <br>
            
            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                Dear Admin,
            </p>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                RMA request {{$rma->id}} for Order {{$rma->order_id}} was refunded. Thank you.
            </p>
        </div>

        <div style="margin-top: 65px;font-size: 16px;color: #5E5E5E;line-height: 24px;display: inline-block">

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {!!
                    __('shop::app.mail.order.help', [
                        'support_email' => '<a style="color:#0041FF" href="mailto:' . config('mail.from.address') . '">' . config('mail.from.address'). '</a>'
                        ])
                !!}
            </p>

            <p style="font-size: 16px;color: #5E5E5E;line-height: 24px;">
                {{ __('shop::app.mail.order.thanks') }}
            </p>
        </div>
    </div>
@endcomponent
