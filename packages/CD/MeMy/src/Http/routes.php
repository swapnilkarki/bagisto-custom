<?php
Route::group(['middleware' => ['web']], function () {
   //landing page
   Route::get('/', 'CD\MeMy\Http\Controllers\MeMyController@index')->defaults('_config', ['view' => 'memy::front.main'
   ])->name('memy.index');
Route::prefix('memykids')->group(function (){
    //  Customer Info Pages
    //products on sale
    Route::get('/sale', 'CD\MeMy\Http\Controllers\MeMyController@productsOnSale')->defaults('_config', ['view' => 'memy::sale.index'
    ])->name('memy.sale');
    //about us page
    Route::get('/about-us', 'CD\MeMy\Http\Controllers\MeMyController@aboutUs')->defaults('_config', ['view' => 'memy::company-info.about-us'
    ])->name('memy.pages.about-us');

    //contact us page
    Route::get('/contact-us', 'CD\MeMy\Http\Controllers\MeMyController@contactUs')->defaults('_config', ['view' => 'memy::company-info.contact-us'
    ])->name('memy.pages.contact-us');

    //Blog page
    Route::get('/blog', 'CD\MeMy\Http\Controllers\MeMyController@blog')->defaults('_config', ['view' => 'memy::company-info.blog'
    ])->name('memy.pages.blog');

    //blog deatail page
    Route::get('/blog-detail', 'CD\MeMy\Http\Controllers\MeMyController@index')->defaults('_config', ['view' => 'memy::company-info.blog-detail'
    ])->name('memy.pages.blog-detail');

    //faq page
    Route::get('/faq', 'CD\MeMy\Http\Controllers\MeMyController@faq')->defaults('_config', ['view' => 'memy::company-info.faq'
    ])->name('memy.pages.faq');

    //partners page
    Route::get('/partners', 'CD\MeMy\Http\Controllers\MeMyController@partners')->defaults('_config', ['view' => 'memy::company-info.partners'
    ])->name('memy.pages.partners');

     //Store front header nav-menu fetch
     Route::get('/categories/{slug}', 'CD\MeMy\Http\Controllers\CategoryController@index')->defaults('_config', [
        'view' => 'memy::products.index'
    ])->name('memy.categories.index');


      //individual products page
      Route::get('/products/{slug}', 'CD\MeMy\Http\Controllers\ProductController@index')->defaults('_config', ['view' => 'memy::products.view'
      ])->name('memy.products.gallery');
        //for vue route product list
      Route::get('/products', 'CD\MeMy\Http\Controllers\ProductController@index')->name('memy.products.vue.gallery');

       // forgot Password Routes
        // Forgot Password Form Show
        Route::get('/forgot-password', 'Webkul\Customer\Http\Controllers\ForgotPasswordController@create')->defaults('_config', [
            'view' => 'memy::customers.signup.forgot-password'
        ])->name('memy.forgot-password.create');

        // Forgot Password Form Store
        Route::post('/forgot-password', 'Webkul\Customer\Http\Controllers\ForgotPasswordController@store')->name('memy.forgot-password.store');

    //login routes
    //login form show
    Route::get('/login', 'CD\MeMy\Http\Controllers\SessionController@show')->defaults('_config', [
        'view' => 'memy::customers.session.index',
    ])->name('memy.session.index');

    // Login form store
    Route::post('/login', 'CD\MeMy\Http\Controllers\SessionController@create')->defaults('_config', [
        'redirect' => 'memy.profile.index'
    ])->name('memy.session.create');

     // Registration Routes
    //registration form show
    Route::get('/register', 'Webkul\Customer\Http\Controllers\RegistrationController@show')->defaults('_config', [
        'view' => 'memy::customers.signup.index'
    ])->name('memy.register.index');

    //registration form store
    Route::post('/register', 'Webkul\Customer\Http\Controllers\RegistrationController@create')->defaults('_config', [
        'redirect' => 'memy.session.index',
    ])->name('memy.register.create');

    // Reset Password Form Show
    Route::get('/reset-password/{token}', 'Webkul\Customer\Http\Controllers\ResetPasswordController@create')->defaults('_config', [
        'view' => 'memy::customers.signup.reset-password'
    ])->name('memy.reset-password.create');

    // Reset Password Form Store
    Route::post('/reset-password', 'Webkul\Customer\Http\Controllers\ResetPasswordController@store')->defaults('_config', [
        'redirect' => 'memy.profile.index'
    ])->name('memy.reset-password.store');

    //Customer Profile Edit Form Store
    Route::post('profile/edit', 'Webkul\Customer\Http\Controllers\CustomerController@update')->defaults('_config', [
        'redirect' => 'memy.profile.index'
    ])->name('customer.profile.edit');

     //checkout and cart
    //Cart Items(listing)
    Route::get('/checkout/cart', 'Webkul\Shop\Http\Controllers\CartController@index')->defaults('_config', [
        'view' => 'memy::checkout.cart.index'
    ])->name('memy.checkout.cart.index');

    //checkout onepage
    Route::get('/checkout/onepage', 'Webkul\Shop\Http\Controllers\OnepageController@index')->defaults('_config', [
        'view' => 'memy::checkout.onepage'
    ])->name('memy.checkout.onepage.index');

     //Cart Update Before Checkout
     Route::post('/checkout/cart', 'Webkul\Shop\Http\Controllers\CartController@updateBeforeCheckout')->defaults('_config', [
        'redirect' => 'memy.checkout.onepage.index'
    ])->name('memy.checkout.cart.update');

    //Checkout Order Successfull
    Route::get('/checkout/success', 'Webkul\Shop\Http\Controllers\OnepageController@success')->defaults('_config', [
        'view' => 'memy::checkout.success'
    ])->name('memy.checkout.success');
    
    // Show Product Review Form Store
    Route::post('/product/{slug}/review', 'Webkul\Shop\Http\Controllers\ReviewController@store')->name('memy.reviews.store');


    Route::prefix('/account')->group(function () {
        //Customer Dashboard Route
        Route::get('index', 'Webkul\Customer\Http\Controllers\AccountController@index')->defaults('_config', [
            'view' => 'memy::customers.account.index'
        ])->name('memy.account.index');

        //Customer Profile Show
        Route::get('profile', 'Webkul\Customer\Http\Controllers\CustomerController@index')->defaults('_config', [
            'view' => 'memy::customers.account.profile.index'
        ])->name('memy.profile.index');

        //Customer Profile Edit Form Show
        Route::get('profile/edit', 'Webkul\Customer\Http\Controllers\CustomerController@edit')->defaults('_config', [
            'view' => 'memy::customers.account.profile.edit'
        ])->name('memy.profile.edit');

        /* Reviews route */
        //Customer reviews
        Route::get('reviews', 'Webkul\Customer\Http\Controllers\CustomerController@reviews')->defaults('_config', [
            'view' => 'memy::customers.account.reviews.index'
        ])->name('memy.reviews.index');

        //Customer review delete
        Route::get('reviews/delete/{id}', 'Webkul\Shop\Http\Controllers\ReviewController@destroy')->defaults('_config', [
            'redirect' => 'memy.reviews.index'
        ])->name('memy.review.delete');

        //Customer all review delete
        Route::get('reviews/all-delete', 'Webkul\Shop\Http\Controllers\ReviewController@deleteAll')->defaults('_config', [
            'redirect' => 'memy.reviews.index'
        ])->name('memy.review.deleteall');

        /*    Routes for Addresses   */
        //Customer Address Show
        Route::get('addresses', 'Webkul\Customer\Http\Controllers\AddressController@index')->defaults('_config', [
            'view' => 'memy::customers.account.address.index'
        ])->name('memy.address.index');

         //Customer Address Edit Form Show
         Route::get('addresses/edit/{id}', 'Webkul\Customer\Http\Controllers\AddressController@edit')->defaults('_config', [
            'view' => 'memy::customers.account.address.edit'
        ])->name('memy.address.edit');

        //Customer Address Edit Form Show
        Route::get('addresses/make-default/{id}', 'Webkul\Customer\Http\Controllers\AddressController@makeDefault')->name('memy.address.makeDefault');

        //Customer Address Edit Form Store
        Route::put('addresses/edit/{id}', 'Webkul\Customer\Http\Controllers\AddressController@update')->defaults('_config', [
            'redirect' => 'memy.address.index'
        ])->name('memy.address.edit');

         //Customer Address Create Form Show
         Route::get('addresses/create', 'Webkul\Customer\Http\Controllers\AddressController@create')->defaults('_config', [
            'view' => 'memy::customers.account.address.create'
        ])->name('memy.address.create');

        //Customer Address Create Form Store
        Route::post('addresses/create', 'Webkul\Customer\Http\Controllers\AddressController@store')->defaults('_config', [
            'redirect' => 'memy.address.index'
        ])->name('memy.address.create');

        /* Wishlist route */
        //Customer wishlist(listing)
        Route::get('wishlist', 'Webkul\Customer\Http\Controllers\WishlistController@index')->defaults('_config', [
            'view' => 'memy::customers.account.wishlist.index'
        ])->name('memy.wishlist.index');

        /* Orders route */
        //Customer orders(listing)
        Route::get('orders', 'Webkul\Shop\Http\Controllers\OrderController@index')->defaults('_config', [
            'view' => 'memy::customers.account.orders.index'
        ])->name('memy.orders.index');

         //Customer orders view summary and status
         Route::get('orders/view/{id}', 'Webkul\Shop\Http\Controllers\OrderController@view')->defaults('_config', [
            'view' => 'memy::customers.account.orders.view'
        ])->name('memy.orders.view');

        Route::get('orders/cancel/{id}', 'Webkul\Admin\Http\Controllers\Sales\OrderController@cancelByCustomer')->defaults('_config', [
            'view' => 'memy::customers.account.orders.index'
        ])->name('memy.orders.cancel')->middleware('customer');;

        //Prints invoice
        Route::get('orders/print/{id}', 'Webkul\Shop\Http\Controllers\OrderController@print')->defaults('_config', [
            'view' => 'shop::customers.account.orders.print'
        ])->name('customer.orders.print');

         /* Cancellation and Return route */
        //Cancellation and Return view
        // Route::view('cancelation-returns','memy::customers.account.cancelation-returns.index')->name('memy.cancelation-returns.index');

        Route::get('cancelation-returns', 'CD\Rma\Http\Controllers\CancellationReturnsController@index')->defaults('_config', [
            'view' => 'memy::customers.account.cancelation-returns.index'
        ])->name('memy.cancelation-returns.index');

          /* Tracking route */
        //product Tracking view
        Route::view('tracking','memy::customers.account.tracking.index')->name('memy.tracking.index');

        //Store front search
        Route::get('/search', 'Webkul\Shop\Http\Controllers\SearchController@index')->defaults('_config', [
            'view' => 'memy::search.search'
        ])->name('memy.search.index');

         //Cart Items Add Configurable for more
        Route::get('checkout/cart/addconfigurable/{slug}', 'Webkul\Shop\Http\Controllers\CartController@addConfigurable')
        ->name('memy.cart.add.configurable');

    });

});

Route::prefix('api')->group(function (){
    Route::get('/sale', 'CD\MeMy\Http\Controllers\MeMyController@fetchProductsOnSale');

    Route::get('/decode/geo-location','CD\MeMy\Http\Controllers\GoogleMapController@decodeGeoCode');

    Route::put('/update/lat-lng/{id}','CD\MeMy\Http\Controllers\GoogleMapController@updateLatLng');
});

 //product search for linked products
 Route::get('tags/search', 'CD\MeMy\Http\Controllers\SearchController@searchForTag')->defaults('_config', [
    'view' => 'admin::catalog.products.edit'
])->name('admin.catalog.products.searchForTag');

Route::post('/tags/add', 'CD\MeMy\Http\Controllers\SearchController@storeTag')->defaults('_config', [
    'view' => 'admin::catalog.products.edit'
])->name('memy.catalog.products.addTag');

Route::get('tags/delete', 'CD\MeMy\Http\Controllers\SearchController@deleteTag')->defaults('_config', [
    'view' => 'admin::catalog.products.edit'
])->name('memy.catalog.products.deleteTag');

 //product search for linked products
 Route::get('product/search', 'CD\MeMy\Http\Controllers\SearchController@searchSuggestion')->defaults('_config', [
    'view' => 'memy::front.main'
])->name('memy.search.products.suggestion');

 //verify account
 Route::get('/verify-account-by-code/{confirmation_code}', 'Webkul\Customer\Http\Controllers\RegistrationController@verifyByCode')->name('customer.verify-by-code');

Route::get('/login/{provider}', 'Webkul\Customer\Http\Controllers\SessionController@redirectToProvider')->name('redirect');
Route::get('/login/{provider}/callback','Webkul\Customer\Http\Controllers\SessionController@handleProviderCallback');

Route::get('generate-shorten-link', 'CD\MeMy\Http\Controllers\ShortLinkController@index');
Route::post('generate-shorten-link', 'CD\MeMy\Http\Controllers\ShortLinkController@store')->name('generate.shorten.link.post');
Route::get('{code}', 'CD\MeMy\Http\Controllers\ShortLinkController@shortenLink')->name('shorten.link')->middleware('customer');

});