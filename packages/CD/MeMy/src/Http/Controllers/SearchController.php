<?php

namespace CD\MeMy\Http\Controllers;

use CD\MeMy\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CD\MeMy\Models\Tag;
use Response;

class SearchController extends Controller
{
        protected $_config;

        protected $tag;
    
        public function __construct(Tag $tag)
        {
            $this->_config = request('_config');
            $this->tag = $tag;
        }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    
    public function searchSuggestion(Request $request)
    {
        if (request()->ajax()) {
            $query = request()->input('query');

            if(!$query && $query == '') return Response::json('no search term');

            $products = \Webkul\Product\Models\ProductFlat::where('visible_individually', '1')
            ->where('name','like','%'.$query.'%')
            ->orderBy('name','asc')
            ->take(5)
            ->get(array('url_key','name','product_id'))->toArray();

            $categories = \Webkul\Category\Models\CategoryTranslation::where('name','like','%'.$query.'%')
            ->take(5)
            ->get(array('slug', 'name','category_id'))
            ->toArray();

            $tag = \CD\MeMy\Models\ProductTag::where('tags','like','%'.$query.'%')
            ->distinct('tags')
            ->take(5)
            ->get(array('tags'))
            ->toArray();

            
            $products   = $this->appendURL($products, 'products');
            $categories   = $this->appendURL($categories, 'categories');

            // Add type of data to each item of each set of results
            $products = $this->appendValue($products, 'product', 'class');
            $categories = $this->appendValue($categories, 'category', 'class');
            $tag = $this->appendValue($tag, 'tag', 'class');

            // Merge all data into one array
            $data = array_merge($products, $categories, $tag);

            return Response::json(array(
                'data'=>$data
            ));

        } else {
            return view($this->_config['view']);
        }
    }

    public function appendValue($data, $type, $element)
	{
		// operate on the item passed by reference, adding the element and type
		foreach ($data as $key => & $item) {
			$item[$element] = $type;
		}
		return $data;		
    }
    
    public function appendURL($data, $prefix)
	{
        // operate on the item passed by reference, adding the url based on slug
        if($prefix == 'products'){
            foreach ($data as $key => & $item) {
                $item['url'] = url('/memykids/'.$prefix.'/'.$item['url_key']);
            }
            return $data;		
        }else if($prefix == 'categories'){
            foreach ($data as $key => & $item) {
                $item['url'] = url('/memykids/'.$prefix.'/'.$item['slug']);
            }
            return $data;
        }else if($prefix == 'products'){

        }
	}
 
    public function searchTag($term)
    {
        $results = \CD\MeMy\Models\ProductTag::where('tags', 'like', '%' . urldecode($term) . '%')
               ->orderBy('tags', 'asc')
               ->distinct()
               ->get(['tags']);
        return $results;
    }

    public function searchForTag()
    {
        if (request()->ajax()) {
            $results = [];

            foreach ($this->searchTag(request()->input('query')) as $row) {
                $results[] = [
                        'tag' => $row->tags,
                    ];
            }
            return response()->json($results);
        } else {
            return view($this->_config['view']);
        }
    }

    public function storeTag(Request $request)
    {

        $tag = $request->tags;

        // $productId = $request->productId;
        
        \CD\MeMy\Models\ProductTag::create($request->all());

        if (request()->ajax()) {
            return response()->json($tag);
        }else {
            return view($this->_config['view']);
        }
        
    }

    public function deleteTag(Request $request)
    {
        $product_id = $request->product_id;
        $tag = $request->tag;

        if (request()->ajax()) {
            $productTag = \CD\MeMy\Models\ProductTag::where('product_id', $product_id)
            ->where('tags',$tag)
            ->delete();

            return response()->json($productTag);
        } else {
            return view($this->_config['view']);
        }

    }
}
    