<?php

namespace CD\MeMy\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Webkul\Sales\Repositories\OrderRepository;
use CD\Rma\Repositories\RmaRepository;
use CD\MeMy\Models\ShortLinks as ShortLink;

class SmsController extends Controller
{
    protected $orderRepository;

    protected $rmaRepository;

    public function __construct(OrderRepository $orderRepository, RmaRepository $rmaRepository)
    {
        $this->orderRepository = $orderRepository;

        $this->rmaRepository = $rmaRepository;
    }

    public function sendSms($confirmation_code,$mobile_number)
    {
        $message =  $confirmation_code.' is your MeMyKids confirmation code. Please enter the code to complete your registration.';
       
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }

    }

    public function sendOrderSms($order){
        $mobile_number=$this->orderRepository->getNumber($order->id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];

        $shortUrlCode = $this->shortUrl(route('memy.orders.view',$order->id));

        $message = 'Dear '.$customer_first_name.', You have sucessfully placed your order. View your order details at '.route('shorten.link', $shortUrlCode).'.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);

        //ADMIN sms info
        $adminShortUrlCode = $this->shortUrl(route('admin.sales.orders.view',$order->id));

        $adminMessage = 'Dear Admin, You have a new order. View the order details at '.route('shorten.link', $adminShortUrlCode).'.';

        $adminNumber = config('app.admin_mobile_number');

        if(config('app.env') == 'production'){
            //customer msg
            $this->sparrowSms($mobile_number,$message);
            //admin msg
            $this->sparrowSms($adminNumber,$adminMessage);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
            \Log::info('ADMIN');
            \Log::info($adminMessage);
            \Log::info($adminNumber);
        }
    }

    public function sendOrderConfirmationSms($invoice){
        $mobile_number=$this->orderRepository->getNumber($invoice->order_id);

        $customer_first_name = explode(' ', $invoice->order->customer_first_name)[0];

        $shortUrlCode = $this->shortUrl(route('memy.orders.view',$invoice->order_id));
        $shortUrlPdf = $this->shortUrl(route('customer.orders.print',$invoice->order->invoices[0]->id));

        $message = 'Dear '.$customer_first_name.', Your order has been confirmed. View your order status and invoice at '.route('shorten.link', $shortUrlCode).' or download your details at '.route('shorten.link', $shortUrlPdf).'.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }
    }

    public function sendOrderShippedSms($shipment){
        $mobile_number=$this->orderRepository->getNumber($shipment->order_id);
        $customer_first_name = explode(' ', $shipment->order->customer_first_name)[0];

        $shortUrlCode = $this->shortUrl(route('memy.orders.view',$shipment->order_id));

        $message = 'Dear '.$customer_first_name.', You order has been shipped. Your expected delivery date is from(--/--/----) to(--/--/----). View your order status with tracking code at '.route('shorten.link', $shortUrlCode).'.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }
    }

    public function sendOrderCompleteSms($order){
        $mobile_number=$this->orderRepository->getNumber($order->id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];

        $message = 'Dear '.$customer_first_name.', Your order has been completed. Remember us for future order.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);

        //ADMIN sms info
        $adminShortUrlCode = $this->shortUrl(route('admin.sales.orders.view',$order->id));

        $adminMessage = 'Dear Admin, Order '.$order->id.' has been completed. View the order details at '.route('shorten.link', $adminShortUrlCode).'.';

        $adminNumber = config('app.admin_mobile_number');

        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
            //admin msg
            $this->sparrowSms($adminNumber,$adminMessage);

        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
            \Log::info('ADMIN');
            \Log::info($adminMessage);
            \Log::info($adminNumber);
        }
    }

    public function sendOrderCancelSms($order){
        $mobile_number=$this->orderRepository->getNumber($order->id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];

        $message = 'Dear '.$customer_first_name.', Your order has been cancelled. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);

        //ADMIN sms info
        $adminShortUrlCode = $this->shortUrl(route('admin.sales.orders.view',$order->id));

        $adminMessage = 'Dear Admin, You have a cancelled order. View the order details at '.route('shorten.link', $adminShortUrlCode).'.';

        $adminNumber = config('app.admin_mobile_number');

        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
            //admin msg
            $this->sparrowSms($adminNumber,$adminMessage);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
            \Log::info('ADMIN');
            \Log::info($adminMessage);
            \Log::info($adminNumber);
        }
    }

    // RMA sms start
    public function sendRmaPlacedSms($rma){
        $order=$this->orderRepository->findOrFail($rma->order_id);
        $mobile_number=$this->orderRepository->getNumber($rma->order_id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];
        $rma_info = $this->rmaRepository->findById($rma->policy_id);
        
        $message = 'Dear '.$customer_first_name.', Your '.$rma_info->type.' request has been placed. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);

        //ADMIN sms infoe
        $adminShortUrlCode = $this->shortUrl(route('rma.requests.index'));

        $adminMessage = 'Dear Admin, You have a new rma request with id '.$rma->id.'. View the details at '.route('shorten.link', $adminShortUrlCode).'.';

        $adminNumber = config('app.admin_mobile_number');

        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
            //admin msg
            $this->sparrowSms($adminNumber,$adminMessage);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
            \Log::info('ADMIN');
            \Log::info($adminMessage);
            \Log::info($adminNumber);
        }
    }

    public function sendRmaProcessingSms($rma){
        $order=$this->orderRepository->findOrFail($rma->order_id);
        $mobile_number=$this->orderRepository->getNumber($rma->order_id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];
        $rma_info = $this->rmaRepository->findById($rma->policy_id);
        
        $message = 'Dear '.$customer_first_name.', Your '.$rma_info->type.' request has been accepted. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }
    }

    public function sendRmaAcceptedSms($rma){
        $order=$this->orderRepository->findOrFail($rma->order_id);
        $mobile_number=$this->orderRepository->getNumber($rma->order_id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];
        $rma_info = $this->rmaRepository->findById($rma->policy_id);
        
        $message = 'Dear '.$customer_first_name.', Your '.$rma_info->type.' request has been accepted. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }
        
    }

    public function sendRmaDeniedSms($rma){
        $order=$this->orderRepository->findOrFail($rma->order_id);
        $mobile_number=$this->orderRepository->getNumber($rma->order_id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];
        $rma_info = $this->rmaRepository->findById($rma->policy_id);
        
        $message = 'Dear '.$customer_first_name.', Your '.$rma_info->type.' request has been denied. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }
    }

    public function sendRmaRefundedSms($rma){
       $order=$this->orderRepository->findOrFail($rma->order_id);
        $mobile_number=$this->orderRepository->getNumber($rma->order_id);
        $customer_first_name = explode(' ', $order->customer_first_name)[0];
        $rma_info = $this->rmaRepository->findById($rma->policy_id);
        
        $message = 'Dear '.$customer_first_name.', Your '.$rma_info->type.' request has been refunded. Please contact us for futher queries.';
        // $comma_seperated_mobile_number = wordwrap($mobile_number, 1,',', true);
         //ADMIN sms infoe
         $adminShortUrlCode = $this->shortUrl(route('rma.requests.index'));

         $adminMessage = 'Dear Admin, Rma request id '.$rma->id.' has been refunded. View the details at '.route('shorten.link', $adminShortUrlCode).'.';
 
         $adminNumber = config('app.admin_mobile_number');
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
            //admin msg
            $this->sparrowSms($adminNumber,$adminMessage);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
            \Log::info('ADMIN');
            \Log::info($adminMessage);
            \Log::info($adminNumber);
        }
    }
    //RMA sms end

    //reset code sms
    public function sendResetCodeSms($confirmation_code,$mobile_number)
    {
        $message =  $confirmation_code.' is your MeMyKids reset code. Please enter the code to complete your password reset.';
       
        if(config('app.env') == 'production'){
            $this->sparrowSms($mobile_number,$message);
        }else if(config('app.env') == 'local'){
            \Log::info($message);
            \Log::info($mobile_number);
        }

    }

    //Sparrow sms 
    public function sparrowSms($mobile_number,$message){
        $args = http_build_query(array(
            'token' => config('app.sparrow_sms_token'),
            'from'  => config('app.sparrow_sms_identity'),
            'to'    => $mobile_number,
            'text'  => $message));

        $url = "http://api.sparrowsms.com/v2/sms/";

        # Make the call using API.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Response
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        \Log::info($response);
    }

    //Generate short URL
    public function shortUrl($link)
    {
        $existingCode = ShortLink::where('link',$link)->first();

        if(!$existingCode){
            $input['link'] = $link;
            $input['code'] = str_random(6);
            ShortLink::create($input);
            return  $input['code'];
        }else{
            return $existingCode->code;
        }
      
        // return redirect('generate-shorten-link')
        //      ->with('success', 'Shorten Link Generated Successfully!');
    }
}