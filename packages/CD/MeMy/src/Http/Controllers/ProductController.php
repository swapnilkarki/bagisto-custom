<?php

namespace CD\MeMy\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Product\Repositories\ProductRepository as Product;
use Cart;

/**
 * Product controller
 *
 * @author    swapnil karki <swapnil@coredreams.com>
 * @copyright 2019 MeMyKids
 */
class ProductController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Product\Repositories\ProductRepository $product
     * @return void
     */
    public function __construct( Product $product)
    {
        $this->product = $product;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $product = $this->product->findBySlugOrFail($slug);

        $customer = auth()->guard('customer')->user();

        if(session()->has('oldCart')){
            Cart::reActivateCart();
        }

        if(session()->has('buyNow')){
            session()->forget('buyNow');
        }

        return view($this->_config['view'], compact('product','customer'));
    }
}
