<?php

namespace CD\MeMy\Http\Controllers;

use CD\MeMy\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CD\MeMy\Models\Tag;
use Cart;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\API\Http\Resources\Catalog\Product as ProductResource;

use CD\Manager\Models\CompanyInfoPages as Pages;

class MeMyController extends Controller
{
        protected $_config;

        protected $tag;

        /**
         * ProductRepository object
         *
         * @var array
         */
        protected $productRepository;
    
        public function __construct(Tag $tag, ProductRepository $productRepository)
        {
            $this->_config = request('_config');
            $this->tag = $tag;
            $this->productRepository = $productRepository;
        }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    
    public function index()
    {
        if(session()->has('oldCart')){
            Cart::reActivateCart();
        }

        if(session()->has('buyNow')){
            session()->forget('buyNow');
        }

        if(!empty(session()->has('error_code')) && session()->get('error_code') == 5){
            if (strpos(url()->previous(), 'memykids') !== false) {
                $intendedUrl = url()->previous();
            } else {
                $intendedUrl = route('memy.index');
            }
            session()->put('url.intended', $intendedUrl);
        }

        return view($this->_config['view']);
    }

    public function aboutUs()
    {
        $pageInfo = Pages::findOrFail(1);
        return view($this->_config['view'])->with('pageInfo',$pageInfo);
    }

    public function contactUs()
    {
        $pageInfo = Pages::findOrFail(2);
        return view($this->_config['view'])->with('pageInfo',$pageInfo);
    }

    public function blog()
    {
        $pageInfo = Pages::findOrFail(3);
        return view($this->_config['view'])->with('pageInfo',$pageInfo);
    }

    public function faq()
    {
        $pageInfo = Pages::findOrFail(4);
        return view($this->_config['view'])->with('pageInfo',$pageInfo);
    }

    public function partners()
    {
        $pageInfo = Pages::findOrFail(5);
        return view($this->_config['view'])->with('pageInfo',$pageInfo);
    }

    
    public function searchTag($term)
    {
        $results = \CD\MeMy\Models\ProductTag::where('tags', 'like', '%' . urldecode($term) . '%')
               ->orderBy('tags', 'asc')
               ->distinct('tags')
               ->get();

        return $results;
    }

    public function searchForTag()
    {
        if (request()->ajax()) {
            $results = [];

            foreach ($this->searchTag(request()->input('query')) as $row) {
                $results[] = [
                        'id' => $row->id,
                        'tag' => $row->tags,
                    ];
            }
            return response()->json($results);
        } else {
            return view($this->_config['view']);
        }
    }

    public function storeTag(Request $request)
    {

        $tag = $request->tags;

        // $productId = $request->productId;
        
        \CD\MeMy\Models\ProductTag::create($request->all());

        if (request()->ajax()) {
            return response()->json($tag);
        }else {
            return view($this->_config['view']);
        }
        
    }

    public function deleteTag(Request $request)
    {
        $product_id = $request->product_id;
        $tag = $request->tag;

        if (request()->ajax()) {
            $productTag = \CD\MeMy\Models\ProductTag::where('product_id', $product_id)
            ->where('tags',$tag)
            ->delete();

            return response()->json($productTag);
        } else {
            return view($this->_config['view']);
        }

    }

    public function searchSuggestion(Request $request){
        if (request()->ajax()) {
            $cars = array("Volvo", "BMW", "Toyota");
            return response()->json(request()->input('query'));
        } else {
            return view($this->_config['view']);
        }
    }

    public function productsOnSale()
    {
        return view($this->_config['view']);
    }

    public function fetchProductsOnSale()
    {
        // return response()->json([
        //     'data' => 'testig testing testing'
        // ]);
        return ProductResource::collection($this->productRepository->getProductsOnSale(request()->input('category_id')));
    }

}
    