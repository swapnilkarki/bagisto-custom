<?php

namespace CD\MeMy\Http\Controllers;

use CD\MeMy\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Webkul\Customer\Repositories\CustomerAddressRepository as Address;


class GoogleMapController extends Controller
{

    /*
        /CustomerAddressRepository object
    */
    protected $address;

    public function __construct(Address $address)
    {
       $this->address = $address;
    }

    public function decodeGeoCode(Request $request)
    {
        $lat = $request['lat'];
        $lng = $request['lng'];

        $latlng = $lat.','.$lng;

        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
        $url = $url.$latlng;
        $url = $url."&key=".config('app.gmaps_api_key');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        if (! $response) {
            return response()->json([
                    'error' => 'Could not fetch data from google api'
                ], 400);
        }

        return $response;

    }
    
    function updateLatLng($id)
    {

        $data = request()->all();

        $addressLatLng = $this->address->updateLatLng($id,$data);

        if(! $addressLatLng){
            return response()->json([
                'error' => 'Could not update data'
            ], 400);

        }

        return response()->json([
            'message' => 'Success'
        ], 200);

    }

}
    