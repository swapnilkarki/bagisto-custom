<?php

namespace CD\MeMy\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

/**
* MeMy service provider
*
* @author   swapnil karki <swapnilk21030@gmail.com>
* @copyright 2019 MeMyKids
*/
class MeMyServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap services.
    *
    * @return void
    */
    public function boot()
  {
    $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');

    $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'memy');

    $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'memy');

    $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

    Event::listen('bagisto.memy.layout.head', function($viewRenderEventManager) {
        $viewRenderEventManager->addTemplate('memy::front.layouts.master');
    });
   
  }

    /**
    * Register services.
    *
    * @return void
    */
    public function register()
    {
     
    }
    
}


