const { mix } = require("laravel-mix");
require("laravel-mix-merge-manifest");

if (mix.inProduction()) {
    var publicPath = 'publishable/assets';
} else {
    var publicPath = "../../../public/vendor/webkul/memy/assets";
}

mix.setPublicPath(publicPath).mergeManifest();

mix.disableNotifications();

mix.js(__dirname + "/src/Resources/assets/js/app.js", "js/memy.js")
    .js(__dirname + "/src/Resources/assets/js/main.js", "js/memy-main.js")
    .js(__dirname + "/src/Resources/assets/js/ease-scroll.js", "js/memy-scroll.js")
    .js(__dirname + "/src/Resources/assets/js/megamenu.js", "js/memy-megamenu.js")
    .js(__dirname + "/src/Resources/assets/js/zoom-image.js", "js/zoom-image.js")
    .js(__dirname + "/src/Resources/assets/js/product-zoom.js", "js/product-zoom.js")
    .copyDirectory(__dirname + '/src/Resources/assets/images', publicPath + '/images')
    .sass(__dirname + "/src/Resources/assets/sass/slick.scss", "css/memy-slick.css")
    .sass(__dirname + "/src/Resources/assets/sass/slick-theme.scss", "css/memy-slick-theme.css")
    .sass(__dirname + "/src/Resources/assets/sass/megamenu.scss", "css/memy-megamenu.css")
    .sass(__dirname + "/src/Resources/assets/sass/master.scss", "css/memy-master.css")
    .sass(__dirname + "/src/Resources/assets/sass/resposive.scss", "css/memy-responsive.css")
    // .sass(__dirname + "/src/Resources/assets/sass/product-zoom.scss", "css/product-zoom.css")
    .sass(__dirname + "/src/Resources/assets/sass/flash-wrapper.scss", "css/flash-wrapper.css")
    .sass(__dirname + "/src/Resources/assets/sass/bootstrap.scss", "css/bootstrap.css")
    // .sass(__dirname + "/src/Resources/assets/sass/s.scss", "css/ion-icons.css")
    .sass(__dirname + "/src/Resources/assets/sass/font-awesome.scss", "css/font-awesome.css")
    .options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}