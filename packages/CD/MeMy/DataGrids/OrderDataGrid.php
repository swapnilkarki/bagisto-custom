<?php

namespace CD\MeMy\DataGrids;

use CD\MeMy\DataGrid\DataGrid;
use DB;

/**
 * OrderDataGrid class
 *
 * @author swapnil karki <swapnil@coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class OrderDataGrid extends DataGrid
{
    protected $index = 'id'; //the column that needs to be treated as index column

    protected $sortOrder = 'desc'; //asc or desc

    public function prepareQueryBuilder()
    {
        // limit concat string length
        // SUBSTR(GROUP_CONCAT(t.name),1,30)
        $queryBuilder = DB::table('orders as order')
                ->join(DB::raw("(SELECT t.order_id, GROUP_CONCAT(t.name) name FROM order_items t GROUP BY t.order_id) as order_items"),function($join){
                  $join->on("order_items.order_id","=","order.id"); })
                ->addSelect('order.id', 'order.status', 'order.created_at', 'order.updated_at', 'order.grand_total','order_items.name')
                ->where('customer_id', auth()->guard('customer')->user()->id);

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('shop::app.customer.account.order.index.order_id'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => 'Product Name',
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => 'Order Date',
            'type' => 'datetime',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

       

        $this->addColumn([
            'index' => 'status',
            'label' => trans('shop::app.customer.account.order.index.status'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'closure' => true,
            'wrapper' => function ($value) {
                if ($value->status == 'processing')
                    return '<span class="badge badge-md badge-info">Processing</span>';
                else if ($value->status == 'completed')
                    return '<span class="badge badge-md badge-success">Completed</span>';
                else if ($value->status == "canceled")
                    return '<span class="badge badge-md badge-danger">Canceled</span>';
                else if ($value->status == "closed")
                    return '<span class="badge badge-md badge-info">Closed</span>';
                else if ($value->status == "pending")
                    return '<span class="badge badge-md badge-warning">Pending</span>';
                else if ($value->status == "pending_payment")
                    return '<span class="badge badge-md badge-warning">Pending Payment</span>';
                else if ($value->status == "fraud")
                    return '<span class="badge badge-md badge-danger">Fraud</span>';
                    else if ($value->status == "shipped")
                return '<span class="badge badge-md badge-info">Shipped</span>';
            },
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'updated_at',
            'label' => 'Status Date',
            'type' => 'datetime',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'grand_total',
            'label' => trans('shop::app.customer.account.order.index.total'),
            'type' => 'price',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'View',
            'method' => 'GET',
            'route' => 'memy.orders.view',
            'icon' => 'custom-icon eye-icon'
        ]);
    }
}