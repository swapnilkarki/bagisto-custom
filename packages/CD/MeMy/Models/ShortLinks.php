<?php

namespace CD\MeMy\Models;

use Illuminate\Database\Eloquent\Model;

class ShortLinks extends Model
{
    protected $fillable = ['code', 'link'];
}
