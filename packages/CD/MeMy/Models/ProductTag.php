<?php

namespace CD\MeMy\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    protected $fillable = ['product_id','tags'];

    public function products(){
        return $this->hasMany('\Webkul\Product\Models\Product');
    }
}
