<?php

namespace CD\MeMy\Models;

use Illuminate\Database\Eloquent\Model;

class SocialLoginTable extends Model
{
    protected $fillable = ['user_id','name','email','social_id','type','avatar','user_data'];
}
