<?php

namespace CD\Manager\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

/**
* Manager service provider
*
* @author    swapnil karki<swapnil@coredreams.com>
* @copyright 2019 Coredreams Innovation Pvt Ltd (http://www.coredreams.com)
*/
class ManagerServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap services.
    *
    * @return void
    */
    public function boot()
    {
        include __DIR__ . '/../Http/routes.php';

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'manager');

        Event::listen('bagisto.admin.layout.head', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('manager::layouts.style');
        });

        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

    }

    /**
    * Register services.
    *
    * @return void
    */
    public function register()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/menu.php', 'menu.admin'
        );
    }
}

