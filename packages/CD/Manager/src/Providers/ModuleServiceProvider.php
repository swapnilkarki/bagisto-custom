<?php

namespace CD\Manager\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \CD\Manager\Models\CompanyInfoPages::class,
        \CD\Manager\Models\Faq::class,
    ];
}