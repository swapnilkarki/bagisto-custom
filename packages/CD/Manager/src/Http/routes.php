<?php
Route::group(['middleware' => ['web']], function () {
    //company info pages 
    Route::prefix('admin/company-info')->group(function () {
        // company info index
        Route::get('/', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@index')->defaults('_config', ['view' => 'manager::admin.sections.company-info.index','section' => 'company-info'
        ])->name('company-info.index');

        //update status
        Route::put('/update/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@statusUpdate')->name('company-info.statusUpdate');

        //edit page open
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@editPage')->defaults('_config', ['view' => 'manager::admin.sections.company-info.edit'
        ])->name('company-info.edit');
        //update Page
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@pageUpdate')->defaults('_config',['section' => 'company-info'])->name('company-info.update');

        //create page open
        Route::get('/create', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@newPage')->defaults('_config', ['view' => 'manager::admin.sections.company-info.create'
        ])->name('company-info.create');
        //create page action
        Route::put('/create', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@createPage')->defaults('_config',['section' => 'company-info'])->name('company-info.create');

        //Delete Page
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@pageDelete')->defaults('_config',['section' => 'company-info'])->name('company-info.delete');

    });

    //shipping and policy pages
    Route::prefix('admin/shipping-and-policy')->group(function () {
        //shipping and policy
        Route::get('/', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@index')->defaults('_config', ['view' => 'manager::admin.sections.shipping-and-policy.index','section' => 'shipping-and-policy'
        ])->name('shipping-and-policy.index');

        //create Page
        Route::get('/create', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@newPage')->defaults('_config', ['view' => 'manager::admin.sections.shipping-and-policy.create'
        ])->name('shipping-and-policy.create');
        // create page action
        Route::put('/create', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@createPage')->defaults('_config',['section' => 'shipping-and-policy'])->name('shipping-and-policy.create');

        //edit page open
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@editPage')->defaults('_config', ['view' => 'manager::admin.sections.shipping-and-policy.edit'
        ])->name('shipping-and-policy.edit');
        //edit page action
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@pageUpdate')->defaults('_config',['section' => 'shipping-and-policy'])->name('shipping-and-policy.update');

        //Delete Page
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@pageDelete')->defaults('_config',['section' => 'shipping-and-policy'])->name('shipping-and-policy.delete');
    });

    //faqs
    Route::prefix('admin/faqs')->group(function () {
        //Faqs
        Route::get('/', 'CD\Manager\Http\Controllers\FaqController@index')->defaults('_config', ['view' => 'manager::admin.sections.faq.index'
        ])->name('faq.index');

        //edit FAQ open
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\FaqController@edit')->defaults('_config', ['view' => 'manager::admin.sections.faq.edit'
        ])->name('faq.edit');
        //edit FAQ action
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\FaqController@update')->name('faq.update');

        //create FAQ
        Route::get('/create', 'CD\Manager\Http\Controllers\FaqController@create')->defaults('_config', ['view' => 'manager::admin.sections.faq.create'
        ])->name('faq.create');
        // create FAQ action
        Route::put('/create', 'CD\Manager\Http\Controllers\FaqController@createFaq')->name('faq.create');

        //Delete FAQ
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\FaqController@delete')->name('faq.delete');

    });

    //customer queries
    Route::prefix('admin/customer-queries')->group(function () {
        //customer queries
        Route::get('/', 'CD\Manager\Http\Controllers\CustomerQueriesController@index')->defaults('_config', ['view' => 'manager::admin.sections.customer-queries.index'
        ])->name('customer-queries.index');

        //update status
        Route::put('/update/{id}', 'CD\Manager\Http\Controllers\CustomerQueriesController@statusUpdate')->name('customer-queries.statusUpdate');

        //customer queries action
        Route::put('/create', 'CD\Manager\Http\Controllers\CustomerQueriesController@askQuestion')->name('customer-queries.create');

        //edit FAQ open
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\CustomerQueriesController@edit')->defaults('_config', ['view' => 'manager::admin.sections.customer-queries.edit'
        ])->name('customer-queries.edit');
        //update customer query
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\CustomerQueriesController@update')->name('customer-queries.update');

        //Delete customer queries
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\CustomerQueriesController@delete')->name('customer-queries.delete');
    });

    //contact-us
    Route::prefix('admin/contact-us')->group(function () {
        //contact us
        Route::get('/', 'CD\Manager\Http\Controllers\ContactUsController@index')->defaults('_config', ['view' => 'manager::admin.sections.contact-us.index'
        ])->name('contact-us.index');

        //edit 
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\ContactUsController@edit')->defaults('_config', ['view' => 'manager::admin.sections.contact-us.edit'
        ])->name('contact-us.edit');
        //update customer question
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\ContactUsController@update')->name('contact-us.update');

        //Delete customer question
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\ContactUsController@delete')->name('contact-us.delete');

        //update status
        Route::put('/update/{id}', 'CD\Manager\Http\Controllers\ContactUsController@statusUpdate')->name('contact-us.statusUpdate');

        //contact-us question 
        Route::put('/postQuestion', 'CD\Manager\Http\Controllers\ContactUsController@postQuestion')->name('contact-us.postQuestion');
    });

    Route::prefix('admin/create-email')->group(function(){
        //create email index
        Route::get('/', 'CD\Manager\Http\Controllers\EmailController@index')->defaults('_config', ['view' => 'manager::admin.customers.email.index'
        ])->name('admin.customers.create-email.index');    

        //edit 
        Route::get('/edit/{id}', 'CD\Manager\Http\Controllers\EmailController@edit')->defaults('_config', ['view' => 'manager::admin.customers.email.edit'
        ])->name('admin.customers.create-email.edit');
        //update customer query
        Route::put('/edit/{id}', 'CD\Manager\Http\Controllers\EmailController@update')->name('admin.customers.create-email.update');

        //create  open
        Route::get('/create', 'CD\Manager\Http\Controllers\EmailController@newTemplate')->defaults('_config', ['view' => 'manager::admin.customers.email.create'
        ])->name('admin.customers.create-email.create');
        //create  action
        Route::put('/create', 'CD\Manager\Http\Controllers\EmailController@createTemplate')->name('admin.customers.create-email.create');

        //Delete
        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\EmailController@delete')->name('admin.customers.create-email.delete');

        //upload image
        Route::post('/upload/image', 'CD\Manager\Http\Controllers\EmailController@uploadImage');

        //preview
        Route::get('/preview/{id}', 'CD\Manager\Http\Controllers\EmailController@preview')->defaults('_config', ['view' => 'manager::admin.customers.email.preview'
        ])->name('admin.customers.create-email.preview');
    });

    Route::prefix('admin/subscribers')->group(function(){
        //create email 
        Route::get('/create-email', 'CD\Manager\Http\Controllers\SubscriberController@create')->defaults('_config', ['view' => 'manager::admin.subscribers.create'
        ])->name('admin.customers.subscribers.create');    

        //send mail
        Route::put('/create', 'CD\Manager\Http\Controllers\SubscriberController@sendMail')->name('admin.customers.subscribers.send-mail');

    });

    Route::prefix('admin/testimonials')->group(function(){
        // company info index
        Route::get('/', 'CD\Manager\Http\Controllers\TestimonialController@index')->defaults('_config', ['view' => 'manager::admin.customers.testimonial.index'])->name('testimonial.index');

        // create mail
        Route::get('/create', 'CD\Manager\Http\Controllers\TestimonialController@create')->defaults('_config', ['view' => 'manager::admin.customers.testimonial.create'])->name('admin.customers.testimonial.create');

        Route::put('/create', 'CD\Manager\Http\Controllers\TestimonialController@create')->name('admin.customers.testimonial.create-new');

        Route::get('/update/{id}', 'CD\Manager\Http\Controllers\TestimonialController@update')->defaults('_config', ['view' => 'manager::admin.customers.testimonial.edit'])->name('admin.customers.testimonial.edit');

        Route::put('/update/{id}', 'CD\Manager\Http\Controllers\TestimonialController@update')->name('admin.customers.testimonial.update');


        Route::put('/status-update/{id}', 'CD\Manager\Http\Controllers\TestimonialController@statusUpdate')->name('admin.customers.testimonial.status-update');

        Route::get('/delete/{id}', 'CD\Manager\Http\Controllers\TestimonialController@destroy')->name('admin.customers.testimonial.delete');

    });



    // shipping-and-policy pages render
    Route::get('memykids/shipping-and-policy/{slug}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@renderPage')->name('shipping-and-policy.page.render');
    //company pages pages render 
    Route::get('memykids/company-info/{slug}', 'CD\Manager\Http\Controllers\CompanyInfoPagesController@renderPage')->name('company-info.page.render');
    //faq
    Route::get('memykids/faq', 'CD\Manager\Http\Controllers\FaqController@renderPage')->name('faq.page.render');
    //testimonial
    Route::get('api/testimonials', 'CD\Manager\Http\Controllers\TestimonialController@getApprovedTestimonails');
});