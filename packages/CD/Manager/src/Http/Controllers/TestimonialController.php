<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\CustomerTestimonial as Testimonial;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['getApprovedTestimonails']]);
        $this->_config = request('_config');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {
        $testimonials = Testimonial::all()->sortByDesc('id');
        return view($this->_config['view'])->with('testimonials', $testimonials);
    }

    //add new testimonial
    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->_config['view']);
        }
        else if ($request->isMethod('put')) {

            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'rating' => 'required ',
                'statement' => 'required',
                'approved' => 'required'
            ]);

            if ($validator->fails()) {
                session()->flash('error', 'Form validation failed.');
                return redirect()->back()->withInput();
           }

            $testimonial=Testimonial::create($request->all());

            if($testimonial){
                session()->flash('success', 'Testimonial Added Sucessfully');
                return redirect()->route('testimonial.index');
            }else{
                session()->flash('error', 'Something went wrong');
                return redirect()->back()->withInput();
            }
        }
    }

    //update approval status of tetimonial
    public function statusUpdate($id, Request $request)
    {
        $testimonial=Testimonial::findOrFail($id);

        $testimonial->approved = request('approved');
    
        $testimonial->save();
    
        session()->flash('success','Status Updated.');
    
        return redirect()->back();
    }

    //uodate/edit testimonial
    public function update($id,Request $request)
    {
        if ($request->isMethod('get')) {
            $testimonial=Testimonial::findOrFail($id);
            return view($this->_config['view'])->with('testimonial',$testimonial);
        }
        else if ($request->isMethod('put')) {

            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'rating' => 'required ',
                'statement' => 'required',
                'approved' => 'required'
            ]);

            if ($validator->fails()) {
                session()->flash('error', 'Form validation failed.');
                return redirect()->back()->withInput();
           }

           $testimonial=Testimonial::findOrFail($id);
           $testimonial = $testimonial->update($request->all());

            if($testimonial){
                session()->flash('success', 'Testimonial Updated Sucessfully');
                return redirect()->route('testimonial.index');
            }else{
                session()->flash('error', 'Something went wrong');
                return redirect()->back()->withInput();
            }
        }

    }

    //delete testimonial
    public function destroy($id)
    {
        $testimonial=Testimonial::findOrFail($id);
  
        if($testimonial){
            if($testimonial->approved == 1){
                session()->flash('error', 'Cannot remove active testimonial');
        
                return redirect()->route('testimonial.index');
            }
            $testimonial->delete();
            session()->flash('success', 'Testimonial Removed Successfully');
        
            return redirect()->route('testimonial.index');
        }else{
            session()->flash('error', 'Something went wrong');
        
            return redirect()->route('testimonial.index');
        }
       
    }

    public function getApprovedTestimonails()
    {
        $testimonials = Testimonial::where('approved',1)
                                    ->orderBy('created_at', 'desc')
                                    ->take(10)
                                    ->get();
        return response()->json([
            'data' => $testimonials
        ]);
    }
}
