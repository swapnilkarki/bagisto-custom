<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\CustomerQueries as Queries;
use Illuminate\Support\Facades\Validator;

class CustomerQueriesController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin',['except' => ['askQuestion']]);
        $this->_config = request('_config');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    $queries = Queries::all()->sortByDesc('id');
    return view($this->_config['view'])->with('queries',$queries);
}

public function askQuestion(Request $request)
{
    $captchValidation = core()->captchaValidation($request['g-recaptcha-response']);

    if(!$captchValidation){
        session()->flash('error', 'Captcha Validation failed.');
        return redirect()->back();
    }


    $data = $request->all();

    Queries::create($data);
    
    session()->flash('success', 'Question placed Sucessfully');
    
    return redirect()->back();
}

public function edit($id){
    $query = Queries::findOrFail($id);
    return view($this->_config['view'])->with('query',$query);
}

public function update($id){
    $query = Queries::findOrFail($id);

    $query->question = request('question');
    $query->answer = request('answer');
    $query->approved = request('approved');

    $query->save();

    session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Query']));
 
    return redirect()->route('customer-queries.index');
}

public function delete($id)
{
    $query = Queries::findOrFail($id);
  
    $query->delete();
    session()->flash('success', 'Customer Query Removed Successfully');

    return redirect()->route('customer-queries.index');
}

public function statusUpdate(Request $request,$id){

    $query = Queries::findOrFail($id);

    $query->approved = request('approved');

    $query->save();

    session()->flash('success','Approve Status Updated.');

    return redirect()->back();

}


}
