<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\CustomerQueries as Queries;
use Illuminate\Support\Facades\Validator;
use CD\Manager\Models\ContactUs as contactUs;

class ContactUsController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['postQuestion']]);
        $this->_config = request('_config');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    $questions = contactUs::all()->sortByDesc('id');;
    return view($this->_config['view'])->with('questions',$questions);
}

public function edit($id)
{
    $questions = contactUs::findOrFail($id);
    return view($this->_config['view'])->with('questions',$questions);
}

public function update($id)
{
    $questions = contactUs::findOrFail($id);

    $questions->remarks = request('remarks');
    $questions->status = request('status');

    $questions->save();

    session()->flash('success', trans('admin::app.response.update-success', ['name' => 'customer question']));
 
    return redirect()->route('contact-us.index');
}

public function delete($id)
{
    $questions = contactUs::findOrFail($id);
  
    $questions->delete();
    session()->flash('success', 'Customer Question Was Removed Successfully');

    return redirect()->route('contact-us.index');
}

public function postQuestion(Request $request)
{
    $validator = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required | email',
        'phone' => 'required | numeric',
        'subject' => 'required',
        'comments' => 'required',
        'g-recaptcha-response' => 'required'
    ]);

    if ($validator->fails()) {
        // Session::flash('error', $validator->messages()->first());
        session()->flash('error', 'Something went wrong.');

        return redirect()->back()->withInput();
   }

   $captchValidation = core()->captchaValidation($request['g-recaptcha-response']);

   if(!$captchValidation){
    session()->flash('error', 'Captcha Validation failed.');
    return redirect()->back();
    }

    $data = $request->all();
    contactUs::create($data);
    session()->flash('success', 'Question Posted Sucessfully.');
    return redirect()->route('company-info.page.render', 'contact-us');
    
}

public function statusUpdate(Request $request,$id){

    $page = contactUs::findOrFail($id);

    $page->status = request('status');

    $page->save();


    session()->flash('success','Status Updated.');

    return redirect()->back();

}

}
