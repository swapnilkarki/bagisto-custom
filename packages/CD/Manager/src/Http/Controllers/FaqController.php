<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\Faq as faqs;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['renderPage']]);
        $this->_config = request('_config');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    $faqs = faqs::all();
    return view($this->_config['view'])->with('faqs',$faqs);
}

public function create(){
    return view($this->_config['view']);
}

public function createFaq(Request $request)
{

    $data = $request->all();

    faqs::create($data);
    
    session()->flash('success', 'FAQ Created Sucessfully');
    
    return redirect()->route('faq.index');
   
}

public function edit($id){
    $faq = faqs::findOrFail($id);
    return view($this->_config['view'])->with('faq',$faq);
}

public function update($id){
    $faq = faqs::findOrFail($id);

    $faq->question = request('question');
    $faq->answer = request('answer');

    $faq->save();

    session()->flash('success', trans('admin::app.response.update-success', ['name' => 'FAQ']));
 
    return redirect()->route('faq.index');
    
}

public function delete($id)
{
    $faq = faqs::findOrFail($id);
  
    $faq->delete();
    session()->flash('success', 'Faq Removed Successfully');

    return redirect()->route('faq.index');
}

public function renderPage()
{
    
    $faqs = faqs::all();
     
    return view('memy::layouts.footer.faq.index')->with('faqs',$faqs);

}
}
