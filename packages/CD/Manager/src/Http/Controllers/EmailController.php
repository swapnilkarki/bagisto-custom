<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use CD\Manager\Models\EmailTemplate;

class EmailController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin');
        $this->_config = request('_config');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    $templates = EmailTemplate::all()->sortByDesc('id');
    return view($this->_config['view'])->with('templates',$templates);
}

public function edit($id){
    $template = EmailTemplate::findOrFail($id);
    return view($this->_config['view'])->with('template',$template);
}

public function update($id){
    $template = EmailTemplate::findOrFail($id);

    $template->title = request('title');
    $template->type = request('type');
    $template->template = request('template');

    $template->save();

    session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Template']));
 
    return redirect()->route('admin.customers.create-email.index');
    
}


public function newTemplate()
{
    return view($this->_config['view']);
}

public function createTemplate(Request $request)
{
    $data = $request->all();
    EmailTemplate::create($data);
    session()->flash('success', 'Template Created Sucessfully');

    return redirect()->route('admin.customers.create-email.index');
    
}

public function delete($id)
{
    $template = EmailTemplate::findOrFail($id);

    $template->delete();
    session()->flash('success', 'Template Deleted Successfully');
  
    return redirect()->back();
}

public function uploadImage(Request $request)
{
    $this->validate($request, [
        'file' => 'required|image|mimes:jpeg,png,jpg,bmp,gif,svg',
      ]);
      if ($request->hasFile('file')) {
        $image = $request->file('file');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/storage/uploads/');
        $image->move($destinationPath, $name);

        $imgpath = '/storage/uploads/' . $name;

        return response()->json(['location' => $imgpath]);
      }
}

public function preview($id){
    $template = EmailTemplate::findOrFail($id);
    return view($this->_config['view'])->with('template',$template);
}


}