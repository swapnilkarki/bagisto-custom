<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\EmailTemplate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use CD\Manager\Mail\NewsLetterMail;

use Webkul\Core\Repositories\SubscribersListRepository as Subscribers;

use CD\Manager\Jobs\SendNewsLetter;

class SubscriberController extends Controller
{
    protected $_config;

    public function __construct(Subscribers $subscribers)
    {
        $this->middleware('admin');
        $this->_config = request('_config');
        $this->subscribers = $subscribers;
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function create()
{
    $templates = EmailTemplate::all();
    return view($this->_config['view'])->with('templates',$templates);
}

public function sendMail(Request $request){

    $template = $request->all();

    $validator = Validator::make($request->all(), [
        'title' => 'required',
        'subject' => 'required ',
        'template' => 'required '
    ]);

    if ($validator->fails()) {
        // Session::flash('error', $validator->messages()->first());
        session()->flash('error', 'Missing Fields');

        return redirect()->back()->withInput();
   }
   
    $subscribers = $this->subscribers->getSubscribed();

    try {
        foreach($subscribers as $subscriber){
            $emailJob = new SendNewsLetter($subscriber,$template);
            dispatch($emailJob);
        }
        session()->flash('success', 'Mail sent');
    } catch (\Exception $e) {
        session()->flash('error', trans('Mail send failed'));
    }

    return redirect()->route('admin.customers.subscribers.index');
}

}