<?php

namespace CD\Manager\Http\Controllers;

use Illuminate\Http\Request;
use CD\Manager\Models\CompanyInfoPages as Pages;
use Illuminate\Support\Facades\Validator;

class CompanyInfoPagesController extends Controller
{
    protected $_config;

    public function __construct()
    {
        $this->middleware('admin', ['except' => ['renderPage']]);
        $this->_config = request('_config');
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    if($this->_config['section'] === 'company-info'){

        $pageInfos = Pages::all()->where('section','company-info')->sortByDesc('id');

    }else if($this->_config['section'] ==='shipping-and-policy'){

        $pageInfos = Pages::all()->where('section','shipping-and-policy')->sortByDesc('id');
     
    }

    return view($this->_config['view'])->with('pageInfos',$pageInfos);
}

public function editPage($id)
{
    $pageInfo = Pages::findOrFail($id);
    return view($this->_config['view'])->with('pageInfo',$pageInfo);
}

public function pageUpdate(Request $request,$id)
{
    $page = Pages::findOrFail($id);

    $page->title = request('title');
    $page->slug = request('slug');
    $page->status = request('status');
    $page->page_content = request('page_content');
    $page->meta_title = request('meta_title');
    $page->meta_description = request('meta_description');
    $page->meta_keywords = request('meta_keywords');

    $page->save();

    session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Page']));

    if($this->_config['section'] === 'company-info'){
        return redirect()->route('company-info.index');
    }else if($this->_config['section'] ==='shipping-and-policy'){
        return redirect()->route('shipping-and-policy.index');
    }
    
}

public function newPage()
{
    return view($this->_config['view']);
}

public function createPage(Request $request)
{
    $validator = Validator::make($request->all(), [
        'slug' => 'required|unique:company_info_pages'
    ]);

    if ($validator->fails()) {
        // Session::flash('error', $validator->messages()->first());
        session()->flash('error', 'Slug already exists');

        return redirect()->back()->withInput();
   }

    $data = $request->all();
    $data['section'] = $this->_config['section'];
    Pages::create($data);
    
    session()->flash('success', 'Page Created Sucessfully');
    
    if($this->_config['section'] === 'company-info'){
        return redirect()->route('company-info.index');
    }else if($this->_config['section'] ==='shipping-and-policy'){
        return redirect()->route('shipping-and-policy.index');
    }
    
}

public function pageDelete($id)
{
    $page = Pages::findOrFail($id);
    if($page->status != 'active'){
        $page->delete();
        session()->flash('success', 'Page Deleted Successfully');
    }else{
        session()->flash('error', 'Cannot Delete Active Page');
    }

    if($this->_config['section'] === 'company-info'){
        return redirect()->route('company-info.index');
    }else if($this->_config['section'] ==='shipping-and-policy'){
        return redirect()->route('shipping-and-policy.index');
    }
}

public function renderPage($slug)
{
    
    $pageInfo = Pages::all()->where('slug',$slug)->first();

    if($slug == 'contact-us'){
        return view('memy::layouts.footer.contact-us.index')->with('pageInfo',$pageInfo);
    }else{
        return view('manager::front.footer.sections.pageRender')->with('pageInfo',$pageInfo);
    }
}


public function statusUpdate(Request $request,$id){

    $page = Pages::findOrFail($id);

    $page->status = request('status');

    $page->save();

    session()->flash('success','Status Updated.');

    return redirect()->back();

}

}
