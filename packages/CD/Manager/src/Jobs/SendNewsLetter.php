<?php

namespace CD\Manager\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use CD\Manager\Mail\NewsLetterMail;

class SendNewsLetter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $subscriber,$template;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subscriber,$template)
    {
       $this->subscriber = $subscriber;
       $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new NewsLetterMail($this->template);
  
        try {
            Mail::to($this->subscriber)->send($email);
            //for mailtrap (has limit 2 mails per 10sec) 
            sleep(5);
        }catch(\Exception $e){
            \Log::warning($e);
        }
    }
}
