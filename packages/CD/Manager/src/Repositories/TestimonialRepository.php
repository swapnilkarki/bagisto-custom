<?php

namespace CD\Manager\Repositories;

use Webkul\Core\Eloquent\Repository;

class TestimonialRepository extends Repository
{
   /**
    * Specify Model class name
    *
    * @return mixed
    */
    function model()
    {
        return 'CD\Manager\Models\CustomerTestimonial';
    }

    
      /**
     * @param array $data
     * @return mixed
     */

    public function getApprovedTestimonails()
    {
        return $this->model->where('approved',1)
                                    ->orderBy('created_at', 'desc')
                                    ->take(10)
                                    ->get();
        // return response()->json([
        //     'data' => $testimonials
        // ]);

        // return $productPolicies;
    }
}