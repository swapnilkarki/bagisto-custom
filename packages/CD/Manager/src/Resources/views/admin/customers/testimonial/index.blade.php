@extends('admin::layouts.content')

@section('page_title')
    Customer Testimonials
@stop

@push('css')
    <style>
        .slider:after {
            content: "False";
        }
        input:checked + .slider:after {
            content: "True";
        }
    </style>
@endpush

@section('content')

<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>Customer Testimonials</h1>
        </div>
        <div class="page-action">
            <a href="{{route('admin.customers.testimonial.create')}}" class="btn btn-lg btn-primary">
                New
            </a>
        </div>
    </div>

    <div class="page-content">
        <div class="table">
            <table class="table">
                <thead>
                    <tr style="height: 65px;">
                    <th class="grid_head">Id</th>
                    <th class="grid_head">Name</th>
                    <th class="grid_head">Rating</th>
                    <th class="grid_head">Statement</th>
                    <th class="grid_head">Approved</th>
                    <th class="grid_head">Date</th>
                    <th class="grid_head">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($testimonials as $testimonial)
                    <tr>
                        <td data-value="id">{{$testimonial->id}}</td>
                        <td data-value="Title">{{$testimonial->full_name}}</td>

                        <td data-value="Rating" style="width:120px;">
                            @for($i=0 ; $i<$testimonial->rating ; $i++)
                            <span class="icon star-blue-icon"></span>
                            @endfor
                        </td>

                        <td data-value="Statement">{{$testimonial->statement}}</td>

                        <td data-value="Approved">
                            <form method="POST" action="{{route('admin.customers.testimonial.status-update',$testimonial->id)}}" ref="statusUpdateForm{{$testimonial->id}}" id="statusUpdateForm{{$testimonial->id}}">
                                @csrf()
                                <input name="_method" type="hidden" value="PUT">
                                <label class="switch">
                                <input type="checkbox" {{$testimonial->approved == '1' ? 'checked' : ''}} name="approved" value="{{$testimonial->approved == '1' ? '0' : '1'}}" onclick="document.getElementById('statusUpdateForm{{$testimonial->id}}').submit();">
                                <span class="slider round "></span>
                                </label>
                            </form>
                        </td>

                        <td data-value="Created_At">{{$testimonial->created_at}}</td>

                        <td class="actions" data-value="Actions">
                        <a href="{{route('admin.customers.testimonial.edit', $testimonial->id)}}"><span class="icon pencil-lg-icon"></span></a>
                        <a href="{{route('admin.customers.testimonial.delete', $testimonial->id)}}"><span class="icon trash-icon"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>     
     
@endsection