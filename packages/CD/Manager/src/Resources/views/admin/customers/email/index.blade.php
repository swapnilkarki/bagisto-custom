@extends('admin::layouts.content')

@section('page_title')
    Email Templates
@stop

@push('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> --}}
<link rel="stylesheet" type="text/css" href=" {{ asset('vendor/webkul/manager/assets/css/jquery.modal.css') }} ">
@endpush

@section('content')

<div class="content">
    <div class="page-header">
        <div class="page-title">
            <h1>Email Templates</h1>
        </div>
        <div class="page-action">
        <a href="{{route('admin.customers.create-email.create')}}" class="btn btn-lg btn-primary">
                New Mail
            </a>
        </div>
    </div>

    <div class="page-content">
        <div class="table">
            <table class="table">
                <thead>
                    <tr style="height: 65px;">
                    <th class="grid_head">id</th>
                    <th class="grid_head">Title</th>
                    <th class="grid_head">type</th>
                    <th class="grid_head">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($templates as $template)
                    <tr>
                        <td data-value="id">{{$template->id}}</td>
                        <td data-value="Title">{{$template->title}}</td>
                        <td data-value="Type">{{$template->type}}</td>
                        <td class="actions" data-value="Actions">
                        <a href="{{route('admin.customers.create-email.preview', $template->id)}}" rel="modal:open"><span class="icon eye-icon"></span></a>
                        <a href="{{route('admin.customers.create-email.edit', $template->id)}}"><span class="icon pencil-lg-icon"></span></a>
                        <a href="{{route('admin.customers.create-email.delete', $template->id)}}"><span class="icon trash-icon"></span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>     
     
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
@endpush