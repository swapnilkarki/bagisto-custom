@extends('admin::layouts.content')

@section('page_title')
   Edit | {{$template->title}}
@stop

@section('content')
<div class="content">
        <form method="POST" action="{{ route('admin.customers.create-email.update', $template->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit {{$template->title}} Template
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content" style="padding-left: 25px">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">Title</label>
                        <input type="text" v-validate="'required'" class="control" id="title" name="title" data-vv-as="&quot;'Title'&quot;" value="{{ $template->title }}"/>
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('Title') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('type') ? 'has-error' : '']">
                        <label for="type" class="required">Type</label>
                        <input type="text" v-validate="'required'" class="control" id="type" name="type" data-vv-as="&quot;'type'&quot;" value="{{ $template->type }}"/>
                        <span class="control-error" v-if="errors.has('type')">@{{ errors.first('Type') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('template') ? 'has-error' : '']">
                        <label for="template" class="required">Template</label>
                        <textarea class="control" v-validate="'required'" id="template" name="template" data-vv-as="&quot;'template'&quot;" >{{$template->template}}</textarea>
                        <span class="control-error" v-if="errors.has('template')">@{{ errors.first('template') }}</span>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
@stop



@push('scripts')
<script src="https://cdn.tiny.cloud/1/b1fizmy28j6qtvhsbj7z8pm0fjww8k1qj2i48p7198l359m4/tinymce/5/tinymce.min.js"></script> 

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#template',
                height: 500,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code preview autosave ',
                toolbar1: 'formatselect | preview restoredraft| bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                valid_elements : '*[*]',
                images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '/admin/create-email/upload/image');
                var token = '{{ csrf_token() }}';
                xhr.setRequestHeader("X-CSRF-Token", token);
                xhr.onload = function() {
                    var json;
                    if (xhr.status != 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    success(json.location);
                };
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            }
            });
        });
    </script>
@endpush
