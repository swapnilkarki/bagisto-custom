@extends('admin::layouts.content')

@section('page_title')
    Customer Testimonials | Create
@stop

@section('content')

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.customers.testimonial.create-new') }}"  @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                        Add New Testimonial
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('admin::app.account.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('full_name') ? 'has-error' : '']">
                        <label for="full_name" class="required">Full Name</label>
                        <input type="text" v-validate="'required'" class="control" id="full_name" name="full_name" data-vv-as="&quot;'full_name'&quot;" value="{{old('full_name')}}"/>
                        <span class="control-error" v-if="errors.has('full_name')">@{{ errors.first('full_name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('rating') ? 'has-error' : '']">
                        <label for="rating" class="required">Rating</label>
                        <?php $selectedOption = old('rating') ?: 'inactive' ?>
                        <select v-validate="'required'" class="control" id="rating" name="rating" data-vv-as="&quot;'rating'&quot;">
                            <option value="1" {{ $selectedOption == '1' ? 'selected' : '' }}>
                                1
                            </option>
                            <option value="2" {{ $selectedOption == '2' ? 'selected' : '' }}>
                                2
                            </option>
                            <option value="3" {{ $selectedOption == '3' ? 'selected' : '' }}>
                                3
                            </option>
                            <option value="4" {{ $selectedOption == '4' ? 'selected' : '' }}>
                                4
                            </option>
                            <option value="5" {{ $selectedOption == '5' ? 'selected' : '' }}>
                                5
                            </option>
                        </select>
                        <span class="control-error" v-if="errors.has('rating')">@{{ errors.first('rating') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('statement') ? 'has-error' : '']">
                        <label for="statement" class="required">Statement</label>
                        <textarea type="text" v-validate="'required'" class="control" id="statement" name="statement" data-vv-as="&quot;'statement'&quot;">{{old('statement')}}</textarea>
                        <span class="control-error" v-if="errors.has('statement')">@{{ errors.first('statement') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('approved') ? 'has-error' : '']">
                        <label for="approved" class="required">Approved</label>
                        <?php $selectedOption = old('approved') ?: '0' ?>
                        <select v-validate="'required'" class="control" id="approved" name="approved" data-vv-as="&quot;'approved'&quot;">
                            <option value="1" {{ $selectedOption == '1' ? 'selected' : '' }}>
                                True
                            </option>
                            <option value="0" {{ $selectedOption == '0' ? 'selected' : '' }}>
                                False
                            </option>
                        </select>
                        <span class="control-error" v-if="errors.has('approved')">@{{ errors.first('approved') }}</span>
                    </div>
                </div>           
            </div>
        </form>
    </div>
@stop