@extends('admin::layouts.master')

@section('page_title')
   Edit | {{$pageInfo->title}}
@stop

@section('content-wrapper')
<div class="inner-section">
    @include('manager::admin.partials.aside-nav')
    <div class="content-wrapper">
        <div class="content">
                <form method="POST" action="{{ route('company-info.update', $pageInfo->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>
                                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                                Edit {{$pageInfo->title}}
                            </h1>
                        </div>

                        <div class="page-action">
                            <button type="submit" class="btn btn-lg btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    <div class="page-content" style="padding-left: 25px">
                        <div class="form-container">
                            @csrf()
                            <input name="_method" type="hidden" value="PUT">

                            <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                                <label for="title" class="required">Title</label>
                                <input type="text" v-validate="'required'" class="control" id="title" name="title" data-vv-as="&quot;'Title'&quot;" value="{{ $pageInfo->title }}"/>
                                <span class="control-error" v-if="errors.has('title')">@{{ errors.first('Title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('slug') ? 'has-error' : '']">
                                <label for="slug" class="required">Slug</label>
                                <input type="text" v-validate="'required'" class="control" id="slug" name="slug" data-vv-as="&quot;'Slug'&quot;" value="{{ $pageInfo->slug }}"/>
                                <span class="control-error" v-if="errors.has('slug')">@{{ errors.first('Slug') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                                <label for="status" class="required">Status</label>
                                <?php $selectedOption = old('status') ?: $pageInfo->status ?>
                                <select v-validate="'required'" class="control" id="status" name="status" data-vv-as="&quot;'Status'&quot;">
                                    <option value="active" {{ $selectedOption == 'active' ? 'selected' : '' }}>
                                        Active
                                    </option>
                                    <option value="inactive" {{ $selectedOption == 'inactive' ? 'selected' : '' }}>
                                            Inactive
                                    </option>
                                    <option value="draft" {{ $selectedOption == 'draft' ? 'selected' : '' }}>
                                        Draft
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('Status') }}</span>
                            </div>

                            <div class="control-group">
                                <label for="page_content" class="required">Page Content</label>
                                <textarea class="control" id="page_content" name="page_content">{{$pageInfo->page_content}}</textarea>
                            
                            </div>

                            <div class="control-group" :class="[errors.has('meta_title') ? 'has-error' : '']">
                                <label for="meta_title" class="required">Meta Title</label>
                                <input type="text" v-validate="'required'" class="control" id="meta_title" name="meta_title" data-vv-as="&quot;'meta_title'&quot;" value="{{ $pageInfo->meta_title }}"/>
                                <span class="control-error" v-if="errors.has('meta_title')">@{{ errors.first('Meta Title') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_description') ? 'has-error' : '']">
                                <label for="meta_description" class="required">Meta Description</label>
                                <textarea type="text" v-validate="'required'" class="control" id="meta_description" name="meta_description" data-vv-as="&quot;'meta_description'&quot;">{{ old('meta_description') ?: $pageInfo->meta_description }}</textarea>
                                <span class="control-error" v-if="errors.has('meta_description')">@{{ errors.first('Meta Description') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('meta_keywords') ? 'has-error' : '']">
                                <label for="meta_keywords" class="required">Meta Keywords</label>
                                <textarea type="text" v-validate="'required'" class="control" id="meta_keywords" name="meta_keywords" data-vv-as="&quot;'meta_keywords'&quot;">{{ old('meta_keywords') ?: $pageInfo->meta_keywords }}</textarea>
                                <span class="control-error" v-if="errors.has('meta_keywords')">@{{ errors.first('Meta Keywords') }}</span>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@stop



@push('scripts')
<script src="https://cdn.tiny.cloud/1/b1fizmy28j6qtvhsbj7z8pm0fjww8k1qj2i48p7198l359m4/tinymce/5/tinymce.min.js"></script> 

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#page_content',
                height: 400,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code preview autosave ',
                toolbar1: 'formatselect | preview restoredraft| bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                valid_elements : '*[*]'
            });
        });
    </script>
@endpush
