@extends('admin::layouts.master')

@section('page_title')
   Customer Queries
@stop

@section('content-wrapper')
<div class="inner-section">
    @include('manager::admin.partials.aside-nav')
    <div class="content-wrapper">
        <div class="content">
                <form method="POST" action="{{ route('contact-us.update', $questions->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>
                                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                                Edit {{$questions->question}}
                            </h1>
                        </div>

                        <div class="page-action">
                            <button type="submit" class="btn btn-lg btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    <div class="page-content" style="padding-left: 25px">
                        <div class="form-container">
                            @csrf()
                            <input name="_method" type="hidden" value="PUT">

                            <div class="control-group">
                                <label for="name" >Name</label>
                                <input type="text" v-validate="'required'" class="control" id="name" name="name" data-vv-as="&quot;'name'&quot;" value="{{ $questions->name }}" readonly/>
                            </div>

                            <div class="control-group">
                                <label for="email">E-mail</label>
                                <input type="text" v-validate="'required'" class="control" id="email" name="email" data-vv-as="&quot;'email'&quot;" value="{{ $questions->email }}" readonly/>
                            </div>

                            <div class="control-group">
                                <label for="phone" >Phone</label>
                                <input type="text" v-validate="'required'" class="control" id="phone" name="phone" data-vv-as="&quot;'phone'&quot;" value="{{ $questions->phone }}" readonly/>
                            </div>

                            <div class="control-group">
                                <label for="subject">Subject</label>
                                <input type="text" v-validate="'required'" class="control" id="subject" name="subject" data-vv-as="&quot;'subject'&quot;" value="{{ $questions->subject }}" readonly/>
                            </div>

                            <div class="control-group">
                                <label for="comments" >Comment</label>
                                <input type="text" v-validate="'required'" class="control" id="comments" name="comments" data-vv-as="&quot;'comments'&quot;" value="{{ $questions->comments }}" readonly/>
                            </div>

                            <div class="control-group" :class="[errors.has('remarks') ? 'has-error' : '']">
                                <label for="remarks" class="required">Remarks</label>
                                <textarea type="text" v-validate="'required'" class="control" id="remarks" name="remarks" data-vv-as="&quot;'remarks'&quot;">{{ $questions->remarks }}</textarea>
                                <span class="control-error" v-if="errors.has('remarks')">@{{ errors.first('remarks') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                                <label for="status" class="required">Status</label>
                                <?php $selectedOption = old('status') ?: $questions->status ?>
                                <select v-validate="'required'" class="control" id="status" name="status" data-vv-as="&quot;'status'&quot;">
                                    <option value="1" {{ $selectedOption == '1' ? 'selected' : '' }}>
                                        Replied
                                    </option>
                                    <option value="0" {{ $selectedOption == '0' ? 'selected' : '' }}>
                                        Not Replied
                                    </option>
                                </select>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@stop
