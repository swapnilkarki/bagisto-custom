@extends('admin::layouts.master')

@section('page_title')
    MeMyKids | Edit Pages
@stop

@push('css')
    <style>
        .slider:after {
            content: "Inactive";
        }
        input:checked + .slider:after {
            content: "Active";
        }
    </style>

@endpush


@section('content-wrapper')
<div class="inner-section">
    
    @include('manager::admin.partials.aside-nav')

    <div class="content-wrapper">
        <div class="content" style="height: 100%;">
            <div class="page-header">
                <div class="page-title">
                    <h1>Company Info Pages</h1>
                </div>
                <div class="page-action">
                <a href="{{route('company-info.create')}}" class="btn btn-lg btn-primary">
                       Add Page
                    </a>
                </div>
            </div>
            <div class="page-content">
                <div class="table">
                    <table class="table">
                        <thead>
                            <tr style="height: 65px;">
                                <th class="grid_head">id</th>
                                <th class="grid_head">Title</th>
                                <th class="grid_head">Status</th>
                                <th class="grid_head">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pageInfos as $pageInfo)
                            <tr>
                                <td data-value="ID">{{$pageInfo->id}}</td>
                                <td>{{$pageInfo->title}}</td>
                                
                                <td data-value="STATUS">
                                    <form method="POST" action="{{route('company-info.statusUpdate',$pageInfo->id)}}" ref="statusUpdateForm{{$pageInfo->id}}" id="statusUpdateForm{{$pageInfo->id}}">
                                        @csrf()
                                        <input name="_method" type="hidden" value="PUT">
                                        <label class="switch">
                                        <input type="checkbox" {{$pageInfo->status == 'active' ? 'checked' : ''}} name="status" value="{{$pageInfo->status == 'active' ? 'inactive' : 'active'}}" onclick="document.getElementById('statusUpdateForm{{$pageInfo->id}}').submit();">
                                        <span class="slider round "></span>
                                        </label>
                                    </form>
                                </td>

                                <td class="actions" data-value="ACTIONS">
                                    <a href="{{route('company-info.edit',$pageInfo->id)}}"><span class="icon pencil-lg-icon"></span></a>
                                    <a href="{{route('company-info.delete',$pageInfo->id)}}"><span class="icon trash-icon"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <div>
        </div>
    </div>
</div>
@stop