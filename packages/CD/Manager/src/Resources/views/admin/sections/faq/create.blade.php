@extends('admin::layouts.master')

@section('page_title')
   Create FAQ
@stop

@section('content-wrapper')
<div class="inner-section">
    @include('manager::admin.partials.aside-nav')
    <div class="content-wrapper">
        <div class="content">
                <form method="POST" action="{{ route('faq.create') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>
                                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                                Create FAQ
                            </h1>
                        </div>

                        <div class="page-action">
                            <button type="submit" class="btn btn-lg btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    <div class="page-content" style="padding-left: 25px">
                        <div class="form-container">
                            @csrf()
                            <input name="_method" type="hidden" value="PUT">

                            <div class="control-group" :class="[errors.has('question') ? 'has-error' : '']">
                                <label for="question" class="required">Question</label>
                            <input type="text" v-validate="'required'" class="control" id="question" name="question" data-vv-as="&quot;'question'&quot;" value="{{old('question')}}"/>
                                <span class="control-error" v-if="errors.has('question')">@{{ errors.first('question') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('answer') ? 'has-error' : '']">
                                <label for="answer" class="required">Answer</label>
                                <textarea type="text" v-validate="'required'" class="control" id="answer" name="answer" data-vv-as="&quot;'answer'&quot;">{{old('answer')}}</textarea>
                                <span class="control-error" v-if="errors.has('answer')">@{{ errors.first('answer') }}</span>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@stop



@push('scripts')
    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#page_content',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                valid_elements : '*[*]'
            });
        });
    </script>
@endpush
