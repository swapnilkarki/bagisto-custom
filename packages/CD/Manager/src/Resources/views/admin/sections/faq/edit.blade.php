@extends('admin::layouts.master')

@section('page_title')
   Edit Faq
@stop

@section('content-wrapper')
<div class="inner-section">
    @include('manager::admin.partials.aside-nav')
    <div class="content-wrapper">
        <div class="content">
                <form method="POST" action="{{ route('faq.update', $faq->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>
                                <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                                Edit {{$faq->question}}
                            </h1>
                        </div>

                        <div class="page-action">
                            <button type="submit" class="btn btn-lg btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    <div class="page-content" style="padding-left: 25px">
                        <div class="form-container">
                            @csrf()
                            <input name="_method" type="hidden" value="PUT">

                            <div class="control-group" :class="[errors.has('question') ? 'has-error' : '']">
                                <label for="question" class="required">Question</label>
                                <input type="text" v-validate="'required'" class="control" id="question" name="question" data-vv-as="&quot;'question'&quot;" value="{{ $faq->question }}"/>
                                <span class="control-error" v-if="errors.has('question')">@{{ errors.first('question') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('answer') ? 'has-error' : '']">
                                <label for="answer" class="required">Answer</label>
                                <textarea type="text" v-validate="'required'" class="control" id="answer" name="answer" data-vv-as="&quot;'answer'&quot;">{{ $faq->answer }}</textarea>
                                <span class="control-error" v-if="errors.has('answer')">@{{ errors.first('answer') }}</span>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@stop



@push('scripts')
<script src="https://cdn.tiny.cloud/1/b1fizmy28j6qtvhsbj7z8pm0fjww8k1qj2i48p7198l359m4/tinymce/5/tinymce.min.js"></script> 

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#page_content',
                height: 400,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code preview autosave ',
                toolbar1: 'formatselect | preview restoredraft| bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                valid_elements : '*[*]'
            });
        });
    </script>
@endpush
