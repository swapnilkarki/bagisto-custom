@extends('admin::layouts.master')

@section('page_title')
    MeMyKids | FAQ's
@stop


@section('content-wrapper')
<div class="inner-section">
    
    @include('manager::admin.partials.aside-nav')

    <div class="content-wrapper">
        <div class="content" style="height: 100%;">
            <div class="page-header">
                <div class="page-title">
                    <h1>Frequently Asked Questions</h1>
                </div>
                <div class="page-action">
                <a href="{{route('faq.create')}}" class="btn btn-lg btn-primary">
                       Add Question
                    </a>
                </div>
            </div>
            <div class="page-content">
                <div class="table">
                    <table class="table">
                        <thead>
                            <tr style="height: 65px;">
                                <th class="grid_head">id</th>
                                <th class="grid_head">Question</th>
                                <th class="grid_head">Answer</th>
                                <th class="grid_head">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($faqs as $faq)
                            <tr>
                                <td data-value="Id">{{$faq->id}}</td>
                                <td data-value="Questions">{{$faq->question}}</td>
                                <td data-value="Answer">{{$faq->answer}}</td>
                                <td class="actions" data-value="Actions">
                                    <a href="{{route('faq.edit',$faq->id)}}"><span class="icon pencil-lg-icon"></span></a>
                                    <a href="{{route('faq.delete',$faq->id)}}"><span class="icon trash-icon"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <div>
        </div>
    </div>
</div>
@stop