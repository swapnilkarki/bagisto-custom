@extends('admin::layouts.master')

@section('page_title')
    MeMyKids | Customer Questions
@stop

@push('css')
    <style>
        .slider:after {
            content: "Pending";
        }
        input:checked + .slider:after {
            content: "Replied";
        }
    </style>

@endpush


@section('content-wrapper')
<div class="inner-section">
    
    @include('manager::admin.partials.aside-nav')

    <div class="content-wrapper">
        <div class="content" style="height: 100%;">
            <div class="page-header">
                <div class="page-title">
                    <h1>Customer Questions</h1>
                </div>
                <div class="page-action">
                </div>
            </div>
            <div class="page-content">
                <div class="table">
                    <table class="table">
                        <thead>
                            <tr style="height: 65px;">
                                <th class="grid_head">id</th>
                                <th class="grid_head">Name</th>
                                <th class="grid_head">E-Mail</th>
                                <th class="grid_head">Phone</th>
                                <th class="grid_head">Subject</th>
                                <th class="grid_head">Comment</th>
                                <th class="grid_head">Status</th>
                                <th class="grid_head">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $question)
                            <tr>
                                <td data-value="ID">{{$question->id}}</td>
                                <td data-value="Name">{{$question->name}}</td>
                                <td data-value="Email">{{$question->email}}</td>
                                <td data-value="Phone">{{$question->phone}}</td>
                                <td data-value="Subject">{{$question->subject}}</td>
                                <td data-value="Comments">{{$question->comments}}</td>
                                <td data-value="Status">
                                    <form method="POST" action="{{route('contact-us.statusUpdate',$question->id)}}" ref="statusUpdateForm{{$question->id}}" id="statusUpdateForm{{$question->id}}">
                                        @csrf()
                                        <input name="_method" type="hidden" value="PUT">
                                        <label class="switch">
                                        <input type="checkbox" {{$question->status == '1' ? 'checked' : ''}} name="status" value="{{$question->status == '1' ? '0' : '1'}}" onclick="document.getElementById('statusUpdateForm{{$question->id}}').submit();">
                                        <span class="slider round "></span>
                                        </label>
                                    </form>
                                </td>

                                <td class="actions" data-value="Actions">
                                    <a href="{{route('contact-us.edit',$question->id)}}"><span class="icon eye-icon"></span></a>
                                    <a href="{{route('contact-us.delete',$question->id)}}"><span class="icon trash-icon"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <div>
        </div>
    </div>
</div>
@stop

