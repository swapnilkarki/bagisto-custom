@extends('admin::layouts.master')

@section('page_title')
    MeMyKids | Customer Queries
@stop

@push('css')
    <style>
        .slider:after {
            content: "Inactive";
        }
        input:checked + .slider:after {
            content: "Active";
        }
    </style>

@endpush


@section('content-wrapper')
<div class="inner-section">
    
    @include('manager::admin.partials.aside-nav')

    <div class="content-wrapper">
        <div class="content" style="height: 100%;">
            <div class="page-header">
                <div class="page-title">
                    <h1>Customer Queries</h1>
                </div>
            </div>
            <div class="page-content">
                <div class="table">
                    <table class="table">
                        <thead>
                            <tr style="height: 65px;">
                                <th class="grid_head">id</th>
                                <th class="grid_head">Question</th>
                                <th class="grid_head">Answer</th>
                                <th class="grid_head">Status</th>
                                <th class="grid_head">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($queries as $query)
                            <tr>
                                <td data-value="Id">{{$query->id}}</td>
                                <td data-value="Question">{{$query->question}}</td>
                                <td data-value="Answer">{{$query->answer}}</td>

                                <td data-value="Actions">
                                    <form method="POST" action="{{route('customer-queries.statusUpdate',$query->id)}}" ref="statusUpdateForm{{$query->id}}" id="statusUpdateForm{{$query->id}}">
                                        @csrf()
                                        <input name="_method" type="hidden" value="PUT">
                                        <label class="switch">
                                        <input type="checkbox" {{$query->approved == '1' ? 'checked' : ''}} name="approved" value="{{$query->approved == '1' ? '0' : '1'}}" onclick="document.getElementById('statusUpdateForm{{$query->id}}').submit();">
                                        <span class="slider round "></span>
                                        </label>
                                    </form>
                                </td>

                                <td class="actions" data-value="Actions">
                                    <a href="{{route('customer-queries.edit',$query->id)}}"><span class="icon pencil-lg-icon"></span></a>
                                    <a href="{{route('customer-queries.delete',$query->id)}}"><span class="icon trash-icon"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </table>
            <div>
        </div>
    </div>
</div>
@stop