<div class="aside-nav">
    <ul>
        <li class="{{Route::currentRouteName()=='company-info.index' || Route::currentRouteName()=='company-info.edit' || Route::currentRouteName()=='company-info.create' ? 'active' : ''}}">
            <a href="{{route('company-info.index')}}">Company Info
                <i class="angle-right-icon"></i> 
            </a>
        </li>

        <li class="{{Route::currentRouteName()=='shipping-and-policy.index' || Route::currentRouteName()=='shipping-and-policy.create' ||Route::currentRouteName()=='shipping-and-policy.edit' ? 'active' : ''}}">
            <a href="{{route('shipping-and-policy.index')}}">Shipping & Policy
                <i class="angle-right-icon"></i>
            </a>
        </li>

        <li class="{{Route::currentRouteName()=='faq.index' || Route::currentRouteName()=='faq.create' ||Route::currentRouteName()=='faq.edit' ? 'active' : ''}}">
        <a href="{{route('faq.index')}}">Faq's
                <i class="angle-right-icon"></i>
            </a>
        </li>

        <li class="{{Route::currentRouteName()=='customer-queries.index' || Route::currentRouteName()=='customer-queries.edit' ? 'active' : ''}}">
        <a href="{{route('customer-queries.index')}}">Customer Faq's
                <i class="angle-right-icon"></i>
            </a>
        </li>

        <li class="{{Route::currentRouteName()=='contact-us.index' || Route::currentRouteName()=='contact-us.edit' ? 'active' : ''}}">
        <a href="{{route('contact-us.index')}}">Customer Questions
                <i class="angle-right-icon"></i>
            </a>
        </li>
    </ul>
</div>