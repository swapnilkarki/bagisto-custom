@extends('admin::layouts.content')

@section('page_title')
  Create Newsletter
@stop

@push('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> --}}
<link rel="stylesheet" type="text/css" href=" {{ asset('vendor/webkul/manager/assets/css/jquery.modal.css') }} ">
@endpush

@section('content')
<div class="content">
    <form method="POST" action="{{route('admin.customers.subscribers.send-mail')}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>
                        Create Newsletter
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                            Send Mail
                    </button>
                </div>
            </div>

            <div class="page-content" style="padding-left: 25px">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">Title</label>
                        <input type="text" v-validate="'required'" class="control" id="title" name="title" data-vv-as="&quot;'Title'&quot;" value="{{old('title')}}"/>
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('Title') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('subject') ? 'has-error' : '']">
                        <label for="subject" class="required">Subject</label>
                        <input type="text" v-validate="'required'" class="control" id="subject" name="subject" data-vv-as="&quot;'subject'&quot;" value="{{old('subject')}}"/>
                        <span class="control-error" v-if="errors.has('subject')">@{{ errors.first('Subject') }}</span>
                    </div>

                    <div class="control-group">
                        <a href="#templatesListModal" rel="modal:open" class="btn btn-lg btn-primary">
                            Choose Template
                        </a>
                    </div>

                    <div class="control-group" :class="[errors.has('template') ? 'has-error' : '']">
                        <label for="template" class="required">Template</label>
                        <textarea class="control" v-validate="'required'" id="template" name="template" data-vv-as="&quot;'template'&quot;">{{old('template')}}</textarea>
                        <span class="control-error" v-if="errors.has('template')">@{{ errors.first('template') }}</span>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
<!-- Modal HTML embedded directly into document -->
<div id="templatesListModal" class="modal">
    <table class="table">
        <thead style="font-size: large;">
            <tr>
            <td scope="col">Id</td>
            <td scope="col">Title</td>
            <td scope="col">type</td>
            <td scope="col">Preview</td>
            </tr>
        </thead>
        <tbody>
            @foreach($templates as $template)
            <tr>
                <td scope="row">{{$template->id}}</td>
                <td>{{$template->title}}</td>
                <td>{{$template->type}}</td>
                <td>
                {{-- <a href="{{route('admin.customers.create-email.preview', $template->id)}}" rel="modal:open"><span class="icon eye-icon"></span></a> --}}
                <a id="useTemplate" onclick="useTemplate({{$template}})" rel="modal:close"><span class="icon eye-icon"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop



@push('scripts')
<script src="https://cdn.tiny.cloud/1/b1fizmy28j6qtvhsbj7z8pm0fjww8k1qj2i48p7198l359m4/tinymce/5/tinymce.min.js"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

<script> 
   function useTemplate($template){
        tinymce.get('template').setContent($template['template']);
        // document.getElementById("template").value =tinyMCE.get('template');
   }
</script> 

<script>
    $(document).ready(function () {
        tinymce.init({
            selector: 'textarea#template',
            height: 500,
            width: "100%",
            plugins: 'image imagetools media wordcount save fullscreen code preview autosave ',
            toolbar1: 'formatselect | preview restoredraft| bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
            image_advtab: true,
            valid_elements : '*[*]',
            images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '/admin/create-email/upload/image');
            var token = '{{ csrf_token() }}';
            xhr.setRequestHeader("X-CSRF-Token", token);
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        }
        });
    });
</script>
@endpush
