{{-- not good practice, either find a way to use with repository patterm or with controller --}}
@php
    $pages = \CD\Manager\Models\CompanyInfoPages::where('section','shipping-and-policy')->where('status','active')->get()
@endphp 

<div class="col-md-4">
    <div class="footer-block">
        <strong>Shipping & Policy</strong>
        <ul class="footer-list">
            @foreach ($pages as $page)
                <li><a href="{{ route('shipping-and-policy.page.render', $page->slug) }}">{{$page->title}}</a></li>
            @endforeach
            {{-- <li><a href="#">Payment</a></li>
            <li><a href="terms-condition.php">Shipment Policy</a></li>
            <li><a href="terms-condition.php">Return Policy</a></li>
            <li><a href="terms-condition.php">Cancellation Policy</a></li>
            <li><a href="terms-condition.php">Terms of use</a></li>
            <li><a href="terms-condition.php">Privacy Policy</a></li> --}}
        </ul>
    </div>
</div>