@extends('memy::layouts.master')

@section('seo')
    <meta name="description" content="{{ trim($pageInfo->meta_description) != "" ? $pageInfo->meta_description : str_limit(strip_tags($pageInfo->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $pageInfo->meta_keywords }}"/>
    <meta name="title" content="{{ $pageInfo->meta_title }}"/>
@stop

@section('page_title')
   {{$pageInfo->title}}
@stop

@section('content')
{!!DbView::make($pageInfo)->field('page_content')->render();!!}
@endsection