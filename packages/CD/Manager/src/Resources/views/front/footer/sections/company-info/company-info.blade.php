{{-- not good practice, either find a way to use with repository patterm or with controller --}}
@php
    $pages = \CD\Manager\Models\CompanyInfoPages::where('section','company-info')->where('status','active')->get()
@endphp 

<div class="col-md-4">
    <div class="footer-block">
        <strong>Company Info</strong>
        <ul class="footer-list">
            @foreach ($pages as $page)
            <li><a href="{{ route('company-info.page.render', $page->slug) }}">{{$page->title}}</a></li>
            @endforeach
            <li><a href="{{route('faq.page.render')}}">FAQ's</a></li>
            {{-- <li><a href="{{route('memy.pages.about-us')}}">About Us</a></li>
            <li><a href="{{route('memy.pages.contact-us')}}">Contact Us</a></li>
            <li><a href="{{route('memy.pages.blog')}}">Blog</a></li>
            <li><a href="{{route('memy.pages.partners')}}">Partners</a></li> --}}
        </ul>
    </div>
</div>