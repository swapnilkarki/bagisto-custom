@extends('memy::layouts.master')

@section('seo')
    <meta name="description" content="{{ trim($pageInfo->meta_description) != "" ? $pageInfo->meta_description : str_limit(strip_tags($pageInfo->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $pageInfo->meta_keywords }}"/>
@stop

@section('page_title')
   {{$pageInfo->title}}
@stop

@section('content')
<section class="contact">
    <div class="contact__parallaxbg">
        <h4 class="contact--tag">Contact Us</h4>
    </div>
    <div class="contact__container">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6">
                    <article class="contact__text">
                        <h5 class="contact--heading">Have a question ?</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                    </article>
                    <div class="contact__form">
                    <form method="POST" action="{{ route('contact-us.postQuestion') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @csrf()
                        <input name="_method" type="hidden" value="PUT">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                        <label for="name" class="required">Name</label>
                                        <input type="text" v-validate="'required'" class="form-control form-box" id="name" name="name" data-vv-as="&quot;'Name'&quot;" value="{{old('name')}}"/>
                                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                                        <label for="email" class="required">Email</label>
                                        <input type="email" v-validate="'required'" class="form-control form-box" id="email" name="email" data-vv-as="&quot;'Email'&quot;" value="{{old('email')}}"/>
                                        <span class="control-error" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="control-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                        <label for="phone" class="required">Phone</label>
                                        <input type="text" v-validate="'required|numeric|max:10|min:10'" class="form-control form-box" id="phone" name="phone" data-vv-as="&quot;'phone'&quot;" value="{{old('phone')}}"/>
                                        <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="control-group" :class="[errors.has('subject') ? 'has-error' : '']">
                                        <label for="subject" class="required">Subject</label>
                                        <input type="subject" v-validate="'required'" class="form-control form-box" id="subject" name="subject" data-vv-as="&quot;'subject'&quot;" value="{{old('subject')}}"/>
                                        <span class="control-error" v-if="errors.has('subject')">@{{ errors.first('subject') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="control-group" :class="[errors.has('comments') ? 'has-error' : '']">
                                    <label for="comments" class="required">comments</label>
                                    <textarea v-validate="'required'" class="form-control form-box" id="comments" name="comments" data-vv-as="&quot;'comments'&quot;" value="{{old('comments')}}"></textarea>
                                    <span class="control-error" v-if="errors.has('comments')">@{{ errors.first('comments') }}</span>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Send</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <article class="contact__text">
                        <h5 class="contact--heading">Contact Information</h5>
                        <address>
                            Tikhidebal, Mahalaxmisthan, Lalitpur <br>
                            Tel: +977-01-5512345 <br>
                            Email: info@memykids.com
                        </address>
                    </article>
                    <div class="contact__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.8944311877704!2d85.3146793145372!3d27.658737734234588!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19d5889c18a1%3A0xf876386435669c81!2sCore+Dreams+Innovations+Pvt.+Ltd.!5e0!3m2!1sen!2snp!4v1553166698141" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- render from database --}}
{{-- {!!DbView::make($pageInfo)->field('page_content')->render();!!} --}}
@endsection