<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerQueries extends Model
{
    protected $fillable = ['question','answer','approved'];
}
