<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;
use CD\Manager\Contracts\CompanyInfoPages as CompanyInfoPagesContract;

class CompanyInfoPages extends Model implements CompanyInfoPagesContract
{
    protected $fillable = ['title','slug', 'page_content', 'status' , 'meta_title', 'meta_description','meta_keywords', 'section'];
}
