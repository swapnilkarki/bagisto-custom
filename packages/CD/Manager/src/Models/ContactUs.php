<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = 'contact_us_questions';

    protected $fillable = ['name','email','phone','subject','comments'];
}
