<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;
use CD\Manager\Contracts\Faq as FaqContract;

class Faq extends Model implements FaqContract
{
    protected $fillable = ['question','answer'];
}
