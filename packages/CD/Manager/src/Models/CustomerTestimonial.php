<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerTestimonial extends Model
{
    protected $fillable = ['full_name','statement','rating','approved'];
}
