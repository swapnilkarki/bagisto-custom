<?php

namespace CD\Manager\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = ['title','type','template'];
}
