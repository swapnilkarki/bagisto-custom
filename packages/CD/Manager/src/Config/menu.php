<?php

return [
    [
        'key' => 'company-info',          // uniquely defined key for menu-icon
        'name' => 'Pages',        //  name of menu-icon
        'route' => 'company-info.index',  // the route for your menu-icon
        'sort' => 5,                    // Sort number on which your menu-icon should display
        'icon-class' => 'cms-icon',   //class of menu-icn
    ],
    [
        'key' => 'company-info.company-info',          // uniquely defined key for menu-icon
        'name' => 'Company-info',        //  name of menu-icon
        'route' => 'company-info.index',  // the route for your menu-icon
        'sort' => 1,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
    [
        'key' => 'company-info.shipping-and-policy',          // uniquely defined key for menu-icon
        'name' => 'Shipping & Policy',        //  name of menu-icon
        'route' => 'shipping-and-policy.index',  // the route for your menu-icon
        'sort' => 2,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
    [
        'key' => 'company-info.faq',          // uniquely defined key for menu-icon
        'name' => 'FAQs',        //  name of menu-icon
        'route' => 'faq.index',  // the route for your menu-icon
        'sort' => 3,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
    [
        'key' => 'company-info.customer-queries',          // uniquely defined key for menu-icon
        'name' => 'Customer FAQs',        //  name of menu-icon
        'route' => 'customer-queries.index',  // the route for your menu-icon
        'sort' => 4,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
    [
        'key' => 'company-info.contact-us-questions',          // uniquely defined key for menu-icon
        'name' => 'Customer Questions',        //  name of menu-icon
        'route' => 'contact-us.index',  // the route for your menu-icon
        'sort' => 5,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
    [
        'key' => 'customers.testimonials',          // uniquely defined key for menu-icon
        'name' => 'Customer Testimonials',        //  name of menu-icon
        'route' => 'testimonial.index',  // the route for your menu-icon
        'sort' => 6,                    // Sort number on which your menu-icon should display
        'icon-class' => '',   //class of menu-icn
    ],
];
