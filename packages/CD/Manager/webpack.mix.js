const { mix } = require("laravel-mix");
require("laravel-mix-merge-manifest");

if (mix.inProduction()) {
    var publicPath = 'publishable/assets';
} else {
    var publicPath = "../../../public/vendor/webkul/manager/assets";
}

mix.setPublicPath(publicPath).mergeManifest();

mix.disableNotifications();

mix.sass(__dirname + "/src/Resources/assets/sass/app.scss", "css/manager.css")
    .sass(__dirname + "/src/Resources/assets/sass/jquery.modal.scss", "css/jquery.modal.css")
    .js(__dirname + "/src/Resources/assets/js/app.js", "js/manager.js")
    .js(__dirname + "/src/Resources/assets/js/jquery.modal.js", "js/jquery.modal.js")
    .copyDirectory(__dirname + "/src/Resources/assets/images", publicPath + "/images")
    .options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}