const { mix } = require("laravel-mix");
require("laravel-mix-merge-manifest");

if (mix.inProduction()) {
    var publicPath = "publishable/assets";
} else {
    var publicPath = "../../../public/vendor/webkul/rma/assets";
}

mix.setPublicPath(publicPath).mergeManifest();

mix.disableNotifications();

mix
    .sass(__dirname + "/src/Resources/assets/sass/app.scss", "css/rma.css")
    .js(__dirname + "/src/Resources/assets/js/app.js", "js/rma.js")
    .copyDirectory(
        __dirname + "/src/Resources/assets/images",
        publicPath + "/images"
    )
    .options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}