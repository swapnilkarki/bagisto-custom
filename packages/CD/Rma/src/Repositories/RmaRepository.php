<?php

namespace CD\Rma\Repositories;

use Webkul\Core\Eloquent\Repository;

class RmaRepository extends Repository
{
   /**
    * Specify Model class name
    *
    * @return mixed
    */
    function model()
    {
        return 'CD\Rma\Contracts\Rma';
    }

      /**
     * @param array $data
     * @return mixed
     */

    public function create(array $data)
    {
        $rma = $this->model->create($data);

        return $rma;
    }

       /**
     * @param $id
     * @return mixed
     */

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

        /**
     * @param $policies
     * @return boolean
     */

    public function hasValidPolicity($policies)
    {
        $returnPolicy = $this->findById($policies->return_policy);
        $warrantyPolicy = $this->findById($policies->warranty_policy);
        $exchangePolicy = $this->findById($policies->exchange_policy);
        if($returnPolicy->validity != 0){
            return true;
        }else if($warrantyPolicy->validity != 0){
            return true;
        }else if($exchangePolicy->validity != 0){
            return true;
        }else{
            return false;
        }
    }

      /**
     * @param array $text
     * @return slug
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
    * get all polices
    *
    * @return mixed
    */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
    * get all return polices
    *
    * @return mixed
    */
    public function getReturnsRules()
    {
        return $this->model->Where([
            'type' => 'returns'
        ])->orderBy('validity', 'asc')->get();
    }

    /**
    * get all warranty polices
    *
    * @return mixed
    */
    public function getWarrantyRules()
    {
        return $this->model->Where([
            'type' => 'warranty'
        ])->orderBy('validity', 'asc')->get();
    }

    /**
    * get all exchange polices
    *
    * @return mixed
    */
    public function getExchangeRules()
    {
        return $this->model->Where([
            'type' => 'exchange'
        ])->orderBy('validity', 'asc')->get();
    }

    public function customValidation($data)
    {
        $validity = $data['validity'];
        $type = $this->model->Where([
            'type' => $data['type']
        ])->get();
        if($type){
            $exists = $type->filter(function ($value) use ($validity){
                return $value['validity'] == $validity;
            });
            return $exists->count() !=0 ? false : true ;
        }else{
            return true;
        }
     
    }

    public function customValidationForEdit($data,$id)
    {
        $validity = $data['validity'];
        $id = $id;
        $type = $this->model->Where([
            'type' => $data['type']
        ])->get();
        if($type){
            $exists = $type->filter(function ($value) use ($validity,$id){
                return $value['validity'] == $validity && $value['id'] != $id;
            });
            return $exists->count() !=0 ? false : true ;
        }else{
            return true;
        }
    }
}
