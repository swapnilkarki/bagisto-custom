<?php

namespace CD\Rma\Repositories;

use Webkul\Core\Eloquent\Repository;

class ProductPoliciesRepository extends Repository
{
   /**
    * Specify Model class name
    *
    * @return mixed
    */
    function model()
    {
        return 'CD\Rma\Contracts\ProductPolicies';
    }

    
      /**
     * @param array $data
     * @return mixed
     */

    public function add(array $data)
    {
        if($this->model->where('product_id',$data['product_id'])->exists())
        {
            $productPolicies = $this->model->where('product_id',$data['product_id'])->update($data);
        }else{
            $productPolicies = $this->model->create($data);
        }

        return $productPolicies;
    }

     /**
     * @param array $product_id
     * @return mixed
     */

    public function getPoliciesByProductId($product_id)
    {
        return $this->model->where('product_id',$product_id)->first();
    }

}