<?php

namespace CD\Rma\Repositories;

use Webkul\Core\Eloquent\Repository;

class RmaRequestsRepository extends Repository
{
   /**
    * Specify Model class name
    *
    * @return mixed
    */
    function model()
    {
        return 'CD\Rma\Contracts\RmaRequests';
    }

    function getAll()
    {
        $rmaRequests =$this->model->orderBy('created_at','DESC')->paginate(10);
        return $rmaRequests;
    }

    function searchResults($term){

        $results = app('CD\Rma\Repositories\RmaRequestsRepository')->scopeQuery(function($query) use($term){
            return $qb = $query->distinct()
                    ->addSelect('rma_requests.*')
                    ->leftJoin('customers', 'rma_requests.customer_id', '=', 'customers.id')
                    ->leftJoin('product_flat', 'rma_requests.product_id', '=', 'product_flat.product_id')
                    ->addSelect('customers.first_name','customers.mobile_number')
                    ->where('rma_requests.id','like', '%' . $term . '%')
                    ->orWhere('rma_requests.status','like', '%' . $term . '%')
                    ->orWhere('customers.first_name','like', '%' . $term . '%')
                    ->orWhere('customers.mobile_number','like', '%' . $term . '%')
                    ->orWhere('product_flat.name','like', '%' . $term . '%')
                    ->orderBy('rma_requests.created_at','DESC');

        })->paginate(10);

        return $results;
    }

    function findByCustomerId($customerId){
        return $this->model->where('customer_id',$customerId)->orderBy('created_at','DESC')->paginate(10);
    }
}