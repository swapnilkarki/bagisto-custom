@extends('admin::layouts.content')

@section('page_title')
   Edit Returns Rule | MeMyKids
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{route('rma.returns.edit',$rma->id)}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit Rule 
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">Title</label>
                        <input type="text" class="control" name="title" v-validate="'required'" value="{{$rma->title}}" data-vv-as="&quot;Title&quot;">
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('validity') ? 'has-error' : '']">
                        <label for="validity" class="required">Validity(In Days)</label>
                        <input type="text" class="control" name="validity" v-validate="'required|numeric'" value="{{$rma->validity}}" data-vv-as="&quot;Validity&quot;">
                        <span class="control-error" v-if="errors.has('validity')">@{{ errors.first('validity') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                        <label for="status" class="required">Status</label>
                        <?php $selectedOption = $rma->status ?>
                        <select v-validate="'required'" class="control" id="status" name="status" data-vv-as="&quot;'Status'&quot;">
                            <option value="1" {{ $selectedOption == 1 ? 'selected' : '' }}>
                                Active
                            </option>
                            <option value="0" {{ $selectedOption == 0 ? 'selected' : '' }}>
                                Inactive
                            </option>
                        </select>
                        <span class="control-error" v-if="errors.has('status')">@{{ errors.first('Status') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('remarks') ? 'has-error' : '']">
                        <label for="remarks" class="required">Description</label>
                        <textarea class="control" name="remarks" v-validate="'required'" data-vv-as="&quot;Description&quot;">{{ $rma->remarks }}</textarea>
                        <span class="control-error" v-if="errors.has('remarks')">@{{ errors.first('remarks') }}</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop