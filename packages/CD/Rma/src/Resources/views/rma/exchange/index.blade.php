@extends('admin::layouts.content')

@section('page_title')
   Exchange | MeMyKids
@stop

@push('css')
    <style>
        .slider:after {
            content: "Inactive";
        }
        input:checked + .slider:after {
            content: "Active";
        }
    </style>

@endpush

@section('content')
<div class="content" style="height: 100%;">
   <div class="page-header">
       <div class="page-title">
           <h1>Exchange Rules</h1>
       </div>

       <div class="page-action">
         <a href="{{route('rma.exchange.create')}}" class="btn btn-lg btn-primary">
             Add Rules
         </a>
      </div>

   </div>

   <div class="page-content">
      <div class="table">
         <table class="table">
            <thead>
                  <tr style="height: 65px;">
                  <th class="grid_head">id</th>
                  <th class="grid_head">Title</th>
                  <th class="grid_head">Validity</th>
                  <th class="grid_head">Status</th>
                  <th class="grid_head">Details</th>
                  <th class="grid_head">Actions</th>
                  </tr>
            </thead>
            <tbody>
                  @foreach($rma as $rma)
                  <tr>
                     <td data-value="Id">{{$rma->id}}</td>
                     <td data-value="Title">{{$rma->title}}</td>
                     <td data-value="Validity">{{$rma->validity}}&nbsp;days</td>

                     @if($rma->validity == 0)
                     <td data-value="Status"><span class="badge badge-info" style="display:inline-block;background-color:#2196F3;width:100px;height: 34px;text-align:center;">Default</span></td>
                     @else
                        <td data-value="Status">
                           <form method="POST" action="{{route('rma.statusUpdate',$rma->id)}}" ref="statusUpdateForm{{$rma->id}}" id="statusUpdateForm{{$rma->id}}">
                                 @csrf()
                                 <input name="_method" type="hidden" value="PUT">
                                 <label class="switch">
                                 <input type="checkbox" {{$rma->status == '1' ? 'checked' : ''}} name="status" value="{{$rma->status == '1' ? '0' : '1'}}" onclick="document.getElementById('statusUpdateForm{{$rma->id}}').submit();">
                                 <span class="slider round "></span>
                                 </label>
                           </form>
                        </td>
                     @endif

                     <td data-value="Remarks">{{$rma->remarks}}</td>
                     <td class="actions" data-value="Actions">
                     <a href="{{route('rma.exchange.edit', $rma->id)}}"><span class="icon pencil-lg-icon"></span></a>
                     <a href="{{route('rma.exchange.remove', $rma->id)}}"><span class="icon trash-icon"></span></a>
                     </td>
                  </tr>
                  @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
@stop