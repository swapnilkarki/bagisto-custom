@extends('admin::layouts.content')

@section('page_title')
   Rma Requests | MeMyKids
@stop


@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@inject('customerRepository', 'Webkul\Customer\Repositories\CustomerRepository')
@inject('RmaRepository','CD\Rma\Repositories\RmaRepository')

@section('content')
<div class="content" style="height: 100%;">
   <div class="page-header">
       <div class="page-title">
           <h1>Rma Requests</h1>
       </div>

       <div class="page-action">
         {{-- <a href="#" class="btn btn-lg btn-primary">
             Add Rules
         </a> --}}
      </div>

   </div>

   <div class="page-content">
        <div class="filter-row-one">
            <form method="GET" ref="search-form" action="{{ route('rma.requests.search') }}" @submit.prevent="onSubmit">
                <div class="search-filter">
                    <input type="search" id="search-field" class="form-control" placeholder="Search Here..." name="search-field" />

                    <div class="icon-wrapper">
                        <span class="icon search-icon search-btn" onclick="document.getElementById('search-form').submit();"></span>
                    </div>
                </div>
            </form>
        </div>

        <?php
            if(isset($_GET['search-field'])){
                $term = $_GET['search-field'];
            }
        ?>
        @if(isset($term))
            <div class="filter-row-two">
                <span class="filter-tag" style="text-transform: capitalize;">
                    <span>Search</span>
                    <span class="wrapper">
                        {{$term}}
                        <a href="{{route('rma.requests.index')}}"><span class="icon cross-icon"></span></a>
                    </span>
                </span>
            </div>
        @endif

        <div class="table">
            <table class="table">
                <thead>
                    <tr style="height: 65px;">
                            <th class="grid_head">Id</th>
                            <th class="grid_head">Customer Name</th>
                            <th class="grid_head">Customer Phone</th>
                            <th class="grid_head">Item</th>
                            <th class="grid_head">Item Name</th>
                            {{-- <th class="grid_head">Qty</th>
                            <th class="grid_head">Price</th> --}}
                            <th class="grid_head">Request Date</th>
                            <th class="grid_head">Status</th>
                            <th class="grid_head">Status Date</th>
                            <th class="grid_head">Type</th>
                            <th class="grid_head">Reason</th>
                            {{-- <th class="grid_head">Remarks</th> --}}
                            <th class="grid_head">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($rmaRequests->count() != 0)
                        <?php $counter = 0;?>
                        @foreach($rmaRequests as $rmaRequest)
                            <?php 
                                $counter++;
                                $product = $productRepository->findByProductId($rmaRequest->product_id);
                                $image = $productImageHelper->getProductBaseImage($product); 
                                $customer = $customerRepository->getCustomerInfromationById($rmaRequest->customer_id);
                                $policy = $RmaRepository->findById($rmaRequest->policy_id);
                            ?>
                            <tr>
                                <td data-value="Id">{{$rmaRequest->id}}</td>
                                <td data-value="Customer Name">{{$customer->first_name}}</td>
                                <td data-value="Mobile Number">{{$customer->mobile_number}}</td>
                                <td data-value="Image">
                                    {{-- <a href="{{ url()->to('/').'/memykids/products/'.$product->url_key }}" title="{{ $product->name }}"> --}}
                                        <img src="{{ $image['small_image_url'] }}" class="img-fluid" alt="memykid" style="height: 60px;width: 60px;">
                                    {{-- </a> --}}
                                </td>
                                <td data-value="Product Name">
                                    {{$product->name}}
                                </td>
                                {{-- <td data-value="Quantity">{{$rmaRequest->quantity}}</td>
                                <td data-value="Price">{{core()->currency($product->price)}}</td> --}}
                                <td data-value="Request Date">{{core()->formatDate($rmaRequest->created_at, 'd M Y')}}</td>

                                <td data-value="Status">
                                    @if($rmaRequest->status == 'refunded' || $rmaRequest->status == 'denied')
                                        @if($rmaRequest->status == 'refunded')
                                            <span class="badge badge-md badge-success">Refunded</span>
                                        @else
                                            <span class="badge badge-md badge-danger">Denied</span>
                                        @endif
                                    @else
                                        <form method="POST" action="{{route('rma.request.statusUpdate',$rmaRequest->id)}}" ref="statusUpdateForm{{$rmaRequest->id}}" id="statusUpdateForm{{$rmaRequest->id}}">
                                            @csrf()
                                            <input name="_method" type="hidden" value="PUT">
                                            <input name="order_id" type="hidden" value={{$rmaRequest->order_item_id}}>
                                            <input name="qty" type="hidden" value={{$rmaRequest->quantity}}>
                                            <select name="status" onChange="document.getElementById('statusUpdateForm{{$rmaRequest->id}}').submit();">
                                                <option value="pending" {{$rmaRequest->status == 'pending' ? 'selected' : ''}}>Pending</option>
                                                <option value="processing" {{$rmaRequest->status == 'processing' ? 'selected' : ''}}>Processing</option>
                                                <option value="refunded" {{$rmaRequest->status == 'refunded' ? 'selected' : ''}}>Refunded</option>
                                                <option value="accepted" {{$rmaRequest->status == 'accepted' ? 'selected' : ''}}>Accepted</option>
                                                <option value="denied" {{$rmaRequest->status == 'denied' ? 'selected' : ''}}>Denied</option>
                                            </select> 
                                        </form>
                                    @endif
                                </td>

                                <td data-value="Status Date">{{core()->formatDate($rmaRequest->updated_at, 'd M Y')}}</td>
                                <td data-value="Type">{{$policy->type}}</td>
                                <td data-value="Reason">{{$rmaRequest->reason}}</td>
                                {{-- <td data-value="Remarks">{{$rmaRequest->remarks}}</td> --}}

                                <td class="actions" data-value="Actions">
                                    <a href="{{route('rma.requests.view',$rmaRequest->id)}}"><span class="icon eye-icon"></span></a>
                                    {{-- <a href="{{route('',$rmaRequest->id)}}"><span class="icon trash-icon"></span></a> --}}
                                </td>
                            </tr>
                        @endforeach

                    @else
                        <tr>
                            <td colspan="10" style="text-align: center;">No Records Found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            {{$rmaRequests->links()}}
        </div>
   </div>

</div>
@stop