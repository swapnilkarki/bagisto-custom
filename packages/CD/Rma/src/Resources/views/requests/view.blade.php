@extends('admin::layouts.content')

@section('page_title')
   Rma Request | MeMyKids
@stop


@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@inject('customerRepository', 'Webkul\Customer\Repositories\CustomerRepository')
@inject('RmaRepository','CD\Rma\Repositories\RmaRepository')

<?php 
$product = $productRepository->findByProductId($rmaRequests->product_id);
$image = $productImageHelper->getProductBaseImage($product); 
$customer = $customerRepository->getCustomerInfromationById($rmaRequests->customer_id);
$policy = $RmaRepository->findById($rmaRequests->policy_id);
?>

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('rma.request.update', $rmaRequests->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Edit {{$rmaRequests->id}}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Save
                    </button>
                </div>
            </div>

            <div class="page-content" style="padding-left: 25px">
                <div class="form-container">
                    @csrf()
                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" >
                        <label for="name" >Customer Name</label>
                        <input type="text" v-validate="'required'" class="control" id="name" name="name" data-vv-as="&quot;'name'&quot;" value="{{$customer->first_name}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="phone" >Customer Phone</label>
                        <input type="text" v-validate="'required'" class="control" id="phone" name="phone" data-vv-as="&quot;'phone'&quot;" value="{{ $customer->mobile_number }}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="phone">Item</label>
                        <img src="{{ $image['small_image_url'] }}" class="img-fluid" alt="memykid" style="height: 120px;width: 120px;">
                    </div>

                    <div class="control-group" >
                        <label for="item_name" >Item Name</label>
                        <input type="text" v-validate="'required'" class="control" id="item_name" name="item_name" data-vv-as="&quot;'item_name'&quot;" value=" {{$product->name}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="qty" >Quantity</label>
                        <input type="text" v-validate="'required'" class="control" id="qty" name="qty" data-vv-as="&quot;'qty'&quot;" value="{{$rmaRequests->quantity}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="price" >Price</label>
                        <input type="text" v-validate="'required'" class="control" id="price" name="price" data-vv-as="&quot;'price'&quot;" value="{{core()->currency($product->price)}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="request_date" >Request Date</label>
                        <input type="text" v-validate="'required'" class="control" id="request_date" name="request_date" data-vv-as="&quot;'request_date'&quot;" value="{{core()->formatDate($rmaRequests->created_at, 'd M Y')}}" readonly/>
                    </div>

                    <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                        <label for="status" class="required">Status</label>
                        <?php $selectedOption = old('status') ?: $rmaRequests->status ?>
                        <select v-validate="'required'" class="control" id="status" name="status" data-vv-as="&quot;'status'&quot;">
                            <option value="pending" {{$selectedOption == 'pending' ? 'selected' : ''}}>Pending</option>
                            <option value="processing" {{$selectedOption == 'processing' ? 'selected' : ''}}>Processing</option>
                            <option value="refunded" {{$selectedOption == 'refunded' ? 'selected' : ''}}>Refunded</option>
                            <option value="accepted" {{$selectedOption == 'accepted' ? 'selected' : ''}}>Accepted</option>
                            <option value="denied" {{$selectedOption == 'denied' ? 'selected' : ''}}>Denied</option>
                        </select>
                        <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                    </div>

                    <div class="control-group" >
                        <label for="status_date" >Status Date</label>
                        <input type="text" v-validate="'required'" class="control" id="status_date" name="status_date" data-vv-as="&quot;'status_date'&quot;" value="{{core()->formatDate($rmaRequests->updated_at, 'd M Y')}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="type" >Type</label>
                        <input type="text" v-validate="'required'" class="control" id="type" name="type" data-vv-as="&quot;'type'&quot;" value="{{$policy->type}}" readonly/>
                    </div>

                    <div class="control-group" >
                        <label for="reason" >Reason</label>
                        <input type="text" v-validate="'required'" class="control" id="reason" name="reason" data-vv-as="&quot;'reason'&quot;" value="{{$rmaRequests->reason}}" readonly/>
                    </div>

                    <div class="control-group" :class="[errors.has('remarks') ? 'has-error' : '']">
                        <label for="remarks" class="required">Remarks</label>
                        <textarea type="text" v-validate="'required'" class="control" id="remarks" name="remarks" data-vv-as="&quot;'remarks'&quot;">{{$rmaRequests->remarks}}</textarea>
                        <span class="control-error" v-if="errors.has('remarks')">@{{ errors.first('remarks') }}</span>
                    </div>

                </div>
            </div>
        </form>
    </div>
@stop