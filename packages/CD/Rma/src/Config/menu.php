<?php

return [
    [
        'key' => 'rma',          // uniquely defined key for menu-icon
        'name' => 'RMA_RULES',        //  name of menu-icon
        'route' => 'rma.returns.index',  // the route for your menu-icon
        'sort' => 4,                    // Sort number on which your menu-icon should display
        'icon-class' => 'return-icon',   //class of menu-icon
    ],[
        'key' => 'rma.returns',
        'name' => 'Return Rules',
        'route' => 'rma.returns.index',
        'sort' => 1,
        'icon-class' => '',
    ],[
        'key' => 'rma.warranty',
        'name' => 'Warranty Rules',
        'route' => 'rma.warranty.index',
        'sort' => 1,
        'icon-class' => '',
    ],[
        'key' => 'rma.exchange',
        'name' => 'Exchange Rules',
        'route' => 'rma.exchange.index',
        'sort' => 1,
        'icon-class' => '',
    ],
    [
        'key' => 'sales.rma',
        'name' => 'Rma Requests',
        'route' => 'rma.requests.index',
        'sort' => 4,
        'icon-class' => '',
    ]
];
