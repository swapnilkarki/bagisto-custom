<?php

namespace CD\Rma\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \CD\Rma\Models\Rma::class,
        \CD\Rma\Models\ProductPolicies::class,
        \CD\Rma\Models\RmaRequests::class,
    ];
}