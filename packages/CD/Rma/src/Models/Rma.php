<?php

namespace CD\Rma\Models;

use Illuminate\Database\Eloquent\Model;
use CD\Rma\Contracts\Rma as RmaContract;

class Rma extends Model implements RmaContract
{
    protected $fillable = ['title', 'slug', 'type','validity','status','remarks'];
}
