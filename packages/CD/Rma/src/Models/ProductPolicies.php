<?php

namespace CD\Rma\Models;

use Illuminate\Database\Eloquent\Model;
use CD\Rma\Contracts\ProductPolicies as ProductPoliciesContract;


class ProductPolicies extends Model implements ProductPoliciesContract
{
    protected $fillable = ['product_id', 'return_policy', 'warranty_policy','exchange_policy'];
}
