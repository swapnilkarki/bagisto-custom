<?php

namespace CD\Rma\Models;

use Illuminate\Database\Eloquent\Model;
use CD\Rma\Contracts\RmaRequests as RmaRequestsContract;

class RmaRequests extends Model implements RmaRequestsContract
{
    protected $fillable = ['customer_id', 'order_id', 'product_id','order_item_id','quantity','policy_id','reason','remarks','status'];
}
