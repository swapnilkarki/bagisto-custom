<?php
Route::group(['middleware' => ['web']], function () {
 Route::prefix('/admin/rma')->group(function () {
    Route::prefix('/returns-rules')->group(function () {
        //index
        Route::get('/', 'CD\Rma\Http\Controllers\RmaController@index')->defaults('_config', [
            'view' => 'rma::rma.returns.index',
            'type' => 'returns'
        ])->name('rma.returns.index');

        //create new rule page
        Route::get('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'view' => 'rma::rma.returns.create'
        ])->name('rma.returns.create');

        Route::put('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'type' => 'returns',
            'redirect' => 'rma.returns.index'
        ])->name('rma.returns.create');

        //edit new rule page
        Route::get('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'view' => 'rma::rma.returns.edit'
        ])->name('rma.returns.edit');

        Route::put('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'redirect' => 'rma.returns.index',
            'type' => 'returns'
        ])->name('rma.returns.edit');

        //remove
        Route::get('/remove/{id}', 'CD\Rma\Http\Controllers\RmaController@remove')->defaults('_config', [
            'redirect' => 'rma.returns.index'
        ])->name('rma.returns.remove');

    });

    Route::prefix('/warranty-rules')->group(function () {
        //index
        Route::get('/', 'CD\Rma\Http\Controllers\RmaController@index')->defaults('_config', [
            'view' => 'rma::rma.warranty.index',
            'type' => 'warranty'
        ])->name('rma.warranty.index');

         //create new rule page
         Route::get('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'view' => 'rma::rma.warranty.create'
        ])->name('rma.warranty.create');

        Route::put('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'type' => 'warranty',
            'redirect' => 'rma.warranty.index'
        ])->name('rma.warranty.create');

         //edit new rule page
         Route::get('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'view' => 'rma::rma.warranty.edit'
        ])->name('rma.warranty.edit');

        Route::put('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'redirect' => 'rma.warranty.index',
            'type' => 'warranty'
        ])->name('rma.warranty.edit');

         //remove
         Route::get('/remove/{id}', 'CD\Rma\Http\Controllers\RmaController@remove')->defaults('_config', [
            'redirect' => 'rma.warranty.index',
        ])->name('rma.warranty.remove');

    });

    Route::prefix('/exchange-rules')->group(function () {
        Route::get('/', 'CD\Rma\Http\Controllers\RmaController@index')->defaults('_config', [
            'view' => 'rma::rma.exchange.index',
            'type' => 'exchange'
        ])->name('rma.exchange.index');

         //create new rule page
         Route::get('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'view' => 'rma::rma.exchange.create'
        ])->name('rma.exchange.create');

        Route::put('/create', 'CD\Rma\Http\Controllers\RmaController@create')->defaults('_config', [
            'type' => 'exchange',
            'redirect' => 'rma.exchange.index'
        ])->name('rma.exchange.create');

         //edit new rule page
         Route::get('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'view' => 'rma::rma.exchange.edit'
        ])->name('rma.exchange.edit');

        Route::put('/edit/{id}', 'CD\Rma\Http\Controllers\RmaController@edit')->defaults('_config', [
            'redirect' => 'rma.exchange.index',
            'type' => 'exchange'
        ])->name('rma.exchange.edit');

         //remove
         Route::get('/remove/{id}', 'CD\Rma\Http\Controllers\RmaController@remove')->defaults('_config', [
            'redirect' => 'rma.exchange.index'
        ])->name('rma.exchange.remove');

    });

    Route::prefix('/requests')->group(function(){
        Route::get('/', 'CD\Rma\Http\Controllers\CancellationReturnsController@allReturnRequests')->defaults('_config', [
                'view' => 'rma::requests.index',
            ])->name('rma.requests.index');

        Route::get('/search', 'CD\Rma\Http\Controllers\CancellationReturnsController@allReturnRequestsSearch')->defaults('_config', [
            'view' => 'rma::requests.index',
        ])->name('rma.requests.search');

        Route::get('/view/{id}', 'CD\Rma\Http\Controllers\CancellationReturnsController@allReturnRequestsView')->defaults('_config', [
            'view' => 'rma::requests.view',
        ])->name('rma.requests.view');

        Route::put('/view/{id}', 'CD\Rma\Http\Controllers\CancellationReturnsController@update')->name('rma.request.update');

        Route::put('/update/{id}', 'CD\Rma\Http\Controllers\CancellationReturnsController@updateStatus')->name('rma.request.statusUpdate');

    });
     //update status
     Route::put('/update/{id}', 'CD\Rma\Http\Controllers\RmaController@statusUpdate')->name('rma.statusUpdate');

});

Route::get('/api/product-policies/{id}', 'CD\Rma\Http\Controllers\RmaController@getProductPoliciesApi');
Route::post('/api/rma/submit', 'CD\Rma\Http\Controllers\RmaController@submitRmaRequest');
});