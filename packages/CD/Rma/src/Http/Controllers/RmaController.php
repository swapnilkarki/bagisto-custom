<?php

namespace CD\Rma\Http\Controllers;

use CD\Rma\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CD\Rma\Repositories\RmaRepository;
use CD\Rma\Repositories\ProductPoliciesRepository;
use CD\Rma\Models\RmaRequests;
use Webkul\Sales\Repositories\OrderItemRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Event;

class RmaController extends Controller
{
    protected $_config;

    /**
     * RmaRepository object
     *
     * @var array
    */
    protected $rma;

     /**
     * ProductPoliciesRepository object
     *
     * @var array
    */
    protected $productPolicies;

     /**
     * RmaRequests object
     *
     * @var array
    */
    protected $rmaRequests;

     /**
     * OrderItemRepository object
     *
     * @var array
    */
    protected $orderItem;

    public function __construct(RmaRepository $rma, ProductPoliciesRepository $productPolicies, RmaRequests $rmaRequests,OrderItemRepository $orderItem)
    {
        $this->middleware('admin', ['except' => ['getProductPoliciesApi','submitRmaRequest']]);
        $this->_config = request('_config');
        $this->rma = $rma;
        $this->productPolicies = $productPolicies;
        $this->rmaRequests = $rmaRequests;
        $this->orderItem = $orderItem;
    }

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/

public function index()
{
    if($this->_config['type'] == 'returns'){
        $rma = $this->rma->getReturnsRules();
    }else if($this->_config['type'] == 'warranty'){
        $rma = $this->rma->getWarrantyRules();
    }else if($this->_config['type'] == 'exchange'){
        $rma = $this->rma->getExchangeRules();
    }
    return view($this->_config['view'])->with('rma',$rma);
}

public function create(Request $request)
{
    if ($request->isMethod('get')) {
        return view($this->_config['view']);
    }
    else if ($request->isMethod('put')) {
        $data = $request->all();
        $data['type'] = $this->_config['type'];
        $data['slug'] = $this->rma->slugify($data['title'].'-'.$data['type'].'-'.$data['validity']);

        $customValidator = $this->rma->customValidation($data);

        if(!$customValidator){
            session()->flash('error', 'This Rule Already Exists');
            return redirect()->back()->withInput();
        }

        $validator = Validator::make($data, [
            'slug' => 'required|unique:rmas,slug',
            'type' => 'required',
            'title' => 'required',
            'validity' => 'required|numeric',
            'status' => 'required',
            'remarks' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Title already exists');

            return redirect()->back()->withInput();
        }

       $rma = $this->rma->create($data);

        if($rma){
            session()->flash('success', 'New Rule Added Sucessfully');
            return redirect()->route($this->_config['redirect']);
        }else{
            session()->flash('error', 'Something Went Wrong');
            return redirect()->back();
        }
    }
}

public function edit($id, Request $request)
{
    if ($request->isMethod('get')) {
        $rma = $this->rma->findOrFail($id);
        return view($this->_config['view'])->with('rma',$rma);
    }
    else if ($request->isMethod('put')) {
       $data = $request->all();
        $data['type'] = $this->_config['type'];
        $data['slug'] = $this->rma->slugify($data['title'].'-'.$data['type'].'-'.$data['validity']);

        $customValidator = $this->rma->customValidationForEdit($data,$id);

        if(!$customValidator){
            session()->flash('error', 'This Rule Already Exists');
            return redirect()->back()->withInput();
        }

        // dd($id);

        $validator = Validator::make($data, [
            'slug' => 'required|unique:rmas,slug,' . $id,
            'type' => 'required',
            'title' => 'required',
            'validity' => 'required|numeric',
            'status' => 'required',
            'remarks' => 'required'
        ]);

        
        if ($validator->fails()) {
            session()->flash('error', 'Title already exists');

            return redirect()->back()->withInput();
        }

        $rma = $this->rma->update($data,$id);

        if($rma){
            session()->flash('success', 'Rule Updated Sucessfully');
            return redirect()->route($this->_config['redirect']);
        }else{
            session()->flash('error', 'Something Went Wrong');
            return redirect()->back();
        }
    }

}

public function remove($id)
{
    $rma = $this->rma->findOrFail($id);
    try {
        $rma->delete();
        session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Rule']));

        return redirect()->route($this->_config['redirect']);
    } catch (\Exception $e) {
        session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Rule']));
    }

    return redirect()->back();
}


public function statusUpdate($id,Request $request){

    $rma = $this->rma->findOrFail($id);

    $rma->status = request('status');

    $rma->save();

    session()->flash('success','Status Updated.');

    return redirect()->back();

}

public function getProductPoliciesApi($id){
    $policies =  $this->productPolicies->getPoliciesByProductId($id);
    $hasValidPolices = $this->rma->hasValidPolicity($policies);
    $return_policy = $this->rma->findById($policies->return_policy);
    $warranty_policy = $this->rma->findById($policies->warranty_policy);
    $exchange_policy = $this->rma->findById($policies->exchange_policy);

    return response()->json([
        'return_policy' => $return_policy,
        'warranty_policy' => $warranty_policy,
        'exchange_policy' => $exchange_policy,
        'hasValidPolices' => $hasValidPolices 
    ]);

}

public function submitRmaRequest(){
    $data = request()->all();

    request()->merge([
        'status' => 'pending'
    ]);
 
    $rma = $this->rmaRequests->create(request()->all());

    $this->orderItem->returnRequestQuantity($data['order_item_id'],$data['quantity']);

    session()->flash('success','Your RMA request has been placed sucessfully.');

    Event::fire('rma.order.placed.after', $rma);

    return response()->json([
        'message' => 'Your RMA request has been placed sucessfully.',
    ]);



}

}