<?php

namespace CD\Rma\Http\Controllers;


use CD\Rma\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductPoliciesController extends Controller
{
    protected $_config;

    /**
     * RmaRepository object
     *
     * @var array
    */
    protected $productPolicies;

    public function __construct()
    {
        $this->middleware('admin');
        $this->_config = request('_config');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {

    }

}