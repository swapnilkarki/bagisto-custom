<?php

namespace CD\Rma\Http\Controllers;


use CD\Rma\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use CD\Rma\Repositories\RmaRequestsRepository;
use Webkul\Sales\Repositories\OrderItemRepository;
use Illuminate\Support\Facades\Event;

class CancellationReturnsController extends Controller
{
    protected $_config;

    protected $rmaRequests;

    protected $orderItem;

    public function __construct(RmaRequestsRepository $rmaRequests,OrderItemRepository $orderItem)
    {
        $this->middleware('admin', ['except' => ['index']]);
        $this->_config = request('_config');

        $this->rmaRequests = $rmaRequests;
        $this->orderItem = $orderItem;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {
        // $rmaRequests = $this->rmaRequests->findWhere([
        //     'customer_id' => auth()->guard('customer')->user()->id
        // ]);

        $rmaRequests = $this->rmaRequests->findByCustomerId(auth()->guard('customer')->user()->id);

        // $rmaRequests = $rmaRequests->sortByDesc('updated_at');
        return view($this->_config['view'])->with('rmaRequests',$rmaRequests);
    }

    public function allReturnRequests(){
        $rmaRequests = $this->rmaRequests->getAll();
        return view($this->_config['view'])->with('rmaRequests',$rmaRequests);
    }

    public function allReturnRequestsSearch(Request $request){
        $searchTerm = $request->input('search-field');

        $rmaRequests = $this->rmaRequests->searchResults($searchTerm);
        // dd($searchTerm);
        return view($this->_config['view'])->with('rmaRequests',$rmaRequests);
    }

    public function allReturnRequestsView($id){
        $rmaRequests = $this->rmaRequests->findOrFail($id);
        return view($this->_config['view'])->with('rmaRequests',$rmaRequests);
    }

    public function update($id){
        $rmaRequests = $this->rmaRequests->findOrFail($id);

        $rmaRequests->status = request('status');
        $rmaRequests->remarks = request('remarks');
    
        $rmaRequests->save();

        session()->flash('success','Information Updated.');
        return redirect()->route('rma.requests.index');

    }

    public function updateStatus($id)
    {
        $request = request()->all();

        $rmaRequests = $this->rmaRequests->findOrFail($id);
        $rmaRequests->status = $request['status'];
        $rmaRequests->save();

        if($request['status'] == 'refunded')
        {
            $this->orderItem->refundedRequestQuantity($request['order_id'],$request['qty']);
            Event::fire('rma.order.refunded.after', $rmaRequests);
        }elseif($request['status'] == 'denied')
        {
            $this->orderItem->deniedRequestQuantity($request['order_id'],$request['qty']);
            Event::fire('rma.order.denied.after', $rmaRequests);
        }elseif(request('status') == 'processing')
            Event::fire('rma.order.processing.after', $rmaRequests);
        elseif(request('status') == 'accepted')
            Event::fire('rma.order.accepted.after', $rmaRequests);

        session()->flash('success','Status Updated.');

        return redirect()->back();
    }

}
