<?php

namespace CD\Khalti\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;


/**
* Khalti service provider
*
* @author    Swapnil Kakri <swapnil@coredreams.cm>
* @copyright 2019 Core Dreams Inovation Pvt. Ltd. <coredreams.com>
*/
class KhaltiServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap services.
    *
    * @return void
    */
    public function boot()
  {
    $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');
    $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'khalti');
    // $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'helloworld'); 

    // Event::listen('bagisto.admin.layout.head', function($viewRenderEventManager) {
    //     $viewRenderEventManager->addTemplate('helloworld::helloworld.layouts.style');
    // });

    $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');
  }

    /**
    * Register services.
    *
    * @return void
    */
    public function register()
    {
      $this->registerConfig();
    }
    
     /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/paymentmethods.php', 'paymentmethods'
        );

        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/system.php', 'core'
        );
    }
}


