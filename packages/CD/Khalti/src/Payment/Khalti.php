<?php

namespace CD\Khalti\Payment;


use Webkul\Payment\Payment\Payment;
/**
 * Money Transfer payment method class
 *
 * @author    CoreDreams <coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt. Ltd (www.coredreams.com)
 */
class Khalti extends Payment
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'khalti';

    public function getRedirectUrl()
    {
        return route('khalti.redirect');
    }
}