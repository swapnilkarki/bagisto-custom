<body>
        You will be greeted with  the Khalti pop up in a few seconds.
        <div id="app">
        <khalti ref="test"></khalti>
        </div>

        <script>
                window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
                    'baseUrl' => url('/'),
                    'routes' => collect(\Route::getRoutes())->mapWithKeys(function ($route) { return [$route->getName() => $route->uri()]; })
                ]) !!};
            </script>
        <script type="text/javascript" src="{{ asset('vendor/webkul/khalti/assets/js/khalti.js') }}"></script>    
      
</body>

