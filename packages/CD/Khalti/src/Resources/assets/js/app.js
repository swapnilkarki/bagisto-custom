import Vue from 'vue'

window.Vue = require('vue');

window.axios = require("axios");


Vue.prototype.$http = axios;

window.route = require('./route');

Vue.component('khalti', require('./components/Khalti.vue'));

window.onload = (function() {

    const app = new Vue({
        el: "#app",

        data: {
            modalIds: {}
        },

        mounted: function() {

        },

        methods: {

        }
    });
});