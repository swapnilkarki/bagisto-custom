<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKhaltisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khaltis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idx');
            $table->string('token');
            $table->string('mobile');
            $table->string('product_identity');
            $table->string('product_name');
            $table->string('product_url');
            $table->string('widget_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khaltis');
    }
}
