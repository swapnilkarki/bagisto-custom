<?php

namespace CD\Khalti\Http\Controllers;

use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;
use CD\Khalti\Models\Khalti; 
use Illuminate\Http\Request;

/**
 * Khalti controller
 *
 * @author    Swapnil Karki<swapnil@coredreams.com>
 * @copyright 2019 Core Dreams Innovation Pvt Ltd (http://www.coredreams.com)
 */
class KhaltiController extends Controller
{
    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $orderRepository;

    protected $khalti;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Attribute\Repositories\OrderRepository  $orderRepository
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        Khalti $khalti
    )
    {
        $this->orderRepository = $orderRepository;
        $this->khalti = $khalti;
    }

    /**
     * Redirects to the paypal.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return view('khalti::khalti.redirect');
    }

    /**
     * Cancel payment from paypal.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        session()->flash('error', 'Khalti payment has been canceled.');

        return redirect()->route('memy.checkout.cart.index');
    }

    /**
     * Success payment
     *
     * @return \Illuminate\Http\Response
     */
    public function success($idx)
    {
        $order = $this->orderRepository->create(Cart::prepareDataForOrder());

        Khalti::where('idx', $idx)
          ->update(['order_id' => $order->id]);

        Cart::deActivateCart();

        session()->flash('order', $order);

        return redirect()->route('memy.checkout.success');
    }

    /**
     * verify payment
     *
     * @return \Illuminate\Http\Response
     */
    public function verify()
    {
        $data = request()->all();

        $args = http_build_query($data);
        
        $url = "https://khalti.com/api/v2/payment/verify/";
        
        # Make the call using API.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        $headers = ['Authorization: Key test_secret_key_ab3f0987e7204f53ab55553b2811ec84'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        // Response
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($status_code == 200){
            return response()->json([
                'message' => 'Your trasaction has been verified and completed.',
                'data' => $response
            ]);
        }else{
            session()->flash('error', 'Could not verify khalti transaction currently.Please try again later.');

            return redirect()->route('memy.checkout.onepage.index');
        }
        
    }


    public function store(Request $request)
    {
        $data = $request->all();
        Khalti::create($data);
    }

    public function getTransaction(Request $request)
    {
        $idx = $request->id;
        return Khalti::where('idx', $idx)->firstOrFail();
    }

}