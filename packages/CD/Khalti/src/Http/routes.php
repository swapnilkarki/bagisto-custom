<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('/khalti/redirect', 'CD\Khalti\Http\Controllers\KhaltiController@redirect')->name('khalti.redirect');
    Route::get('/khalti/cancel', 'CD\Khalti\Http\Controllers\KhaltiController@cancel')->name('khalti.cancel');
    Route::put('/khalti/verify', 'CD\Khalti\Http\Controllers\KhaltiController@verify')->name('khalti.verify');
    Route::get('/khalti/success/{idx}', 'CD\Khalti\Http\Controllers\KhaltiController@success')->name('khalti.success');
    Route::get('/khalti/store', 'CD\Khalti\Http\Controllers\KhaltiController@store')->name('khalti.store');
    Route::get('/khalti/get', 'CD\Khalti\Http\Controllers\KhaltiController@getTransaction')->name('khalti.get');
});

// Route::view('/khalti/redirect', 'khalti::khalti.redirect')->name('khalti.redirect');

