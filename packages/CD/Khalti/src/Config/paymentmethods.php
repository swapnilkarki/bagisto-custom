<?php
return [
    'khalti' => [
        'code' => 'khalti',
        'title' => 'Khalti',
        'description' => 'Khalti Standard',
        'class' => 'CD\Khalti\Payment\Khalti',
        'active' => true,
        'sort' => 4
    ]
];