<?php

namespace CD\Khalti\Models;

use Illuminate\Database\Eloquent\Model;

class Khalti extends Model
{
    protected $fillable = ['idx','token','mobile','product_identity','product_name','product_url','widget_id','amount'];
}